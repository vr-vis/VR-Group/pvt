# - Find Cube
# Find the Cube includes and library
#
#  CUBE_INCLUDES    - where to find cube/cube.h
#  CUBE_LIBRARIES   - List of libraries when using Cube.
#  CUBE_FOUND       - True if Cube was found.
#
#  Use environment variable CUBE_ROOT to point to the directory that contains
#  - include/cube/Cube.h
#  - lib/


# -- quiet shortcut ------------------------------------------------------------

if (CUBE_INCLUDES AND CUBE_LIBS)
  # Already in cache, be silent
  set (CUBE_FIND_QUIETLY TRUE)
endif (CUBE_INCLUDES AND CUBE_LIBS)

# -- cubelib-config ------------------------------------------------------------

# find cubelib-config
if (DEFINED ENV{CUBE_ROOT})
  find_program (CUBELIB_CONFIG
    cubelib-config
    PATHS ENV CUBE_ROOT
    NO_DEFAULT_PATH
    PATH_SUFFIXES bin
  )
endif (DEFINED ENV{CUBE_ROOT})

if (NOT CUBELIB_CONFIG)
  find_program (CUBELIB_CONFIG
    cubelib-config
    PATH_SUFFIXES bin
)
endif (NOT CUBELIB_CONFIG)


# use cubelib-config
if (CUBELIB_CONFIG)
  execute_process (
    COMMAND ${CUBELIB_CONFIG} --include
    OUTPUT_STRIP_TRAILING_WHITESPACE
    OUTPUT_VARIABLE _CUBELIB_CONFIG__CUBE_INCLUDE_PATH
  )
  execute_process (
    COMMAND ${CUBELIB_CONFIG} --ldflags
    OUTPUT_STRIP_TRAILING_WHITESPACE
    OUTPUT_VARIABLE _CUBELIB_CONFIG__CUBE_DIR
  )
  string(REGEX REPLACE "[\r\n]" " " _CUBELIB_CONFIG__CUBE_DIR "${_CUBELIB_CONFIG__CUBE_DIR}")
  separate_arguments(_CUBELIB_CONFIG__CUBE_DIR)
  string(REGEX MATCHALL "-L[^;]*" _CUBELIB_CONFIG__CUBE_DIR "${_CUBELIB_CONFIG__CUBE_DIR}")
  string(REGEX REPLACE "-L([^;]*)" "\\1" _CUBELIB_CONFIG__CUBE_DIR "${_CUBELIB_CONFIG__CUBE_DIR}")

endif (CUBELIB_CONFIG)


# -- cube-config ---------------------------------------------------------------

if (NOT CUBELIB_CONFIG)

# find cube-config
if (DEFINED ENV{CUBE_ROOT})
  find_program (CUBELIB_CONFIG
    cube-config
    PATHS ENV CUBE_ROOT
    NO_DEFAULT_PATH
    PATH_SUFFIXES bin
  )
endif (DEFINED ENV{CUBE_ROOT})

if (NOT CUBELIB_CONFIG)
  find_program (CUBELIB_CONFIG
    cube-config
    PATH_SUFFIXES bin
)
endif (NOT CUBELIB_CONFIG)


# use cube-config
if (CUBELIB_CONFIG)
  execute_process (
    COMMAND ${CUBELIB_CONFIG} --cube-include-path
    OUTPUT_STRIP_TRAILING_WHITESPACE
    OUTPUT_VARIABLE _CUBELIB_CONFIG__CUBE_INCLUDE_PATH
  )
  execute_process (
    COMMAND ${CUBELIB_CONFIG} --cube-dir
    OUTPUT_STRIP_TRAILING_WHITESPACE
    OUTPUT_VARIABLE _CUBELIB_CONFIG__CUBE_DIR
  )
endif (CUBELIB_CONFIG)

endif (NOT CUBELIB_CONFIG)


# -- cube ----------------------------------------------------------------------

# find cube includes via CUBE_ROOT, cube-config or standard search paths
find_path (CUBE_INCLUDES
  cube/Cube.h cubelib/Cube.h
  HINTS ${_CUBELIB_CONFIG__CUBE_INCLUDE_PATH}/..
  PATH_SUFFIXES include
)

# find cube includes via CUBE_INCLUDES, CUBE_ROOT or standard search paths
find_library (CUBE_LIBRARIES
  NAMES cube4 libcube4
  HINTS ${_CUBELIB_CONFIG__CUBE_DIR}/..
  PATH_SUFFIXES lib)


# -- finalize ------------------------------------------------------------------

# handle the QUIETLY and REQUIRED arguments and set CUBE_FOUND to TRUE if
# all listed variables are TRUE
include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (Cube DEFAULT_MSG CUBE_LIBRARIES CUBE_INCLUDES)

mark_as_advanced (CUBE_LIBRARIES CUBE_LIBRARIES CUBE_INCLUDES)
