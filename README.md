# pvt performance visualization toolkit

pvt is Copyright (c) 2014-2016 RWTH Aachen University, Germany,
Virtual Reality & Immersive Visualization Group.

## License

This framework is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In the future, we may decide to add a commercial license 
at our own discretion without further notice.

If you are using pvt in a project, work or publication,
please mention "pvt performance visualization toolkit"
and cite \[[1](#references)\] *and* 

* \[[2](#references)\] if you are using the correlation analysis or 
* \[[3](#references)\] if you are using the geometry mapping. 


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/].

## Finding Qt 5 (for pv_app)
* Define an evironment variable `QT5_DIR`
  * Windows: it must be set to a specific build directory e.g., `/Qt/5.5/msvc2013_64/`
  * Mac (Homebrew): `/usr/local/opt/qt5` 

## References

[1] Tom Vierjahn, Marc-André Hermanns, Bernd Mohr, Matthias S. Müller, Torsten W. Kuhlen, Bernd Hentschel, "Using Directed Variance to Identify Meaningful Views in Call-Path Performance Profiles". Accepted for publication: 3rd Workshop on Visual Performance Analysis (VPA), 2016.

[2] Tom Vierjahn, Marc-André Hermanns, Bernd Mohr, Matthias S. Müller, Torsten Wolfgang Kuhlen, Bernd Hentschel, "Correlating Sub-Phenomena in Performance Data in the Frequency Domain". Accepted for poster presentation: LDAV 2016 – The 6th IEEE Symposium on Large Data Analysis and Visualization, 2016.

[3] Tom Vierjahn, Torsten W. Kuhlen, Matthias S. Müller, Bernd Hentschel, "Visualizing Performance Data With Respect to the Simulated Geometry". Presented at: 1st JARA-HPC Symposium, 2016.
