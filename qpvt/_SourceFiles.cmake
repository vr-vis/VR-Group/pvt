#-------------------------------------------------------------------------------
# pvt performance visualization toolkit
#
# Copyright (c) 2014-2016 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualisation Group.
#-------------------------------------------------------------------------------
#                                 License
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# In the future, we may decide to add a commercial license
# at our own discretion without further notice.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

set(SourceFiles
  include/qpvt/api.hpp

  include/qpvt/delegates/directed_variance_glyph_delegate.hpp
  include/qpvt/delegates/severity_glyph_delegate.hpp

  include/qpvt/models/cnode_impact_list_model.hpp
  include/qpvt/models/cnode_tree_model.hpp
  include/qpvt/models/correlated_views_model.hpp
  include/qpvt/models/metric_tree_model.hpp
  include/qpvt/models/perf_data_tree_model.hpp

  include/qpvt/utility/camera_tools.hpp
  
  include/qpvt/widgets/cartesian_topology_viewer_widget.hpp
  include/qpvt/widgets/cnode_impact_selector_widget.hpp
  include/qpvt/widgets/data_range_h_slider.hpp
  include/qpvt/widgets/data_range_v_slider.hpp
  include/qpvt/widgets/geometry_viewer_widget.hpp
  include/qpvt/widgets/parallel_coordinates_plot_widget.hpp
  include/qpvt/widgets/perf_data_viewer_widget.hpp
  include/qpvt/widgets/range_h_slider.hpp
  include/qpvt/widgets/range_slider.hpp
  include/qpvt/widgets/range_v_slider.hpp
  include/qpvt/widgets/slice_slider.hpp
  include/qpvt/widgets/squarified_cushion_treemap_widget.hpp
  include/qpvt/widgets/viewer_widget.hpp

  src/delegates/directed_variance_glyph_delegate.cpp
  src/delegates/severity_glyph_delegate.cpp
  
  src/models/cnode_impact_list_model.cpp
  src/models/cnode_tree_model.cpp
  src/models/correlated_views_model.cpp
  src/models/metric_tree_model.cpp
  
  src/utility/camera_tools.cpp
  
  src/widgets/cartesian_topology_viewer_widget.cpp
  src/widgets/cnode_impact_selector_widget.cpp
  src/widgets/data_range_h_slider.cpp
  src/widgets/data_range_v_slider.cpp
  src/widgets/geometry_viewer_widget.cpp
  src/widgets/parallel_coordinates_plot_widget.cpp
  src/widgets/perf_data_viewer_widget.cpp
  src/widgets/range_h_slider.cpp
  src/widgets/range_slider.cpp
  src/widgets/range_v_slider.cpp
  src/widgets/slice_slider.cpp
  src/widgets/squarified_cushion_treemap_widget.cpp
  src/widgets/viewer_widget.cpp
)
