//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_METRIC_TREE_MODEL_HPP_
#define QPVT_METRIC_TREE_MODEL_HPP_

#include <pvt/perf_data_cache_metric.hpp>

#include <qpvt/api.hpp>
#include <qpvt/models/perf_data_tree_model.hpp>

namespace pvt {
class severity_view;
}

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API metric_tree_model : public perf_data_tree_model<pvt::metric>
{
  Q_OBJECT
  
public:
  metric_tree_model(const pvt::severity_view* p_severity_view,
                    QObject*                  p_parent = nullptr);
  
  QVariant data(const QModelIndex& index, int role) const override;
  
private:
  mutable pvt::perf_data_cache_metric m_perf_data_cache;
};

} // namespace qpvt

#endif // #ifndef QPVT_METRIC_TREE_MODEL_HPP_
