//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_PERF_DATA_TREE_MODEL_HPP_
#define QPVT_PERF_DATA_TREE_MODEL_HPP_

#include <QAbstractItemModel>

#include <pvt/metric.hpp>
#include <pvt/perf_data.hpp>
#include <pvt/severity_view.hpp>

#include <qpvt/api.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
template <typename perf_data_item>
class QPVT_API perf_data_tree_model : public QAbstractItemModel
{
public:
  struct column_info 
  {
    column_info(std::string title, QVariant alignment) 
    : m_title(title), m_alignment(alignment)
    {
      
    }
    std::string m_title;
    QVariant    m_alignment;
  };
  
  enum user_roles
  {
    search_perf_data_item_role = Qt::UserRole,
    get_perf_data_item_pointer_role,
    sort_role,
    get_severity_role,
    get_severity_total_role,
    get_severity_self_role,
    get_relative_severity_role,
    using_total_severities_role,
    directed_variance_role,
    last_user_role
  };
  
  perf_data_tree_model     (const pvt::severity_view* p_severity_view, 
                            QObject*                  p_parent = nullptr)
  : QAbstractItemModel(p_parent)
  , mp_severity_view  (p_severity_view)
  {

  }

  int           columnCount(const QModelIndex&   parent = QModelIndex()) 
  const override
  {
    return m_column_infos.size();
  }
  
  Qt::ItemFlags flags      (const QModelIndex&   index) 
  const override
  {
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
  }
  
  QVariant      headerData (int                  section, 
                            Qt::Orientation      orientation, 
                            int                  role = Qt::DisplayRole)
  const override
  {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
      return m_column_infos[section].m_title.c_str();
    return QVariant();
  }
  
  QModelIndex   index      (int                  row,
                            int                  column,
                            const QModelIndex&   parent = QModelIndex())
  const override
  {
    QModelIndex result;

    if (!hasIndex(row, column, parent))
      return result;
    
    // If invisible root:
    if (!parent.isValid()) 
    {
      perf_data_item* const p_perf_data_item_index = mp_severity_view->
        get_performance_data()->get_root_perf_data_items<perf_data_item>()[row];

      result =
        createIndex(row, column, static_cast<void*>(p_perf_data_item_index));
    }
    else
    {
      const perf_data_item* const p_parent_perf_data_item =
        static_cast<const perf_data_item*>(parent.internalPointer());
      
      perf_data_item* const p_perf_data_item_for_index =
        p_parent_perf_data_item->get_children()[row];
      
      result = createIndex(row, column, p_perf_data_item_for_index);
    }

    return result;
  }  
  
  QModelIndex   parent     (const QModelIndex&   index) 
  const override
  {
    if (!index.isValid())
      return QModelIndex();
    
    perf_data_item* const p_perf_data_item_for_index =
      static_cast<perf_data_item*>(index.internalPointer());
    perf_data_item* const p_parent_perf_data_item    = 
      p_perf_data_item_for_index->parent();

    // If parent is invisible root:
    if (p_parent_perf_data_item == nullptr) 
      return QModelIndex();
    
    perf_data_item* const p_grandparent_perf_data_item =
      p_parent_perf_data_item->parent();

    // If parent is root perf_data_item:
    if (p_grandparent_perf_data_item == nullptr) 
    {
      typename std::vector<perf_data_item*>::const_iterator parent_iter =
        std::find(mp_severity_view->get_performance_data()->
                  get_root_perf_data_items<perf_data_item>().begin(),
                  mp_severity_view->get_performance_data()->
                  get_root_perf_data_items<perf_data_item>().end  (),
                  p_parent_perf_data_item);

      return createIndex(
        parent_iter - mp_severity_view->get_performance_data()->
          get_root_perf_data_items<perf_data_item>().begin(),
        0,
        p_parent_perf_data_item);
    }
    else
    {
      typename std::vector<perf_data_item*>::const_iterator parent_iter =
        std::find(p_grandparent_perf_data_item->get_children().begin(),
                  p_grandparent_perf_data_item->get_children().end  (),
                  p_parent_perf_data_item);

      return createIndex(
        parent_iter - p_grandparent_perf_data_item->get_children().begin(),
        0,
        p_parent_perf_data_item);
    }
  }

  int           rowCount   (const QModelIndex&   parent = QModelIndex()) 
  const override
  {
    if (parent.column() > 0)
      return 0;
    
    // If root:
    if (!parent.isValid())  
      return mp_severity_view->get_performance_data()->
        get_root_perf_data_items<perf_data_item>().size();
    else
      return static_cast<perf_data_item*>(parent.internalPointer())->
        get_num_children();
  }
  
protected:
  QVariant get_severity_string(double value) const
  {
    QString severity_string;
    severity_string += QString::number(value, 'g', 3);
    return severity_string;
  }

  const pvt::severity_view* mp_severity_view;
  std::vector<column_info>  m_column_infos;
};

} // namespace qpvt

#endif // #ifndef QPVT_PERF_DATA_TREE_MODEL_HPP_
