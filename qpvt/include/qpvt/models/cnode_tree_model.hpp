//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_CNODE_TREE_MODEL_HPP_
#define QPVT_CNODE_TREE_MODEL_HPP_

#include <pvt/perf_data_cache_cnode.hpp>

#include <qpvt/api.hpp>
#include <qpvt/models/perf_data_tree_model.hpp>

namespace pvt {
class cnode;
class metric;
}

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API cnode_tree_model : public perf_data_tree_model<pvt::cnode>
{
  Q_OBJECT
  
public:
  enum cnode_tree_model_roles
  {
    suitable_view_threshold_role = last_user_role
  };

  cnode_tree_model(const pvt::severity_view* p_severity_view,
                   QObject*                  p_parent = nullptr);
  
  QVariant data(const QModelIndex& index, int role) const override;

  pvt::suitable_3d_view_computer& get_suitable_3d_view_computer() const
  {
    return m_perf_data_cache.get_suitable_3d_view_computer();
  }

  void set_dirty()
  {
    m_perf_data_cache.set_dirty();
    emit dataChanged(QModelIndex(), QModelIndex());
  }
  
private:
  QVariant get_directed_variance_as_qvariant(const pvt::cnode* p_cnode) const;
  
  mutable pvt::perf_data_cache_cnode m_perf_data_cache;
};

} // namespace qpvt

#endif // #ifndef QPVT_CNODE_TREE_MODEL_HPP_
