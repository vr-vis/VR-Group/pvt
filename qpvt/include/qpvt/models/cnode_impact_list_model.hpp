//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_CNODE_IMPACT_LIST_MODEL_HPP_
#define QPVT_CNODE_IMPACT_LIST_MODEL_HPP_

#include <QAbstractTableModel>

#include <qpvt/api.hpp>

namespace pvt {
class cnode;
class perf_data;
class severity_view;
class suitable_3d_view_computer;
}

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API cnode_impact_list_model : public QAbstractTableModel
{
  Q_OBJECT
  
public:
  enum user_roles
  {
    search_perf_data_item_role      = Qt::UserRole,
    get_perf_data_item_pointer_role,
    sort_role
  };
  
  cnode_impact_list_model(
    pvt::suitable_3d_view_computer* p_suitable_3d_view_computer,
    QObject*                        p_parent = nullptr);
  
  void setup(pvt::perf_data* p_perf_data, pvt::severity_view* p_severity_view);

  void set_exclude_dimension_from_std_dev   (std::size_t dimension_id, bool exclude);
  void clear_exclude_dimensions_from_std_dev();
  
  int           columnCount(const QModelIndex& parent = QModelIndex())
  const override;
  
  int           rowCount   (const QModelIndex& parent = QModelIndex())
  const override;
  
  QVariant      headerData (int section, Qt::Orientation orientation, int role)
  const override;
  
  QVariant      data       (const QModelIndex& index, int role = Qt::DisplayRole)
  const override;

  Qt::ItemFlags flags      (const QModelIndex& index) const override;

private:
  QString  construct_call_path(const pvt::cnode*         p_cnode)       const;
  QVariant format_number      (double                    value)         const;
  QVariant data_display       (const QModelIndex&        index,
                               const pvt::severity_view& severity_view,
                               const pvt::cnode*         p_cnode)       const;
  QVariant data_sort          (const QModelIndex&        index,
                               const pvt::severity_view& severity_view,
                               const pvt::cnode*         p_cnode)       const;
  double   compute_std_dev    (const pvt::severity_view& severity_view) const;

  pvt::suitable_3d_view_computer* mp_suitable_3d_view_computer;
  pvt::perf_data*                 mp_perf_data;
  pvt::severity_view*             mp_severity_view;
  std::vector<bool>               m_exclude_dimensions_from_std_dev;
  
};

} // namespace qpvt

#endif // #ifndef QPVT_CNODE_IMPACT_LIST_MODEL_HPP_
