//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_CORRELATED_VIEWS_MODEL_HPP_
#define QPVT_CORRELATED_VIEWS_MODEL_HPP_

#include <vector>

#include <QAbstractTableModel>

#include <pvt/correlated_severity_view.hpp>

#include <qpvt/api.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API correlated_views_model : public QAbstractTableModel
{
  Q_OBJECT
  
public:
  correlated_views_model(QObject* p_parent = nullptr);

  const pvt::correlated_severity_view& get_correlated_view(
    const QModelIndex& index)
  {
    return m_correlated_views.at(index.row());
  }
  
  void setup(const std::vector<pvt::correlated_severity_view>& correlated_views)
  {
    beginInsertRows(QModelIndex(), 0, correlated_views.size());
    m_correlated_views = correlated_views;
    endInsertRows();
    emit dataChanged(QModelIndex(), QModelIndex());
  }
  
  int columnCount    (const QModelIndex& parent = QModelIndex()) 
  const override
  {
    return 4;
  }
  int rowCount       (const QModelIndex& parent = QModelIndex())
  const override
  {
    return m_correlated_views.size();
  }
  QVariant data      (const QModelIndex& index, int role = Qt::DisplayRole)
  const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role)
  const override;
  
private:
  QString compute_severity_string(
    const pvt::correlated_severity_view& correlated_view) const;
  
  std::vector<pvt::correlated_severity_view> m_correlated_views;
};

} // namespace qpvt

#endif // #ifndef QPVT_CORRELATED_VIEWS_MODEL_HPP_
