//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_PARALLEL_COORDINATES_PLOT_WIDGET_HPP_
#define QPVT_PARALLEL_COORDINATES_PLOT_WIDGET_HPP_

#include <memory>

#include <QWidget>

#include <pvt/rendering/renderable_parallel_coordinates_plot.hpp>

#include <qpvt/api.hpp>

class QFrame;
class QGridLayout;
class QLabel;
class QPushButton;

namespace pvt {
class camera;
class scene;
}

namespace qpvt {

class viewer_widget;

//------------------------------------------------------------------------------
class QPVT_API parallel_coordinates_plot_widget : public QWidget
{
  Q_OBJECT

public:
  using parallel_coords_plot = pvt::renderable_parallel_coordinates_plot;
  using axis                 = pvt::renderable_parallel_coordinates_plot::axis ;
  using entry                = pvt::renderable_parallel_coordinates_plot::entry;

  parallel_coordinates_plot_widget(QWidget* p_parent = nullptr);
  
  void set_entries         (const std::vector<entry>& entries);
  void set_entry_selected  (unsigned int entry_index, bool select);
  void deselect_all_entries();
  
public slots:
  void update();

private:
  void         setup_ui     ();
  QLabel*      create_label (const QString& text);
  QPushButton* create_button(const QString& text, 
                             std::vector<QPushButton*>& group);
  void         toggle_axis  (unsigned int index, bool inverted = true);

  viewer_widget*                        mp_viewer_widget;

  std::unique_ptr<pvt::camera>          mp_viewer_camera;
  std::unique_ptr<pvt::scene>           mp_scene;
  std::unique_ptr<parallel_coords_plot> mp_parallel_coords_plot;

  QGridLayout*                          mp_grid_layout;
  QFrame*                               mp_viewer_frame;
  std::vector<QPushButton*>             m_upper_buttons;
  std::vector<QPushButton*>             m_lower_buttons;

private slots:
  void on_upper_button_clicked(int index);
  void on_lower_button_clicked(int index);
};

} // namespace qpvt

#endif // #ifndef QPVT_PARALLEL_COORDINATES_PLOT_WIDGET_HPP_
