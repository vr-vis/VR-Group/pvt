//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_DATA_RANGE_H_SLIDER_HPP_
#define QPVT_DATA_RANGE_H_SLIDER_HPP_

#include <vector>

#include <qpvt/api.hpp>
#include <qpvt/widgets/range_h_slider.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API data_range_h_slider : public range_h_slider
{
  Q_OBJECT
  
  typedef float                     data_value_t;
  typedef std::vector<data_value_t> data_value_cont_t;
  
public:
  data_range_h_slider(QWidget* p_parent = nullptr, Qt::WindowFlags flags = 0);

  const data_value_cont_t& get_min_data_values () const
  {
    return m_min_data_values ;
  }
  const data_value_cont_t& get_max_data_values () const
  {
    return m_max_data_values ;
  }
  const data_value_cont_t& get_mean_data_values() const
  {
    return m_mean_data_values;
  }

  void set_min_value    (data_value_t value)
  {
    m_min_value = value;
  }
  void set_max_value    (data_value_t value)
  {
    m_max_value = value;
  }

  void set_min_is_zero  (bool min_is_zero)
  {
    m_min_is_zero = min_is_zero;
  }

  void add_data_value   (data_value_t min_value, 
                         data_value_t max_value, 
                         data_value_t mean_value)
  {
    m_min_data_values .push_back(min_value );
    m_max_data_values .push_back(max_value );
    m_mean_data_values.push_back(mean_value);
    m_data_prepared = false;
  }
  void clear_data_values() 
  { 
    m_min_data_values .clear();
    m_max_data_values .clear();
    m_mean_data_values.clear();
  }

  void set_data_dirty   ()
  {
    m_data_prepared = false;
    update();
  }
  void update_recursive ()
  {
    update();
    range_h_slider::update();
    range_slider  ::update();
  }

  void prepare_data     ();
  void set_colors       (QColor min_color, QColor max_color, QColor mean_color);

  void initializeGL     () override;
  void paintGL          () override;
  
private:
  data_value_cont_t                  m_min_data_values;
  data_value_cont_t                  m_max_data_values;
  data_value_cont_t                  m_mean_data_values;

  data_value_t                       m_min_data = 1.0;
  data_value_t                       m_max_data = 0.0;

  data_value_t                       m_min_value;
  data_value_t                       m_max_value;

  bool                               m_min_is_zero   = true;
  bool                               m_data_prepared = false;
  
  pvt::optional<pvt::vertex_array>   m_vertex_array;
  pvt::optional<pvt::vertex_buffer>  m_vertex_buffer;
  pvt::optional<pvt::shader_program> m_shader_program_h;
  pvt::optional<pvt::shader_program> m_shader_program_v;
};

} // namespace qpvt

#endif // #ifndef QPVT_DATA_RANGE_H_SLIDER_HPP_
