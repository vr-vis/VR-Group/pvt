//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_RANGE_V_SLIDER_HPP_
#define QPVT_RANGE_V_SLIDER_HPP_

#include <qpvt/api.hpp>
#include <qpvt/widgets/range_slider.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API range_v_slider : public range_slider
{
  Q_OBJECT

public:
  range_v_slider(QWidget* p_parent = nullptr, Qt::WindowFlags flags = 0);
  
signals:
  void range_from_other_requested();
  
protected:
  bool is_vertical() override 
  {                                                            
    return true;                                               
  }            

  void          handle_range_change (const QPointF& position)       override;
  void          handle_cursor_change(const QPointF& position)       override;
  slider_handle inside_handle       (const QPointF& position) const override;
};

} // namespace qpvt

#endif // #ifndef QPVT_RANGE_V_SLIDER_HPP_
