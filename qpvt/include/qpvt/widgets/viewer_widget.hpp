//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_VIEWER_WIDGET_HPP_
#define QPVT_VIEWER_WIDGET_HPP_

#include <QOpenGLWidget>

#include <qpvt/api.hpp>
#include <qpvt/utility/camera_tools.hpp>

namespace pvt {
class scene;
class camera;
}

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API viewer_widget : public QOpenGLWidget
{
  Q_OBJECT
  
public:
  viewer_widget(pvt::camera*    p_camera,
                QWidget*        p_parent = nullptr,
                Qt::WindowFlags flags    = 0);

  pvt::camera* get_camera     () const
  {
    return mp_camera;
  }
  void         set_scene      (pvt::scene*  p_scene )
  {
    mp_scene = p_scene;
  }
  void         set_camera     (pvt::camera* p_camera)
  {
    mp_camera = p_camera;
    m_camera_tools.set_camera(p_camera);
  }
  void         set_clear_color(float r, float g, float b, float a)
  {
    m_clear_color[0] = r;
    m_clear_color[1] = g;
    m_clear_color[2] = b;
    m_clear_color[3] = a;
  }
  
signals:
  void view_updated();
  void clicked     (int x, int y, Qt::KeyboardModifiers modifiers);
  
protected:
  void initializeGL     ()                      override;
  void resizeGL         (int width, int height) override;
  void paintGL          ()                      override;
  
  void mouseMoveEvent   (QMouseEvent* p_event)  override;
  void mousePressEvent  (QMouseEvent* p_event)  override;
  void mouseReleaseEvent(QMouseEvent* p_event)  override;
  
private:
  pvt::scene*  mp_scene;
  pvt::camera* mp_camera;
  camera_tools m_camera_tools;
  float        m_clear_color[4];
};

} // namespace qpvt

#endif // #ifndef QPVT_VIEWER_WIDGET_HPP_
