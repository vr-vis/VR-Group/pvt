//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_CNODE_IMPACT_SELECTOR_WIDGET_HPP_
#define QPVT_CNODE_IMPACT_SELECTOR_WIDGET_HPP_

#include <QWidget>

#include <qpvt/api.hpp>

class QAbstractButton;
class QButtonGroup;
class QDialogButtonBox;
class QGroupBox;
class QLineEdit;
class QSortFilterProxyModel;
class QTableView;
class QVBoxLayout;

namespace pvt {
class cnode;
class perf_data;
class severity_view;
}

namespace qpvt {

class cnode_impact_list_model;
class cnode_tree_model;

//------------------------------------------------------------------------------
class QPVT_API cnode_impact_selector_widget : public QWidget
{
  Q_OBJECT
  
public:
  cnode_impact_selector_widget(pvt::perf_data*     p_perf_data,
                               pvt::severity_view* p_severity_view,
                               cnode_tree_model*   p_cnode_tree_model,
                               QWidget*            p_parent = nullptr,
                               Qt::WindowFlags     flags    = 0);
  
  void invalidate_model         ();
  void select_and_focus_on_cnode(const pvt::cnode* p_cnode);
  
signals:
  void cnode_selected(pvt::cnode* p_cnode, bool cnode_total);
  
private slots:
  void handle_cnode_clicked   (const QModelIndex& index);
  void data_changed_std_dev   ();
  void data_changed_suggestion();
  void filter_changed         ();
  void threshold_changed      (QAbstractButton* p_button);
  
private:
  void setup_ui                            ();
  void setup_data_model                    ();
  void setup_sort_proxy_model              ();
  void setup_table_view                    ();
  void setup_exclude_dimension_from_std_dev();
  void setup_filter                        ();
  void setup_threshold                     ();
  void setup_layout                        ();
  
  pvt::perf_data*          mp_perf_data;
  pvt::severity_view*      mp_severity_view;

  cnode_tree_model*        mp_cnode_tree_model;  /// < \todo Find a better implementation for this
  cnode_impact_list_model* mp_cnode_impact_list_model;

  QVBoxLayout*             mp_layout;
  QSortFilterProxyModel*   mp_cnode_impact_list_proxy_model;
  QTableView*              mp_cnode_impact_list_table;
  QGroupBox*               mp_filter_std_dev_group_box;
  QButtonGroup*            mp_filter_std_dev_checkboxes;  
  QGroupBox*               mp_filter_rows_group_box;
  QButtonGroup*            mp_filter_rows_checkboxes;
  QGroupBox*               mp_threshold_group_box;
  QDialogButtonBox*        mp_dialog_button_box;
  QLineEdit*               mp_variation_threshold;
};

} // namespace qpvt

#endif // #ifndef QPVT_CNODE_IMPACT_SELECTOR_WIDGET_HPP_
