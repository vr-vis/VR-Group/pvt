//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_PERF_DATA_VIEWER_WIDGET_HPP_
#define QPVT_PERF_DATA_VIEWER_WIDGET_HPP_

#include <list>

#include <QWidget>

#include <pvt/correlated_severity_view.hpp>
#include <pvt/severity_view.hpp>

#include <qpvt/api.hpp>
#include <qpvt/models/correlated_views_model.hpp>
#include <qpvt/widgets/cartesian_topology_viewer_widget.hpp>
#include <qpvt/widgets/geometry_viewer_widget.hpp>
#include <qpvt/widgets/squarified_cushion_treemap_widget.hpp>

class QCheckBox;
class QItemSelection;
class QSortFilterProxyModel;
class QSplitter;
class QTableView;
class QTreeView;

namespace pvt {
class camera;
class perf_data;
}

namespace qpvt {

class cnode_impact_selector_widget;
class cnode_tree_model;
class correlated_views_model;
class directed_variance_glyph_delegate;
class metric_tree_model;
class viewer_widget;

//------------------------------------------------------------------------------
class QPVT_API perf_data_viewer_widget : public QWidget
{
  Q_OBJECT
  
public:
  enum type 
  {
    metric_cnode_views = 0,
    correlated_views   = 1
  };
  
   perf_data_viewer_widget(pvt::perf_data* p_perf_data,
                           type            type,
                           QWidget*        p_parent = nullptr,
                           Qt::WindowFlags flags    = 0);
  ~perf_data_viewer_widget();

  void setup_correlated_views_model(
    const std::vector<pvt::correlated_severity_view>& correlated_views);

  const pvt::severity_view& get_severity_view() const
  {
    return m_severity_view;
  }

  viewer_widget*            get_viewer       () const
  {
    return mp_cartesian_topology_viewer_widget->get_viewer();
  }
  pvt::camera*              get_camera       () const
  {
    return mp_cartesian_topology_viewer_widget->get_camera();
  }
  void                      set_camera       (pvt::camera* p_camera)
  {
    mp_cartesian_topology_viewer_widget->set_camera(p_camera);
  }                                          

  std::pair<float, float>   get_filter_range () const;
  void                      set_filter_range (float min, float max);

  bool get_cnodes_inclusive () const;
  bool get_metrics_inclusive() const;

signals:
  void find_correlated_views_requested  ();
  void filter_range_from_other_requested();
  
private slots:
  void handle_metric_selection_change (const QItemSelection& selected, 
                                       const QItemSelection& deselected);
  void handle_cnode_selection_change  (const QItemSelection& selected, 
                                       const QItemSelection& deselected);
  void handle_correlated_view_clicked (const QModelIndex&    index);
  void handle_cnode_selected_by_impact(pvt::cnode*           p_cnode, 
                                       bool                  cnode_total);
                                      
  void cnodes_view_section_moved (int logical_index,
                                  int old_visual_index,
                                  int new_visual_index);
  void metrics_view_section_moved(int logical_index,
                                  int old_visual_index,
                                  int new_visual_index);

  void filter_suitable_3d_view               (bool checked);
  void filter_ok_clicked                     (bool checked);
  void filter_cancel_clicked                 (bool checked);
  void set_threshold_suitable_3d_view_clicked(bool checked);
  void fade_out_directed_variance_clicked    (bool checked);

private:
  void setup_ui();
  
  void determine_selected_metrics ();
  void determine_selected_cnodes  ();
  void update_severities_in_viewer();
  void open_cnode_impact_selector ();
  
  void select_and_focus_on_cnode (const pvt::cnode*  p_cnode);
  void select_and_focus_on_metric(const pvt::metric* p_metric);
  
  type                               m_type;
                                     
  pvt::perf_data*                    mp_perf_data;
  pvt::severity_view                 m_severity_view;
                                     
  cnode_tree_model*                  mp_cnode_tree_model;
  QSortFilterProxyModel*             mp_cnode_tree_model_sort_proxy;
  metric_tree_model*                 mp_metric_tree_model;
  correlated_views_model*            mp_correlated_views_model;
                                     
  cnode_impact_selector_widget*      mp_cnode_impact_selector_widget      = nullptr;
  cartesian_topology_viewer_widget*  mp_cartesian_topology_viewer_widget  = nullptr;
  geometry_viewer_widget*            mp_geometry_viewer_widget            = nullptr;
  squarified_cushion_treemap_widget* mp_squarified_cushion_treemap_widget = nullptr;
  directed_variance_glyph_delegate*  mp_directed_variance_glyph_delegate  = nullptr;
                                     
  QTableView*                        mp_correlation_table_view = nullptr;
  QTreeView*                         mp_cnodes_view            = nullptr;
  QTreeView*                         mp_metrics_view           = nullptr;
  QWidget*                           mp_filter_widget          = nullptr;
  std::vector<QCheckBox*>            m_filter_check_boxes;
};

} // namespace qpvt

#endif // #ifndef QPVT_PERF_DATA_VIEWER_WIDGET_HPP_
