//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_SQUARIFIED_CUSHION_TREEMAP_WIDGET_HPP_
#define QPVT_SQUARIFIED_CUSHION_TREEMAP_WIDGET_HPP_

#include <memory>

#include <pvt/cnode.hpp>
#include <pvt/rendering/treemap.hpp>
#include <pvt/rendering/treemap_interaction_policy_basic.hpp>
#include <pvt/rendering/treemap_shading_policy_cushion.hpp>
#include <pvt/rendering/treemap_tessellation_policy_squarified.hpp>

#include <qpvt/api.hpp>

// Native GL must be included before any other GL.
#include <QOpenGLWidget>

namespace pvt {
class perf_data;
}

namespace qpvt {

typedef pvt::treemap<pvt::cnode,
                     pvt::treemap_tessellation_policy_squarified,
                     pvt::treemap_shading_policy_cushion,
                     pvt::treemap_interaction_policy_basic>
  squarified_cushion_cnode_treemap;

//------------------------------------------------------------------------------
class QPVT_API squarified_cushion_treemap_widget : public QOpenGLWidget
{
  Q_OBJECT

public:
  squarified_cushion_treemap_widget(
    pvt::perf_data* p_perf_data,
    QWidget*        p_parent = nullptr);

  void set_focus (pvt::cnode*        p_focus_cnode);
  void set_metric(const pvt::metric* p_metric);

signals:
  void cnode_selected(pvt::cnode* p_cnode);

private:
  void        update_weights      (const pvt::metric* p_metric);
  void        zoom_towards        (pvt::cnode*        p_cnode);
  std::string build_tooltip_string(pvt::cnode*        p_cnode);

  void initializeGL   ()                           override;
  void resizeGL       (int width, int height)      override;
  void paintGL        ()                           override;

  bool event          (QEvent*      p_event      ) override;
  void mousePressEvent(QMouseEvent* p_press_event) override;
  void wheelEvent     (QWheelEvent* p_wheel_event) override;
  
  pvt::perf_data*                                   mp_perf_data;
  pvt::data_array<double>                           m_weights;
  std::unique_ptr<squarified_cushion_cnode_treemap> mp_treemap;

  pvt::cnode*  mp_cached_focus_node = nullptr;
  pvt::metric* mp_cached_metric     = nullptr;
};

} // namespace qpvt

#endif // #ifndef QPVT_SQUARIFIED_CUSHION_TREEMAP_WIDGET_HPP_
