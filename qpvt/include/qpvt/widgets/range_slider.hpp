//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_RANGE_SLIDER_HPP_
#define QPVT_RANGE_SLIDER_HPP_

#include <pvt/optional.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/gl/shader_program.hpp>
#include <pvt/gl/vertex_array.hpp>

#include <qpvt/api.hpp>

// Native GL must be included before any other GL.
#include <QOpenGLWidget>

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API range_slider : public QOpenGLWidget
{
  Q_OBJECT
  
public:
  range_slider(QWidget* p_parent = nullptr, Qt::WindowFlags flags = 0);
  
  float get_min             () const
  {
    return m_min;
  }
  float get_max             () const
  {
    return m_max;
  }
  
  void  set_min             (float min) 
  {
    m_min = std::max(0.0f, std::min(m_max - 0.005f, min));
    update();
  }
  void  set_max             (float max) 
  {
    m_max = std::max(m_min + 0.005f, std::min(1.0f, max));
    update();
  }
        
  void  set_background_color(const QColor& color);
  void  set_covers_color    (const QColor& color)
  {
    m_covers_color = color;
  }
  
signals:
  void  range_changed       (float min, float max);
  void  range_changed_end   (float min, float max);
  
protected:
  enum slider_handle 
  {
    no_handle  = 0,
    min_handle = 1,
    max_handle = 2,
    mid_handle = 3
  };

  static void           setup_viewport     (int width, int height);
  void                  draw_covers_handles();
  
  virtual bool          is_vertical         ()                              = 0;
  virtual void          handle_range_change (const QPointF& position)       = 0;
  virtual void          handle_cursor_change(const QPointF& position)       = 0;
  virtual slider_handle inside_handle       (const QPointF& position) const = 0;

  void                  initializeGL        ()                      override;
  void                  resizeGL            (int width, int height) override;
  virtual void          paintGL             ()                      override;

  void                  leaveEvent          (QEvent*      p_event)  override;
  void                  mousePressEvent     (QMouseEvent* p_event)  override;
  void                  mouseReleaseEvent   (QMouseEvent* p_event)  override;
  void                  mouseMoveEvent      (QMouseEvent* p_event)  override;
  
  float                              m_min                      = 0.0;
  float                              m_max                      = 1.0;
  QWidget*                           mp_min_handle;
  QWidget*                           mp_max_handle;
  slider_handle                      m_active_handle            = no_handle;

  QPointF                            m_initial_local_position;
  float                              m_initial_min;
  float                              m_initial_max;

  QColor                             m_background_color         = QColor(255, 255, 255);
  QColor                             m_covers_color             = QColor(128, 128, 128, 127);
  QColor                             m_handles_color            = QColor(96 , 96 , 96 , 255);
  
  pvt::optional<pvt::vertex_array>   m_cover_vertex_array;
  pvt::optional<pvt::vertex_buffer>  m_cover_vertex_buffer;
  pvt::optional<pvt::shader_program> m_cover_shader_program;
};

} // namespace qpvt

#endif // #ifndef QPVT_RANGE_SLIDER_HPP_
