//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_GEOMETRY_VIEWER_WIDGET_HPP_
#define QPVT_GEOMETRY_VIEWER_WIDGET_HPP_

#include <memory>

#include <QFrame>
#include <QSplitter>
#include <QTableWidget>

#include <pvt/data_array.hpp>
#include <pvt/system_geometry.hpp>
#include <pvt/rendering/camera.hpp>
#include <pvt/rendering/clickable_scene.hpp>
#include <pvt/rendering/renderable_geometry.hpp>

#include <qpvt/api.hpp>
#include <qpvt/widgets/parallel_coordinates_plot_widget.hpp>
#include <qpvt/widgets/viewer_widget.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API geometry_viewer_widget : public QSplitter
{
  Q_OBJECT

  enum class interactor_mode
  {
    select_one,
    toggle_one,
    deselect_one,
    deselect_all,
  };

public:
  explicit geometry_viewer_widget(pvt::system_geometry*  p_system_geometry);

  template <typename type>
  void map_geometric_severities (const pvt::data_array<type>& thread_severities);
  
private:
  void setup_viewer          ();
  void setup_parallel_coords ();
  void setup_table           ();
  
  void update_viewer         (const pvt::data_array<float>& geometry_severities);
  void update_parallel_coords(const pvt::data_array<float>& geometry_severities);
  void update_table          (const pvt::data_array<float>& geometry_severities);

  void handle_geometry_selection(int index, interactor_mode interactor_mode);

  pvt::system_geometry*                             mp_system_geometry;

  std::vector<unsigned>                             m_selected_indices;

  viewer_widget*                                    mp_viewer_widget;
  std::unique_ptr<parallel_coordinates_plot_widget> mp_parallel_coords_widget;
  QTableWidget*                                     mp_table_widget;

  pvt::camera*                                      mp_camera;
  pvt::clickable_scene*                             mp_scene;
  std::vector<pvt::renderable_geometry*>            mp_renderable_geometries;

  QFrame*                                           mp_viewer_frame;
};


//------------------------------------------------------------------------------
template <typename type>
void 
geometry_viewer_widget::map_geometric_severities
(const pvt::data_array<type>& thread_severities)
{
  if (mp_system_geometry)
  {
    pvt::data_array<float> geometry_severities;
    mp_system_geometry->map_severities(thread_severities, geometry_severities);
    update_viewer         (geometry_severities);
    update_table          (geometry_severities);
    update_parallel_coords(geometry_severities);
    update                ();
  }
}

} // namespace qpvt

#endif // #ifndef QPVT_GEOMETRY_VIEWER_WIDGET_HPP_
