//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_RANGE_H_SLIDER_HPP_
#define QPVT_RANGE_H_SLIDER_HPP_

#include <qpvt/api.hpp>
#include <qpvt/widgets/range_slider.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API range_h_slider : public range_slider
{
  Q_OBJECT

public:
  range_h_slider(QWidget* p_parent = nullptr, Qt::WindowFlags flags = 0);
  
protected:
  bool is_vertical() override
  {
    return false;
  }

  void          handle_range_change (const QPointF& pos)       override;
  void          handle_cursor_change(const QPointF& pos)       override;
  slider_handle inside_handle       (const QPointF& pos) const override;
};

} // namespace qpvt

#endif // #ifndef QPVT_RANGE_H_SLIDER_HPP_
