//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PV_CARTESIAN_TOPOLOGY_VIEWER_WIDGET__H_
#define PV_CARTESIAN_TOPOLOGY_VIEWER_WIDGET__H_

#include <algorithm>

#include <pvt/data_array.hpp>

#include <QApplication>
#include <QComboBox>
#include <QFrame>
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QWidget>

#include <pvt/cartesian_topology.hpp>
#include <pvt/rendering/renderable_cartesian_grid.hpp>
#include <pvt/rendering/renderable_cartesian_topology.hpp>

#include <qpvt/api.hpp>
#include <qpvt/widgets/slice_slider.hpp>
#include <qpvt/widgets/viewer_widget.hpp>

class QComboBox;
class QDropEvent;
class QGridLayout;
class QSlider;

namespace pvt {
class camera;
class renderable_axes;
class scene;
}

namespace qpvt {
  
class data_range_h_slider;
class data_range_v_slider;

//------------------------------------------------------------------------------
class QPVT_API cartesian_topology_viewer_widget : public QWidget
{
  Q_OBJECT

public:
  enum type_flags
  {
    without_find_correlated_view_button = 0x00,
    with_find_correlated_view_button    = 0x01,
  };
  
  explicit cartesian_topology_viewer_widget(
    pvt::cartesian_topology*  p_cartesian_topology,
    type_flags                type_flags,
    QWidget*                  p_parent      = nullptr,
    Qt::WindowFlags           window_flags  = 0);
  ~cartesian_topology_viewer_widget();
  
  template <typename T>
  void map_data_to_topology(const pvt::data_array<T>& data);

  pvt::camera* get_camera() const
  {
    return mp_camera;
  }
  void         set_camera(pvt::camera* p_camera)
  {
    mp_camera = p_camera;
    mp_viewer_widget->set_camera(p_camera);
  }
  
  viewer_widget* get_viewer() const
  {
    return mp_viewer_widget;
  }
  
  std::pair<float, float> get_filter_range() const;
  void                    set_filter_range(float min, float max);
  
signals:
  void find_correlated_views_clicked    ();
  void filter_range_from_other_requested();
  
private slots:
  void value_range_changed        (float min, float max);
  void value_range_changed_end    (float min, float max);
  void min_mode_activated         (const QString& min_mode);
  void offsets_changed            (int value);
  void dimension_range_changed    (std::size_t dimension, float min, float max);
  void dimension_range_changed_end(std::size_t dimension, float min, float max);
  
private:
  void setup_ui                          ();
  void setup_viewer_widget               ();
  void setup_dimension_labels            ();
  void setup_dimension_sliders           ();
  void setup_gradient_value_slider       ();
  void setup_find_correlated_views_button();
  void setup_hud                         ();
  
  void update_dimension_labels           ();
  void update_axis_scales                () const;
  void update_slider_plots               ();
  void update_hud                        ();
  void resize_hud                        ();
  
  void get_min_max_mean_for_range(
    float*                          p_min,
    float*                          p_max,
    float*                          p_mean,
    const std::vector<std::size_t>& minima,
    const std::vector<std::size_t>& maxima) const;

  void scale_offset_slider_plot(std::size_t index);
  void toggle_overview_plots   (bool checked);
  
  void mousePressEvent  (QMouseEvent*     p_event) override;
  void mouseMoveEvent   (QMouseEvent*     p_event) override;
  void mouseReleaseEvent(QMouseEvent*     p_event) override;
  void dragEnterEvent   (QDragEnterEvent* p_event) override;
  void dropEvent        (QDropEvent*      p_event) override;

  type_flags                          m_type_flags;

  QGridLayout*                        mp_grid_layout;
  QFrame*                             mp_viewer_frame;
  QLabel*                             mp_hud;
  
  viewer_widget*                      mp_viewer_widget;
  pvt::camera*                        mp_camera;

  std::vector<data_range_h_slider*>   m_horizontal_sliders;
  data_range_v_slider*                mp_histogram_slider;
  std::vector<slice_slider*>          m_offset_sliders;
  std::vector<QPushButton*>           m_offset_slider_scale_buttons;
  QComboBox*                          mp_minimum_combo_box;
  
  pvt::cartesian_topology*            mp_cartesian_topology;
  pvt::scene*                         mp_renderable_scene;
  pvt::renderable_cartesian_grid*     mp_renderable_cartesian_grid;
  pvt::renderable_cartesian_topology* mp_renderable_cartesian_topology;
  pvt::renderable_axes*               mp_renderable_axes;
  
  std::vector<QLabel*>                m_dimension_labels;
  std::vector<std::size_t>            m_projection;

  pvt::data_array<float>*             m_topology_mapping = nullptr;

  float                               m_minimum                 = 0.0f;
  float                               m_maximum                 = 1.0f;
  float                               m_minimum_in_projection;
  float                               m_maximum_in_projection;
  int                                 m_base_offset             = 0.0;

  bool                                m_is_dragging             = false;
  QPoint                              m_drag_start_position;
  std::size_t                         mp_dragged_label_index;
  QLabel*                             mp_dragged_label          = nullptr;
  QPixmap*                            mp_dragged_pixmap         = nullptr;
  
};

//------------------------------------------------------------------------------
template <typename T>
void 
cartesian_topology_viewer_widget::map_data_to_topology
(const pvt::data_array<T>& data)
{
  m_minimum = std::numeric_limits<float>::max();
  m_maximum = 0.0f;
    
  mp_cartesian_topology->map_data(data,
                                  *m_topology_mapping);

  for (double value : *m_topology_mapping)
  {
    m_minimum = std::min(m_minimum, static_cast<float>(value));
    m_maximum = std::max(m_maximum, static_cast<float>(value));
  }
    
  const auto& cart_topo_idx = mp_cartesian_topology->get_index();

  const auto num_cells_x = cart_topo_idx.get_dimension_sizes()[m_projection[0]];
  const auto num_cells_y = cart_topo_idx.get_dimension_sizes()[m_projection[1]];
  const auto num_cells_z = cart_topo_idx.get_dimension_sizes()[m_projection[2]];
    
  float maximum_in_projection = -std::numeric_limits<float>::infinity();
  float minimum_in_projection =  std::numeric_limits<float>::infinity();

  std::vector<std::size_t> curr_coords_for_min_max(
    cart_topo_idx.get_num_dimensions(), 0);

  for (  curr_coords_for_min_max[m_projection[0]] = 0;
         curr_coords_for_min_max[m_projection[0]] < num_cells_x;
       ++curr_coords_for_min_max[m_projection[0]])
  {
    for (  curr_coords_for_min_max[m_projection[1]] = 0;
           curr_coords_for_min_max[m_projection[1]] < num_cells_y;
         ++curr_coords_for_min_max[m_projection[1]])
    {
      for (  curr_coords_for_min_max[m_projection[2]] = 0;
             curr_coords_for_min_max[m_projection[2]] < num_cells_z;
           ++curr_coords_for_min_max[m_projection[2]])
      {
        const std::size_t index_for_coordinates = 
          cart_topo_idx.get_index(curr_coords_for_min_max) + 
          m_base_offset;

        const float curr_val =
          m_topology_mapping->at(index_for_coordinates);

        maximum_in_projection = std::max(maximum_in_projection, curr_val);
        minimum_in_projection = std::min(minimum_in_projection, curr_val);
      }
    }
  }
  
  const float dataset_minimum =
    m_minimum *
    static_cast<float>(mp_minimum_combo_box->currentText().compare("min" )== 0)+
    minimum_in_projection *
    static_cast<float>(mp_minimum_combo_box->currentText().compare("minP")== 0);
    
  mp_renderable_cartesian_topology->set_minimum(dataset_minimum);
  mp_renderable_cartesian_topology->set_maximum(m_maximum);
  mp_renderable_cartesian_topology->set_dirty();
  mp_viewer_widget->update();
  std::for_each(m_offset_sliders.begin(), m_offset_sliders.end(),
  [this](slice_slider* slider)
  {
    slider->set_line_plot_min(m_minimum);
    slider->set_line_plot_max(m_maximum);
    slider->manual_update(); 
  });
  update_slider_plots();
}

} // namespace qpvt

#endif // #ifndef PV_CARTESIAN_TOPOLOGY_VIEWER_WIDGET__H_
