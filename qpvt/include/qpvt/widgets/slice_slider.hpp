//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_SLICE_SLIDER_HPP_
#define QPVT_SLICE_SLIDER_HPP_

#include <QWidget>

#include <qpvt/api.hpp>

class QSlider;
class QVBoxLayout;

namespace pvt {
class renderable_line_plot;
class renderable_line_plot_axes;
class scene;
}

namespace qpvt {
class viewer_widget;

//------------------------------------------------------------------------------
class QPVT_API slice_slider : public QWidget
{
  Q_OBJECT
  
public:
  explicit slice_slider(pvt::cartesian_topology* p_cartesian_topology,
                        pvt::data_array<float>*  p_data_in_topology,
                        bool                     with_line_plot       = false,
                        QWidget*                 p_parent             = nullptr,
                        Qt::WindowFlags          flags                = 0);
  
  int get_value() const
  {
    return mp_slider->value();
  }

  void set_value(int value)
  {
    mp_slider->setValue(value);
  }
  void set_min  (int min  )
  {
    mp_slider->setMinimum(min);
  }
  void set_max  (int max  )
  {
    mp_slider->setMaximum(max);
  }
  
  void set_line_plot_min(float min);
  void set_line_plot_max(float max);
  void set_dimension_id(std::size_t dimension_id);
  
  void scale_up       ();
  void scale_down     ();
  void toggle_overview(bool enabled);

  void manual_update();

signals:
  void valueChanged(int value);

private:
  std::size_t                     m_dimension_id;
  bool                            m_with_line_plot;

  pvt::cartesian_topology*        mp_cartesian_topology;
  pvt::scene*                     mp_renderable_scene;
  pvt::renderable_line_plot_axes* mp_line_plot_axes;
  pvt::renderable_line_plot*      mp_line_plot;

  viewer_widget*                  mp_viewer_widget;

  QVBoxLayout*                    mp_layout;
  QVBoxLayout*                    mp_viewer_layout;
  QSlider*                        mp_slider;
};

} // namespace qpvt

#endif // #ifndef QPVT_SLICE_SLIDER_HPP_
