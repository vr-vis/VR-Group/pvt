//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_DIRECTED_VARIANCE_GLYPH_DELEGATE_HPP_
#define QPVT_DIRECTED_VARIANCE_GLYPH_DELEGATE_HPP_

#include <QAbstractItemDelegate>

#include <qpvt/api.hpp>

namespace pvt {
  class perf_data;
}

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API directed_variance_glyph_delegate : public QAbstractItemDelegate
{
Q_OBJECT
    
public:
  directed_variance_glyph_delegate(
    pvt::perf_data* p_perf_data, 
    QObject*        p_parent   = nullptr);
  
  void  paint   (QPainter*                   p_painter,
                 const QStyleOptionViewItem& option,
                 const QModelIndex&          index) const override;
  QSize sizeHint(const QStyleOptionViewItem& option,
                 const QModelIndex&          index) const override;
  
  bool get_fade_out_enabled() const
  {
    return m_fade_out_enabled;
  }
  void set_fade_out_enabled(bool enabled)
  {
    m_fade_out_enabled = enabled;
  }

private:
  pvt::perf_data* mp_perf_data;
  bool            m_fade_out_enabled = true;
};

} // namespace qpvt

#endif // #ifndef QPVT_DIRECTED_VARIANCE_GLYPH_DELEGATE_HPP_
