//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef QPVT_CAMERA_TOOLS_HPP_
#define QPVT_CAMERA_TOOLS_HPP_

#include <map>

#include <Eigen/Core>

#include <qpvt/api.hpp>

class QMouseEvent;

namespace pvt {
class camera;
}

namespace qpvt {

//------------------------------------------------------------------------------
class QPVT_API camera_tools
{
public:
  enum tool
  {
    none   = 0,
    tumble = 1,
    dolly  = 2,
    track  = 3
  };
  
  camera_tools(pvt::camera* p_camera);

  void set_camera(pvt::camera* p_camera) 
  {
    mp_camera = p_camera; 
  }
  bool is_active () const
  {
    return m_active_tool != none;
  }
  
  void activate  (tool tool, const Eigen::Vector2f& mouse_position);
  void update    (           const Eigen::Vector2f& mouse_position);
  void deactivate();

  bool handle_mouse_press  (QMouseEvent* p_event, Qt::KeyboardModifiers keyboard_modifiers);
  bool handle_mouse_move   (QMouseEvent* p_event, Qt::KeyboardModifiers keyboard_modifiers);
  bool handle_mouse_release(QMouseEvent* p_event, Qt::KeyboardModifiers keyboard_modifiers);
  
private:
  typedef std::map<std::pair<Qt::MouseButton, Qt::KeyboardModifiers>, tool> button_modifiers_toolmap_t;

  pvt::camera*               mp_camera     = nullptr;
  tool                       m_active_tool = camera_tools::none;
  button_modifiers_toolmap_t m_button_modifiers_tool_map;
  
  float                      m_initial_lambda   = 0;
  float                      m_initial_phi      = 0;
  float                      m_initial_distance = 0;
  Eigen::Vector2f            m_initial_mouse_position;
  Eigen::Vector3f            m_initial_coi;
};

} // namespace qpvt

#endif // #ifndef QPVT_CAMERA_TOOLS_HPP_
