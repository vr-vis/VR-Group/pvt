#-------------------------------------------------------------------------------
# pvt performance visualization toolkit
#
# Copyright (c) 2014-2016 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualisation Group.
#-------------------------------------------------------------------------------
#                                 License
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# In the future, we may decide to add a commercial license
# at our own discretion without further notice.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

cmake_minimum_required(VERSION 2.8.11)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

project(qpvt)

include( "./_SourceFiles.cmake" )

# Assign source groups.
include("${CMAKE_SOURCE_DIR}/cmake/assign_source_group.cmake")
assign_source_group(${SourceFiles})

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# C++11
ADD_DEFINITIONS(
  -std=c++11
)

include_directories(./include)

#-------------------------------------------------------------------------------
# dependencies
#-------------------------------------------------------------------------------

# Eigen3
find_package(Eigen3 REQUIRED)
include_directories(${EIGEN3_INCLUDE_DIR})

# FFTW
find_package(FFTW REQUIRED)
include_directories(${FFTW_INCLUDES})

# OpenGL
find_package(OpenGL)
include_directories(${OPENGL_INCLUDE_DIR})

# GLEW (only on Windows for definitions of OpenGL 3.0)
if(WIN32)
  find_package(GLEW REQUIRED)
  if(GLEW_FOUND)
    include_directories(${GLEW_INCLUDE_DIRS})
  endif()
endif()

# Qt5
if (DEFINED ENV{QT5_DIR})
  set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} $ENV{QT5_DIR})
  include_directories(BEFORE $ENV{QT5_DIR}/include)
endif()

set(CMAKE_AUTOMOC ON)

find_package(Qt5Core    REQUIRED)
find_package(Qt5Network REQUIRED)
find_package(Qt5OpenGL  REQUIRED)
find_package(Qt5Widgets REQUIRED)

QT5_ADD_RESOURCES(QT_RCC_SRCS  ${RESOURCES})
QT5_WRAP_CPP     (QT_MOC_FILES ${QT_FILES_TO_MOC})
QT5_WRAP_UI      (QT_UI_SRCS   ${UI})

# pvt
include_directories(../pvt/include)

#-------------------------------------------------------------------------------
# library
#-------------------------------------------------------------------------------

# Toggle for shared or static library.
set(BUILD_SHARED OFF CACHE BOOL "Build as a shared library.")

# Create library
if   (BUILD_SHARED)
  add_library(${PROJECT_NAME} SHARED ${SourceFiles} ./_SourceFiles.cmake)
  add_definitions(-DPVT_DLL -DQPVT_DLL -DQPVT_EXPORT)
else ()
  add_library(${PROJECT_NAME}        ${SourceFiles} ./_SourceFiles.cmake)
endif()

target_link_libraries(${PROJECT_NAME}
  ${OPENGL_LIBRARIES}
  Qt5::Core
  Qt5::OpenGL
  Qt5::Widgets
  pvt
)

if(WIN32)
  target_link_libraries(${PROJECT_NAME} ${GLEW_LIBRARY})
endif()

set(${PROJECT_NAME}_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR} PARENT_SCOPE)
