//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <QMouseEvent>

#include <pvt/rendering/camera.hpp>

#include <qpvt/utility/camera_tools.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
camera_tools::camera_tools
(pvt::camera* p_camera)
: mp_camera    (p_camera)
, m_active_tool(camera_tools::none)
, m_button_modifiers_tool_map({{
    std::make_pair(std::make_pair(Qt::LeftButton, Qt::AltModifier), 
                                  camera_tools::tumble),
    std::make_pair(std::make_pair(Qt::LeftButton, Qt::AltModifier | Qt::ShiftModifier), 
                                  camera_tools::track),
    std::make_pair(std::make_pair(Qt::RightButton, Qt::AltModifier),
                                  camera_tools::dolly)
}})
{

}

//------------------------------------------------------------------------------
void
camera_tools::activate
(tool                   tool, 
 const Eigen::Vector2f& mouse_position)
{
  if (mp_camera == nullptr)
    return;
  
  m_initial_mouse_position = mouse_position;
  m_initial_lambda         = mp_camera->get_lambda  ();
  m_initial_phi            = mp_camera->get_phi     ();
  m_initial_distance       = mp_camera->get_distance();
  m_initial_coi            = mp_camera->get_coi     ();
  
  switch (tool)
  {
    case camera_tools::tumble:
    case camera_tools::dolly:
    case camera_tools::track:
      m_active_tool = tool;
      break;
    default:
      break;
  }
}


//------------------------------------------------------------------------------
void
camera_tools::update
(const Eigen::Vector2f& mouse_position)
{
  if (mp_camera == nullptr)
  {
    return;
  }
  
  const Eigen::Vector2f mouse_delta = mouse_position - m_initial_mouse_position;
  
  const float tumble_scale =
    0.01f * static_cast<float>(m_active_tool == camera_tools::tumble);
  const float dolly_scale  =
    1.0f * static_cast<float> (m_active_tool == camera_tools::dolly );
  const float track_scale =
    0.5f * static_cast<float> (m_active_tool == camera_tools::track );
  
  const Eigen::Matrix4f rotation_mat(mp_camera->get_rotation_matrix());
  mp_camera->set_lambda  (m_initial_lambda   - tumble_scale * mouse_delta.x());
  mp_camera->set_phi     (m_initial_phi      + tumble_scale * mouse_delta.y());
  mp_camera->set_distance(m_initial_distance - dolly_scale  * mouse_delta.y());
  mp_camera->set_coi     (m_initial_coi +
    track_scale * mouse_delta.x() * rotation_mat.block(0,0, 1, 3).transpose() -
    track_scale * mouse_delta.y() * rotation_mat.block(1,0, 1, 3).transpose());
}


//------------------------------------------------------------------------------
void
camera_tools::deactivate
()
{
  m_active_tool = camera_tools::none;
}


//------------------------------------------------------------------------------
bool
camera_tools::handle_mouse_press
(QMouseEvent*          p_event,
 Qt::KeyboardModifiers keyboard_modifiers)
{
  const Eigen::Vector2f mouse_pos(p_event->localPos().x(), 
                                  p_event->localPos().y());
  
  auto tool_id_iter = m_button_modifiers_tool_map.find(
    std::make_pair(p_event->button(), keyboard_modifiers));
  if (tool_id_iter != m_button_modifiers_tool_map.end())
  {
    activate(tool_id_iter->second, mouse_pos);
    return true;
  }
  else
  {
    activate(camera_tools::none, mouse_pos);
    return true;
  }
}


//------------------------------------------------------------------------------
bool
camera_tools::handle_mouse_move
(QMouseEvent*          p_event,
 Qt::KeyboardModifiers keyboard_modifiers)
{
  if (is_active())
  {
    update(Eigen::Vector2f(p_event->localPos().x(), p_event->localPos().y()));
    return true;
  }
  return false;
}


//------------------------------------------------------------------------------
bool
camera_tools::handle_mouse_release
(QMouseEvent*          p_event,
 Qt::KeyboardModifiers keyboard_modifiers)
{
  if (is_active())
  {
    deactivate();
    return true;
  }
  return false;
}

} // namespace qpvt
