//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <QPainter>
#include <QTreeView>

#include <qpvt/delegates/severity_glyph_delegate.hpp>
#include <qpvt/models/cnode_tree_model.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
severity_glyph_delegate::severity_glyph_delegate
(QObject* p_parent)
: QAbstractItemDelegate(p_parent)
{
  
}


//------------------------------------------------------------------------------
void
severity_glyph_delegate::paint
(QPainter*                   p_painter,
 const QStyleOptionViewItem& option,
 const QModelIndex&          index)
const
{
  // Determine depth for indentation.
  std::size_t depth = 0;
  QModelIndex curr_index = index.parent();
  while(curr_index.isValid())
  {
    ++depth;
    curr_index = curr_index.parent();
  }
  
  const double relative_severity =
    index.data(cnode_tree_model::get_relative_severity_role).toDouble();

  // Paint.
  const int inner_border_width = 1;
  const int indent             = 4;

  const QRect outline_rect(option.rect.x() + depth * indent,
                           option.rect.y(),
                           option.rect.height(),
                           option.rect.height());
  const QRect inner_rect  (option.rect.x() + inner_border_width  + depth * indent,
                           option.rect.y() + inner_border_width,
                           option.rect.height() - 2 * inner_border_width,
                           option.rect.height() - 2 * inner_border_width);
  
  p_painter->save();
  
  p_painter->setOpacity(relative_severity);
  p_painter->setBrush  (Qt::black);
  
  p_painter->setPen    (Qt::NoPen);
  p_painter->drawRect  (inner_rect);

  p_painter->drawText  (0, 10, QString::number(depth));
  p_painter->restore   ();
}


//------------------------------------------------------------------------------
QSize
severity_glyph_delegate::sizeHint
(const QStyleOptionViewItem & option,
 const QModelIndex &          index)
const
{
  return QSize(4,4);
}

} // namespace qpvt
