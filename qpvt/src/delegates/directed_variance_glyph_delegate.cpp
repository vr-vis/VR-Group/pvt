//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cmath>

#include <QPainter>

#include <qpvt/delegates/directed_variance_glyph_delegate.hpp>
#include <qpvt/models/cnode_tree_model.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
directed_variance_glyph_delegate::directed_variance_glyph_delegate
(pvt::perf_data* p_perf_data,
 QObject*        p_parent   )
: QAbstractItemDelegate(p_parent   )
, mp_perf_data         (p_perf_data)
{
  
}


//------------------------------------------------------------------------------
void
directed_variance_glyph_delegate::paint
(QPainter*                   p_painter,
 const QStyleOptionViewItem& option,
 const QModelIndex&          index)
const
{
  const int left     = option.rect.x     ();
  const int top      = option.rect.y     ();
  const int height   = option.rect.height();
  const int baseline = top + height - 1;
  
  const QColor baseline_color(255 - 64, 255 - 64, 255 - 64);
  const QColor bar_color     (255 - 64, 255 - 64, 255 - 64);
  
  const QList<QVariant> directed_variances(
    index.data(cnode_tree_model::directed_variance_role).toList());

  const std::size_t num_dims = directed_variances.size();
  
  const double severity =
    index.data(cnode_tree_model::get_severity_role).toDouble();
  const double mean_severity =
    severity / static_cast<double>(mp_perf_data->get_data_size());

  const double relative_severity =
    index.data(cnode_tree_model::get_relative_severity_role).toDouble();

  /// \todo make this user-defineable
  const double scaled_relative_severity =
    std::max(0.0, std::min(1.0, (relative_severity - 0.001) / 0.009));

  const double suitable_3d_view_threshold =
    index.data(cnode_tree_model::suitable_view_threshold_role).toDouble();
  
  const double center_value =
    suitable_3d_view_threshold * suitable_3d_view_threshold *
    mean_severity * mean_severity;

  p_painter->save();
  
  // Clear painting area.
  p_painter->setPen  (Qt::NoPen);
  p_painter->setBrush(Qt::NoBrush);
  p_painter->drawRect(option.rect.x(),
                      option.rect.y(),
                      num_dims * option.rect.height(),
                      num_dims * option.rect.height());
  
  p_painter->setOpacity(m_fade_out_enabled ? scaled_relative_severity : 1.0);
  
  // Draw baseline.
  p_painter->setPen(baseline_color);
  for (std::size_t i = 0; i < num_dims; ++i)
    p_painter->drawLine(left + i * height + 1         , baseline,
                        left + i * height + height - 2, baseline);

  // Draw bars.
  p_painter->setBrush(bar_color);

  double max_dir_var = 1.0;
  for (std::size_t i = 0; i < num_dims; ++i)
    max_dir_var = std::max(max_dir_var, directed_variances[i].toDouble());
  
  for (std::size_t i = 0; i < num_dims; ++i)
  {
    const double curr_dir_var = directed_variances[i].toDouble();

    const double norm_curr_dir_var =
      std::min(curr_dir_var / (2.0 * center_value), 1.0);

    const int max_height = height - 2;
    const int bar_height = norm_curr_dir_var * max_height;
    
    p_painter->drawRect(left + i * height + 1,
                        top  + 1 + max_height - bar_height,
                        height - 2,
                        bar_height);

    p_painter->drawLine(left + i * height,
                        top  + 1 + max_height / 2,
                        left + i * height + 1,
                        top  + 1 + max_height / 2);
    
    p_painter->drawLine(left + i * height + height - 2,
                        top  + 1 + max_height / 2,
                        left + i * height + height - 1,
                        top  + 1 + max_height / 2);
  }
  
  p_painter->restore();
}


//------------------------------------------------------------------------------
QSize
directed_variance_glyph_delegate::sizeHint
(const QStyleOptionViewItem& option,
 const QModelIndex&          index)
const
{
  return QSize(4,4);
}

} // namespace qpvt
