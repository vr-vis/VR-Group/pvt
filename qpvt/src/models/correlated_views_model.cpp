//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/cnode.hpp>
#include <pvt/metric.hpp>
#include <pvt/perf_data.hpp>

#include <qpvt/models/correlated_views_model.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
// \todo Find a way to handle multiple selected Metrics, Cnodes
//------------------------------------------------------------------------------
correlated_views_model::correlated_views_model
(QObject* p_parent)
: QAbstractTableModel(p_parent)
{

}


//------------------------------------------------------------------------------
QVariant
correlated_views_model::data
(const QModelIndex &index,
 int role)
const
{
  if (role == Qt::DisplayRole)
  {
    const pvt::correlated_severity_view& correlated_view =
      m_correlated_views.at(index.row());
    switch (index.column())
    {
      case 0:
        return QString::number(
          correlated_view.get_correlation_coefficient(), 'f', 2);
      case 1:
        return compute_severity_string(correlated_view);
      case 2:
        return (correlated_view.get_severity_view().get_metric()
          ->get_name() + std::string(" (") + std::string(
          correlated_view.get_severity_view().is_metric_total() 
          ? "Total" : "Self") + std::string(")")).c_str();
      case 3:
        return (correlated_view.get_severity_view().get_cnode()
          ->get_name() + std::string(" (") + std::string(
          correlated_view.get_severity_view().is_cnode_total() 
          ? "Total" : "Self") + std::string(")")).c_str();
      default:
        break;
    }
  }
  return QVariant();
}


//------------------------------------------------------------------------------
QVariant
correlated_views_model::headerData
(int             section,
 Qt::Orientation orientation,
 int             role)
const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) 
    {
      switch (section)
      {
        case 0:
          return QString("Corr.");
        case 1:
          return QString("Severity");
        case 2:
          return QString("Metric");
        case 3:
          return QString("Code Region");
        default:
          break;
      }
    }
  }
  return QVariant();
}


//------------------------------------------------------------------------------
QString
correlated_views_model::compute_severity_string
(const pvt::correlated_severity_view& correlated_view)
const
{
  const pvt::severity_view& severity_view(correlated_view.get_severity_view());
  const double severity =
    severity_view.get_performance_data()->get_severity(severity_view);
  return QString::number(severity, 'g', 3) + " " +
    severity_view.get_metric()->get_unit_of_measurement().c_str();
}

} // namespace qpvt
