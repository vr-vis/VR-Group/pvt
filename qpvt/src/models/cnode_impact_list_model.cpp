//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cmath>

#include <pvt/cartesian_topology.hpp>
#include <pvt/cartesian_topology_index.hpp>
#include <pvt/cnode.hpp>
#include <pvt/directed_variance.hpp>
#include <pvt/fft_statistics.hpp>
#include <pvt/perf_data.hpp>
#include <pvt/severity_view.hpp>
#include <pvt/suitable_3d_view_computer.hpp>

#include <qpvt/models/cnode_impact_list_model.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
cnode_impact_list_model::cnode_impact_list_model
(pvt::suitable_3d_view_computer* p_suitable_3d_view_computer,
 QObject*                        p_parent)
: QAbstractTableModel         (p_parent)
, mp_suitable_3d_view_computer(p_suitable_3d_view_computer)
{
  
}


//------------------------------------------------------------------------------
void
cnode_impact_list_model::setup
(pvt::perf_data*     p_perf_data,
 pvt::severity_view* p_severity_view)
{
  mp_perf_data     = p_perf_data;
  mp_severity_view = p_severity_view;
  
  assert(mp_perf_data->get_cartesian_topologies().size() != 0);

  const auto& cart_topo = mp_perf_data->get_cartesian_topologies()[0];
  const auto& num_dims  = cart_topo.get_num_dimensions();
  
  m_exclude_dimensions_from_std_dev = std::vector<bool>(num_dims, false);
  
  beginResetModel ();
  beginInsertRows (QModelIndex(), 0, p_perf_data->get_cnodes().size());
  endInsertRows   ();
  endResetModel   ();

  emit dataChanged(QModelIndex(), QModelIndex());
}


//------------------------------------------------------------------------------
void
cnode_impact_list_model::set_exclude_dimension_from_std_dev
(std::size_t dimension_id, 
 bool        exclude)
{
  m_exclude_dimensions_from_std_dev[dimension_id] = exclude;
}


//------------------------------------------------------------------------------
void
cnode_impact_list_model::clear_exclude_dimensions_from_std_dev
()
{
  for (std::size_t i = 0; i < m_exclude_dimensions_from_std_dev.size(); ++i)
    m_exclude_dimensions_from_std_dev[i] = false;
}

//------------------------------------------------------------------------------
int
cnode_impact_list_model::columnCount
(const QModelIndex& parent)
const
{
  assert(mp_perf_data->get_cartesian_topologies().size() != 0);
  return 6 + mp_perf_data->get_cartesian_topologies()[0].get_num_dimensions();
}


//------------------------------------------------------------------------------
int
cnode_impact_list_model::rowCount
(const QModelIndex& parent)
const
{
  return mp_perf_data->get_cnodes().size();
}


//------------------------------------------------------------------------------
QVariant
cnode_impact_list_model::headerData
(int             section,
 Qt::Orientation orientation,
 int             role)
const
{
  assert(mp_perf_data->get_cartesian_topologies().size() != 0);

  const auto& cart_topo_idx = mp_perf_data->get_cartesian_topologies()[0];
  
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal) 
    {
      if (section == 0)
        return QString("Severity Self");
      else if (section == 1)
        return QString("Mean");
      else if (section == 2)
        return QString("Std. Dev.");
      else if (section == 3)
        return QString("~");
      else if (section > 3 && section < columnCount() - 2)
        return QString(cart_topo_idx.get_dimension_names()[section - 4].c_str());
      else if (section == columnCount() - 2)
        return QString("Function");
      else if (section == columnCount() - 1)
        return QString("Call Path");
      else
        return QVariant();
    }
  }
  return QVariant();
}


//------------------------------------------------------------------------------
QVariant
cnode_impact_list_model::data
(const QModelIndex& index,
 int                role)
const
{
  if (!index.isValid())
    return QVariant();
  
  pvt::cnode& p_cnode = 
    const_cast<pvt::cnode&>(mp_perf_data->get_cnodes()[index.row()]);

  assert(mp_severity_view->get_metric() != nullptr);
  
  const pvt::severity_view internal_severity_view(
    mp_perf_data,
    mp_severity_view->get_metric(),
    mp_severity_view->is_metric_total(),
    &p_cnode,
    pvt::severity_view::self);
  
  switch (role)
  {
    case Qt::DisplayRole:
      return data_display(index, internal_severity_view, &p_cnode);

    case Qt::TextAlignmentRole:
      if (index.column() == 3)
        return Qt::AlignHCenter;
      else if (index.column() < columnCount() - 2)
        return Qt::AlignRight;
      else
        return Qt::AlignLeft;
      
    case cnode_impact_list_model::search_perf_data_item_role:
      return QVariant::fromValue(p_cnode.get_id());
      
    case cnode_impact_list_model::sort_role:
      return data_sort(index, internal_severity_view, &p_cnode);

    case cnode_impact_list_model::get_perf_data_item_pointer_role:
      return QVariant::fromValue(reinterpret_cast<void*>(&p_cnode));
  }
  return QVariant();
}


//------------------------------------------------------------------------------
Qt::ItemFlags
cnode_impact_list_model::flags
(const QModelIndex& index)
const
{
  return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}


//------------------------------------------------------------------------------
QString
cnode_impact_list_model::construct_call_path
(const pvt::cnode* p_cnode)
const
{
  QString call_path = "";
  const pvt::cnode* p_parent = p_cnode;
  while (p_parent != nullptr)
  {
    call_path = QString(p_parent->get_name().c_str()) + call_path;
    call_path = QString("/") + call_path;
    p_parent  = p_parent->parent();
  }
  return call_path;
}


//------------------------------------------------------------------------------
QVariant
cnode_impact_list_model::format_number
(double value)
const
{
  return QString::number(value, 'g', 3);
}


//------------------------------------------------------------------------------
QVariant
cnode_impact_list_model::data_display
(const QModelIndex&        index,
 const pvt::severity_view& severity_view,
 const pvt::cnode*         p_cnode)
const
{
  if (index.column() == 0) // Sev. Self
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
      return format_number(mp_perf_data->get_severity(severity_view));
    else
      return QVariant();
  }
  else if (index.column() == 1)  // Mean
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
      return format_number(mp_perf_data->get_mean(severity_view));
    else
      return QVariant();
  }
  else if (index.column() == 2)  // Std. Dev.
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
      return format_number(compute_std_dev(severity_view));
    else
      return QVariant();
  }
  else if (index.column() == 3)  // Var. Indicator
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
    {
      assert(mp_perf_data->get_cartesian_topologies().size() != 0);

      const auto& cart_topo_idx = mp_perf_data->get_cartesian_topologies()[0];
      const auto& num_dims      = cart_topo_idx.get_num_dimensions      ();

      pvt::directed_variance dir_var(num_dims);
      mp_perf_data->get_directed_variance(severity_view, dir_var);

      pvt::suitable_3d_view_computer suitable_3d_view_computer(num_dims);
      suitable_3d_view_computer.set_filter_dimension_enabled(
        m_exclude_dimensions_from_std_dev);
      suitable_3d_view_computer.set_threshold(
        mp_suitable_3d_view_computer->get_threshold());

      const bool suitable = suitable_3d_view_computer.is_suitable_3d_view(
        dir_var,
        mp_perf_data->get_mean(severity_view));
      return     suitable ? QVariant("~") : QVariant();
    }
    else
      return QVariant();
  }
  else if (index.column() > 3 &&
           index.column() < columnCount() - 2)  // Dir. Var.
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
    {
        assert(mp_perf_data->get_cartesian_topologies().size() != 0);

        const auto& cart_topo = mp_perf_data->get_cartesian_topologies()[0];
        const auto& num_dims  = cart_topo.get_num_dimensions();

        const std::size_t dim_id = index.column() - 4;

        pvt::directed_variance dir_var(num_dims);
        mp_perf_data->get_directed_variance(severity_view, dir_var);

        return format_number(sqrt(dir_var[dim_id]));
    }
    else
      return QVariant();
  }
  else if (index.column() == columnCount() - 2)  // Function
    return p_cnode->get_name().c_str();
  else if (index.column() == columnCount() - 1)  // Call path
    return construct_call_path(p_cnode);
  else
    return QVariant();
}


//------------------------------------------------------------------------------
QVariant
cnode_impact_list_model::data_sort
(const QModelIndex&        index,
 const pvt::severity_view& severity_view,
 const pvt::cnode*         p_cnode)
const
{
  if (index.column() == 0) // Sev. Self
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
      return mp_perf_data->get_severity(severity_view);
    else
      return QVariant();
  }
  else if (index.column() == 1)  // Mean
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
      return mp_perf_data->get_mean(severity_view);
    else
      return QVariant();
  }
  else if (index.column() == 2)  // Std. Dev.
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
      return compute_std_dev(severity_view);
    else
      return QVariant();
  }
  else if (index.column() == 3)  // Var. Indicator
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
    {
      assert(mp_perf_data->get_cartesian_topologies().size() != 0);

      const auto& cart_topo = mp_perf_data->get_cartesian_topologies()[0];
      const auto& num_dims  = cart_topo.get_num_dimensions();

      pvt::directed_variance dir_var(num_dims);
      mp_perf_data->get_directed_variance(severity_view, dir_var);

      pvt::suitable_3d_view_computer suitable_3d_view_computer(num_dims);
      suitable_3d_view_computer.set_filter_dimension_enabled(
        m_exclude_dimensions_from_std_dev);
      
      const bool suitable = suitable_3d_view_computer.is_suitable_3d_view(
        dir_var,
        mp_perf_data->get_mean(severity_view));
      return     suitable ? QVariant("~") : QVariant();
    }
    else
      return QVariant();
  }
  else if (index.column() > 3 &&
           index.column() < columnCount() - 2)  // Dir. Var.
  {
    if (severity_view.get_metric() != nullptr &&
        p_cnode                    != nullptr)
    {
      assert(mp_perf_data->get_cartesian_topologies().size() != 0);

      const auto& cart_topo = mp_perf_data->get_cartesian_topologies()[0];
      const auto& num_dims  = cart_topo.get_num_dimensions();

      const std::size_t dim_id = index.column() - 4;

      pvt::directed_variance dir_var(num_dims);
      mp_perf_data->get_directed_variance(severity_view, dir_var);

      return sqrt(dir_var[dim_id]);
    }
    else
      return QVariant();
  }
  else if (index.column() == columnCount() - 2)  // Function
    return p_cnode->get_name().c_str();
  else if (index.column() == columnCount() - 1)  // Call Path
    return construct_call_path(p_cnode);
  else
    return QVariant();
}


//------------------------------------------------------------------------------
double
cnode_impact_list_model::compute_std_dev
(const pvt::severity_view& p_severity_view)
const
{
  const std::size_t num_dims = m_exclude_dimensions_from_std_dev.size();
  
  pvt::directed_variance dir_var(num_dims);
  mp_perf_data->get_directed_variance(p_severity_view, dir_var);
  
  const double filtered_variance =
    std::inner_product(dir_var.begin(),
                       dir_var.end  (),
                       m_exclude_dimensions_from_std_dev.begin(),
                       0.0,
                       std::plus<double>(),
  [](double dv, bool f) 
  {
    return dv * static_cast<double>(!f);
  });
  
  return sqrt(filtered_variance);
}

} // namespace qpvt
