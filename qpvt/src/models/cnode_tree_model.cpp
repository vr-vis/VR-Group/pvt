//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/metric.hpp>

#include <qpvt/models/cnode_tree_model.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
cnode_tree_model::cnode_tree_model
(const pvt::severity_view* p_severity_view,
 QObject*                  p_parent)
: perf_data_tree_model(p_severity_view, p_parent)
, m_perf_data_cache   (p_severity_view)
{
  m_column_infos.push_back(column_info("Code Region", Qt::AlignLeft ));
  m_column_infos.push_back(column_info(""           , Qt::AlignLeft ));
  m_column_infos.push_back(column_info("~"          , Qt::AlignLeft ));
  m_column_infos.push_back(column_info(""           , Qt::AlignLeft ));
  m_column_infos.push_back(column_info("Severity"   , Qt::AlignRight));
  m_column_infos.push_back(column_info("Self"       , Qt::AlignRight));
  m_column_infos.push_back(column_info(""           , Qt::AlignLeft ));
}


//------------------------------------------------------------------------------
QVariant
cnode_tree_model::data
(const QModelIndex& index,
 int                role)
const
{
  if (!index.isValid())
    return QVariant();
  
  pvt::cnode* const p_cnode = static_cast<pvt::cnode*>(index.internalPointer());
  
  switch (role)
  {
    case Qt::DisplayRole:
      switch (index.column())
      {
        case 0:
          return p_cnode->get_name().c_str();
        case 2:
          return QString(m_perf_data_cache.is_suitable_3d_view             
            (p_cnode) ? "~ " : "") + 
                 QString(m_perf_data_cache.is_suitable_3d_view_in_children 
            (p_cnode) ? "*"  : "");
        case 4:
          return get_severity_string(
            m_perf_data_cache.get_severity_total(p_cnode));
        case 5:
          return get_severity_string(
            m_perf_data_cache.get_severity_self(p_cnode));
        case 6:
          return
            mp_severity_view->get_metric() != nullptr
              ? QString(mp_severity_view->get_metric()->
                        get_unit_of_measurement().c_str()) 
              : QString();
        default:
          return QVariant();
      }
      
    case Qt::TextAlignmentRole:
      return m_column_infos[index.column()].m_alignment;
    
    case cnode_tree_model::search_perf_data_item_role:
      return QVariant::fromValue(p_cnode->get_id());
    
    case cnode_tree_model::sort_role:
      switch (index.column())
      {
        case 4:
          return m_perf_data_cache.get_severity_total(p_cnode);
        default:
          return QVariant();
      }
      
    case cnode_tree_model::get_perf_data_item_pointer_role:
      return QVariant::fromValue(index.internalPointer());
     
    case cnode_tree_model::get_severity_role:
      return QVariant::fromValue(
        index.data(cnode_tree_model::using_total_severities_role).toBool() ?
        m_perf_data_cache.get_severity_total(p_cnode) :
        m_perf_data_cache.get_severity_self (p_cnode));
      
    case cnode_tree_model::get_severity_total_role:
      return QVariant::fromValue(m_perf_data_cache.get_severity_total(p_cnode));
     
    case cnode_tree_model::get_severity_self_role:
      return QVariant::fromValue(m_perf_data_cache.get_severity_self (p_cnode));
    
    case cnode_tree_model::get_relative_severity_role:
      return QVariant::fromValue(
        index.data(cnode_tree_model::using_total_severities_role).toBool() ?
          m_perf_data_cache.get_severity_total_relative(p_cnode) :
          m_perf_data_cache.get_severity_self_relative (p_cnode));
    
    case cnode_tree_model::using_total_severities_role:
      return QVariant(mp_severity_view->is_cnode_total());
   
    case cnode_tree_model::directed_variance_role:
      return get_directed_variance_as_qvariant(p_cnode);

    case cnode_tree_model::suitable_view_threshold_role:
      return m_perf_data_cache.get_suitable_3d_view_computer().get_threshold();
  }
  return QVariant();
};


//------------------------------------------------------------------------------
QVariant
cnode_tree_model::get_directed_variance_as_qvariant
(const pvt::cnode* p_cnode)
const
{
  QList<QVariant> directed_variances;
  std::for_each(m_perf_data_cache.get_directed_variance(p_cnode).begin(),
                m_perf_data_cache.get_directed_variance(p_cnode).end  (),
  [&directed_variances](double variance)
  {
    directed_variances.append(QVariant::fromValue(variance));
  });
  return directed_variances;
}

} // namespace qpvt
