//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/metric.hpp>

#include <qpvt/models/metric_tree_model.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
metric_tree_model::metric_tree_model
(const pvt::severity_view* p_severity_view,
 QObject*                  p_parent)
: perf_data_tree_model(p_severity_view, p_parent)
, m_perf_data_cache   (p_severity_view)
{
  m_column_infos.push_back(column_info("Performance Metric", Qt::AlignLeft ));
  m_column_infos.push_back(column_info("Severity"          , Qt::AlignRight));
  m_column_infos.push_back(column_info("Self"              , Qt::AlignRight));
  m_column_infos.push_back(column_info(""                  , Qt::AlignLeft ));
}


//------------------------------------------------------------------------------
QVariant
metric_tree_model::data
(const QModelIndex& index,
 int                role)
const
{
  if (!index.isValid())
    return QVariant();
  
  pvt::metric* const p_metric = 
    static_cast<pvt::metric*>(index.internalPointer());

  switch (role)
  {
    case Qt::DisplayRole:
      switch (index.column())
      {
        case 0:
          return p_metric->get_name().c_str();
        case 1:
          return get_severity_string(
            m_perf_data_cache.get_severity_total(p_metric));
        case 2:
          return get_severity_string(
            m_perf_data_cache.get_severity_self(p_metric));
        case 3:
          return QString(p_metric->get_unit_of_measurement().c_str());
        default:
          return QVariant();
      }
      break;
    
    case Qt::TextAlignmentRole:
      return m_column_infos[index.column()].m_alignment;
      
    case metric_tree_model::search_perf_data_item_role:
      return QVariant::fromValue(p_metric->get_id());
      
    case metric_tree_model::get_perf_data_item_pointer_role:
      return QVariant::fromValue(index.internalPointer());
      
    case metric_tree_model::get_severity_total_role:
      return QVariant::fromValue(
        m_perf_data_cache.get_severity_total(p_metric));
      
    case metric_tree_model::get_severity_self_role:
      return QVariant::fromValue(
        m_perf_data_cache.get_severity_self(p_metric));
      
    case metric_tree_model::get_relative_severity_role:
      return QVariant::fromValue(
        index.data(metric_tree_model::using_total_severities_role).toBool() ?
          m_perf_data_cache.get_severity_total_relative(p_metric) :
          m_perf_data_cache.get_severity_self_relative (p_metric));
      
    case metric_tree_model::using_total_severities_role:
      return QVariant(mp_severity_view->is_metric_total());
      
    case metric_tree_model::directed_variance_role:
      return QVariant();
  }
  return QVariant();
};

} // namespace qpvt
