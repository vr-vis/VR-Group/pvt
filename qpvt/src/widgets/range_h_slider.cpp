//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <qpvt/widgets/range_h_slider.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
range_h_slider::range_h_slider
(QWidget*        p_parent,
 Qt::WindowFlags flags)
: range_slider(p_parent, flags)
{
  
}


//------------------------------------------------------------------------------
void
range_h_slider::handle_range_change
(const QPointF& pos)
{
  if (m_active_handle == min_handle)
    set_min(pos.x() / static_cast<float>(width()));
  else if (m_active_handle == max_handle)
    set_max(pos.x() / static_cast<float>(width()));
  else if (m_active_handle == mid_handle)
  {
    const float delta_x = (pos.x() - m_initial_local_position.x()) /
                          static_cast<float>(width());

    float new_min = m_initial_min + delta_x;
    float new_max = m_initial_max + delta_x;

    if (new_max > 1.0f)
    {
      new_max = 1.0f;
      new_min = 1.0 - (get_max() - get_min());
    }
    else if (new_min < 0.0f)
    {
      new_min = 0.0f;
      new_max = (get_max() - get_min());
    }

    set_min(new_min);
    set_max(new_max);
  }
  update();
  emit(range_changed(m_min, m_max));
}


//------------------------------------------------------------------------------
void
range_h_slider::handle_cursor_change
(const QPointF& pos)
{
  const Qt::CursorShape current_shape = cursor().shape();
        Qt::CursorShape new_shape     = cursor().shape();

  switch (inside_handle(pos))
  {
    case min_handle:
      new_shape = Qt::SizeHorCursor;
      break;
    case max_handle:
      new_shape = Qt::SizeHorCursor;
      break;
    case mid_handle:
      if (get_max() - get_min() < 0.995)
      {
        if (m_active_handle == mid_handle)
          new_shape = Qt::ClosedHandCursor;
        else
          new_shape = Qt::OpenHandCursor;
      }
      break;
    default:
      if (cursor().shape() != parentWidget()->cursor().shape())
        unsetCursor();
      break;
  }
  
  if (current_shape != new_shape)
    setCursor(new_shape);
}


//------------------------------------------------------------------------------
range_slider::slider_handle
range_h_slider::inside_handle
(const QPointF& pos)
const
{
  const float half_width = 2.0f;
  if      (pos.x() > m_min * width() - half_width &&
           pos.x() < m_min * width() + half_width)
    return min_handle;
  else if (pos.x() > m_max * width() - half_width &&
           pos.x() < m_max * width() + half_width)
    return max_handle;
  else if (pos.x() > m_min * width() + half_width &&
           pos.x() < m_max * width() - half_width)
    return mid_handle;
  return no_handle;
}

} // namespace qpvt
