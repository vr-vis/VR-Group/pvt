//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <QSlider>
#include <QVBoxLayout>

#include <pvt/cartesian_topology.hpp>
#include <pvt/rendering/renderable_line_plot.hpp>
#include <pvt/rendering/renderable_line_plot_axes.hpp>
#include <pvt/rendering/scene.hpp>

#include <qpvt/widgets/slice_slider.hpp>
#include <qpvt/widgets/viewer_widget.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
slice_slider::slice_slider
(pvt::cartesian_topology* p_cartesian_topology,
 pvt::data_array<float>*  p_data_in_topology,
 bool                     with_line_plot,
 QWidget*                 p_parent,
 Qt::WindowFlags          flags)
: QWidget(p_parent, flags)
, m_dimension_id        (0)
, m_with_line_plot      (with_line_plot)
, mp_cartesian_topology (p_cartesian_topology)
, mp_renderable_scene   (new pvt::scene)
, mp_line_plot_axes     (with_line_plot ? new pvt::renderable_line_plot_axes : nullptr)
, mp_line_plot          (with_line_plot ? new pvt::renderable_line_plot(p_cartesian_topology, p_data_in_topology) : nullptr)
, mp_viewer_widget      (new viewer_widget(nullptr))
, mp_layout             (new QVBoxLayout)
, mp_viewer_layout      (new QVBoxLayout)
, mp_slider             (new QSlider(Qt::Horizontal))
{
  QSizePolicy size_policy = sizePolicy();
  size_policy.setControlType(QSizePolicy::Slider);
  setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
  
  mp_layout->setMargin (0);
  mp_layout->setSpacing(0);
  setLayout(mp_layout);
  
  if (m_with_line_plot)
  {
    mp_renderable_scene->add_renderable(mp_line_plot);
    mp_renderable_scene->add_renderable(mp_line_plot_axes);

    mp_viewer_layout->setContentsMargins(8, 0, 8, 0);
    mp_viewer_layout->setSpacing        (0);
    
    mp_viewer_widget->setSizePolicy   (QSizePolicy::Preferred, 
                                       QSizePolicy::Maximum);
    mp_viewer_widget->setMaximumHeight(32);
    mp_viewer_widget->setMinimumHeight(32);
    mp_viewer_widget->set_clear_color (0.93f, 0.93f, 0.93f, 0.93f);
    mp_viewer_widget->set_scene       (mp_renderable_scene);

    mp_viewer_layout->addWidget(mp_viewer_widget);
    mp_layout       ->addLayout(mp_viewer_layout);
  }
  mp_layout->addWidget(mp_slider);
  
  QObject::connect(mp_slider, &QAbstractSlider::valueChanged, [this] (int value)
  {
    emit(valueChanged(value));
  });
}


//------------------------------------------------------------------------------
void
slice_slider::set_line_plot_min
(float min)
{
  if (m_with_line_plot)
    mp_line_plot->set_minimum(min);
}


//------------------------------------------------------------------------------
void
slice_slider::set_line_plot_max
(float max)
{
  if (m_with_line_plot)
    mp_line_plot->set_maximum(max);
}


//------------------------------------------------------------------------------
void
slice_slider::set_dimension_id
(std::size_t dimension_id)
{
  m_dimension_id = dimension_id;
  if (mp_line_plot != nullptr)
  {
    mp_line_plot     ->set_dimension_id(dimension_id);
    mp_line_plot_axes->set_num_axes    (mp_cartesian_topology->get_index().
      get_dimension_sizes()[dimension_id]);
  }
  mp_viewer_widget->update();
}


//------------------------------------------------------------------------------
void
slice_slider::scale_up
()
{
  mp_viewer_widget->setMaximumHeight(128);
  mp_viewer_widget->setMinimumHeight(128);
}


//------------------------------------------------------------------------------
void
slice_slider::scale_down
()
{
  mp_viewer_widget->setMaximumHeight(32);
  mp_viewer_widget->setMinimumHeight(32);
}


//------------------------------------------------------------------------------
void
slice_slider::toggle_overview
(bool enabled)
{
  mp_viewer_widget->setVisible(enabled);
}


//------------------------------------------------------------------------------
void
slice_slider::manual_update
()
{
  if (m_with_line_plot)
  {
    mp_line_plot    ->set_dirty();
    mp_viewer_widget->update   ();
  }
}

} // namespace qpvt
