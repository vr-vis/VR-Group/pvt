//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <iostream>

#include <pvt/gl/shader.hpp>
#include <pvt/rendering/shader_strings.hpp>

#include <qpvt/widgets/data_range_v_slider.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
data_range_v_slider::data_range_v_slider
(QWidget*        p_parent,
 Qt::WindowFlags flags)
: range_v_slider(p_parent, flags)
{

}


//------------------------------------------------------------------------------
void
data_range_v_slider::prepare_data
()
{
  m_shader_program->detach_all_shaders();

  const unsigned int num_data_values = m_data_values.size();
  if (num_data_values != 0)
  {
    const unsigned int buffer_size_per_data_value = 4 * sizeof(float);

    m_vertex_array ->bind();
    m_vertex_buffer->bind();

    m_vertex_buffer->allocate(num_data_values * buffer_size_per_data_value);

    m_shader_program->attach_shader(pvt::vertex_shader  (
      pvt::shader_strings::data_slider_vert));
    m_shader_program->attach_shader(pvt::geometry_shader(
      pvt::shader_strings::data_slider_geom));
    m_shader_program->attach_shader(pvt::fragment_shader(
      pvt::shader_strings::data_slider_frag));
    m_shader_program->link();
    m_shader_program->bind();
    m_shader_program->set_attribute_buffer  ("vertex", 4, GL_FLOAT);
    m_shader_program->enable_attribute_array("vertex");

    for (auto value_idx = 0u; value_idx < num_data_values; ++value_idx)
    {
      const float value = static_cast<float>(m_data_values[value_idx]);
      m_data_maximum = std::max(value, m_data_maximum);

      float buffer_data[] = {
        static_cast<float>(value_idx), value, 0.0f, 0.0f };

      m_vertex_buffer->set_sub_data(value_idx * buffer_size_per_data_value,
                                    buffer_size_per_data_value,
                                    buffer_data);
    }

    m_shader_program->unbind();
    m_vertex_buffer ->unbind();
    m_vertex_array  ->unbind();
  }
  m_data_prepared = true;
  update();
}


//------------------------------------------------------------------------------
void
data_range_v_slider::initializeGL
()
{
  range_slider::initializeGL();

  float vertices[] = { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f };

  m_vertex_array  .emplace();
  m_vertex_buffer .emplace();
  m_shader_program.emplace();

  m_vertex_array ->bind();
  m_vertex_buffer->bind();

  m_vertex_buffer->set_data(2 * 3 * sizeof(float), vertices);

  m_shader_program->attach_shader(pvt::vertex_shader(
    pvt::shader_strings::data_slider_vert));
  m_shader_program->attach_shader(pvt::geometry_shader(
    pvt::shader_strings::data_slider_geom));
  m_shader_program->attach_shader(pvt::fragment_shader(
    pvt::shader_strings::data_slider_frag));
  m_shader_program->link();
  m_shader_program->bind();
  m_shader_program->set_attribute_buffer  ("vertex", 4, GL_FLOAT);
  m_shader_program->enable_attribute_array("vertex");

  m_shader_program->unbind();
  m_vertex_buffer ->unbind();
  m_vertex_array  ->unbind();
}


//------------------------------------------------------------------------------
void
data_range_v_slider::paintGL
()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  if (!m_data_prepared)
    prepare_data();

  m_vertex_array  ->bind();
  m_shader_program->bind();

  m_shader_program->set_uniform("isVertical" , is_vertical());
  m_shader_program->set_uniform("maxValueInv", 1.0f / m_max_value); // TODO: Initialize m_max_value.
  m_shader_program->set_uniform("maxIndexInv", 1.0f / m_data_values.size());
  m_shader_program->set_uniform("color"      , Eigen::Vector4f(
    m_color.red  () / 255.0f,
    m_color.green() / 255.0f,
    m_color.blue () / 255.0f,
    0.5f
  ));

  glDrawArrays(GL_POINTS, 0, m_data_values.size());

  m_shader_program->unbind();
  m_vertex_array  ->unbind();

  draw_covers_handles();
}

} // namespace qpvt
