//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <QApplication>
#include <QMouseEvent>

#include <pvt/rendering/camera.hpp>
#include <pvt/rendering/scene.hpp>

#include <qpvt/widgets/viewer_widget.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
viewer_widget::viewer_widget
(pvt::camera*    p_camera,
 QWidget*        p_parent,
 Qt::WindowFlags flags)
: QOpenGLWidget (p_parent, flags)
, mp_scene      (nullptr)
, mp_camera     (p_camera)
, m_camera_tools(p_camera)
{
  set_clear_color(0.0F, 0.0F, 0.0F, 0.0F);
  QObject::connect(this, SIGNAL(view_updated()), this, SLOT(update()));
}


//------------------------------------------------------------------------------
void
viewer_widget::initializeGL
()
{
  glClearColor(m_clear_color[0],
               m_clear_color[1],
               m_clear_color[2],
               m_clear_color[3]);
  glEnable(GL_DEPTH_TEST);
  
  if (mp_scene != nullptr)
    mp_scene->setup();
}


//------------------------------------------------------------------------------
void
viewer_widget::resizeGL
(int width, int height)
{
  const float pixel_ratio = devicePixelRatio();
  const int   side        = std::min(width * pixel_ratio, height * pixel_ratio);
  glViewport((width  * pixel_ratio - side) / 2,
             (height * pixel_ratio - side) / 2,
             side,
             side);
  if (mp_camera != nullptr)
    mp_camera->set_aspect(static_cast<float>(width) / 
                          static_cast<float>(height));
}


//------------------------------------------------------------------------------
void
viewer_widget::paintGL
()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  if (mp_scene != nullptr)
  {
    if (mp_camera != nullptr)
      mp_camera->set_aspect(static_cast<float>(width ()) / 
                            static_cast<float>(height()));
    mp_scene->render(mp_camera);
  }
}


//------------------------------------------------------------------------------
void
viewer_widget::mouseMoveEvent
(QMouseEvent* p_event)
{
  if (m_camera_tools.handle_mouse_move(p_event, 
                                       QApplication::keyboardModifiers()))
  {
    emit view_updated();
  }
}


//------------------------------------------------------------------------------
void
viewer_widget::mousePressEvent
(QMouseEvent* p_event)
{
  if (m_camera_tools.handle_mouse_press(p_event, 
                                        QApplication::keyboardModifiers()))
  {
    emit view_updated();
  }

  auto local_pos = p_event->localPos();
  emit clicked(local_pos.x(), height() - local_pos.y(), QApplication::keyboardModifiers());
}


//------------------------------------------------------------------------------
void
viewer_widget::mouseReleaseEvent
(QMouseEvent* p_event)
{
  if (m_camera_tools.handle_mouse_release(p_event, 
                                          QApplication::keyboardModifiers()))
  {
    emit view_updated();
  }
}

} // namespace qpvt
