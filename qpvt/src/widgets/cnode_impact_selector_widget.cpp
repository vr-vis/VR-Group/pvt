//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cassert>

#include <QButtonGroup>
#include <QDialogButtonBox>
#include <QCheckBox>
#include <QFormLayout>
#include <QGroupBox>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QSortFilterProxyModel>
#include <QTableView>
#include <QVBoxLayout>

#include <pvt/perf_data.hpp>
#include <pvt/cartesian_topology.hpp>
#include <pvt/suitable_3d_view_computer.hpp>

#include <qpvt/models/cnode_impact_list_model.hpp>
#include <qpvt/models/cnode_tree_model.hpp>
#include <qpvt/widgets/cnode_impact_selector_widget.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
cnode_impact_selector_widget::cnode_impact_selector_widget
(pvt::perf_data*     p_perf_data,
 pvt::severity_view* p_severity_view,
 cnode_tree_model*   p_cnode_tree_model,
 QWidget*            p_parent,
 Qt::WindowFlags     flags)
: QWidget            (p_parent, flags)
, mp_perf_data       (p_perf_data)
, mp_severity_view   (p_severity_view)
, mp_cnode_tree_model(p_cnode_tree_model)
{
  setup_ui();
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::invalidate_model
()
{
  mp_cnode_impact_list_proxy_model->invalidate();
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::select_and_focus_on_cnode
(const pvt::cnode* p_cnode)
{
  const bool block_signals =
    mp_cnode_impact_list_table->selectionModel()->blockSignals(true);
  
  // find, select cnode in CnodesView
  const QModelIndexList found_cnode_indices_for_selection(
    mp_cnode_impact_list_proxy_model->match(
      mp_cnode_impact_list_proxy_model->index(0,0),
       cnode_impact_list_model::search_perf_data_item_role,
       QVariant::fromValue(p_cnode->get_id()),
       1,
       Qt::MatchExactly));
  
  const QModelIndex cnode_index_for_selection(
    found_cnode_indices_for_selection.size() > 0 ?
    found_cnode_indices_for_selection[0] :
    QModelIndex());
  
  mp_cnode_impact_list_table->clearSelection();
  mp_cnode_impact_list_table->selectionModel()->select(
    cnode_index_for_selection,
    QItemSelectionModel::Select |
    QItemSelectionModel::Rows);
  
  mp_cnode_impact_list_table->scrollTo(cnode_index_for_selection,
                                       QAbstractItemView::PositionAtCenter);
  
  mp_cnode_impact_list_table->selectionModel()->blockSignals(block_signals);
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::handle_cnode_clicked
(const QModelIndex& index)
{
  const QVariant cnode_variant(
    index.data(cnode_impact_list_model::get_perf_data_item_pointer_role));

  pvt::cnode* const p_cnode = 
    static_cast<pvt::cnode*>(cnode_variant.value<void*>());
  
  emit cnode_selected(p_cnode, false);
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::data_changed_std_dev
()
{
  mp_cnode_impact_list_model->clear_exclude_dimensions_from_std_dev();

  for (auto p_button : mp_filter_std_dev_checkboxes->buttons())
  {
    const int  id      = mp_filter_std_dev_checkboxes->id(p_button);
    const bool checked = p_button->isChecked();
    mp_cnode_impact_list_model->set_exclude_dimension_from_std_dev(id, checked);
  }
  
  const int num_rows = mp_cnode_impact_list_proxy_model->rowCount();
  mp_cnode_impact_list_proxy_model->dataChanged(
    mp_cnode_impact_list_proxy_model->index(0,2),
    mp_cnode_impact_list_proxy_model->index(num_rows,3));
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::data_changed_suggestion
()
{
  mp_cnode_impact_list_model->clear_exclude_dimensions_from_std_dev();

  for (auto p_button : mp_filter_std_dev_checkboxes->buttons())
  {
    const int  id      = mp_filter_std_dev_checkboxes->id(p_button);
    const bool checked = p_button->isChecked();
    mp_cnode_impact_list_model->set_exclude_dimension_from_std_dev(id, checked);
  }
  
  const int num_rows = mp_cnode_impact_list_proxy_model->rowCount();
  mp_cnode_impact_list_proxy_model->dataChanged(
    mp_cnode_impact_list_proxy_model->index(0,3),
    mp_cnode_impact_list_proxy_model->index(num_rows,3));
  /// \todo Update filter
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::filter_changed
()
{
  mp_cnode_impact_list_proxy_model->setFilterKeyColumn(3);
  if (mp_filter_rows_checkboxes->button(0)->isChecked())
    mp_cnode_impact_list_proxy_model->setFilterFixedString("~");
  else
    mp_cnode_impact_list_proxy_model->setFilterFixedString("");
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::threshold_changed
(QAbstractButton* p_button)
{
  switch (mp_dialog_button_box->buttonRole(p_button))
  {
    case QDialogButtonBox::ApplyRole:
      mp_cnode_tree_model->get_suitable_3d_view_computer().
        set_threshold(mp_variation_threshold->text().toDouble());
      data_changed_suggestion();
      break;
      
    case QDialogButtonBox::ResetRole:
      mp_variation_threshold->setText(
        QString::number(mp_cnode_tree_model->get_suitable_3d_view_computer().
          get_threshold()));
      break;
      
    default:
      break;
  }
  /// \todo Update filter
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::setup_ui
()
{
  setWindowTitle("Cnode Explorer");

  setup_data_model                    ();
  setup_sort_proxy_model              ();
  setup_table_view                    ();
  setup_exclude_dimension_from_std_dev();
  setup_filter                        ();
  setup_threshold                     ();
  setup_layout                        ();
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::setup_data_model
()
{
  mp_cnode_impact_list_model = new cnode_impact_list_model(
    &mp_cnode_tree_model->get_suitable_3d_view_computer());

  mp_cnode_impact_list_model->setup(mp_perf_data, mp_severity_view);
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::setup_sort_proxy_model
()
{
  mp_cnode_impact_list_proxy_model = new QSortFilterProxyModel;
  mp_cnode_impact_list_proxy_model->setSourceModel(
    mp_cnode_impact_list_model);
  mp_cnode_impact_list_proxy_model->setSortRole(
    cnode_impact_list_model::sort_role);
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::setup_table_view
()
{
  mp_cnode_impact_list_table = new QTableView;
  mp_cnode_impact_list_table->setModel(mp_cnode_impact_list_proxy_model);
  mp_cnode_impact_list_table->setSelectionBehavior(
    QAbstractItemView::SelectRows);
  mp_cnode_impact_list_table->setAlternatingRowColors(true);
  mp_cnode_impact_list_table->setSortingEnabled      (true);

  auto p_palette = new QPalette(mp_cnode_impact_list_table->palette());
  p_palette->setBrush(QPalette::AlternateBase, QColor(245, 245, 245));

  mp_cnode_impact_list_table->setPalette(*p_palette);
  
  mp_cnode_impact_list_table->horizontalHeader()->setSectionResizeMode(
    QHeaderView::ResizeToContents);
  
  QObject::connect(mp_cnode_impact_list_table, 
                   &QTableView::clicked,
                   this, 
                   &cnode_impact_selector_widget::handle_cnode_clicked);
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::setup_exclude_dimension_from_std_dev
()
{
  mp_filter_std_dev_group_box =
    new QGroupBox("Exclude Dimension from Std. Dev.");
  
  mp_filter_std_dev_checkboxes = new QButtonGroup;
  mp_filter_std_dev_checkboxes->setExclusive(false);
  
  QHBoxLayout* p_group_layout = new QHBoxLayout;

  /// \todo make topology user-selectable
  assert(mp_perf_data->get_cartesian_topologies().size() != 0);

  const auto& cart_topo = mp_perf_data->get_cartesian_topologies()[0];
  for (std::size_t i = 0; i < cart_topo.get_num_dimensions(); ++i)
  {
    auto p_checkbox = new QCheckBox(cart_topo.get_dimension_names()[i].c_str());
    mp_filter_std_dev_checkboxes->addButton(p_checkbox, i);
    p_group_layout              ->addWidget(p_checkbox);
  }
  
  p_group_layout->addItem(new QSpacerItem(0,
                                          0,
                                          QSizePolicy::MinimumExpanding,
                                          QSizePolicy::Minimum));

  mp_filter_std_dev_group_box->setLayout(p_group_layout);
  
  QObject::connect(mp_filter_std_dev_checkboxes,
    static_cast<void(QButtonGroup::*)(QAbstractButton *, bool)>
      (&QButtonGroup::buttonToggled),
  [=](QAbstractButton *button, bool checked)
  {
    data_changed_std_dev();
  });
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::setup_filter
()
{
  mp_filter_rows_group_box =
    new QGroupBox("Show only rows with ...");
  
  mp_filter_rows_checkboxes = new QButtonGroup;
  mp_filter_rows_checkboxes->setExclusive(false);
  
  QHBoxLayout* p_group_layout = new QHBoxLayout;

  auto p_checkbox = new QCheckBox("variation above threshold");
  mp_filter_rows_checkboxes->addButton(p_checkbox, 0);
  p_group_layout           ->addWidget(p_checkbox);
  
  p_group_layout->addItem(new QSpacerItem(0,
                                          0,
                                          QSizePolicy::MinimumExpanding,
                                          QSizePolicy::Minimum));

  mp_filter_rows_group_box->setLayout(p_group_layout);
  
  QObject::connect(mp_filter_rows_checkboxes,
    static_cast<void(QButtonGroup::*)(QAbstractButton *, bool)>(
      &QButtonGroup::buttonToggled),
  [=](QAbstractButton *button, bool checked)
  {
    filter_changed();
  });
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::setup_threshold
()
{
  mp_threshold_group_box = new QGroupBox("Threshold");
  
  mp_dialog_button_box = new QDialogButtonBox;
  mp_dialog_button_box->addButton("Reset", QDialogButtonBox::ResetRole);
  mp_dialog_button_box->addButton("Apply", QDialogButtonBox::ApplyRole);
  
  mp_variation_threshold = new QLineEdit;
  mp_variation_threshold->setText(QString::number(
    mp_cnode_tree_model->get_suitable_3d_view_computer().get_threshold()));
  
  QFormLayout* p_form_layout = new QFormLayout;
  p_form_layout->setFormAlignment(Qt::AlignLeft);
  p_form_layout->addRow          ("variation-based suggestion",
                                  mp_variation_threshold);
  
  QVBoxLayout* p_v_box_layout = new QVBoxLayout;
  p_v_box_layout->addLayout(p_form_layout);
  p_v_box_layout->addWidget(mp_dialog_button_box);
  
  mp_threshold_group_box->setLayout(p_v_box_layout);
  
  QObject::connect(mp_dialog_button_box, &QDialogButtonBox::clicked,
  [=](QAbstractButton* p_button)
  {
    threshold_changed(p_button);
  });
}


//------------------------------------------------------------------------------
void
cnode_impact_selector_widget::setup_layout
()
{
  mp_layout = new QVBoxLayout;
  setLayout(mp_layout);
  mp_layout->addWidget(new QLabel("Overview:"));
  mp_layout->addWidget(mp_cnode_impact_list_table );
  mp_layout->addWidget(mp_filter_std_dev_group_box);
  mp_layout->addWidget(mp_threshold_group_box     );
  mp_layout->addWidget(mp_filter_rows_group_box   );
}

} // namespace qpvt

