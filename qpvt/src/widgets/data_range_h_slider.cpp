//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <iostream>

#include <pvt/gl/shader.hpp>
#include <pvt/rendering/shader_strings.hpp>

#include <qpvt/widgets/data_range_h_slider.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
data_range_h_slider::data_range_h_slider
(QWidget*        p_parent,
 Qt::WindowFlags flags)
: range_h_slider(p_parent, flags)
{

}


//------------------------------------------------------------------------------
void
data_range_h_slider::prepare_data
()
{
  m_shader_program_h->detach_all_shaders();
  m_shader_program_v->detach_all_shaders();

  const unsigned int num_data_values = m_mean_data_values.size();
  if (num_data_values != 0)
  {
    const unsigned int buffer_size_per_data_value = 4 * sizeof(float);

    m_vertex_array ->bind();
    m_vertex_buffer->bind();

    m_vertex_buffer->allocate(num_data_values * buffer_size_per_data_value);

    m_shader_program_h->attach_shader(pvt::vertex_shader(
      pvt::shader_strings::data_slider_line_vert));
    m_shader_program_h->attach_shader(pvt::geometry_shader(
      pvt::shader_strings::data_slider_line_horizontal_geom));
    m_shader_program_h->attach_shader(pvt::fragment_shader(
      pvt::shader_strings::data_slider_line_frag));
    m_shader_program_h->link();
    m_shader_program_h->bind();
    m_shader_program_h->set_attribute_buffer  ("vertex", 4, GL_FLOAT);
    m_shader_program_h->enable_attribute_array("vertex");

    m_shader_program_v->attach_shader(pvt::vertex_shader(
      pvt::shader_strings::data_slider_line_vert));
    m_shader_program_v->attach_shader(pvt::geometry_shader(
      pvt::shader_strings::data_slider_line_vertical_geom));
    m_shader_program_v->attach_shader(pvt::fragment_shader(
      pvt::shader_strings::data_slider_line_frag));
    m_shader_program_v->link();
    m_shader_program_v->bind();
    m_shader_program_v->set_attribute_buffer  ("vertex", 4, GL_FLOAT);
    m_shader_program_v->enable_attribute_array("vertex");

    m_max_data = 0.0f;
    m_min_data = std::numeric_limits<data_value_t>::infinity();
    for (unsigned int value_idx = 0u; value_idx < num_data_values; ++value_idx)
    {
      const float min_value  = static_cast<float>(m_min_data_values [value_idx]);
      const float max_value  = static_cast<float>(m_max_data_values [value_idx]);
      const float mean_value = static_cast<float>(m_mean_data_values[value_idx]);

      m_max_data = std::max(max_value, m_max_data);
      m_min_data = std::min(min_value, m_min_data);

      float buffer_data[] = {
        static_cast<float>(value_idx), min_value, max_value, mean_value };

      m_vertex_buffer->set_sub_data(value_idx * buffer_size_per_data_value,
                                    buffer_size_per_data_value,
                                    buffer_data);
    }

    m_shader_program_v->unbind();
    m_vertex_buffer   ->unbind();
    m_vertex_array    ->unbind();
  }
  m_data_prepared = true;
  update();
}


//------------------------------------------------------------------------------
void data_range_h_slider::set_colors
(QColor min_color, 
 QColor max_color, 
 QColor mean_color)
{
  m_shader_program_h->bind();
  m_shader_program_h->set_uniform("minColor" , Eigen::Vector4f(
    min_color.red  () / 255.0f,
    min_color.green() / 255.0f,
    min_color.blue () / 255.0f,
    min_color.alpha() / 255.0f
  ));
  m_shader_program_h->set_uniform("maxColor" , Eigen::Vector4f(
    max_color.red  () / 255.0f,
    max_color.green() / 255.0f,
    max_color.blue () / 255.0f,
    max_color.alpha() / 255.0f
  ));
  m_shader_program_h->set_uniform("meanColor", Eigen::Vector4f(
    mean_color.red  () / 255.0f,
    mean_color.green() / 255.0f,
    mean_color.blue () / 255.0f,
    mean_color.alpha() / 255.0f
  ));
  m_shader_program_h->unbind();

  m_shader_program_v->bind();
  m_shader_program_v->set_uniform("minColor" , Eigen::Vector4f(
    min_color.red  () / 255.0f,
    min_color.green() / 255.0f,
    min_color.blue () / 255.0f,
    min_color.alpha() / 255.0f
  ));
  m_shader_program_v->set_uniform("maxColor" , Eigen::Vector4f(
    max_color.red  () / 255.0f,
    max_color.green() / 255.0f,
    max_color.blue () / 255.0f,
    max_color.alpha() / 255.0f
  ));
  m_shader_program_v->set_uniform("meanColor", Eigen::Vector4f(
    mean_color.red  () / 255.0f,
    mean_color.green() / 255.0f,
    mean_color.blue () / 255.0f,
    mean_color.alpha() / 255.0f
  ));
  m_shader_program_v->unbind();
}


//------------------------------------------------------------------------------
void
data_range_h_slider::initializeGL
()
{
  range_slider::initializeGL();

  float data[] = { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f };

  m_vertex_array    .emplace();
  m_vertex_buffer   .emplace();
  m_shader_program_h.emplace();
  m_shader_program_v.emplace();

  m_vertex_array ->bind();
  m_vertex_buffer->bind();

  m_vertex_buffer->set_data(2 * 3 * sizeof(float), data);

  m_shader_program_h->attach_shader(pvt::vertex_shader  (
    pvt::shader_strings::data_slider_line_vert));
  m_shader_program_h->attach_shader(pvt::geometry_shader(
    pvt::shader_strings::data_slider_line_horizontal_geom));
  m_shader_program_h->attach_shader(pvt::fragment_shader(
    pvt::shader_strings::data_slider_line_frag));
  m_shader_program_h->link();
  m_shader_program_h->bind();
  m_shader_program_h->set_attribute_buffer  ("vertex", 4, GL_FLOAT);
  m_shader_program_h->enable_attribute_array("vertex");
  
  m_shader_program_v->attach_shader(pvt::vertex_shader  (
    pvt::shader_strings::data_slider_line_vert));
  m_shader_program_v->attach_shader(pvt::geometry_shader(
    pvt::shader_strings::data_slider_line_vertical_geom));
  m_shader_program_v->attach_shader(pvt::fragment_shader(
    pvt::shader_strings::data_slider_line_frag));
  m_shader_program_v->link();
  m_shader_program_v->bind();
  m_shader_program_v->set_attribute_buffer  ("vertex", 4, GL_FLOAT);
  m_shader_program_v->enable_attribute_array("vertex");

  m_shader_program_v->unbind();
  m_vertex_buffer   ->unbind();
  m_vertex_array    ->unbind();

  prepare_data();
}


//------------------------------------------------------------------------------
void
data_range_h_slider::paintGL
()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  if (!m_data_prepared)
    prepare_data();

  m_vertex_array->bind();

  m_shader_program_h->bind();
  m_shader_program_h->set_uniform("maxValue"   , m_max_value);
  m_shader_program_h->set_uniform("minValue"   , m_min_value);
  m_shader_program_h->set_uniform("maxIndexInv", 1.0f / m_mean_data_values.size());
  m_shader_program_h->set_uniform("pixelSize"  , Eigen::Vector2f(1.0f / width(),
                                                                 1.0f / height()));
  glLineWidth (4);
  glDrawArrays(GL_POINTS, 0, m_mean_data_values.size());
  m_shader_program_h->unbind();

  m_shader_program_v->bind();
  m_shader_program_v->set_uniform("maxValue"   , m_max_value);
  m_shader_program_h->set_uniform("minValue"   , m_min_value);
  m_shader_program_v->set_uniform("maxIndexInv", 1.0f / m_mean_data_values.size());
  m_shader_program_v->set_uniform("pixelSize"  , Eigen::Vector2f(1.0f / width(),
                                                                 1.0f / height()));
  glLineWidth(4);
  glDrawArrays(GL_LINE_STRIP, 0, m_mean_data_values.size());
  m_shader_program_v->unbind();

  m_vertex_array->unbind();

  draw_covers_handles();
}

} // namespace qpvt
