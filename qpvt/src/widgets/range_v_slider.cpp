//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <iostream>

#include <QAction>

#include <qpvt/widgets/range_v_slider.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
range_v_slider::range_v_slider
(QWidget*        p_parent,
 Qt::WindowFlags flags)
: range_slider(p_parent, flags)
{
  QAction* p_action_from_other = new QAction("From other", this);
  connect (p_action_from_other, &QAction::triggered, 
    [this] (bool) 
    {
      emit range_from_other_requested();
    });

  addAction           (p_action_from_other   );
  setContextMenuPolicy(Qt::ActionsContextMenu);
}


//------------------------------------------------------------------------------
void
range_v_slider::handle_range_change
(const QPointF& position)
{
  if (m_active_handle == min_handle)
    set_min((height() - position.y()) / static_cast<float>(height()));
  else if (m_active_handle == max_handle)
    set_max((height() - position.y()) / static_cast<float>(height()));
  else if (m_active_handle == mid_handle)
  {
    const float delta_y = (position.y() - m_initial_local_position.y()) /
                           static_cast<float>(height());

    float new_min = m_initial_min - delta_y;
    float new_max = m_initial_max - delta_y;

    if (new_max > 1.0f)
    {
      new_max = 1.0f;
      new_min = 1.0 - (get_max() - get_min());
    }
    else if (new_min < 0.0f)
    {
      new_min = 0.0f;
      new_max = get_max() - get_min();
    }

    set_min(new_min);
    set_max(new_max);
  }
  update();
  emit(range_changed(m_min, m_max));
}


//------------------------------------------------------------------------------
void
range_v_slider::handle_cursor_change
(const QPointF& position)
{
  const Qt::CursorShape current_shape = cursor().shape();
        Qt::CursorShape new_shape     = cursor().shape();

  switch (inside_handle(position))
  {
    case min_handle:
      new_shape = Qt::SizeVerCursor;
      break;
    case max_handle:
      new_shape = Qt::SizeVerCursor;
      break;
    case mid_handle:
      if (get_max() - get_min() < 0.99)
        new_shape = m_active_handle == mid_handle ? Qt::ClosedHandCursor : Qt::OpenHandCursor;
      break;
    default:
      if (cursor().shape() != parentWidget()->cursor().shape())
        unsetCursor();
      break;
  }
  
  if (current_shape != new_shape)
    setCursor(new_shape);
}


//------------------------------------------------------------------------------
range_slider::slider_handle
range_v_slider::inside_handle
(const QPointF& position)
const
{
  const float half_height = 5.0f;
  if      (position.y() > (1.0 - m_min) * height() - half_height &&
           position.y() < (1.0 - m_min) * height() + half_height)
    return min_handle;
  else if (position.y() > (1.0 - m_max) * height() - half_height &&
           position.y() < (1.0 - m_max) * height() + half_height)
    return max_handle;
  else if (position.y() < (1.0 - m_min) * height() + half_height &&
           position.y() > (1.0 - m_max) * height() - half_height)
    return mid_handle;
  return no_handle;
}

} // namespace qpvt
