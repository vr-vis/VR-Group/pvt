﻿//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <vector>

#include <Eigen/Dense>

#include <QCheckBox>
#include <QComboBox>
#include <QDrag>
#include <QDropEvent>
#include <QGridLayout>
#include <QMimeData>
#include <QPainter>
#include <QPushButton>

#include <pvt/cartesian_topology.hpp>
#include <pvt/cartesian_topology_index.hpp>
#include <pvt/rendering/camera.hpp>
#include <pvt/rendering/renderable_axes.hpp>
#include <pvt/rendering/scene.hpp>

#include <qpvt/widgets/cartesian_topology_viewer_widget.hpp>
#include <qpvt/widgets/data_range_h_slider.hpp>
#include <qpvt/widgets/data_range_v_slider.hpp>
#include <qpvt/widgets/slice_slider.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
cartesian_topology_viewer_widget::cartesian_topology_viewer_widget
(pvt::cartesian_topology* p_cartesian_topology,
 type_flags               type_flags,
 QWidget*                 p_parent,
 Qt::WindowFlags          window_flags)
: QWidget              (p_parent, window_flags)
, m_type_flags         (type_flags)
, mp_cartesian_topology(p_cartesian_topology)
, mp_renderable_scene  (new pvt::scene)
{
  const auto& cart_topo_idx   = mp_cartesian_topology->get_index ();
  const auto& dimension_sizes = cart_topo_idx.get_dimension_sizes();
  
  for (std::size_t i = 0; i < dimension_sizes.size(); ++i)
    m_projection.push_back(i);
  
  m_topology_mapping =
    new pvt::data_array<float>(cart_topo_idx.get_num_positions());
  
  mp_renderable_cartesian_grid = new pvt::renderable_cartesian_grid(
    Eigen::Vector3f(dimension_sizes[m_projection[0]],
                    dimension_sizes[m_projection[1]],
                    dimension_sizes[m_projection[2]]),
    Eigen::Vector3f(1, 1, 1));
  mp_renderable_scene->add_renderable(mp_renderable_cartesian_grid);

  mp_renderable_axes = new pvt::renderable_axes(Eigen::Vector3i(
    dimension_sizes[m_projection[0]],
    dimension_sizes[m_projection[1]],
    dimension_sizes[m_projection[2]]));
  mp_renderable_scene->add_renderable(mp_renderable_axes);
    
  mp_renderable_cartesian_topology = new pvt::renderable_cartesian_topology(
    mp_cartesian_topology,
    m_topology_mapping);
  mp_renderable_scene->add_renderable(mp_renderable_cartesian_topology);
  mp_renderable_cartesian_topology->set_projection(m_projection);
  
  setup_ui();
  
  mp_viewer_widget->set_scene(mp_renderable_scene);
  
  update_axis_scales();
}


//------------------------------------------------------------------------------
/// \todo Fix m_pViewerCamera leaking memory.
//------------------------------------------------------------------------------
cartesian_topology_viewer_widget::~cartesian_topology_viewer_widget
()
{
  delete mp_renderable_scene;
  delete m_topology_mapping;
}



//------------------------------------------------------------------------------
std::pair<float, float>
cartesian_topology_viewer_widget::get_filter_range
()
const
{
  return std::pair<float, float>(mp_histogram_slider->get_min(),
                                 mp_histogram_slider->get_max());
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::set_filter_range
(float min, 
 float max)
{
  mp_histogram_slider->set_min(min);
  mp_histogram_slider->set_max(max);
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::value_range_changed
(float min,
 float max)
{
  mp_renderable_cartesian_topology->set_filtered_range(min, max);
  mp_renderable_cartesian_topology->set_dirty();
  mp_viewer_widget->update();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::value_range_changed_end
(float min,
 float max)
{
  value_range_changed(min, max);
  update_slider_plots();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::min_mode_activated
(const QString& min_mode)
{
  const bool min_is_zero = (min_mode.compare("0") == 0);
  
  for (auto curr_slider : m_horizontal_sliders)
    curr_slider->set_min_is_zero(min_is_zero);

  update_slider_plots();

  mp_renderable_cartesian_topology->set_map_minimum_to_zero(!min_is_zero);
  mp_viewer_widget                ->update();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::offsets_changed
(int value)
{
  const auto& cart_topo_idx     = mp_cartesian_topology->get_index();
  const auto& dimension_offsets = cart_topo_idx.get_dimension_offsets();

  m_base_offset = 0;

  for (std::size_t i = 0; i < m_offset_sliders.size(); ++i)
    m_base_offset += m_offset_sliders[i]->get_value() * 
                     dimension_offsets[m_projection[i + 3]];

  mp_renderable_cartesian_topology->set_base_offset(m_base_offset);
  mp_viewer_widget                ->update();

  update_slider_plots();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::dimension_range_changed
(std::size_t dimension, 
 float       min, 
 float       max)
{
  mp_renderable_cartesian_topology->set_range_on_dimension(
    dimension, min, max);
  mp_renderable_cartesian_topology->set_dirty();
  mp_viewer_widget->update();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::dimension_range_changed_end
(std::size_t dimension,
 float       min,
 float       max)
{
  dimension_range_changed(dimension, min, max);
  update_slider_plots    ();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::setup_ui
()
{
  mp_grid_layout = new QGridLayout;
  mp_grid_layout->setContentsMargins(6, 12, 12, 12);
  mp_grid_layout->setSpacing        (6);
  setLayout(mp_grid_layout);

  setup_viewer_widget               ();
  setup_dimension_labels            ();
  setup_dimension_sliders           ();
  setup_gradient_value_slider       ();
  setup_find_correlated_views_button();

  setAcceptDrops(true);
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::setup_viewer_widget
()
{
  mp_camera        = new pvt::camera;
  mp_viewer_widget = new viewer_widget(mp_camera);

  QObject::connect(mp_viewer_widget, 
                   &viewer_widget::resized,
                   this, 
                   &cartesian_topology_viewer_widget::resize_hud);

  mp_viewer_frame = new QFrame;
  mp_viewer_frame->setFrameShape (QFrame::Box);
  mp_viewer_frame->setFrameShadow(QFrame::Sunken);
  mp_viewer_frame->setLayout     (new QVBoxLayout);
  mp_viewer_frame->layout        ()->setMargin(0);
  mp_viewer_frame->layout        ()->addWidget(mp_viewer_widget);
  mp_grid_layout ->addWidget     (mp_viewer_frame, 0, 0, 1, 2);
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::setup_dimension_labels
()
{
  for (std::size_t i = 0; i < mp_cartesian_topology->get_num_dimensions(); ++i)
  {
    const std::string curr_dimension_name(
      mp_cartesian_topology->get_dimension_names()[m_projection[i]]);

    m_dimension_labels.push_back(new QLabel(curr_dimension_name.c_str()));
    m_dimension_labels.back()->setSizePolicy(QSizePolicy::Maximum,
                                             QSizePolicy::Preferred);

    mp_grid_layout->addWidget(m_dimension_labels.back(), i+1, 0);
  }
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::setup_dimension_sliders
()
{
  int i = 0;
  for (const auto& curr_dim_size : 
       mp_cartesian_topology->get_index().get_dimension_sizes())
  {
    if (i < 3)
    {
      QFrame* p_temp_frame = new QFrame;
      p_temp_frame->setFrameShape (QFrame::Box    );
      p_temp_frame->setFrameShadow(QFrame::Sunken );
      p_temp_frame->setLayout     (new QVBoxLayout);
      p_temp_frame->setSizePolicy (QSizePolicy::Preferred, QSizePolicy::Maximum);
      p_temp_frame->layout        ()->setMargin(0);

      m_horizontal_sliders.push_back(new data_range_h_slider);
      m_horizontal_sliders.back()->setMinimumHeight(24);
      m_horizontal_sliders.back()->setMaximumHeight(32);

      QObject::connect(m_horizontal_sliders.back(), &range_slider::range_changed,
      [this, i] (float min, float max)
      {
        dimension_range_changed(i, min, max);
      });
      QObject::connect(m_horizontal_sliders.back(), &range_slider::range_changed_end, this,
      [this, i] (float min, float max)
      {
        dimension_range_changed_end(i, min, max);
      });
      
      p_temp_frame->layout()->addWidget(m_horizontal_sliders.back());

      mp_grid_layout->addWidget(p_temp_frame, i + 1, 1);
    }
    else
    {
      slice_slider* p_offset_slider = new slice_slider(mp_cartesian_topology,
                                                       m_topology_mapping,
                                                       true);
      p_offset_slider->set_min         (0);
      p_offset_slider->set_max         (curr_dim_size - 1);
      p_offset_slider->set_dimension_id(i);
      mp_grid_layout ->addWidget       (p_offset_slider, i + 1, 1);
      m_offset_sliders.push_back       (p_offset_slider);
      
      QObject::connect(p_offset_slider,
                       &slice_slider::valueChanged,
                       this,
                       &cartesian_topology_viewer_widget::offsets_changed);
      
      
      QPushButton* p_offset_slider_scale_button = new QPushButton(" + ");
      p_offset_slider_scale_button->setSizePolicy(QSizePolicy::Maximum,
                                                  QSizePolicy::Maximum);

      mp_grid_layout->addWidget(p_offset_slider_scale_button, i + 1, 3);
      
      m_offset_slider_scale_buttons.push_back(p_offset_slider_scale_button);

      QObject::connect(p_offset_slider_scale_button, &QAbstractButton::clicked,
      [this, i] (bool)
      {
        scale_offset_slider_plot(i-3);
      });
    }
    ++i;
  }
  if (mp_cartesian_topology->get_num_dimensions() > 3)
  {
    QCheckBox* p_show_overview_above_slice_sliders = new QCheckBox("Overview");
    p_show_overview_above_slice_sliders->setChecked(true);
    mp_grid_layout->addWidget(p_show_overview_above_slice_sliders, 2, 3, 1, 1);
    QObject::connect(p_show_overview_above_slice_sliders,
                     &QCheckBox::toggled,
                     [=](bool checked){toggle_overview_plots(checked);});
    
  }
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::setup_gradient_value_slider
()
{
  QWidget* p_gradient_value_widget = new QWidget;
  p_gradient_value_widget->setSizePolicy(QSizePolicy::Maximum,
                                         QSizePolicy::Preferred);
  
  QGridLayout* p_gradient_value_widget_layout = new QGridLayout;
  p_gradient_value_widget->setLayout(p_gradient_value_widget_layout);
  p_gradient_value_widget_layout->setMargin(0);
  p_gradient_value_widget_layout->setSpacing(0);
  
  // Max label
  QLabel* p_max_label = new QLabel("max");
  p_max_label->setAlignment (Qt::AlignVCenter | Qt::AlignHCenter);
  p_max_label->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
  p_gradient_value_widget_layout->addWidget(p_max_label, 0, 0, 1, 2);
  
  // Min combo box
  mp_minimum_combo_box = new QComboBox;
  mp_minimum_combo_box->addItem("0");
  mp_minimum_combo_box->addItem("min");

  if (mp_cartesian_topology->get_num_dimensions() > 3)
    mp_minimum_combo_box->addItem("minP");
  
  p_gradient_value_widget_layout->addWidget(mp_minimum_combo_box, 2, 0, 1, 2);
  
  QObject::connect(mp_minimum_combo_box, 
                   SIGNAL(activated         (const QString&)),
                   this, 
                   SLOT  (min_mode_activated(const QString&)));
  
  // Gradient
  QLabel* p_gradient_label = new QLabel;
  p_gradient_label->setMinimumWidth(8);
  p_gradient_label->setMaximumWidth(8);
  
  QPixmap gradient_pixmap(8,256*devicePixelRatio());
  gradient_pixmap.setDevicePixelRatio(devicePixelRatio());

  QPainter        gradient_painter(&gradient_pixmap);
  QLinearGradient gradient;
  gradient.setStart    (QPoint(0.0, 0.0));
  gradient.setFinalStop(QPoint(0.0, 255.0));
  gradient.setColorAt  (1.0, Qt::green);
  gradient.setColorAt  (0.5, Qt::yellow);
  gradient.setColorAt  (0.0, Qt::red);
  gradient_painter.fillRect(gradient_pixmap.rect(), gradient);
  gradient_painter.end();
  p_gradient_label->setPixmap(gradient_pixmap.scaledToHeight(100));
  p_gradient_label->setScaledContents(true);

  QFrame* p_gradient_frame = new QFrame;
  p_gradient_frame->setFrameShape (QFrame::Box);
  p_gradient_frame->setFrameShadow(QFrame::Sunken);
  p_gradient_frame->setSizePolicy (QSizePolicy::Maximum, QSizePolicy::Preferred);
  p_gradient_frame->setLayout     (new QVBoxLayout);
  p_gradient_frame->layout        ()->setMargin(0);
  p_gradient_frame->layout        ()->addWidget(p_gradient_label);
  
  p_gradient_value_widget_layout->addWidget(p_gradient_frame, 1, 0);
  
  // Histogram Slider
  mp_histogram_slider = new data_range_v_slider;
  mp_histogram_slider->setMinimumWidth(56);
  mp_histogram_slider->setMaximumWidth(56);
  
  QFrame* p_histogram_slider_frame = new QFrame;
  p_histogram_slider_frame->setFrameShape (QFrame::Box);
  p_histogram_slider_frame->setFrameShadow(QFrame::Sunken);
  p_histogram_slider_frame->setSizePolicy (QSizePolicy::Maximum, QSizePolicy::Preferred);
  p_histogram_slider_frame->setLayout     (new QVBoxLayout);
  p_histogram_slider_frame->layout        ()->setMargin(0);
  p_histogram_slider_frame->layout        ()->addWidget(mp_histogram_slider);
  
  p_gradient_value_widget_layout->addWidget(p_histogram_slider_frame, 1, 1);
  
  mp_grid_layout->addWidget(p_gradient_value_widget, 0, 3);
  
  QObject::connect(mp_histogram_slider, 
                   &range_slider::range_changed,
                   this, 
                   &cartesian_topology_viewer_widget::value_range_changed);
  QObject::connect(mp_histogram_slider, 
                   &range_slider::range_changed_end,
                   this, 
                   &cartesian_topology_viewer_widget::value_range_changed_end);
  
  QObject::connect(mp_histogram_slider, &data_range_v_slider::range_from_other_requested,
  [this]()
  {
    emit filter_range_from_other_requested();
  });
  
  setup_hud();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::setup_find_correlated_views_button
()
{
  QVBoxLayout* p_v_box_layout = new QVBoxLayout;
  p_v_box_layout->setMargin (0);
  p_v_box_layout->setSpacing(0);
  auto p_find_correlated_views_button =
    new QPushButton(m_type_flags & with_find_correlated_view_button 
      ? "Corr..." 
      : "Close");
  p_v_box_layout->addWidget(p_find_correlated_views_button);

  QWidget* p_container = new QWidget;
  p_container   ->setLayout(p_v_box_layout);
  mp_grid_layout->addWidget(p_container, 1, 3, 1, 1);

  QObject::connect(p_find_correlated_views_button, &QPushButton::clicked,
  [this] (bool)
  {
    emit find_correlated_views_clicked();
  });
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::setup_hud
()
{
  // HUD
  mp_hud = new QLabel(mp_viewer_widget);
  mp_hud->setAttribute (Qt::WA_TransparentForMouseEvents);
  mp_hud->setAlignment (Qt::AlignTop | Qt::AlignLeft);
  mp_hud->setStyleSheet("QLabel { color : white; }");
  update_hud();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::update_dimension_labels
()
{
  for (std::size_t i = 0; i < m_dimension_labels.size(); ++i)
  {
    const auto dim_name(mp_cartesian_topology->get_dimension_names()
      [m_projection[i]]);
    m_dimension_labels[i]->setText(dim_name.c_str());
  }
}


//------------------------------------------------------------------------------
/// \todo Make this more accessible.
/// \todo Find a non-hardcoded way to identify CPU dimensions.
/// \todo Make CPU axis scale user defineable.
//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::update_axis_scales
()
const
{
  Eigen::Vector3f axis_scale(1.0, 1.0, 1.0);
  for (std::size_t i = 0; i < 3; ++i)
  {
    const std::size_t curr_projected_dimension = m_projection[i];
    std::string       curr_dim_name(
      mp_cartesian_topology->get_dimension_names()[curr_projected_dimension]);

    std::transform(curr_dim_name.begin(),
                   curr_dim_name.end  (),
                   curr_dim_name.begin(),
                   tolower);
    
    if (curr_dim_name.compare("coreid") == 0 ||
        curr_dim_name.compare("hwthrd") == 0)
      axis_scale.data()[i] = 0.25;
  }
  mp_renderable_cartesian_topology->set_scale(axis_scale);
  mp_renderable_axes              ->set_scale(axis_scale);
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::update_slider_plots
()
{
  const auto& cart_topo_idx = mp_cartesian_topology->get_index();
  const auto num_cells_x = cart_topo_idx.get_dimension_sizes()[m_projection[0]];
  const auto num_cells_y = cart_topo_idx.get_dimension_sizes()[m_projection[1]];
  const auto num_cells_z = cart_topo_idx.get_dimension_sizes()[m_projection[2]];
  
  const std::size_t min_x = m_horizontal_sliders[0]->get_min() * (num_cells_x - 1);
  const std::size_t max_x = m_horizontal_sliders[0]->get_max() * (num_cells_x - 1);
  const std::size_t min_y = m_horizontal_sliders[1]->get_min() * (num_cells_y - 1);
  const std::size_t max_y = m_horizontal_sliders[1]->get_max() * (num_cells_y - 1);
  const std::size_t min_z = m_horizontal_sliders[2]->get_min() * (num_cells_z - 1);
  const std::size_t max_z = m_horizontal_sliders[2]->get_max() * (num_cells_z - 1);
  
  std::vector<std::size_t> minima({{min_x, min_y, min_z}});
  std::vector<std::size_t> maxima({{max_x, max_y, max_z}});
  
  m_maximum_in_projection = -std::numeric_limits<float>::infinity();
  m_minimum_in_projection =  std::numeric_limits<float>::infinity();

  std::vector<std::size_t> coords_for_min_max(cart_topo_idx.get_num_dimensions(), 0);
  for (  coords_for_min_max[m_projection[0]] = 0;
         coords_for_min_max[m_projection[0]] < num_cells_x;
       ++coords_for_min_max[m_projection[0]])
  {
    for (  coords_for_min_max[m_projection[1]] = 0;
           coords_for_min_max[m_projection[1]] < num_cells_y;
         ++coords_for_min_max[m_projection[1]])
    {
      for (  coords_for_min_max[m_projection[2]] = 0;
             coords_for_min_max[m_projection[2]] < num_cells_z;
           ++coords_for_min_max[m_projection[2]])
      {
        const std::size_t index_for_coordinates = 
          cart_topo_idx.get_index(coords_for_min_max) + m_base_offset;
        
        const float curr_val =
          m_topology_mapping->at(index_for_coordinates);

        m_maximum_in_projection = std::max(m_maximum_in_projection, curr_val);
        m_minimum_in_projection = std::min(m_minimum_in_projection, curr_val);
      }
    }
  }

  float minimum_in_range =  std::numeric_limits<float>::infinity();
  float maximum_in_range = -std::numeric_limits<float>::infinity();
  
  // Data along x
  m_horizontal_sliders[0]->clear_data_values();
  std::vector<std::size_t> current_minima(minima);
  std::vector<std::size_t> current_maxima(maxima);
  for (unsigned int x = 0; x < num_cells_x; ++x)
  {
    float max_val  = 0.0f;
    float min_val  = 0.0f;
    float mean_val = 0.0f;

    current_minima[0] = x;
    current_maxima[0] = x;

    get_min_max_mean_for_range(&min_val, 
                               &max_val, 
                               &mean_val,
                               current_minima, 
                               current_maxima);

    m_horizontal_sliders[0]->add_data_value(min_val, max_val, mean_val);

    maximum_in_range = std::max(maximum_in_range, max_val);
    minimum_in_range = std::min(minimum_in_range, min_val);
  }
  
  m_horizontal_sliders[1]->clear_data_values();
  current_minima = minima;
  current_maxima = maxima;
  for (unsigned int y = 0; y < num_cells_y; ++y)
  {
    float max_val = 0.0f;
    float min_val = 0.0f;
    float mean_val = 0.0f;

    current_minima[1] = y;
    current_maxima[1] = y;

    get_min_max_mean_for_range(&min_val, 
                               &max_val, 
                               &mean_val,
                               current_minima, 
                               current_maxima);

    m_horizontal_sliders[1]->add_data_value(min_val, max_val, mean_val);

    maximum_in_range = std::max(maximum_in_range, max_val);
    minimum_in_range = std::min(minimum_in_range, min_val);
  }
  
  m_horizontal_sliders[2]->clear_data_values();
  current_minima = minima;
  current_maxima = maxima;
  for (unsigned int z = 0; z < num_cells_z; ++z)
  {
    float max_val  = 0.0f;
    float min_val  = 0.0f;
    float mean_val = 0.0f;

    current_minima[2] = z;
    current_maxima[2] = z;

    get_min_max_mean_for_range(&min_val, 
                               &max_val, 
                               &mean_val,
                               current_minima, 
                               current_maxima);

    m_horizontal_sliders[2]->add_data_value(min_val, max_val, mean_val);

    maximum_in_range = std::max(maximum_in_range, max_val);
    minimum_in_range = std::min(minimum_in_range, min_val);
  }
  
  // Histogram
  const float dataset_minimum = 
    m_minimum * 
    static_cast<float>(mp_minimum_combo_box->currentText().compare("min" ) == 0) +
    m_minimum_in_projection * 
    static_cast<float>(mp_minimum_combo_box->currentText().compare("minP") == 0);

  const unsigned int num_hist_bins = 50u;
  unsigned int hist_vals[num_hist_bins];
  for (unsigned int i = 0; i < num_hist_bins; ++i)
    hist_vals[i] = 0u;

  const float overall_maximum_inv = 1.0f / (m_maximum - dataset_minimum);

  std::vector<std::size_t> current_coordinates(
    cart_topo_idx.get_num_dimensions(), 0);

  for (  current_coordinates[m_projection[0]] = 0;
         current_coordinates[m_projection[0]] < num_cells_x;
       ++current_coordinates[m_projection[0]])
  {
    for (  current_coordinates[m_projection[1]] = 0;
           current_coordinates[m_projection[1]] < num_cells_y;
         ++current_coordinates[m_projection[1]])
    {
      for (  current_coordinates[m_projection[2]] = 0;
             current_coordinates[m_projection[2]] < num_cells_z;
           ++current_coordinates[m_projection[2]])
      {
        const std::size_t index_for_coordinates = 
          cart_topo_idx.get_index(current_coordinates) + m_base_offset;

        const float curr_val =
          m_topology_mapping->at(index_for_coordinates) - 
          dataset_minimum;

        const float curr_rel_val = curr_val * overall_maximum_inv;

        unsigned int curr_bin = std::min(
          50u,
          static_cast<unsigned int>(curr_rel_val * (num_hist_bins - 1) + 0.5f));

        ++hist_vals[curr_bin];
      }
    }
  }
  
  mp_histogram_slider->clear_data_values();
  unsigned int max_hist_bin_value = 0u;
  for (unsigned int i = 0; i < num_hist_bins; ++i)
  {
    mp_histogram_slider->add_data_value(static_cast<float>(hist_vals[i]));
    max_hist_bin_value = std::max(hist_vals[i], max_hist_bin_value);
  }
  
  // Widget updates.
  m_horizontal_sliders[0]->set_max_value(m_maximum );
  m_horizontal_sliders[1]->set_max_value(m_maximum );
  m_horizontal_sliders[2]->set_max_value(m_maximum );
  m_horizontal_sliders[0]->set_min_value(dataset_minimum);
  m_horizontal_sliders[1]->set_min_value(dataset_minimum);
  m_horizontal_sliders[2]->set_min_value(dataset_minimum);
  mp_histogram_slider    ->set_max_value(static_cast<float>(max_hist_bin_value));
  
  mp_renderable_cartesian_topology->set_maximum(m_maximum );
  mp_renderable_cartesian_topology->set_minimum(dataset_minimum);
  
  m_horizontal_sliders[0]->set_data_dirty();
  m_horizontal_sliders[1]->set_data_dirty();
  m_horizontal_sliders[2]->set_data_dirty();
  mp_histogram_slider    ->set_data_dirty();
  
  m_horizontal_sliders[0]->update_recursive();
  m_horizontal_sliders[1]->update_recursive();
  m_horizontal_sliders[2]->update_recursive();
  mp_histogram_slider    ->update();
  
  update_hud();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::update_hud
()
{
  QString text;
  text += "<html><small><tab>";
  
  text += "<tr><td>Dataset:</td>";
  text += "<td>&nbsp;&nbsp;&nbsp;</td>";
  text += "<td align=right>";
  text += QString::number(m_minimum);
  text += " ... ";
  text += QString::number(m_maximum);
  text += "</td>";
  text += "<td>&nbsp;&nbsp;&nbsp;</td>";
  text += "<td>";
  text += "(";
  text += QString::number(m_maximum - m_minimum);
  text += ")";
  text += "</td></tr>";
  
  if (mp_cartesian_topology->get_num_dimensions() > 3)
  {
    text += "<tr><td>3D Slice:</td>";
    text += "<td>&nbsp;&nbsp;&nbsp;</td>";
    text += "<td align=right>";
    text += QString::number(m_minimum_in_projection);
    text += " ... ";
    text += QString::number(m_maximum_in_projection);
    text += "</td>";
    text += "<td>&nbsp;&nbsp;&nbsp;</td>";
    text += "<td>";
    text += "(";
    text += QString::number(m_maximum_in_projection - m_minimum_in_projection);
    text += ")";
    text += "</td></tr>";
  }
  
  const float dataset_minimum =
    m_minimum *
    static_cast<float>(mp_minimum_combo_box->currentText().compare("min" ) == 0) +
    m_minimum_in_projection *
    static_cast<float>(mp_minimum_combo_box->currentText().compare("minP") == 0);

  text += "<tr><td>Colormap:</td>";
  text += "<td>&nbsp;&nbsp;&nbsp;</td>";
  text += "<td align=right>";
  text += QString::number(dataset_minimum);
  text += " ... ";
  text += QString::number(m_maximum);
  text += "</td>";
  text += "<td>&nbsp;&nbsp;&nbsp;</td>";
  text += "<td>";
  text += "(";
  text += QString::number(m_maximum - dataset_minimum);
  text += ")";
  text += "</td></tr>";
  
  text += "</tab></small></html>";
  
  mp_hud->setText(text);
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::resize_hud
()
{
  mp_hud->setGeometry(5,
                      5,
                      mp_viewer_widget->width () - 10,
                      mp_viewer_widget->height() - 10);
}


//------------------------------------------------------------------------------
/// \todo: Clean this up
//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::get_min_max_mean_for_range
(float*                          p_min,
 float*                          p_max,
 float*                          p_mean,
 const std::vector<std::size_t>& minima,
 const std::vector<std::size_t>& maxima)
const
{
  const auto& cart_topo_idx = mp_cartesian_topology->get_index();
  
  *p_min  =  std::numeric_limits<float>::infinity();
  *p_max  = -std::numeric_limits<float>::infinity();
  *p_mean = 0.0f;

  std::size_t num_values = 0;
  
  std::vector<std::size_t> coordinates(cart_topo_idx.get_num_dimensions(), 0);
  std::vector<std::size_t> projected_coordinates(minima);

  const std::size_t num_projected_coords = projected_coordinates.size();
  while (projected_coordinates[0] <= maxima[0])
  {
    for (std::size_t proj_idx = 0; proj_idx < num_projected_coords; ++proj_idx)
      coordinates[m_projection[proj_idx]] = projected_coordinates[proj_idx];
    
    const std::size_t index_for_coordinates =
      cart_topo_idx.get_index(coordinates) + m_base_offset;

    const float value = m_topology_mapping->at(index_for_coordinates);

    *p_min = std::min(*p_min, value);
    *p_max = std::max(*p_max, value);
    *p_mean += value;

    ++num_values;
    
    ++projected_coordinates.back();
    
    for (std::size_t i = num_projected_coords - 1; i > 0; --i)
    {
      if (projected_coordinates[i] > maxima[i])
      {
        projected_coordinates[i] = minima[i];
        ++projected_coordinates[i-1];
      }
      else
        break;
    }
  }

  *p_mean /= static_cast<float>(num_values);
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::scale_offset_slider_plot
(std::size_t index)
{
  if (m_offset_slider_scale_buttons[index]->text().compare(" + ") == 0)
  {
    for (std::size_t i = 0; i < m_offset_slider_scale_buttons.size(); ++i)
    {
      if (i != index)
      {
        m_offset_slider_scale_buttons[i]->setText   (" + ");
        m_offset_sliders             [i]->scale_down();
      }
    }
    m_offset_slider_scale_buttons[index]->setText (" – ");
    m_offset_sliders             [index]->scale_up();
  }
  else if (m_offset_slider_scale_buttons[index]->text().compare(" – ") == 0)
  {
    m_offset_slider_scale_buttons[index]->setText   (" + ");
    m_offset_sliders             [index]->scale_down();
  }
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::toggle_overview_plots
(bool checked)
{
  for (slice_slider* p_curr_slice_slider : m_offset_sliders)
    p_curr_slice_slider->toggle_overview(checked);
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::mousePressEvent
(QMouseEvent* p_event)
{
  for (std::size_t i = 0; i < m_dimension_labels.size(); ++i)
  {
    if (p_event->button() == Qt::LeftButton && 
        m_dimension_labels[i]->geometry().contains(p_event->pos()))
    {
      m_drag_start_position  = p_event->pos();
      mp_dragged_label       = m_dimension_labels[i];
      mp_dragged_label_index = i;
      break;
    }
  }
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::mouseMoveEvent
(QMouseEvent* p_event)
{
  if (!(p_event->buttons() & Qt::LeftButton))
    return;
  if (mp_dragged_label == nullptr)
    return;
  if ((p_event->pos() - m_drag_start_position).manhattanLength()
      < QApplication::startDragDistance())
    return;

  if (!m_is_dragging)
  {
    QDrag*     p_drag      = new QDrag(this);
    QMimeData* p_mime_data = new QMimeData;
    p_mime_data->setText    (QString::number(mp_dragged_label_index));
    p_drag     ->setMimeData(p_mime_data);
    
    mp_dragged_pixmap = new QPixmap(mp_dragged_label->size());
    mp_dragged_label->render(mp_dragged_pixmap);

    p_drag->setPixmap(*mp_dragged_pixmap);

    m_is_dragging = true;

    p_drag->exec();
  }
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::mouseReleaseEvent
(QMouseEvent* p_event)
{
  mp_dragged_label = nullptr;
  if (mp_dragged_pixmap != nullptr)
  {
    delete mp_dragged_pixmap;
    mp_dragged_pixmap = nullptr;
  }
  m_is_dragging = false;
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::dragEnterEvent
(QDragEnterEvent* p_event)
{
  if (p_event->mimeData()->hasFormat("text/plain"))
    p_event->acceptProposedAction();
}


//------------------------------------------------------------------------------
void
cartesian_topology_viewer_widget::dropEvent
(QDropEvent* p_event)
{
  const std::size_t dragged_index       = p_event->mimeData()->text().toULong();
  std::size_t       dropped_index       = m_dimension_labels.size();
  
  for (std::size_t i = 0; i < m_dimension_labels.size(); ++i)
  {
    const int curr_center = 
            m_dimension_labels[i]->pos().y() + 
      0.5 * m_dimension_labels[i]->height();

    if (p_event->pos().y() < curr_center)
    {
      dropped_index = i;
      break;
    }
  }
  
  std::list<std::size_t> temp_projection(m_projection.begin(),
                                         m_projection.end  ());

  std::list<std::size_t>::iterator dragged_iter = temp_projection.begin();
  std::advance(dragged_iter, dragged_index);
  std::list<std::size_t>::iterator dropped_iter = temp_projection.begin();
  std::advance(dropped_iter, dropped_index);
  temp_projection.splice(dropped_iter, temp_projection, dragged_iter);
  std::copy(temp_projection.begin(),
            temp_projection.end  (),
            m_projection   .begin());
  
  mp_dragged_label = nullptr;
  if (mp_dragged_pixmap != nullptr)
  {
    delete mp_dragged_pixmap;
    mp_dragged_pixmap = nullptr;
  }
  m_is_dragging = false;
  
  const auto& cart_topo_idx   = mp_cartesian_topology->get_index ();
  const auto& dimension_sizes = cart_topo_idx.get_dimension_sizes();
  
  for (std::size_t i = 0; i < m_horizontal_sliders.size(); ++i)
  {
    m_horizontal_sliders[i]->set_min(0.0f);
    m_horizontal_sliders[i]->set_max(1.0f);
    m_horizontal_sliders[i]->update ();
    dimension_range_changed(i, 0.0, 1.0);
  }
  for (std::size_t i = 0; i < m_offset_sliders.size(); ++i)
  {
    m_offset_sliders[i]->set_value       (0);
    m_offset_sliders[i]->set_min         (0);
    m_offset_sliders[i]->set_max         (dimension_sizes[m_projection[i+3]] - 1);
    m_offset_sliders[i]->set_dimension_id(m_projection[i+3]);
  }
  
  update_dimension_labels();

  mp_renderable_cartesian_topology->set_projection(m_projection);
  
  mp_renderable_cartesian_grid->set_size(
    Eigen::Vector3f(dimension_sizes[m_projection[0]],
                    dimension_sizes[m_projection[1]],
                    dimension_sizes[m_projection[2]]));

  mp_renderable_axes->set_lengths(
    Eigen::Vector3i(
    dimension_sizes[m_projection[0]],
    dimension_sizes[m_projection[1]],
    dimension_sizes[m_projection[2]]));
  
  mp_viewer_widget->update();

  update_slider_plots();
  update_axis_scales ();
}

} // namespace qpvt
