//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QSignalMapper>

#include <pvt/rendering/camera.hpp>
#include <pvt/rendering/scene.hpp>

#include <qpvt/widgets/parallel_coordinates_plot_widget.hpp>
#include <qpvt/widgets/viewer_widget.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
parallel_coordinates_plot_widget::parallel_coordinates_plot_widget(QWidget* parent)
: QWidget(parent)
, mp_viewer_camera       (new pvt::camera)
, mp_scene               (new pvt::scene)
, mp_parallel_coords_plot(new parallel_coords_plot)
, mp_grid_layout         (new QGridLayout)
{
  mp_viewer_widget = new viewer_widget(mp_viewer_camera.get());
  
  QObject::connect(mp_viewer_widget, &viewer_widget::resized, [&] 
  {
    mp_parallel_coords_plot->set_size(Eigen::Vector2i(
      mp_viewer_widget->width (), 
      mp_viewer_widget->height()));

    // Get center of each upper button. Update axes to the x positions.
    std::vector<axis> axes;
    for (auto i = 0; i < 5; i++)
    {
      auto item = mp_grid_layout->itemAtPosition(0, i);
      axes.emplace_back((double) item->geometry().center().x() / geometry().width());
    }
    mp_parallel_coords_plot->set_axes(axes);
  });

  mp_parallel_coords_plot->set_axes      ({ 0.0f, 0.25f, 0.5f, 0.75f, 1.0f });
  mp_scene               ->add_renderable(mp_parallel_coords_plot.get());
  mp_viewer_widget       ->set_scene     (mp_scene.get());

  mp_viewer_frame = new QFrame;
  mp_viewer_frame->setLayout(new QHBoxLayout);
  mp_viewer_frame->layout()->setContentsMargins(0, 0, 0, 0);
  mp_viewer_frame->layout()->addWidget(mp_viewer_widget);

  mp_viewer_widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  setup_ui();

  setLayout(mp_grid_layout);
}


//------------------------------------------------------------------------------
void 
parallel_coordinates_plot_widget::set_entries
(const std::vector<entry>& entries)
{
  mp_parallel_coords_plot->set_entries(entries);
}


//------------------------------------------------------------------------------
void 
parallel_coordinates_plot_widget::set_entry_selected
(unsigned entry_index, bool select)
{
  mp_parallel_coords_plot->set_entry_selected(entry_index, select);
}


//------------------------------------------------------------------------------
void parallel_coordinates_plot_widget::deselect_all_entries()
{
  mp_parallel_coords_plot->deselect_all_entries();
}


//------------------------------------------------------------------------------
void parallel_coordinates_plot_widget::setup_ui()
{
  mp_grid_layout->setContentsMargins(0, 0, 0, 8);

  auto button_signal_mapper = new QSignalMapper();
  for (auto i = 0; i < 5; i++)
  {
    auto current_button = create_button("1", m_upper_buttons);
    QObject::connect(current_button,        SIGNAL(clicked()), 
                     button_signal_mapper,  SLOT  (map    ()));
    button_signal_mapper->setMapping(current_button, i);
    QObject::connect(button_signal_mapper,  SIGNAL(mapped(int)),
                     this,                  SLOT  (on_upper_button_clicked(int)));

    mp_grid_layout->addWidget(current_button, 0, i);
  }
  
  mp_grid_layout->addWidget(mp_viewer_frame, 1, 0, 1, 5);

  button_signal_mapper = new QSignalMapper();
  for (auto i = 0; i < 5; i++)
  {
    auto current_button = create_button("0", m_lower_buttons);
    QObject::connect(current_button,       SIGNAL(clicked()),
                     button_signal_mapper, SLOT  (map    ()));
    button_signal_mapper->setMapping(current_button, i);
    QObject::connect(button_signal_mapper, SIGNAL(mapped(int)),
                     this,                 SLOT  (on_lower_button_clicked(int)));
    mp_grid_layout->addWidget(current_button, 2, i);
  }

  mp_grid_layout->addWidget(create_label("Severity"          ), 3, 0);
  mp_grid_layout->addWidget(create_label("Severity / Element"), 3, 1);
  mp_grid_layout->addWidget(create_label("Severity / Area"   ), 3, 2);
  mp_grid_layout->addWidget(create_label("Num. Elements"     ), 3, 3);
  mp_grid_layout->addWidget(create_label("Area"              ), 3, 4);
}
  
  
//------------------------------------------------------------------------------
void
parallel_coordinates_plot_widget::update
()
{
  QWidget::update();
  mp_viewer_widget->update();
}


//------------------------------------------------------------------------------
QLabel* 
parallel_coordinates_plot_widget::create_label
(const QString& text)
{
  auto p_label = new QLabel(text, this);
  p_label->setAlignment (Qt::AlignCenter);
  p_label->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
  return p_label;
}


//------------------------------------------------------------------------------
QPushButton*
parallel_coordinates_plot_widget::create_button
(const QString& text, std::vector<QPushButton*>& group)
{
  auto p_button = new QPushButton(text, this);
  group.push_back(p_button);
  p_button->setFlat      (true);
  p_button->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
  return p_button;
}


//------------------------------------------------------------------------------
void
parallel_coordinates_plot_widget::toggle_axis
(unsigned int index, bool inverted)
{
  m_upper_buttons[index]->setText(inverted ? "0" : "1");
  m_lower_buttons[index]->setText(inverted ? "1" : "0");
  mp_parallel_coords_plot->set_axis_inverted(index, inverted);
  this->update();
}


//------------------------------------------------------------------------------
void
parallel_coordinates_plot_widget::on_upper_button_clicked
(int index)
{
  toggle_axis(index, false);
}


//------------------------------------------------------------------------------
void
parallel_coordinates_plot_widget::on_lower_button_clicked
(int index)
{
  toggle_axis(index, true);
}

} // namespace qpvt
