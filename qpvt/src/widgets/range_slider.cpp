//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <QMouseEvent>

#include <pvt/gl/shader.hpp>
#include <pvt/rendering/shader_strings.hpp>

#include <qpvt/widgets/range_slider.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
range_slider::range_slider
(QWidget*        p_parent,
 Qt::WindowFlags flags)
: QOpenGLWidget(p_parent, flags)
{
  setMouseTracking     (true);
  setAutoFillBackground(true);
}


//------------------------------------------------------------------------------
void
range_slider::set_background_color
(const QColor& color)
{
  m_background_color = color;
  if (context() != nullptr)
    glClearColor(color.redF(), color.greenF(), color.blueF(), 0.0f);
}


//------------------------------------------------------------------------------
void
range_slider::setup_viewport
(int width, int height)
{
  glViewport(0.0, 0.0, width, height);
}


//------------------------------------------------------------------------------
void
range_slider::draw_covers_handles
()
{
  glEnable   (GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  m_cover_vertex_array  ->bind();
  m_cover_shader_program->bind();

  m_cover_shader_program->set_uniform("color", Eigen::Vector4f(
    m_covers_color.red  (),
    m_covers_color.green(),
    m_covers_color.blue (),
    m_covers_color.alpha()));

  m_cover_shader_program->set_uniform("value"     , m_min);
  m_cover_shader_program->set_uniform("isMax"     , false);
  m_cover_shader_program->set_uniform("isVertical", is_vertical());
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  m_cover_shader_program->set_uniform("value"     , m_max);
  m_cover_shader_program->set_uniform("isMax"     , true);
  m_cover_shader_program->set_uniform("isVertical", is_vertical());
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  m_cover_shader_program->unbind();
  m_cover_vertex_array  ->unbind();
}


//------------------------------------------------------------------------------
void
range_slider::initializeGL()
{
  set_background_color(m_background_color);

  float min_cover_vertices[] = {
    0.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    1.0f, 1.0f, 0.0f
  };

  m_cover_vertex_array  .emplace();
  m_cover_vertex_buffer .emplace();
  m_cover_shader_program.emplace();

  m_cover_vertex_array ->bind();
  m_cover_vertex_buffer->bind();
  m_cover_vertex_buffer->set_data(12 * sizeof(float), min_cover_vertices);
  
  m_cover_shader_program->attach_shader(
    pvt::vertex_shader  (pvt::shader_strings::cover_vert));
  m_cover_shader_program->attach_shader(
    pvt::fragment_shader(pvt::shader_strings::cover_frag));
  m_cover_shader_program->link();
  m_cover_shader_program->bind();

  m_cover_shader_program->set_attribute_buffer  ("pos", 3, GL_FLOAT, 0);
  m_cover_shader_program->enable_attribute_array("pos");
  
  m_cover_shader_program->unbind();
  m_cover_vertex_buffer ->unbind();
  m_cover_vertex_array  ->unbind();
}


//------------------------------------------------------------------------------
void
range_slider::resizeGL
(int width, int height)
{
  const float pixel_ratio = devicePixelRatio();
  const int   side        = std::min(width * pixel_ratio, height * pixel_ratio);
  glViewport((width  * pixel_ratio - side) / 2,
             (height * pixel_ratio - side) / 2,
             side,
             side);
}


//------------------------------------------------------------------------------
void
range_slider::paintGL
()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  draw_covers_handles();
}


//------------------------------------------------------------------------------
void
range_slider::leaveEvent
(QEvent* p_event)
{
  if (m_active_handle == no_handle)
    unsetCursor();
}


//------------------------------------------------------------------------------
void
range_slider::mousePressEvent
(QMouseEvent* p_event)
{
  const QPointF mouse_position(p_event->localPos());
  m_active_handle          = inside_handle(mouse_position);
  m_initial_local_position = mouse_position;
  m_initial_min            = get_min();
  m_initial_max            = get_max();
  handle_cursor_change(mouse_position);
}


//------------------------------------------------------------------------------
void
range_slider::mouseReleaseEvent
(QMouseEvent* p_event)
{
  m_active_handle = no_handle;
  handle_cursor_change(p_event->localPos());
  emit(range_changed_end(m_min, m_max));
}


//------------------------------------------------------------------------------
void
range_slider::mouseMoveEvent
(QMouseEvent* p_event)
{
  const QPointF mouse_position(p_event->localPos());
  if (m_active_handle == min_handle ||
      m_active_handle == max_handle ||
      m_active_handle == mid_handle)
  {
    handle_range_change(mouse_position);
  }
  handle_cursor_change(p_event->localPos());
}

} // namespace qpvt
