//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <QHeaderView>
#include <QLayout>
#include <QMouseEvent>

#include <pvt/rendering/renderable_geometry.hpp>

#include <qpvt/widgets/geometry_viewer_widget.hpp>
#include <qpvt/widgets/parallel_coordinates_plot_widget.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
geometry_viewer_widget::geometry_viewer_widget
(pvt::system_geometry*  p_system_geometry)
: mp_system_geometry(p_system_geometry)
, mp_scene          (new pvt::clickable_scene)
{
  setOrientation(Qt::Orientation::Vertical);

  setup_viewer         ();
  setup_parallel_coords();
  setup_table          ();
}


//------------------------------------------------------------------------------
void 
geometry_viewer_widget::setup_viewer
()
{
  mp_camera = new pvt::camera();
  mp_camera->set_lambda  (402.526 / 180.0f * 3.1415926f);
  mp_camera->set_phi     (35.8327 / 180.0f * 3.1415926f);
  mp_camera->set_distance(3500);
  mp_camera->set_coi     (Eigen::Vector3f(0, 0, 0));

  mp_viewer_widget = new viewer_widget(mp_camera);
  mp_viewer_widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  mp_viewer_frame = new QFrame;
  mp_viewer_frame->setFrameShape   (QFrame::Box);
  mp_viewer_frame->setFrameShadow  (QFrame::Sunken);
  mp_viewer_frame->setLayout       (new QVBoxLayout);
  mp_viewer_frame->layout          ()->setMargin(0);
  mp_viewer_frame->layout          ()->addWidget(mp_viewer_widget);
  mp_viewer_frame->setMinimumHeight(192);
  mp_viewer_frame->setSizePolicy   (QSizePolicy::MinimumExpanding, 
                                    QSizePolicy::MinimumExpanding);
  addWidget(mp_viewer_frame);

  for (auto& geometry : mp_system_geometry->get_geometries())
  {
    auto renderable_geometry = new pvt::renderable_geometry(&geometry);
    mp_renderable_geometries.push_back(renderable_geometry);
    mp_scene->add_renderable(renderable_geometry);
  }

  mp_viewer_widget->set_scene(mp_scene);

  // Get device pixel ratio and apply to clickable scene size.
  QObject::connect(mp_viewer_widget, &viewer_widget::resized, 
  [&] ()
  {
    auto ratio = window()->devicePixelRatio();
    mp_scene->resize(Eigen::Vector2i(ratio * mp_viewer_widget->width (), 
                                     ratio * mp_viewer_widget->height()));
  });

  QObject::connect(mp_viewer_widget, &viewer_widget::clicked, 
  [&](int width, int height, Qt::KeyboardModifiers modifiers)
  {
    mp_viewer_widget->makeCurrent();
    
    auto ratio = window()->devicePixelRatio();
    auto id    = mp_scene->get_id(Eigen::Vector2i(ratio * width, ratio * height));

    if (id == -1)
    {
      if (modifiers == Qt::NoModifier)
        handle_geometry_selection(id, interactor_mode::deselect_all);
      else
        return;
    }
    else
    {
      if (modifiers == Qt::NoModifier)
        handle_geometry_selection(id, interactor_mode::select_one);
      else if (modifiers == Qt::ShiftModifier)
        handle_geometry_selection(id, interactor_mode::toggle_one);
      else if (modifiers == Qt::ControlModifier)
        handle_geometry_selection(id, interactor_mode::deselect_one);
    }
  });
}


//------------------------------------------------------------------------------
void geometry_viewer_widget::setup_parallel_coords()
{
  mp_parallel_coords_widget.reset(new parallel_coordinates_plot_widget());
  addWidget(mp_parallel_coords_widget.get());
}


//------------------------------------------------------------------------------
void 
geometry_viewer_widget::setup_table
()
{
  std::vector<QString> table_header_strings;
  table_header_strings.push_back("Name");
  table_header_strings.push_back("Severity");
  table_header_strings.push_back("Severity/Elements");
  table_header_strings.push_back("Severity/Area");
  table_header_strings.push_back("Elements");
  table_header_strings.push_back("Area");

  auto column_count = table_header_strings.size();
  
  mp_table_widget = new QTableWidget();
  mp_table_widget->setRowCount   (mp_system_geometry->get_geometries().size());
  mp_table_widget->setColumnCount(column_count);
  mp_table_widget->setAlternatingRowColors(true);

  QPalette table_color_palette(mp_table_widget->palette());
  table_color_palette.setBrush(QPalette::AlternateBase, QColor(245, 245, 245));
  mp_table_widget->setPalette(table_color_palette);

  QStringList table_headers;
  for (auto table_header_string : table_header_strings)
    table_headers << table_header_string;

  mp_table_widget->setHorizontalHeaderLabels(table_headers);
  mp_table_widget->verticalHeader  ()->setVisible(false);
  mp_table_widget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  mp_table_widget->setEditTriggers     (QAbstractItemView::NoEditTriggers);
  mp_table_widget->setSelectionBehavior(QAbstractItemView::SelectRows);
  mp_table_widget->setSizePolicy       (QSizePolicy::Preferred,
                                        QSizePolicy::Preferred);
  mp_table_widget->setSelectionMode    (QAbstractItemView::MultiSelection);

  QObject::connect(mp_table_widget, &QTableWidget::itemSelectionChanged,
  [&]()
  {
    auto row_model_index_list = mp_table_widget->selectionModel()->selectedRows();
    m_selected_indices.clear();
    for (auto row_model_index : row_model_index_list)
      m_selected_indices.push_back(row_model_index.row());
  
    // Update subwidgets according to selected indices.
    for (auto renderable_geometry : mp_renderable_geometries)
      renderable_geometry->set_selected(false);
    mp_parallel_coords_widget->deselect_all_entries();
    for (auto geometry_index : m_selected_indices)
    {
      mp_renderable_geometries[geometry_index]->set_selected(true);
      mp_parallel_coords_widget->set_entry_selected(geometry_index, true);
    }
    mp_viewer_widget         ->update();
    mp_parallel_coords_widget->update();
  });
  addWidget(mp_table_widget);
}


//------------------------------------------------------------------------------
void geometry_viewer_widget::update_viewer
(const pvt::data_array<float>& geometric_severities)
{
  auto max_severity = *std::max_element(geometric_severities.begin(), 
                                        geometric_severities.end  ());
  for (auto i = 0; i < mp_renderable_geometries.size(); i++)
    mp_renderable_geometries[i]->set_severity(geometric_severities[i] / 
                                              max_severity);

  for (auto renderable_geometry : mp_renderable_geometries)
    renderable_geometry->set_selected(false);
  for (auto geometry_index : m_selected_indices)
    mp_renderable_geometries[geometry_index]->set_selected(true);
  mp_viewer_widget->update();
}


//------------------------------------------------------------------------------
void 
geometry_viewer_widget::update_parallel_coords
(const pvt::data_array<float>& geometric_severities)
{
  const auto& geometries = mp_system_geometry->get_geometries();

  std::vector<float> severity_per_vertex(geometries.size());
  for (auto i = 0; i < severity_per_vertex.size(); i++)
    severity_per_vertex[i] = geometric_severities[i] / 
                             geometries[i].get_vertices().size();

  std::vector<float> severity_per_area(geometries.size());
  for (auto i = 0; i < severity_per_area.size(); i++)
    severity_per_area[i] = geometric_severities[i] / geometries[i].get_area();
  
  auto max_severity            = *std::max_element(geometric_severities.begin(), 
                                                   geometric_severities.end  ());
  auto max_severity_per_vertex = *std::max_element(severity_per_vertex .begin(), 
                                                   severity_per_vertex .end  ());
  auto max_severity_per_area   = *std::max_element(severity_per_area   .begin(), 
                                                   severity_per_area   .end  ());
  auto max_num_vertices = 0.0f;
  for (auto i = 0; i < geometries.size(); i++)
    if (geometries[i].get_vertices().size() > max_num_vertices)
      max_num_vertices = geometries[i].get_vertices().size();

  auto max_area = 0.0f;
  for (auto i = 0; i < geometries.size(); i++)
    if (geometries[i].get_area() > max_area)
      max_area = geometries[i].get_area();

  std::vector<parallel_coordinates_plot_widget::entry> entry_list;
  if (max_severity != 0.0f)
  {
    for (std::size_t i = 0; i < geometries.size(); ++i)
    {
      bool is_geometry_selected = std::find(m_selected_indices.begin(),
                                            m_selected_indices.end  (),
                                      i) != m_selected_indices.end  ();

      entry_list.push_back(parallel_coordinates_plot_widget::entry(
      { geometric_severities [i]            / max_severity, 
        severity_per_vertex  [i]            / max_severity_per_vertex, 
        severity_per_area    [i]            / max_severity_per_area, 
        geometries[i].get_vertices().size() / max_num_vertices,
        geometries[i].get_area()            / max_area },
        is_geometry_selected));
    }

    mp_parallel_coords_widget->set_entries(entry_list);
  }

  mp_parallel_coords_widget->deselect_all_entries();
  for (auto geometry_index : m_selected_indices)
    mp_parallel_coords_widget->set_entry_selected(geometry_index, true);
  mp_parallel_coords_widget->update();
}


//------------------------------------------------------------------------------
void
geometry_viewer_widget::update_table
(const pvt::data_array<float>& geometric_severities)
{
  // Iterate geometries.
  const auto& geometries = mp_system_geometry->get_geometries();
  for (std::size_t i = 0; i < geometries.size(); ++i)
  {
    auto& geometry     = geometries[i];
    auto  vertex_count = geometry.get_vertices().size();
    auto  area         = geometry.get_area();

    // Note: QTable takes ownership.
    auto name_item              
      = new QTableWidgetItem(geometry.get_name().c_str());
    auto severity_item          
      = new QTableWidgetItem(std::to_string(geometric_severities[i]).c_str());
    auto severity_per_elem_item 
      = new QTableWidgetItem(std::to_string(geometric_severities[i] / 
                                            vertex_count).c_str());
    auto severity_per_area_item 
      = new QTableWidgetItem(std::to_string(geometric_severities[i] / 
                                            area).c_str());
    auto elements_item          
      = new QTableWidgetItem(std::to_string(vertex_count).c_str());
    auto area_item              
      = new QTableWidgetItem(std::to_string(area).c_str());

    mp_table_widget->setItem(i, 0, name_item);
    mp_table_widget->setItem(i, 1, severity_item);
    mp_table_widget->setItem(i, 2, severity_per_elem_item);
    mp_table_widget->setItem(i, 3, severity_per_area_item);
    mp_table_widget->setItem(i, 4, elements_item);
    mp_table_widget->setItem(i, 5, area_item);
  }

  mp_table_widget->clearSelection();
  for (auto geometry_index : m_selected_indices)
    mp_table_widget->selectRow(geometry_index);
}


//------------------------------------------------------------------------------
void
geometry_viewer_widget::handle_geometry_selection
(int index, interactor_mode interactor_mode)
{
  // Update selected indices according to interactor mode.
  if      (interactor_mode == interactor_mode::select_one  )
  {
    m_selected_indices.clear    ();
    m_selected_indices.push_back(index);
  }
  else if (interactor_mode == interactor_mode::toggle_one  )
  {
    auto index_it = std::find(m_selected_indices.begin(), 
                              m_selected_indices.end  (), 
                              index);

    if (index_it != m_selected_indices.end())
    {
      m_selected_indices.erase(std::remove(m_selected_indices.begin(), 
                                           m_selected_indices.end  (), 
                                           index), 
                               m_selected_indices.end());
    }
    else
    {
      m_selected_indices.push_back(index);
    }
  }
  else if (interactor_mode == interactor_mode::deselect_one)
  {
    auto index_it = std::find(m_selected_indices.begin(), 
                              m_selected_indices.end  (), 
                              index);

    if (index_it != m_selected_indices.end())
    {
      m_selected_indices.erase(std::remove(m_selected_indices.begin(), 
                                           m_selected_indices.end  (), 
                                           index), 
                               m_selected_indices.end());
    }
  }
  else if (interactor_mode == interactor_mode::deselect_all)
  {
    m_selected_indices.clear();
  }

  // Update all subwidgets according to selected indices.
  for (auto renderable_geometry : mp_renderable_geometries)
    renderable_geometry->set_selected(false);

  mp_parallel_coords_widget->deselect_all_entries();
  mp_table_widget          ->blockSignals        (true);
  mp_table_widget          ->clearSelection      ();

  for (auto geometry_index : m_selected_indices)
  {
    mp_renderable_geometries[geometry_index]->set_selected      (true);
    mp_parallel_coords_widget               ->set_entry_selected(geometry_index, true);
    mp_table_widget                         ->selectRow         (geometry_index);
  }

  mp_table_widget          ->blockSignals(false);
  mp_viewer_widget         ->update  ();
  mp_parallel_coords_widget->update  ();
  mp_table_widget          ->setFocus();

}
} // namespace qpvt
