//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <vector>

#include <QMouseEvent>
#include <QToolTip>

#include <pvt/perf_data.hpp>
#include <pvt/severity_view.hpp>

#include <qpvt/widgets/squarified_cushion_treemap_widget.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
squarified_cushion_treemap_widget::squarified_cushion_treemap_widget(
 pvt::perf_data* p_perf_data,
 QWidget*        p_parent) 
: QOpenGLWidget (p_parent)
, mp_perf_data  (p_perf_data)
{
  setWindowTitle  ("Squarified Cushion Treemap");
  setGeometry     (0, 0, 256, 256);
  setMouseTracking(true);
}


//------------------------------------------------------------------------------
void 
squarified_cushion_treemap_widget::set_focus
(pvt::cnode* p_focus_cnode)
{
  if (mp_treemap)
  {
    makeCurrent();
    mp_treemap->set_root_node(p_focus_cnode);
    mp_treemap->build();
  }
  else
  {
    mp_cached_focus_node = p_focus_cnode;
  }
  update();
}


//------------------------------------------------------------------------------
void 
squarified_cushion_treemap_widget::set_metric
(const pvt::metric* p_metric)
{
  if (mp_treemap)
  {
    makeCurrent();
    update_weights(p_metric);
    mp_treemap->set_weights(&m_weights);
    mp_treemap->build();
  }
  else
  {
    mp_cached_metric = const_cast<pvt::metric*>(p_metric);
  }
  update();
}


//------------------------------------------------------------------------------
void 
squarified_cushion_treemap_widget::update_weights
(const pvt::metric* p_metric)
{
  pvt::severity_view severity_view(mp_perf_data);
  severity_view.set_metric(p_metric);
  severity_view.set_cnode(&mp_perf_data->get_cnodes()[0]);
  m_weights.resize(mp_perf_data->get_data_size());
  mp_perf_data->get_severities(severity_view, m_weights);
}


//------------------------------------------------------------------------------
void 
squarified_cushion_treemap_widget::zoom_towards
(pvt::cnode* p_cnode)
{
  if (p_cnode == nullptr)
    return;

  auto p_next_focus = p_cnode;
  if  (p_next_focus != mp_treemap->get_root_node())
  {
    while (p_next_focus->parent() != mp_treemap->get_root_node() &&
      p_next_focus->parent() != nullptr)
      p_next_focus = p_next_focus->parent();
  }

  set_focus     (p_next_focus);
  cnode_selected(p_next_focus);
}


//------------------------------------------------------------------------------
std::string 
squarified_cushion_treemap_widget::build_tooltip_string
(pvt::cnode* p_cnode)
{
  if (p_cnode == nullptr)
    return "N/A";

  auto tooltip_text = 
    p_cnode->get_name() + " (ID: " + std::to_string(p_cnode->get_id()) + ")\n"
    + "Severity : "   + std::to_string(m_weights.at(p_cnode->get_id())) + "\n"
    + "Hierarchy: \n";

  std::vector<std::string> node_names;
  node_names.push_back(p_cnode->get_name());
  auto   p_traverser_node = p_cnode->parent();
  while (p_traverser_node != nullptr)
  {
    node_names.push_back(p_traverser_node->get_name());
    p_traverser_node = p_traverser_node->parent();
  }

  for (auto i = 0; i < node_names.size(); i++)
  {
    for (auto j = 0; j <= i; j++)
      tooltip_text += "  ";

    tooltip_text += "- " + node_names[node_names.size() - 1 - i];

    if (i != node_names.size() - 1)
      tooltip_text += "\n";
  }

  return tooltip_text;
}


//------------------------------------------------------------------------------
void 
squarified_cushion_treemap_widget::initializeGL
()
{
  if (!mp_cached_focus_node)
    mp_cached_focus_node = mp_perf_data->get_root_cnodes()[0];
  if (!mp_cached_metric)
    mp_cached_metric = const_cast<pvt::metric*>(&mp_perf_data->get_metrics()[0]);

  update_weights(mp_cached_metric);
  mp_treemap = std::unique_ptr<squarified_cushion_cnode_treemap>(
    new squarified_cushion_cnode_treemap(
      Eigen::Vector2i(256, 256),
      mp_perf_data->get_cnodes(),
      &m_weights,
      mp_cached_focus_node,
      0.01,
      false));
  mp_treemap->set_scale_factor(0.75);
  mp_treemap->build();
}


//------------------------------------------------------------------------------
void 
squarified_cushion_treemap_widget::resizeGL
(int width, int height)
{
  glViewport(0, 0, width, height);
  mp_treemap->set_size(Eigen::Vector2i(width, height));
  mp_treemap->build();
}


//------------------------------------------------------------------------------
void 
squarified_cushion_treemap_widget::paintGL
()
{
  glClear(GL_COLOR_BUFFER_BIT);
  mp_treemap->render();
}


//------------------------------------------------------------------------------
bool 
squarified_cushion_treemap_widget::event
(QEvent* p_event)
{
  makeCurrent();

  if (p_event->type() == QEvent::ToolTip) 
  {
    auto p_help_event = static_cast<QHelpEvent*>(p_event);
    auto position     = p_help_event->pos();
    auto p_cnode      = mp_treemap->get_node(
      Eigen::Vector2i(position.x(), height() - position.y()));

    QToolTip::hideText();
    if (p_cnode != nullptr)
      QToolTip::showText(p_help_event->globalPos(), 
                         QString::fromStdString(build_tooltip_string(p_cnode)));
    return true;
  }
  return QWidget::event(p_event);
}


//------------------------------------------------------------------------------
void 
squarified_cushion_treemap_widget::mousePressEvent
(QMouseEvent* p_press_event)
{
  makeCurrent();

  auto position = p_press_event->localPos();
  auto p_cnode  = mp_treemap->get_node(
    Eigen::Vector2i(position.x(), height() - position.y()));

  if (p_cnode == nullptr)
    return;

  if (p_press_event->button() == Qt::MouseButton::LeftButton)
  {
    mp_treemap->mark_node(p_cnode);
    cnode_selected       (p_cnode);
  }
  else if (p_press_event->button() == Qt::MouseButton::RightButton)
  {
    zoom_towards(p_cnode);
  }
}


//------------------------------------------------------------------------------
void 
squarified_cushion_treemap_widget::wheelEvent
(QWheelEvent* p_wheel_event)
{
  makeCurrent();

  if (p_wheel_event->delta() < 0)
  {
    auto p_current_focus = mp_treemap->get_root_node();
    if  (p_current_focus != nullptr && p_current_focus->parent() != nullptr)
    {
      set_focus     (p_current_focus->parent());
      cnode_selected(p_current_focus->parent());
    }
  }
  else
  {
    auto position = p_wheel_event->pos();
    auto p_cnode  = mp_treemap->get_node(
      Eigen::Vector2i(position.x(), height() - position.y()));
    zoom_towards(p_cnode);
  }

  p_wheel_event->accept();
}

} // namespace qpvt
