//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <QAction>
#include <QCheckBox>
#include <QHboxLayout>
#include <QHeaderView>
#include <QInputDialog>
#include <QPushButton>
#include <QSortFilterProxyModel>
#include <QSplitter>
#include <QTableView>
#include <QTreeView>

#include <pvt/perf_data.hpp>
#include <pvt/suitable_3d_view_computer.hpp>

#include <qpvt/delegates/directed_variance_glyph_delegate.hpp>
#include <qpvt/delegates/severity_glyph_delegate.hpp>
#include <qpvt/models/cnode_tree_model.hpp>
#include <qpvt/models/correlated_views_model.hpp>
#include <qpvt/models/metric_tree_model.hpp>
#include <qpvt/widgets/cnode_impact_selector_widget.hpp>
#include <qpvt/widgets/perf_data_viewer_widget.hpp>

namespace qpvt {

//------------------------------------------------------------------------------
perf_data_viewer_widget::perf_data_viewer_widget
(pvt::perf_data* p_perf_data,
 type            type,
 QWidget*        p_parent,
 Qt::WindowFlags flags)
: QWidget                  (p_parent, flags)
, m_type                   (type)
, mp_perf_data             (p_perf_data)
, m_severity_view          (p_perf_data)
, mp_cnode_tree_model      (new cnode_tree_model (&m_severity_view))
, mp_metric_tree_model     (new metric_tree_model(&m_severity_view))
, mp_correlated_views_model(new correlated_views_model)
{
  setup_ui();
}


//------------------------------------------------------------------------------
perf_data_viewer_widget::~perf_data_viewer_widget
()
{
  mp_cnodes_view ->setModel(nullptr);
  mp_metrics_view->setModel(nullptr);
  destroy();
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::setup_correlated_views_model
(const std::vector<pvt::correlated_severity_view>& correlated_views)
{
  mp_correlated_views_model->setup(correlated_views);
  mp_correlation_table_view->resizeRowsToContents();
}


//------------------------------------------------------------------------------
std::pair<float, float>
perf_data_viewer_widget::get_filter_range
()
const
{
  return mp_cartesian_topology_viewer_widget->get_filter_range();
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::set_filter_range
(float min, float max)
{
  mp_cartesian_topology_viewer_widget->set_filter_range(min, max);
}


//------------------------------------------------------------------------------
bool
perf_data_viewer_widget::get_cnodes_inclusive
()
const
{
  return mp_cnodes_view->header()->visualIndex(4) == 3;
}


//------------------------------------------------------------------------------
bool
perf_data_viewer_widget::get_metrics_inclusive
()
const
{
  return mp_metrics_view->header()->visualIndex(1) == 0;
}

//------------------------------------------------------------------------------
void
perf_data_viewer_widget::handle_metric_selection_change
(const QItemSelection & selected, const QItemSelection & deselected)
{
  if (m_type == correlated_views)
  {
    const bool old_block_state_correlation_view =
      mp_correlation_table_view->selectionModel()->blockSignals(true);
    mp_correlation_table_view->selectionModel()->clear();
    mp_correlation_table_view->selectionModel()->blockSignals(
      old_block_state_correlation_view);
  }
  
  determine_selected_metrics();
  
  mp_cnode_tree_model->set_dirty();
  mp_cnode_tree_model_sort_proxy->invalidate();

  /// \todo Find a more general implementation
  if (m_type == perf_data_viewer_widget::metric_cnode_views)
    if (mp_cnode_impact_selector_widget != nullptr)
      mp_cnode_impact_selector_widget->invalidate_model();
  
  determine_selected_cnodes  ();
  update_severities_in_viewer();
  
  mp_cnodes_view->update();

  /// \todo Find a more general implementation
  if (m_type == perf_data_viewer_widget::metric_cnode_views)
    if (mp_cnode_impact_selector_widget != nullptr)
      mp_cnode_impact_selector_widget->update();
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::handle_cnode_selection_change
(const QItemSelection & selected, const QItemSelection & deselected)
{
  if (m_type == correlated_views)
  {
    const bool old_block_state_correlation_view =
      mp_correlation_table_view->selectionModel()->blockSignals(true);
    mp_correlation_table_view->selectionModel()->clear();
    mp_correlation_table_view->selectionModel()->blockSignals(
      old_block_state_correlation_view);
  }
  
  determine_selected_cnodes  ();
  update_severities_in_viewer();

  auto cnode = m_severity_view.get_cnode();
  if (mp_cnode_impact_selector_widget != nullptr && 
      cnode                           != nullptr)
  {
    mp_cnode_impact_selector_widget->select_and_focus_on_cnode(cnode);
  }
}

//------------------------------------------------------------------------------
void
perf_data_viewer_widget::handle_correlated_view_clicked
(const QModelIndex& index)
{
  // Disable signals.
  const bool old_block_state_metrics =
    mp_metrics_view->selectionModel()->blockSignals(true);
  const bool old_block_state_cnodes =
    mp_cnodes_view->selectionModel()->blockSignals(true);
  
  const pvt::correlated_severity_view& correlated_view =
    mp_correlated_views_model->get_correlated_view(index);
  const pvt::severity_view& severity_view = correlated_view.get_severity_view();
  
  // Change column order in TreeViews depending on inclusive/exclusive data.
  if (( severity_view.is_metric_total() && mp_metrics_view->header()->logicalIndex(0) != 1) ||
      (!severity_view.is_metric_total() && mp_metrics_view->header()->logicalIndex(0) != 2))
  {
    const bool old_state = mp_metrics_view->header()->blockSignals(true);
    mp_metrics_view->header()->moveSection(0, 1);
    mp_metrics_view->header()->blockSignals(old_state);
  }
  
  if ((severity_view .is_cnode_total() && !get_cnodes_inclusive()) ||
      (!severity_view.is_cnode_total() &&  get_cnodes_inclusive()))
  {
    const bool old_state = mp_cnodes_view->header()->blockSignals(true);
    mp_cnodes_view->header()->moveSection(3, 4);
    mp_cnodes_view->header()->blockSignals(old_state);
  }
  
  assert(severity_view.is_valid());

  select_and_focus_on_metric(severity_view.get_metric());
  select_and_focus_on_cnode (severity_view.get_cnode ());
  
  // Update 3D view.
  pvt::data_array<double> severities(mp_perf_data->get_data_size());
  mp_perf_data->get_severities(severity_view, severities);
  mp_cartesian_topology_viewer_widget->map_data_to_topology(severities);
  if (mp_geometry_viewer_widget)
    mp_geometry_viewer_widget->map_geometric_severities(severities);

  // Enable signals.
  mp_metrics_view->selectionModel()->blockSignals(old_block_state_metrics);
  mp_cnodes_view ->selectionModel()->blockSignals(old_block_state_cnodes );
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::handle_cnode_selected_by_impact
(pvt::cnode* p_cnode,
 bool        cnode_total)
{
  // Change column order in TreeViews depending on inclusive / exclusive data.
  if (get_cnodes_inclusive() != cnode_total)
    mp_cnodes_view->header()->moveSection(3, 4);
  
  assert(p_cnode != nullptr);
  select_and_focus_on_cnode(p_cnode);
  
  // Update 3D view.
  pvt::data_array<double> severities(mp_perf_data->get_data_size());
  mp_perf_data->get_severities(m_severity_view, severities);
  mp_cartesian_topology_viewer_widget->map_data_to_topology(severities);
  if (mp_geometry_viewer_widget)
    mp_geometry_viewer_widget->map_geometric_severities(severities);
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::cnodes_view_section_moved
(int logical_index,
 int old_visual_index,
 int new_visual_index)
{
  const bool valid_columns = logical_index    == 4 || logical_index    == 5;
  const bool valid_targets = new_visual_index == 3 || new_visual_index == 4;

  if (valid_columns && valid_targets)
    handle_metric_selection_change(QItemSelection(), QItemSelection());
  else
  {
    const bool old_state = mp_cnodes_view->header()->blockSignals(true);
    mp_cnodes_view->header()->moveSection (new_visual_index, old_visual_index);
    mp_cnodes_view->header()->blockSignals(old_state);
  }
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::metrics_view_section_moved
(int logical_index,
 int old_visual_index,
 int new_visual_index)
{
  const bool valid_columns = logical_index    == 1 || logical_index    == 2;
  const bool valid_targets = new_visual_index == 0 || new_visual_index == 1;

  if (valid_columns && valid_targets)
    handle_metric_selection_change(QItemSelection(), QItemSelection());
  else
  {
    const bool old_state = mp_metrics_view->header()->blockSignals(true);
    mp_metrics_view->header()->moveSection (new_visual_index, old_visual_index);
    mp_metrics_view->header()->blockSignals(old_state);
  }
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::filter_suitable_3d_view
(bool checked)
{
  if (mp_filter_widget == nullptr)
  {
    mp_filter_widget = new QWidget;
    mp_filter_widget->setWindowModality(Qt::ApplicationModal);
    mp_filter_widget->setWindowTitle   ("Filter variation-based suggestion");
    mp_filter_widget->setLayout        (new QVBoxLayout);
    mp_filter_widget->layout           ()->addWidget(
      new QLabel("Tick to exclude dimension:"));

    for (const std::string& curr_dimension_name :
         mp_perf_data->get_cartesian_topologies()[0].get_dimension_names())
    {
      QCheckBox* p_checkbox = new QCheckBox(curr_dimension_name.c_str());
      m_filter_check_boxes.push_back(p_checkbox);
      mp_filter_widget->layout()->addWidget(p_checkbox);
    }
    QWidget* p_button_bar = new QWidget;
    mp_filter_widget->layout()->addWidget(p_button_bar);
    p_button_bar->setLayout(new QHBoxLayout);
    p_button_bar->layout()->setMargin(0);
    
    QPushButton* p_ok_button     = new QPushButton("Ok");
    QPushButton* p_cancel_button = new QPushButton("Cancel");
    p_button_bar->layout()->addWidget(p_ok_button);
    p_button_bar->layout()->addWidget(p_cancel_button);
    
    QObject::connect(p_ok_button, 
                     &QPushButton::clicked,
                     this, 
                     &perf_data_viewer_widget::filter_ok_clicked);

    QObject::connect(p_cancel_button, 
                     &QPushButton::clicked,
                     this, 
                     &perf_data_viewer_widget::filter_cancel_clicked);
  }
  
  for (std::size_t i = 0; i < m_filter_check_boxes.size(); ++i)
  {
    m_filter_check_boxes[i]->setChecked(mp_cnode_tree_model->
      get_suitable_3d_view_computer().get_filter_dimension_enabled(i));
  }
  
  mp_filter_widget->show();
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::filter_ok_clicked
(bool checked)
{
  for (std::size_t i = 0; i < m_filter_check_boxes.size(); ++i)
  {
    mp_cnode_tree_model->get_suitable_3d_view_computer().
      set_filter_dimension_enabled(
        i,
        m_filter_check_boxes[i]->isChecked());
  }
  
  mp_cnode_tree_model->set_dirty();
  mp_cnode_tree_model_sort_proxy->invalidate();
  
  mp_filter_widget->hide();
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::filter_cancel_clicked
(bool checked)
{
  mp_filter_widget->hide();
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::set_threshold_suitable_3d_view_clicked
(bool checked)
{
  bool         ok        = false;
  const int    decimals  = 5;
  const double threshold =
    QInputDialog::getDouble(
      this,
      "Threshold for variation-based suggestion",
      "Min. variation",
      mp_cnode_tree_model->get_suitable_3d_view_computer().get_threshold(),
      0.0,
      1.0,
      decimals,
      &ok);

  if (ok)
  {
    mp_cnode_tree_model->get_suitable_3d_view_computer().
      set_threshold(threshold);
    mp_cnode_tree_model->set_dirty();
    mp_cnode_tree_model_sort_proxy->invalidate();
  }
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::fade_out_directed_variance_clicked
(bool checked)
{
  mp_directed_variance_glyph_delegate->set_fade_out_enabled(checked);
  mp_cnode_tree_model->set_dirty();
  mp_cnode_tree_model_sort_proxy->invalidate();
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::setup_ui
()
{
  // Central widget / window.
  QSplitter* p_metric_cnode_splitter = new QSplitter;
  QSplitter* p_main_splitter         = new QSplitter;

  p_metric_cnode_splitter->setOrientation(Qt::Vertical);

  if (m_type == metric_cnode_views)
    p_main_splitter->addWidget(p_metric_cnode_splitter);
  else if (m_type == correlated_views)
  {
    QWidget* p_tab_container = new QWidget;
    p_tab_container->setLayout(new QVBoxLayout);
    p_tab_container->layout   ()->setContentsMargins(12, 12, 6, 12);
    
    QTabWidget* p_tab_widget = new QTabWidget;
    
    mp_correlation_table_view = new QTableView;
    mp_correlation_table_view->setSelectionBehavior(
      QAbstractItemView::SelectRows);
    mp_correlation_table_view->setModel(mp_correlated_views_model);
    
    mp_correlation_table_view->horizontalHeader()->resizeSection(0,  82);
    mp_correlation_table_view->horizontalHeader()->resizeSection(1, 120);
    mp_correlation_table_view->horizontalHeader()->resizeSection(2, 164);
    mp_correlation_table_view->horizontalHeader()->resizeSection(3, 164);

    mp_correlation_table_view->setAlternatingRowColors(true);
    
    p_tab_widget->addTab(mp_correlation_table_view, "Correlated Views"    );
    p_tab_widget->addTab(p_metric_cnode_splitter  , "Metric / Code Region");
    
    p_tab_container->layout()->addWidget(p_tab_widget);
    p_main_splitter->addWidget(p_tab_container);
    
    QObject::connect(mp_correlation_table_view, 
                     &QTableView::clicked,
                     this, 
                     &perf_data_viewer_widget::handle_correlated_view_clicked);
  }

  setLayout(new QHBoxLayout);
  layout()->setMargin (0);
  layout()->setSpacing(0);
  layout()->addWidget (p_main_splitter);
  
  // setup, connect metric tree view, container
  mp_metrics_view = new QTreeView(this);
  QPalette* p_palette = new QPalette(mp_metrics_view->palette());
  p_palette->setBrush(QPalette::AlternateBase, QColor(245, 245, 245));
  mp_metrics_view->setPalette(*p_palette);
  
  mp_metrics_view->setWindowTitle         (QObject::tr("Performance Metrics"));
  mp_metrics_view->setModel               (mp_metric_tree_model);
  mp_metrics_view->header                 ()->moveSection(0, 3);
  mp_metrics_view->setAlternatingRowColors(true);

  QObject::connect(mp_metrics_view->selectionModel(),
                   &QItemSelectionModel::selectionChanged,
                   this,
                   &perf_data_viewer_widget::handle_metric_selection_change);
  QObject::connect(mp_metrics_view->header(),
                   &QHeaderView::sectionMoved,
                   this,
                   &perf_data_viewer_widget::metrics_view_section_moved);
  
  QWidget* p_metrics_view_container = new QWidget(this);
  p_metrics_view_container->setLayout(new QHBoxLayout);
  p_metrics_view_container->layout()->setContentsMargins(12, 12, 6, 12);
  p_metrics_view_container->layout()->addWidget(mp_metrics_view);
  p_metric_cnode_splitter->addWidget(p_metrics_view_container);
  
  // Setup, connect cnode tree view, container.
  mp_cnodes_view = new QTreeView(this);
  mp_cnodes_view->setPalette    (*p_palette);
  mp_cnodes_view->setWindowTitle(QObject::tr("Call Tree"));

  mp_cnode_tree_model_sort_proxy = new QSortFilterProxyModel;
  mp_cnode_tree_model_sort_proxy->setSourceModel(mp_cnode_tree_model);
  mp_cnode_tree_model_sort_proxy->setSortRole   (cnode_tree_model::sort_role);

  mp_cnodes_view->setModel                (mp_cnode_tree_model_sort_proxy);
  mp_cnodes_view->setSortingEnabled       (true);
  mp_cnodes_view->sortByColumn            (4, Qt::DescendingOrder);
  mp_cnodes_view->setItemDelegateForColumn(1,
    new severity_glyph_delegate(mp_cnodes_view));
  mp_directed_variance_glyph_delegate =
    new directed_variance_glyph_delegate(mp_perf_data, mp_cnodes_view);
  mp_cnodes_view->setItemDelegateForColumn(3,
    mp_directed_variance_glyph_delegate);
  mp_cnodes_view->header()->moveSection(0, 6);
  mp_cnodes_view->setAlternatingRowColors(true);
  
  QObject::connect(mp_cnodes_view->selectionModel(),
                   &QItemSelectionModel::selectionChanged,
                   this,
                   &perf_data_viewer_widget::handle_cnode_selection_change);
  QObject::connect(mp_cnodes_view->header(),
                   &QHeaderView::sectionMoved,
                   this,
                   &perf_data_viewer_widget::cnodes_view_section_moved);
  
  // setup squarified cushion treemap widget
  if (m_type == metric_cnode_views)
  {
    mp_squarified_cushion_treemap_widget =
      new squarified_cushion_treemap_widget(mp_perf_data, this);
    mp_squarified_cushion_treemap_widget->show();
    mp_squarified_cushion_treemap_widget->setAttribute(Qt::WA_DeleteOnClose);

    QObject::connect(
      mp_squarified_cushion_treemap_widget,
      &squarified_cushion_treemap_widget::cnode_selected,
      [&] (pvt::cnode* node)
      {
        select_and_focus_on_cnode(node);
      });
    QObject::connect(
      mp_metrics_view->selectionModel(),
      &QItemSelectionModel::selectionChanged,
      [&] (const QItemSelection & selected, const QItemSelection & deselected) 
      {
        mp_squarified_cushion_treemap_widget->set_metric(m_severity_view.get_metric());
      });
  }

  if (m_type != metric_cnode_views)
  {
    QWidget* p_cnode_view_container = new QWidget(this);
    p_cnode_view_container->setLayout (new QHBoxLayout);
    p_cnode_view_container->layout    ()->setContentsMargins(12, 12, 6, 12);
    p_cnode_view_container->layout    ()->addWidget(mp_cnodes_view);
    p_metric_cnode_splitter->addWidget(p_cnode_view_container);
  }
  else
  {
    QWidget* p_cnode_view_container = new QWidget(this);
    p_cnode_view_container->setLayout(new QHBoxLayout);
    p_cnode_view_container->layout()->setContentsMargins(12, 12, 6, 12);

    QTabWidget* p_tab_widget = new QTabWidget;
    p_tab_widget->addTab(mp_cnodes_view                      , "Hierarchy");
    p_tab_widget->addTab(mp_squarified_cushion_treemap_widget, "Treemap"  );
    p_cnode_view_container->layout()->addWidget(p_tab_widget);
    
    p_metric_cnode_splitter->addWidget(p_cnode_view_container);
  }

  if (mp_perf_data->get_cartesian_topologies()[0].
    get_num_dimensions() == 3)
  {
    mp_cnodes_view->header()->resizeSection(1, 32);
    mp_cnodes_view->header()->resizeSection(3, 56);
  }
  else if (mp_perf_data->get_cartesian_topologies()[0].
    get_num_dimensions() == 7)
  {
    mp_cnodes_view->header()->resizeSection(0, 781);
    mp_cnodes_view->header()->resizeSection(1, 44);
    mp_cnodes_view->header()->resizeSection(3, 128);
  }

  mp_metrics_view->header()->resizeSection(1, 68);
  mp_metrics_view->header()->resizeSection(2, 68);
  mp_metrics_view->header()->resizeSection(3, 44);
  
  mp_cnodes_view ->header()->resizeSection(2, 33);
  mp_cnodes_view ->header()->resizeSection(4, 70);
  mp_cnodes_view ->header()->resizeSection(5, 70);
  mp_cnodes_view ->header()->resizeSection(6, 33);

  if (m_type == correlated_views)
    mp_correlation_table_view->setPalette(*p_palette);

  auto p_viewer_container_widget = new QWidget(this);
  auto p_viewer_tab_widget       = new QTabWidget;
  p_viewer_container_widget->setLayout(new QHBoxLayout);
  p_viewer_container_widget->layout()->addWidget(p_viewer_tab_widget);
  p_main_splitter->addWidget(p_viewer_container_widget);

  // Setup topology viewer widget.
  if (mp_perf_data->get_cartesian_topologies().size() > 0)
  {
    mp_cartesian_topology_viewer_widget =
      new cartesian_topology_viewer_widget(
        const_cast<pvt::cartesian_topology*>(&mp_perf_data->get_cartesian_topologies()[0]),
          m_type == metric_cnode_views ?
            cartesian_topology_viewer_widget::with_find_correlated_view_button :
              cartesian_topology_viewer_widget::without_find_correlated_view_button);
    p_viewer_tab_widget->addTab(mp_cartesian_topology_viewer_widget, "Cartesian Topology");

    QObject::connect(
      mp_cartesian_topology_viewer_widget,
      &cartesian_topology_viewer_widget::filter_range_from_other_requested,
      [this] ()
      {
        emit filter_range_from_other_requested();
      });
    
    if (mp_perf_data->get_cartesian_topologies()[0].get_num_dimensions() == 7)
      p_main_splitter->setSizes({{10000, 12000}});
    else
      p_main_splitter->setSizes({{10000, 16180}});
  }

  // setup geometry viewer widget.
  if (mp_perf_data->get_system_geometries().size() > 0)
  {
    mp_geometry_viewer_widget = new geometry_viewer_widget(
      const_cast<pvt::system_geometry*>(&mp_perf_data->get_system_geometries()[0]));
    p_viewer_tab_widget->addTab(mp_geometry_viewer_widget, "Geometry");
  }
  mp_cnodes_view->setContextMenuPolicy(Qt::ActionsContextMenu);
  if (m_type == metric_cnode_views)
  {
    QObject::connect(mp_cartesian_topology_viewer_widget,
                     &cartesian_topology_viewer_widget::find_correlated_views_clicked,
                     [this](){emit find_correlated_views_requested();});
    
    
    QAction* p_cnode_tree_view_filter_3d_action =
      new QAction("Filter variation-based suggestion ...",
                  mp_cnodes_view);
    mp_cnodes_view->addAction(p_cnode_tree_view_filter_3d_action);
    QObject::connect(p_cnode_tree_view_filter_3d_action,
                     &QAction::triggered,
                     this,
                     &perf_data_viewer_widget::filter_suitable_3d_view);
    
    QAction* p_cnode_tree_view_suitable_3d_threshold_action =
      new QAction("Set threshold for variation-based suggestion ...",
                  mp_cnodes_view);
    mp_cnodes_view->addAction(p_cnode_tree_view_suitable_3d_threshold_action);
    QObject::connect(p_cnode_tree_view_suitable_3d_threshold_action,
                     &QAction::triggered,
                     this,
                     &perf_data_viewer_widget::set_threshold_suitable_3d_view_clicked);
    
    if (m_type == metric_cnode_views)
    {
      QAction* p_cnode_impact_selector_action = new QAction("Open Cnode Explorer ...",
                                                            mp_cnodes_view);
      mp_cnodes_view->addAction(p_cnode_impact_selector_action);
      QObject::connect(
        p_cnode_impact_selector_action,
        &QAction::triggered,
        [=] (bool checked)
        {
          open_cnode_impact_selector();
        });
    }
  }
  else
  {
    QObject::connect(
      mp_cartesian_topology_viewer_widget,
      &cartesian_topology_viewer_widget::find_correlated_views_clicked,
      [this] ()
       {
         hide();
       });
    mp_cnodes_view->header()->hideSection(6);
  }
  
  QAction* p_fade_out_directed_variance_action =
    new QAction("Fade out directed variation", mp_cnodes_view);
  p_fade_out_directed_variance_action->setCheckable(true);
  p_fade_out_directed_variance_action->setChecked(
    mp_directed_variance_glyph_delegate->get_fade_out_enabled());
  mp_cnodes_view->addAction(p_fade_out_directed_variance_action);
  QObject::connect(p_fade_out_directed_variance_action,
                   &QAction::triggered,
                   this,
                   &perf_data_viewer_widget::fade_out_directed_variance_clicked);
}


//------------------------------------------------------------------------------
/// \todo Check if there is a better way to handle selected metrics
//------------------------------------------------------------------------------
void
perf_data_viewer_widget::determine_selected_metrics
()
{
  m_severity_view.set_metric_total(get_metrics_inclusive());

  for (const QModelIndex& curr_model_index :
       mp_metrics_view->selectionModel()->selectedRows())
  {
    const QVariant curr_value(
      curr_model_index.data(metric_tree_model::get_perf_data_item_pointer_role));

    pvt::metric* const p_curr_metric = 
      static_cast<pvt::metric*>(curr_value.value<void*>());

    m_severity_view.set_metric(p_curr_metric);
  }
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::determine_selected_cnodes
()
{
  m_severity_view.set_cnodes_total(get_cnodes_inclusive());

  for (const QModelIndex& curr_model_index :
       mp_cnodes_view->selectionModel()->selectedRows())
  {
    const QVariant curr_value(
      curr_model_index.data(cnode_tree_model::get_perf_data_item_pointer_role));

    pvt::cnode* const p_curr_cnode = 
      static_cast<pvt::cnode*>(curr_value.value<void*>());

    m_severity_view.set_cnode(p_curr_cnode);
  }
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::update_severities_in_viewer
()
{
  pvt::data_array<double> accumulated_severities(mp_perf_data->get_data_size());

  m_severity_view.get_performance_data()->get_severities(
    m_severity_view,
    accumulated_severities);

  mp_cartesian_topology_viewer_widget->map_data_to_topology(
    accumulated_severities);

  if (mp_geometry_viewer_widget)
    mp_geometry_viewer_widget->map_geometric_severities(accumulated_severities);
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::open_cnode_impact_selector
()
{
  if (m_type == metric_cnode_views)
  {
    mp_cnode_impact_selector_widget =
    new cnode_impact_selector_widget(mp_perf_data,
                                     &m_severity_view,
                                     mp_cnode_tree_model);

    QObject::connect(mp_cnode_impact_selector_widget,
                     &cnode_impact_selector_widget::cnode_selected,
                     this,
                     &perf_data_viewer_widget::handle_cnode_selected_by_impact);

    auto cnode = m_severity_view.get_cnode();
    if  (cnode != nullptr)
    {
      mp_cnode_impact_selector_widget->select_and_focus_on_cnode(cnode);
    }

    mp_cnode_impact_selector_widget->show();
  }
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::select_and_focus_on_cnode
(const pvt::cnode* p_cnode)
{
  // Find, select cnode in CnodesView.
  const QModelIndexList found_cnode_indices_for_selection(
    mp_cnode_tree_model_sort_proxy->match(
      mp_cnode_tree_model->index(0,0),
      cnode_tree_model::search_perf_data_item_role,
      QVariant::fromValue(p_cnode->get_id()),
      1,
      Qt::MatchExactly | Qt::MatchRecursive));

  const QModelIndex cnode_index_for_selection(
    found_cnode_indices_for_selection.size() > 0 ?
    found_cnode_indices_for_selection[0] :
    QModelIndex());
  
  mp_cnodes_view->clearSelection();
  mp_cnodes_view->selectionModel()->select(cnode_index_for_selection,
                                           QItemSelectionModel::Select |
                                           QItemSelectionModel::Rows);
  
  determine_selected_cnodes  ();
  update_severities_in_viewer();
  
  // expand cnode, scroll to it in CnodesView
  mp_cnodes_view->collapseAll();
  QModelIndex cnode_index_to_expand = cnode_index_for_selection.parent();
  while (cnode_index_to_expand.isValid())
  {
    mp_cnodes_view->expand(cnode_index_to_expand);
    cnode_index_to_expand = cnode_index_to_expand.parent();
  }
  mp_cnodes_view->scrollTo(cnode_index_for_selection, 
                           QAbstractItemView::PositionAtCenter);
}


//------------------------------------------------------------------------------
void
perf_data_viewer_widget::select_and_focus_on_metric
(const pvt::metric* p_metric)
{
  // Find, select metric in MetricsView.
  const QModelIndexList found_metric_indices_for_selection(
    mp_metric_tree_model->match(mp_metric_tree_model->index(0,0),
                                metric_tree_model::search_perf_data_item_role,
                                QVariant::fromValue(p_metric->get_id()),
                                1,
                                Qt::MatchExactly | Qt::MatchRecursive));
  const QModelIndex metric_index_for_selection(
    found_metric_indices_for_selection.size() > 0 ?
    found_metric_indices_for_selection[0] :
    QModelIndex());
  
  mp_metrics_view->clearSelection();
  mp_metrics_view->selectionModel()->select(metric_index_for_selection,
                                            QItemSelectionModel::Select |
                                            QItemSelectionModel::Rows);
  
  determine_selected_metrics();
  
  mp_cnode_tree_model->set_dirty();
  mp_cnode_tree_model_sort_proxy->invalidate();
  
  determine_selected_cnodes();
  update_severities_in_viewer();
  
  mp_cnodes_view->update();
  
  // Expand metric, scroll to it in MetricsView.
  mp_metrics_view->collapseAll();

  QModelIndex metric_index_to_expand = metric_index_for_selection.parent();
  while (metric_index_to_expand.isValid())
  {
    mp_metrics_view->expand(metric_index_to_expand);
    metric_index_to_expand = metric_index_to_expand.parent();
  }
  mp_metrics_view->scrollTo(metric_index_for_selection,
                            QAbstractItemView::PositionAtCenter);
}

} // namespace qpvt
