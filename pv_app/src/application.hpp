//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PV_APP_APPLICATION_HPP_
#define PV_APP_APPLICATION_HPP_

#include <vector>

#include <pvt/correlated_severity_view.hpp>
#include <pvt/perf_data.hpp>

class QSplitter;

namespace qpvt {
  class perf_data_viewer_widget;
}

namespace pv_app {

//------------------------------------------------------------------------------
class application
{
public:
  int run(int& argc, char** argv);
  
private:
  void setup_opengl_surface_format   ();
  void setup_ui                      ();
  
  void find_correlated_views         ();
  void assign_filter_range_from_other(
    qpvt::perf_data_viewer_widget* p_source_perf_data_viewer_widget,
    qpvt::perf_data_viewer_widget* p_target_perf_data_viewer_widget);

  pvt::perf_data                             m_perf_data;
  std::vector<pvt::correlated_severity_view> m_correlated_views;

  qpvt::perf_data_viewer_widget* mp_perf_data_viewer_widget_top    = nullptr;
  qpvt::perf_data_viewer_widget* mp_perf_data_viewer_widget_bottom = nullptr;
  QSplitter*                     mp_main_splitter                  = nullptr;
};

} // namespace pv_app

#endif // #ifndef PV_APP_APPLICATION_HPP_
