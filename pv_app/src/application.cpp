//------------------------------------------------------------------------------
// pv_app performance visualization application
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cassert>
#include <iostream>

#include <QApplication>
#include <QHeaderView>
#include <QProgressDialog>
#include <QSplitter>
#include <QSurfaceFormat>

#include <pvt/cnode.hpp>
#include <pvt/correlation_computer.hpp>
#include <pvt/metric.hpp>
#include <pvt/severity_view.hpp>
#include <pvt/rendering/camera.hpp>

#include <qpvt/widgets/perf_data_viewer_widget.hpp>

#include "application.hpp"

namespace pv_app {

//------------------------------------------------------------------------------
int
application::run
(int&   argc,
 char** argv)
{
  if (argc < 2)
  {
    std::cerr << "No filename given." << std::endl;
    return EXIT_FAILURE;
  }
  
  setup_opengl_surface_format();
  QApplication app(argc, argv);

  const bool loaded = m_perf_data.load(argv[1]);
  if (!loaded)
    return 0;
  
  setup_ui();
  
  return app.exec();
}


//------------------------------------------------------------------------------
void
application::setup_opengl_surface_format
()
{
  QSurfaceFormat format;
  format.setProfile     (QSurfaceFormat::CoreProfile );
  format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
  format.setSamples     (9);
  format.setVersion     (3, 2);
  QSurfaceFormat::setDefaultFormat(format);
}


//------------------------------------------------------------------------------
void
application::setup_ui
()
{
  mp_main_splitter = new QSplitter;
  mp_main_splitter->setWindowTitle("pv_app");
  mp_main_splitter->setOrientation(Qt::Vertical);
  
  mp_perf_data_viewer_widget_top    = new qpvt::perf_data_viewer_widget(
    &m_perf_data,
    qpvt::perf_data_viewer_widget::metric_cnode_views);

  mp_perf_data_viewer_widget_bottom = new qpvt::perf_data_viewer_widget(
    &m_perf_data,
    qpvt::perf_data_viewer_widget::correlated_views);
  
  QObject::connect(
    mp_perf_data_viewer_widget_top,
    &qpvt::perf_data_viewer_widget::find_correlated_views_requested,
    [this] ()
    {
      find_correlated_views();
    });
  
  QObject::connect(
    mp_perf_data_viewer_widget_top,
    &qpvt::perf_data_viewer_widget::filter_range_from_other_requested,
    [this] ()
    {
      assign_filter_range_from_other(mp_perf_data_viewer_widget_bottom,
                                     mp_perf_data_viewer_widget_top);
    });

  QObject::connect(
    mp_perf_data_viewer_widget_bottom,
    &qpvt::perf_data_viewer_widget::filter_range_from_other_requested,
    [this]()
    {
      assign_filter_range_from_other(mp_perf_data_viewer_widget_top,
                                     mp_perf_data_viewer_widget_bottom);
    });
  
  pvt::camera* p_camera_bottom =
    mp_perf_data_viewer_widget_bottom->get_camera();
  mp_perf_data_viewer_widget_bottom->set_camera(
    mp_perf_data_viewer_widget_top ->get_camera());
  delete p_camera_bottom;
  
  QObject::connect(mp_perf_data_viewer_widget_top   ->get_viewer(),
                   SIGNAL(view_updated()),
                   mp_perf_data_viewer_widget_bottom->get_viewer(),
                   SLOT  (update()));
  QObject::connect(mp_perf_data_viewer_widget_bottom->get_viewer(),
                   SIGNAL(view_updated()),
                   mp_perf_data_viewer_widget_top   ->get_viewer(),
                   SLOT  (update()));
  
  mp_main_splitter->addWidget(mp_perf_data_viewer_widget_top   );
  mp_main_splitter->addWidget(mp_perf_data_viewer_widget_bottom);
  
  mp_perf_data_viewer_widget_bottom->hide();
  
  mp_main_splitter->setGeometry(0, 0, 1440, 873);
  //mp_main_splitter->setGeometry(0, 0, 1075, 536);

  mp_main_splitter->show();
}


//------------------------------------------------------------------------------
void
application::find_correlated_views
()
{
  const pvt::severity_view& reference_severity_view =
    mp_perf_data_viewer_widget_top->get_severity_view();
  
  if (!reference_severity_view.is_valid())
    return;
  
  QProgressDialog process_dialog("Computing correlations ...", QString(), 0, -1);
  process_dialog.setModal  (true);
  process_dialog.setMinimum(0   );
  process_dialog.setMaximum(100 );
  process_dialog.show      ();

  QApplication::processEvents();
  m_correlated_views = m_perf_data.get_correlated_views(reference_severity_view, 
  [&](double progress_percentage)
  {
    process_dialog.setValue(progress_percentage * 100);
    QApplication::processEvents();
  });
  process_dialog.setMinimum(-1);
  QApplication::processEvents();
  
  sort(m_correlated_views.begin(), m_correlated_views.end());
  
  mp_perf_data_viewer_widget_bottom->setup_correlated_views_model(
    m_correlated_views);
  mp_perf_data_viewer_widget_bottom->show();

  mp_main_splitter->setSizes({{
    mp_main_splitter->size().height() / 2,
    mp_main_splitter->size().height() / 2}});
  
  process_dialog.close();
}


//------------------------------------------------------------------------------
void
application::assign_filter_range_from_other
(qpvt::perf_data_viewer_widget* p_source_perf_data_viewer_widget,
 qpvt::perf_data_viewer_widget* p_target_perf_data_viewer_widget)
{
  auto range = p_source_perf_data_viewer_widget->get_filter_range();
  p_target_perf_data_viewer_widget->set_filter_range(range.first, range.second);
}

} // namespace pv_app
