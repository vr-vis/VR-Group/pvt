//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_HPP_
#define PVT_PERF_DATA_HPP_

#include <iostream>
#include <functional>
#include <memory>
#include <string>
#include <vector>

#include <fftw3.h>

#include <pvt/api.hpp>
#include <pvt/cartesian_topology.hpp>
#include <pvt/cnode.hpp>
#include <pvt/data_array.hpp>
#include <pvt/export_flags.hpp>
#include <pvt/metric.hpp>
#include <pvt/sysnode.hpp>
#include <pvt/system_geometry.hpp>
#include <pvt/io/perf_data_file.hpp>

namespace pvt {

class correlated_severity_view;
class directed_variance;
class fft;
class fft_statistics;
class severity_view;

//------------------------------------------------------------------------------
class PVT_API perf_data
{
public:
  bool load (const std::string& filename);
  bool write(const std::string& filename, export_flags export_flags)
  {
    std::vector<metric const*> metric_refs;
    std::vector<cnode  const*> cnode_refs;

    for (auto& current_metric : m_metrics)
      metric_refs.push_back(&current_metric);
    for (auto& current_cnode : m_cnodes)
      cnode_refs.push_back(&current_cnode);

    return write(filename, export_flags, metric_refs, cnode_refs);
  }
  bool write(const std::string& filename, export_flags export_flags,
             const std::vector<const metric*>& metric_ids,
             const std::vector<const cnode *>& cnode_ids) const;
  
  template<typename type>
  bool attach_to_existing(type* perf_data_object);

  std::size_t                            get_data_size           () const
  {
    return m_data_size;
  }
  const std::vector<cnode>&              get_cnodes              () const
  {
    return m_cnodes;
  }
  const std::vector<cnode*>&             get_root_cnodes         () const
  {
    return m_root_cnodes;
  }
  const std::vector<metric>&             get_metrics             () const
  {
    return m_metrics;
  }
  const std::vector<metric*>&            get_root_metrics        () const
  {
    return m_root_metrics;
  }
  const std::vector<sysnode>&            get_sysnodes            () const
  {
    return m_sysnodes;
  }
  const std::vector<sysnode*>&           get_root_sysnodes       () const
  {
    return m_root_sysnodes;
  }
  const std::vector<cartesian_topology>& get_cartesian_topologies() const
  {
    return m_cartesian_topologies;
  }
  const std::vector<system_geometry>&    get_system_geometries   () const
  {
    return m_system_geometries;
  }
  const std::string&                     get_filename            () const;

  std::vector<severity_view>             get_all_severity_views  ();

  std::vector<correlated_severity_view>  get_correlated_views    (
    const severity_view&        severity_view, 
    std::function<void(double)> progress_callback     = nullptr,
    double                      correlation_threshold = 0.75);

  template <typename T>
  const std::vector<T*>&                 get_root_perf_data_items() const;
  
  double get_severity              (const severity_view& severity_view ) const;
  void   get_severities            (const severity_view& severity_view,
                                    data_array<double>&  severities    ) const;
  void   get_fft                   (const severity_view& severity_view,
                                    fft&                 fft           ) const;
  void   get_fft_statistics        (const severity_view& severity_view, 
                                    fft_statistics&      fft_statistics) const;
  void   get_directed_variance     (const severity_view& severity_view,
                                    directed_variance&   directed_variance) const;
  double get_mean                  (const severity_view& severity_view) const;
  double get_variance              (const severity_view& severity_view) const;
  double get_standard_deviation    (const severity_view& severity_view) const;
  double get_autocorrelation_off_dc(const severity_view& severity_view) const; 
  
private:
  bool initialize();

  std::unique_ptr<perf_data_file> mp_perf_data_file;
  std::size_t                     m_data_size = 0;
  std::vector<cnode>              m_cnodes;
  std::vector<cnode*>             m_root_cnodes;
  std::vector<metric>             m_metrics;
  std::vector<metric*>            m_root_metrics;
  std::vector<sysnode>            m_sysnodes;
  std::vector<sysnode*>           m_root_sysnodes;
  std::vector<cartesian_topology> m_cartesian_topologies;
  std::vector<system_geometry>    m_system_geometries;
};


//------------------------------------------------------------------------------
template <>
inline
const std::vector<metric*>&
perf_data::get_root_perf_data_items<metric>
()
const
{
  return m_root_metrics;
}


//------------------------------------------------------------------------------
template <>
inline
const std::vector<cnode*>&
perf_data::get_root_perf_data_items<cnode>
()
const
{
  return m_root_cnodes;
}


//------------------------------------------------------------------------------
template <>
inline
const std::vector<sysnode*>&
perf_data::get_root_perf_data_items<sysnode>
()
const
{
  return m_root_sysnodes;
}
} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_HPP_
