//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_CORRELATION_COMPUTER_HPP_
#define PVT_CORRELATION_COMPUTER_HPP_

#include <string>

#include <fftw3.h>

#include <pvt/api.hpp>
#include <pvt/data_array.hpp>
#include <pvt/dft_inverse.hpp>
#include <pvt/fft.hpp>
#include <pvt/severity_view.hpp>

namespace pvt {

class cartesian_topology;
class cnode;
class metric;
class perf_data;

//------------------------------------------------------------------------------
class PVT_API correlation_computer
{
public:
  correlation_computer(const severity_view&      severity_view,
                       const cartesian_topology* p_cartesian_topology);
  
  double compute_correlation_coefficient(const severity_view& severity_view);
  
  /// \todo Put back into ComputeMaxCrosscorrelation, placed here for test
  double get_max_element(const data_array<double>::const_iterator& begin,
                         const data_array<double>::const_iterator& end)
  const
  {
    const data_array<double>::const_iterator found =
      std::max_element(begin,
                       end,
                       [](double a, double b){return fabs(a) < fabs(b);});
    return found == end ? 0.0 : *found;
  }
  
private:
  double compute_max_cross_correlation();
  
  const perf_data*    mp_perf_data;
  
  fft                 m_source_fft;
  fft                 m_other_fft;
  
  fft                 m_fft_product_buffer;
  data_array<double>  m_inverse_fft_product_buffer;
  
  dft_inverse         m_dft_inverse;
  
  double              m_auto_correlation_source;
};

} // namespace pvt

#endif // #ifndef PVT_CORRELATION_COMPUTER_HPP_
