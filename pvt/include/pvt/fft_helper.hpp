//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_FFT_HELPER_HPP_
#define PVT_FFT_HELPER_HPP_

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <pvt/api.hpp>
#include <pvt/cartesian_topology_index.hpp>
#include <pvt/data_array.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API fft_helper
{
public:
  static
  std::vector<std::size_t> get_dim_sizes_for_fft
  (const cartesian_topology_index& cart_topo_idx)
  {
    std::vector<std::size_t> dim_sizes(cart_topo_idx.get_dimension_sizes());
    std::for_each(dim_sizes.begin(),
                  dim_sizes.end  (),
                  [](std::size_t& v){ v = v / 2 + 1; });
    return dim_sizes;
  }
  
  /// \todo Find a safer way to tell this apart from DimSizesForFft
  static
  std::vector<std::size_t> get_dim_sizes_for_fftw
  (const cartesian_topology_index& cart_topo_idx)
  {
    std::vector<std::size_t> dim_sizes(cart_topo_idx.get_dimension_sizes());
    dim_sizes.back() = dim_sizes.back() / 2 + 1;
    return dim_sizes;
  }
  
  static
  std::size_t get_num_data_for_fftw
  (const cartesian_topology_index& cart_topo_idx)
  {
    const std::vector<std::size_t> dim_sizes(
      get_dim_sizes_for_fftw(cart_topo_idx));
    
    return
      std::accumulate(
        dim_sizes.begin(), 
        dim_sizes.end()  , 
        1                , 
        [](std::size_t product, std::size_t factor)
        {
          return product *= factor;
        });
  }
  
  class fftw_dim_sizes
  {
  public:
    fftw_dim_sizes(const cartesian_topology_index& cart_topo_idx)
      : m_dim_sizes(cart_topo_idx.get_dimension_sizes().begin(),
                    cart_topo_idx.get_dimension_sizes().end())
    {

    }
    const int* data() const { return m_dim_sizes.data(); }
    
  private:
    std::vector<int> m_dim_sizes;
  };
  
private:
   fft_helper();
  ~fft_helper();
   fft_helper(const fft_helper&);
};

} // namespace pvt

#endif // #ifndef PVT_FFT_HELPER_HPP_
