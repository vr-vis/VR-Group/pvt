//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_CORRELATED_SEVERITY_VIEW_HPP_
#define PVT_CORRELATED_SEVERITY_VIEW_HPP_

#include <pvt/severity_view.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class correlated_severity_view
{
public:
  correlated_severity_view(const severity_view& severity_view,
                           double               correlation_coefficient)
  : m_severity_view          (severity_view)
  , m_correlation_coefficient(correlation_coefficient)
  {

  }
  
  const  severity_view& get_severity_view          () const
  {
    return m_severity_view;
  }
  double                get_correlation_coefficient() const
  {
    return m_correlation_coefficient;
  }
  
  bool operator< (const correlated_severity_view& other) const
  {
    return m_correlation_coefficient > other.m_correlation_coefficient;
  }
  
private:
  severity_view m_severity_view;
  double        m_correlation_coefficient;
};

} // namespace pvt

#endif // #ifndef PVT_CORRELATED_SEVERITY_VIEW_HPP_
