//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_FFT_HPP_
#define PVT_FFT_HPP_

#include <cassert>
#include <cmath>
#include <string>

#include <fftw3.h>

#include <pvt/api.hpp>
#include <pvt/cartesian_topology_index.hpp>
#include <pvt/fft_helper.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API fft
{
public:
  fft(const cartesian_topology_index& cart_topo_idx)
  : m_cart_topo_idx     (fft_helper::get_dim_sizes_for_fftw(cart_topo_idx))
  , mp_fft_data         (static_cast<fftw_complex*>(fftw_malloc(
                         m_cart_topo_idx.get_num_positions() * 
                         sizeof(fftw_complex))))
  , m_num_data          (m_cart_topo_idx.get_num_positions())
  , m_normalizer        (1.0 / 
                         static_cast<double>(cart_topo_idx.get_num_positions()))
  , m_is_guaranteed_zero(false)
  , m_non_redundant_last_dimension
                        (cart_topo_idx.get_dimension_sizes().back() % 2 == 0)
  {
    
  }
  
  ~fft()
  {
    if (mp_fft_data != nullptr)
    {
      fftw_free(mp_fft_data);
      mp_fft_data = nullptr;
    }
  }

  double& real(std::size_t i)
  {
    m_is_guaranteed_zero = false;
    return mp_fft_data[i][0];
  }
  double  real(std::size_t i) const
  {
    return mp_fft_data[i][0];
  }
  double& imag(std::size_t i)
  {
    m_is_guaranteed_zero = false;
    return mp_fft_data[i][1];
  }
  double  imag(std::size_t i) const
  {
    return mp_fft_data[i][1];
  }

  double get_mean              () const
  {
    return sqrt(mod_sq_at(0)) * m_normalizer;
  }
  double get_variance          () const;
  double get_standard_deviation() const
  {
    return sqrt(get_variance());
  }

  double get_spectral_energy   (std::size_t i) const
  {
    return mod_sq_at(i) * m_normalizer * m_normalizer;
  }

  void clear_dc()
  {
    mp_fft_data[0][0] = 0.0;
    mp_fft_data[0][1] = 0.0;
  }
  
  std::size_t get_num_data() const
  {
    return m_num_data;
  }
  
  /// \todo Find a better way in Dft* to remove these
  fftw_complex*       data()
  {
    m_is_guaranteed_zero = false;
    return mp_fft_data;
  }
  const fftw_complex* data() const
  {
    return mp_fft_data;
  }
  
  void dump(const std::string& filename) const
  {
    auto file = fopen(filename.c_str(), "wb");
    fwrite(mp_fft_data, m_num_data * sizeof(fftw_complex), 1, file);
    fclose(file);
  }
  void read(const std::string& filename)
  {
    auto file = fopen(filename.c_str(), "rb");
    if (file != nullptr)
    {
      fread(mp_fft_data, m_num_data * sizeof(fftw_complex), 1, file);
      m_is_guaranteed_zero = false;
    }
    else
    {
      zero();
    }
    fclose(file);
  }

  void multiply_by_conjugate(const fft& other, fft& result)
  {
    assert(get_num_data() == other .get_num_data());
    assert(get_num_data() == result.get_num_data());
    /// \todo Find a way to avoid if/else
    if (m_is_guaranteed_zero || other.m_is_guaranteed_zero)
    {
      result.zero();
    }
    else
    {
      for (std::size_t i = 0; i < m_num_data; ++i)
      {
        result.real(i) = real(i) * other.real(i) + imag(i) * other.imag(i);
        result.imag(i) = imag(i) * other.real(i) - real(i) * other.imag(i);
      }
    }
  }
  
  void square_with_conjugate(fft& result)
  {
    assert(get_num_data() == result.get_num_data());
    /// \todo Find a way to avoid if/else
    if (m_is_guaranteed_zero)
    {
      result.zero();
    }
    else
    {
      for (std::size_t i = 0; i < m_num_data; ++i)
      {
        result.real(i) = real(i) * real(i) + imag(i) * imag(i);
        result.imag(i) = 0.0;
      }
    }
  }
  
  void zero();
  
  /// \todo Check if this is guaranteed for all possible cases
  bool is_guaranteed_zero() const { return m_is_guaranteed_zero; }
  
  template <typename T>
  void convert_to_frequency
  (std::size_t     frequency_index,
   std::vector<T>& frequency      ) const
  {
    m_cart_topo_idx.get_coordinate(frequency_index, frequency);
  }
  
  template <typename T>
  void convert_to_dc_centered_frequency
  (std::size_t     frequency_index,
   std::vector<T>& frequency      ) const
  {
    convert_to_frequency(frequency_index, frequency);
    
    const auto dc_shift_callable =
    []
    (std::size_t frequency_component,
     std::size_t dimension_size)
    {
      const auto half_dimension_size = dimension_size / 2;
      const auto in_upper_half =
        frequency_component > half_dimension_size;
      const auto shift =
        static_cast<double>(in_upper_half) *
        static_cast<double>(dimension_size);
      return static_cast<double>(frequency_component) - shift;
    };
    
    std::transform(  frequency.begin(),
                   --frequency.end  (),
                     m_cart_topo_idx.get_dimension_sizes().begin(),
                     frequency.begin(),
                     dc_shift_callable);
  }
  
  template <typename T>
  T get_frequency_norm_squared(const std::vector<T>  frequency) const
  {
    const auto add_squared_callable =
    [](T result, T element)
    {
      return result + element * element;
    };
    return std::accumulate(frequency.begin(),
                           frequency.end  (),
                           0.0,
                           add_squared_callable);
  }
  
  template <typename T>
  T get_frequency_norm        (const std::vector<T>  frequency) const
  {
    return sqrt(get_frequency_norm_squared(frequency));
  }
  
  template <typename T>
  bool is_on_boundary         (const std::vector<T>& frequency) const
  {
    return
       (frequency.back() == 0) ||
      ((frequency.back() == static_cast<T>(
        m_cart_topo_idx.get_dimension_sizes().back() - 1))
        &&
        m_non_redundant_last_dimension);
  }

private:
  double mod_sq_at(std::size_t i) const
  {
    return real(i) * real(i) + imag(i) * imag(i);
  }
  
  cartesian_topology_index m_cart_topo_idx;
  fftw_complex*            mp_fft_data;
  std::size_t              m_num_data;
  double                   m_normalizer;
  
  bool                     m_is_guaranteed_zero;
  bool                     m_non_redundant_last_dimension;
};

} // namespace pvt

#endif // #ifndef PVT_FFT_HPP_
