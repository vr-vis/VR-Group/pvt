//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_NAME_ID_PAIR_HPP_
#define PVT_NAME_ID_PAIR_HPP_

#include <string>
#include <sstream>

#include <pvt/api.hpp>

namespace pvt {

//------------------------------------------------------------------------------
template <char id_char>
class PVT_API name_id_pair
{
public:
  name_id_pair(const std::string& name, std::size_t id)
  : m_name(name), m_id(id)
  {
    
  }

  const std::string&  get_name() const
  {
    return m_name;
  }
  std::size_t         get_id  () const
  {
    return m_id  ;
  }
  
  const std::string   get_id_string() const
  {
    std::stringstream id_string;
    id_string << id_char << m_id;
    return id_string.str();
  }
  
protected:
  std::string m_name;
  std::size_t m_id;
};

} // namespace pvt

#endif // #ifndef PVT_NAME_ID_PAIR_HPP_
