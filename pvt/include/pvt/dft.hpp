//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_DFT_HPP_
#define PVT_DFT_HPP_

#include <fftw3.h>

#include <pvt/api.hpp>
#include <pvt/fft_helper.hpp>

namespace pvt {

template <class T> class data_array;
class fft;
class cartesian_topology_index;

//------------------------------------------------------------------------------
class PVT_API dft
{
public:
   dft(data_array<double>&             input_data,
       const cartesian_topology_index& input_topology_index,
       fft*                            p_output_fft);
  
  ~dft();
  
  void compute() const
  {
    fftw_execute(m_fft_plan);
  }
  
  static void compute(data_array<double>&             input_data,
                      const cartesian_topology_index& input_topology_index,
                      fft*                            p_output_fft)
  {
    dft dft(input_data, input_topology_index, p_output_fft);
    dft.compute();
  }
  
private:
  fft_helper::fftw_dim_sizes m_fftw_dim_sizes;
  fftw_plan                  m_fft_plan;
};

} // namespace pvt

#endif // #ifndef PVT_DFT_HPP_
