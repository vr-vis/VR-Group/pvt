//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_SYSTEM_GEOMETRY_HPP_
#define PVT_SYSTEM_GEOMETRY_HPP_

#include <string>
#include <vector>

#include <pvt/api.hpp>
#include <pvt/geometry.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API system_geometry
{
public:
  system_geometry(const std::vector<geometry>& geometries)
  : m_geometries (geometries)
  {

  }

  const std::vector<geometry>& get_geometries() const
  {
    return m_geometries;
  }

  template <typename input_type, typename output_type>
  void map_severities(const std::vector<input_type >& system_data,
                            std::vector<output_type>& geometry_mapping) const;

private:
  std::vector<geometry> m_geometries;
};

template <typename input_type, typename output_type>
void system_geometry::map_severities
(const std::vector<input_type >& system_data,
       std::vector<output_type>& geometry_mapping) const
{
  geometry_mapping.clear();
  for (auto& geometry : m_geometries)
  {
    output_type mapping_value(0);
    for (auto& severity_index : geometry.get_severity_indices())
      mapping_value = mapping_value + output_type(system_data[severity_index]);
    geometry_mapping.push_back(mapping_value);
  }
}

} // namespace pvt

#endif // #ifndef PVT_SYSTEM_GEOMETRY_HPP_
