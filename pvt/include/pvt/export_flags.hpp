//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_EXPORT_FLAGS_HPP_
#define PVT_EXPORT_FLAGS_HPP_

namespace pvt {

//------------------------------------------------------------------------------
enum export_flags 
{
  export_nothing                 = 0x00,
  export_metadata                = 0x01,
  export_severities              = 0x01 << 1,
  export_fft                     = 0x01 << 2,
  export_directed_variances      = 0x01 << 3,
  export_data                    = export_severities | export_fft | export_directed_variances,
  export_metadata_only           = export_metadata,
  export_directed_variances_only = export_directed_variances,
  export_all                     = 0xff,
  recompute_spectra              = 0x0100 << 2,
  recompute_directed_variances   = 0x0100 << 3
};

} // namespace pvt

#endif // #define PVT_EXPORT_FLAGS_HPP_
