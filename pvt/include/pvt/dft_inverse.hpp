//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_DFT_INVERSE_HPP_
#define PVT_DFT_INVERSE_HPP_

#include <fftw3.h>

#include <pvt/api.hpp>
#include <pvt/fft_helper.hpp>

namespace pvt {

template <class T> class data_array;
class fft;

//------------------------------------------------------------------------------
class PVT_API dft_inverse
{
public:
   dft_inverse(fft&                            input_fft,
               const cartesian_topology_index& topology_index,
               data_array<double>*             p_output_data);
  
  ~dft_inverse();
  
  void compute() const
  {
    fftw_execute(m_fft_inverse_plan);
  }
  
  static void compute(fft&                            input_fft,
                      const cartesian_topology_index& topology_index,
                      data_array<double>*             p_output_data)
  {
    dft_inverse dft_inverse(input_fft, topology_index, p_output_data);
    dft_inverse.compute();
  }
  
  
private:
  fft_helper::fftw_dim_sizes m_fftw_dim_sizes;
  fftw_plan                  m_fft_inverse_plan;
};

} // namespace pvt

#endif // #ifndef PVT_DFT_INVERSE_HPP_
