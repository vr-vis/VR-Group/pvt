//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_SHADERS_HPP_
#define PVT_SHADERS_HPP_

#include <string>

#include <pvt/api.hpp>

//------------------------------------------------------------------------------
namespace pvt {

namespace shader_strings {

extern PVT_API std::string axes_vert;
extern PVT_API std::string axes_geom;
extern PVT_API std::string axes_frag;
extern PVT_API std::string cartesian_grid_vert;
extern PVT_API std::string cartesian_grid_frag;
extern PVT_API std::string color_treemap_vert;
extern PVT_API std::string color_treemap_frag;
extern PVT_API std::string cover_vert;
extern PVT_API std::string cover_frag;
extern PVT_API std::string cushion_treemap_vert;
extern PVT_API std::string cushion_treemap_frag;
extern PVT_API std::string data_slider_line_vert;
extern PVT_API std::string data_slider_line_horizontal_geom;
extern PVT_API std::string data_slider_line_vertical_geom;
extern PVT_API std::string data_slider_line_frag;
extern PVT_API std::string data_slider_vert;
extern PVT_API std::string data_slider_geom;
extern PVT_API std::string data_slider_frag;
extern PVT_API std::string line_plot_axes_vert;
extern PVT_API std::string line_plot_axes_frag;
extern PVT_API std::string line_plot_vert;
extern PVT_API std::string line_plot_frag;
extern PVT_API std::string line_plot_2_vert;
extern PVT_API std::string line_plot_2_frag;
extern PVT_API std::string mesh_id_vert;
extern PVT_API std::string mesh_id_frag;
extern PVT_API std::string mesh_vert;
extern PVT_API std::string mesh_frag;
extern PVT_API std::string node_id_vert;
extern PVT_API std::string node_id_frag;
extern PVT_API std::string parallel_coord_axis_vert;
extern PVT_API std::string parallel_coord_axis_geom;
extern PVT_API std::string parallel_coord_axis_frag;
extern PVT_API std::string parallel_coord_vert;
extern PVT_API std::string parallel_coord_geom;
extern PVT_API std::string parallel_coord_frag;
extern PVT_API std::string cartesian_topology_vert;
extern PVT_API std::string cartesian_topology_frag;

}

} // namespace pvt

#endif // #ifndef PVT_SHADERS_HPP_