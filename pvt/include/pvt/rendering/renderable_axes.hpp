//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_RENDERABLE_AXES_HPP_
#define PVT_RENDERABLE_AXES_HPP_

#include <Eigen/Dense>

#include <pvt/api.hpp>
#include <pvt/optional.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/rendering/renderable.hpp>
#include <pvt/rendering/vertex_array_shader_pair.hpp>

namespace pvt {

class camera;

//------------------------------------------------------------------------------
class PVT_API renderable_axes : public renderable, vertex_array_shader_pair
{
public:
  renderable_axes(const Eigen::Vector3i& lengths);
  
  void on_setup ()                       override;
  void on_render(const camera* p_camera) override;

  void set_lengths(const Eigen::Vector3i& lengths) { m_lengths = lengths; } 
  void set_scale  (const Eigen::Vector3f& scale  ) { m_scale   = scale  ; }
  
private:
  optional<vertex_buffer> m_vertex_buffer;
  Eigen::Vector3i         m_lengths;
  Eigen::Vector3f         m_scale;
};

} // namespace pvt

#endif // #ifndef PVT_RENDERABLE_AXES_HPP_
