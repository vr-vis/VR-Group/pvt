//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_TREEMAP_SHADING_POLICY_CUSHION_HPP_
#define PVT_TREEMAP_SHADING_POLICY_CUSHION_HPP_

#include <array>
#include <vector>

#include <Eigen/Dense>

#include <pvt/api.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/gl/shader_program.hpp>
#include <pvt/gl/vertex_array.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API treemap_shading_policy_cushion
{
public:
  treemap_shading_policy_cushion();

  void set_height        (const float& height)
  {
    m_height = height;
  }
  void set_scale_factor  (const float& scale_factor)
  {
    m_scale_factor = scale_factor;
  }
  void set_ambient_coef  (const float& ambient_coef)
  {
    m_ambient_coef = ambient_coef;
  }
  void set_diffuse_coef  (const float& diffuse_coef)
  {
    m_diffuse_coef = diffuse_coef;
  }
  void set_light_position(const Eigen::Vector3f& light_position)
  {
    m_light_position = light_position;
  }
  void set_base_color    (const Eigen::Vector4f& base_color)
  {
    m_base_color = base_color;
  }

protected:
  void update_parameters(const Eigen::AlignedBox2f& rectangle,
                         Eigen::AlignedBox2f&       surface,
                         int                        recursion_depth);
  void append_rectangle (const Eigen::AlignedBox2f& rectangle,
                         Eigen::AlignedBox2f&       surface);

  void build (const Eigen::Vector2i& size);
  void render();

private:
  static void add_ridge(
    const Eigen::AlignedBox2f& rectangle,
    Eigen::AlignedBox2f&       surface,
    const float&               height,
    int                        direction);

  float           m_height         = 0.5F;
  float           m_scale_factor   = 0.75F;
  float           m_ambient_coef   = 0.1568F;
  float           m_diffuse_coef   = 0.8431F;
  Eigen::Vector3f m_light_position = Eigen::Vector3f(0.0976F, 0.195F, 0.976F);
  Eigen::Vector4f m_base_color     = Eigen::Vector4f(1.0, 1.0, 1.0, 1.0);

  std::array<std::vector<float>, 3> m_attributes;
  std::vector<unsigned int>         m_indices;

  Eigen::Vector2i                   m_size;
  vertex_array                      m_vertex_array;
  std::array<vertex_buffer, 3>      m_attribute_buffers;
  index_buffer                      m_index_buffer;
  shader_program                    m_shader_program;
  int                               m_draw_count = 0;

};

} // namespace pvt

#endif // #ifndef PVT_TREEMAP_SHADING_POLICY_CUSHION_HPP_