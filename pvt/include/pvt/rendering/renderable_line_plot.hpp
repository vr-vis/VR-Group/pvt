//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_RENDERABLE_LINE_PLOT_HPP_
#define PVT_RENDERABLE_LINE_PLOT_HPP_

#include <vector>

#include <pvt/api.hpp>
#include <pvt/data_array.hpp>
#include <pvt/optional.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/gl/texture.hpp>
#include <pvt/rendering/renderable.hpp>
#include <pvt/rendering/vertex_array_shader_pair.hpp>

namespace pvt {

class cartesian_topology;
template <typename t> 
class data_array;

//------------------------------------------------------------------------------
class PVT_API renderable_line_plot : public renderable, vertex_array_shader_pair
{
public:
  renderable_line_plot(const cartesian_topology* p_cartesian_topology,
                       const data_array<float>*  p_topology_mapping);
  
  void on_setup ()                      override;
  void on_render(const camera* pCamera) override;

  void set_minimum(float min) { m_minimum      = min;  }
  void set_maximum(float max) { m_maximum      = max;  }
  void set_dirty  ()          { m_needs_upload = true; }

  void set_dimension_id(std::size_t dimension_id);
  
private:
  void init_shader      ();
  void init_geometry    ();
  void init_data_texture();
  void update_uniforms  ();
  void render_geometry  ();
  void upload           ();
  
  optional<vertex_buffer>   m_vertex_buffer;
  optional<vertex_buffer>   m_vertex_id_buffer;
  optional<vertex_buffer>   m_data_buffer;
  optional<buffer_texture>  m_data_texture;

  const cartesian_topology* mp_cartesian_topology;
  const data_array<float>*  mp_topology_mapping;

  std::vector<float>        m_axis_positions;
  std::vector<int>          m_axis_ids;
  float                     m_minimum;
  float                     m_maximum;
  int                       m_num_axes;
  int                       m_data_index_factor_for_axis;
  int                       m_data_index_factor_for_next_axis;
  bool                      m_needs_upload;
};

} // namespace pvt

#endif // #ifndef PVT_RENDERABLE_LINE_PLOT_HPP_
