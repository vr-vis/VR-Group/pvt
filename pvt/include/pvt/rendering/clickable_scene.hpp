//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_CLICKABLE_SCENE_HPP_
#define PVT_CLICKABLE_SCENE_HPP_

#include <Eigen/Core>

#include <pvt/api.hpp>
#include <pvt/optional.hpp>
#include <pvt/gl/framebuffer.hpp>
#include <pvt/gl/texture.hpp>
#include <pvt/rendering/scene.hpp>

namespace pvt {

class camera;

//------------------------------------------------------------------------------
class PVT_API clickable_scene : public scene
{
public:
  clickable_scene() {}
  virtual ~clickable_scene() {}

  void setup () override;
  void render(const camera* p_camera) override;

  void resize(const Eigen::Vector2i& size);

  int         get_id        (const Eigen::Vector2i& position);
  renderable* get_renderable(const Eigen::Vector2i& position);

private:
  bool                  m_needs_upload = true;
  Eigen::Vector2i       m_size;
  optional<framebuffer> m_click_framebuffer;
  optional<texture_2d>  m_click_texture;

};

} // namespace pvt

#endif // #ifndef PVT_CLICKABLE_SCENE_HPP_
