//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_TREEMAP_TESSELLATION_POLICY_SQUARIFIED_HPP_
#define PVT_TREEMAP_TESSELLATION_POLICY_SQUARIFIED_HPP_

#include <vector>

#include <Eigen/Dense>

#include <pvt/api.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API treemap_tessellation_policy_squarified
{
protected:
  static void tessellate(
    const Eigen::AlignedBox2f&        rectangle,
    const std::vector<float>&         weights,
    int                               recursion_depth,
    std::vector<Eigen::AlignedBox2f>& output_subrectangles);

private:
  static void squarify(
    const Eigen::AlignedBox2f&        container_rectangle,
    const Eigen::AlignedBox2f&        boundary_rectangle,
    const std::vector<float>&         weights,
    std::vector<Eigen::AlignedBox2f>& current_row,
    std::vector<Eigen::AlignedBox2f>& output_subrectangles);

  static float get_worst_aspect_ratio(
    const float&                            shortest_edge_length,
    const std::vector<Eigen::AlignedBox2f>& row);
};

} // namespace pvt

#endif // #ifndef PVT_TREEMAP_TESSELLATION_POLICY_SQUARIFIED_HPP_