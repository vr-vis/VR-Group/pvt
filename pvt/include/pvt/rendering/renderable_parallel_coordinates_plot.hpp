//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_RENDERABLE_PARALLEL_COORDINATES_PLOT_HPP_
#define PVT_RENDERABLE_PARALLEL_COORDINATES_PLOT_HPP_

#include <pvt/api.hpp>
#include <pvt/optional.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/gl/shader_program.hpp>
#include <pvt/gl/vertex_array.hpp>
#include <pvt/rendering/renderable.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API renderable_parallel_coordinates_plot : public renderable
{
public:
  struct axis
  {
    axis(float x_offset, bool is_inverted = false)
    : m_x_offset(x_offset), m_is_inverted(is_inverted)
    {
      
    }
    float m_x_offset;
    bool  m_is_inverted;
  };
  struct entry
  {
    entry(const std::vector<float>& percentages, bool is_selected = false)
    : m_percentages(percentages), m_is_selected(is_selected)
    {
      
    }
    std::vector<float> m_percentages;
    bool               m_is_selected;
  };

  void on_setup ()                       override;
  void on_render(const camera* p_camera) override;

  void set_axes            (const std::vector<axis>&  axes)
  {
    m_axes         = axes;
    m_needs_upload = true;
  }
  void set_entries         (const std::vector<entry>& entries)
  {
    m_entries      = entries;
    m_needs_upload = true;
  }

  void set_color           (const Eigen::Vector4f& color)
  {
    m_color = color;
  }
  void set_selected_color  (const Eigen::Vector4f& selected_color)
  {
    m_selected_color = selected_color;
  }
  void set_size            (const Eigen::Vector2i& size)
  {
    m_size      = size     ;
  }
  void set_draw_axes       (bool                   draw_axes)
  {
    m_draw_axes = draw_axes;
  }

  void set_entry_selected  (unsigned int index, bool selected = true);
  void set_axis_inverted   (unsigned int index, bool inverted = true);
  void deselect_all_entries();
  
private:
  void upload();
               
  optional<vertex_array>   m_plot_vertex_array;
  optional<shader_program> m_plot_shader_program;
  optional<vertex_buffer>  m_plot_vertex_buffer;
  optional<vertex_array>   m_axes_vertex_array;
  optional<shader_program> m_axes_shader_program;
  optional<vertex_buffer>  m_axes_vertex_buffer;
  std::size_t              m_axes_draw_count = 0;

  std::vector<axis>        m_axes;
  std::vector<entry>       m_entries;

  Eigen::Vector4f          m_color          = Eigen::Vector4f(0.0, 0.0, 0.0, 1.0);
  Eigen::Vector4f          m_selected_color = Eigen::Vector4f(1.0, 0.5, 0.0, 1.0);
  Eigen::Vector2i          m_size           = Eigen::Vector2i(140, 32);
  bool                     m_draw_axes      = true;
  bool                     m_needs_upload   = true;  
};

} // namespace pvt

#endif // #ifndef PVT_RENDERABLE_PARALLEL_COORDINATES_PLOT_HPP_
