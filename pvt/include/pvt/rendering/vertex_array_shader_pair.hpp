//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_VERTEX_ARRAY_SHADER_PAIR_HPP_
#define PVT_VERTEX_ARRAY_SHADER_PAIR_HPP_

#include <pvt/api.hpp>
#include <pvt/optional.hpp>
#include <pvt/gl/vertex_array.hpp>
#include <pvt/gl/shader_program.hpp>

namespace pvt {

class camera;

//------------------------------------------------------------------------------
class PVT_API vertex_array_shader_pair
{
protected:
  void setup      ();
  void bind       ();
  void unbind     ();
  void pre_render (const camera* p_camera);
  void post_render();

  optional<vertex_array>   m_vertex_array;
  optional<shader_program> m_shader_program;
};

} // namespace pvt

#endif // #ifndef PVT_VERTEX_ARRAY_SHADER_PAIR_HPP_
