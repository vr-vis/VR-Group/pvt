//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_RENDERABLE_GEOMETRY_HPP_
#define PVT_RENDERABLE_GEOMETRY_HPP_

#include <pvt/api.hpp>
#include <pvt/geometry.hpp>
#include <pvt/optional.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/gl/shader_program.hpp>
#include <pvt/gl/vertex_array.hpp>
#include <pvt/rendering/clickable.hpp>
#include <pvt/rendering/renderable.hpp>

namespace pvt {

class camera;

//------------------------------------------------------------------------------
class PVT_API renderable_geometry 
: public renderable, public clickable
{
public:
  renderable_geometry(const geometry* geometry, 
                      float           severity = 0.0, 
                      bool            selected = false);

  void on_setup                 ()                       override;
  void on_render                (const camera* p_camera) override;
  void on_render_to_click_buffer(const camera* p_camera) override;

  void set_severity(float severity) { m_severity = severity; }
  void set_selected(bool  selected) { m_selected = selected; }
  
private:
  template <typename type, typename buffer_type>
  void upload_buffer_data(const std::vector<type>& data, buffer_type& buffer)
  {
    buffer.bind    ();
    buffer.set_data(data.size() * sizeof(type), data.data());
    buffer.unbind  ();
  }

  optional<vertex_array>   m_vertex_array;
  optional<shader_program> m_shader_program;
  optional<vertex_array>   m_click_vertex_array;
  optional<shader_program> m_click_shader_program;

  optional<vertex_buffer>  m_vertex_buffer;
  optional<vertex_buffer>  m_normal_buffer;
  optional<vertex_buffer>  m_texcoord_buffer;
  optional<index_buffer>   m_index_buffer;
  std::size_t              m_draw_count = 0;

  const geometry*          mp_geometry;
  float                    m_severity = 0.0;
  bool                     m_selected = false;

};

} // namespace pvt

#endif // #ifndef PVT_RENDERABLE_GEOMETRY_HPP_
