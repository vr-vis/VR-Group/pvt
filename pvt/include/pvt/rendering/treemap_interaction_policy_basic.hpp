//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_TREEMAP_INTERACTION_POLICY_BASIC_HPP_
#define PVT_TREEMAP_INTERACTION_POLICY_BASIC_HPP_

#include <array>
#include <vector>

#include <Eigen/Dense>

#include <pvt/api.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/gl/framebuffer.hpp>
#include <pvt/gl/shader_program.hpp>
#include <pvt/gl/texture.hpp>
#include <pvt/gl/vertex_array.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API treemap_interaction_policy_basic
{
public:
  treemap_interaction_policy_basic();

  void append_rectangle(const Eigen::AlignedBox2f& rectangle, unsigned int id);
  void build           (const Eigen::Vector2i& size);
  void render          ();

  unsigned int get_node(const Eigen::Vector2i& position);

private:
  std::vector<float>           m_vertices;
  std::vector<unsigned int>    m_ids;
  std::vector<unsigned int>    m_indices;

  vertex_array                 m_vertex_array;
  std::array<vertex_buffer, 2> m_attribute_buffers;
  index_buffer                 m_index_buffer;
  shader_program               m_shader_program;
  int                          m_draw_count = 0;

  framebuffer                  m_framebuffer;
  texture_2d                   m_framebuffer_texture;
  Eigen::Vector2i              m_size;
};

} // namespace pvt

#endif // #ifndef PVT_TREEMAP_INTERACTION_POLICY_BASIC_HPP_