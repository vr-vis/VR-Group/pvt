//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_SCENE_HPP_
#define PVT_SCENE_HPP_

#include <memory>
#include <vector>

#include <pvt/api.hpp>
#include <pvt/rendering/renderable.hpp>

namespace pvt {

class camera;

//------------------------------------------------------------------------------
class PVT_API scene
{
public:
  scene() {}
  virtual ~scene() {}
  
  virtual void setup ();
  virtual void render(const camera* p_camera);

  // Note: Assumes ownership of the given pointer to renderable.
  void add_renderable   (renderable* p_renderable);
  void remove_renderable(renderable* p_renderable);

protected:
  scene(const scene&) = delete;
  scene& operator=(const scene&) = delete;

  std::vector<std::unique_ptr<renderable>> m_renderables;
};

} // namespace pvt

#endif // #ifndef PVT_SCENE_HPP_
