//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_RENDERABLE_CARTESIAN_TOPOLOGY_HPP_
#define PVT_RENDERABLE_CARTESIAN_TOPOLOGY_HPP_

#include <iostream>
#include <limits>
#include <vector>

#include <Eigen/Dense>

#include <pvt/api.hpp>
#include <pvt/data_array.hpp>
#include <pvt/optional.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/gl/texture.hpp>
#include <pvt/rendering/renderable.hpp>
#include <pvt/rendering/vertex_array_shader_pair.hpp>

namespace pvt {

class camera;
class cartesian_topology;

//------------------------------------------------------------------------------
class PVT_API renderable_cartesian_topology 
: public renderable, vertex_array_shader_pair
{
public:
  renderable_cartesian_topology(
    const cartesian_topology* p_cartesian_topology,
    const data_array<float>*  p_topology_mapping);
  
  void on_setup ()                       override;
  void on_render(const camera* p_camera) override;

  void set_base_offset        (int base_offset)
  {
    m_base_offset = base_offset;
  }
  void set_range_on_dimension (std::size_t dimension, float min, float max)
  {
    m_ranges_on_dimensions[dimension] = std::pair<float, float>(min, max);
  }
  void set_projection         (const std::vector<std::size_t>& projection)
  {
    m_projection = projection;
  }
  void set_scale              (const Eigen::Vector3f& scale)
  {
    m_scale = scale;
  }

  void set_minimum            (float minimum)
  {
    m_minimum = minimum;
  }
  void set_maximum            (float maximum)
  {
    m_maximum = maximum;
  }
  void set_filtered_range     (float filtered_minimum, 
                               float filtered_maximum)
  {
    m_filtered_minimum = filtered_minimum;
    m_filtered_maximum = filtered_maximum;
  }
  void set_map_minimum_to_zero(bool  map_minimum_to_zero)
  {
    m_map_minimum_to_zero = map_minimum_to_zero;
  }
  void set_dirty                   ()
  {
    m_needs_upload = true;
  }
  
private:
  void init_shader      ();
  void init_geometry    ();
  void init_data_texture();
  void update_uniforms  ();
  void render_geometry  ();
  void upload           ();

  optional<vertex_buffer>              m_vertex_buffer;
  optional<vertex_buffer>              m_data_vertex_buffer;
  optional<buffer_texture>             m_data_texture;
                                       
  const cartesian_topology*            mp_cartesian_topology;
  const data_array<float>*             mp_topology_mapping;

  int                                  m_base_offset = 0;
  std::vector<std::size_t>             m_dimension_offsets;
  std::vector<std::pair<float, float>> m_ranges_on_dimensions;
  std::vector<std::size_t>             m_projection;
  Eigen::Vector3f                      m_scale = Eigen::Vector3f(1.0f, 1.0f, 1.0f);

  float                                m_minimum             = 0.0f;
  float                                m_maximum             = 1.0f;
  float                                m_filtered_minimum    = 0.0f;
  float                                m_filtered_maximum    = 1.0f;
  bool                                 m_map_minimum_to_zero = false;
  bool                                 m_needs_upload        = true;
};

} // namespace pvt

#endif // #ifndef PVT_RENDERABLE_CARTESIAN_TOPOLOGY_HPP_
