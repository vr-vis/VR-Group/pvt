//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_CAMERA_HPP_
#define PVT_CAMERA_HPP_

#include <Eigen/Dense>

#include <pvt/api.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API camera
{
public:
  camera();

  Eigen::Matrix4f get_rotation_matrix   () const;
  Eigen::Matrix4f get_view_matrix       () const;
  Eigen::Matrix4f get_projection_matrix () const;

  float get_distance() const { return m_distance; }
  float get_lambda  () const { return m_lambda  ; }
  float get_phi     () const { return m_phi     ; }
  
  void  set_distance (float distance) { m_distance = distance; }
  void  set_lambda   (float lambda)   { m_lambda   = lambda  ; }
  void  set_phi      (float phi)
  {
    m_phi = std::max(-89.0f / 180.0f * 3.1415926f,
            std::min( 89.0f / 180.0f * 3.1415926f,
            phi));
  }
  void  set_aspect   (float aspect)   { m_aspect   = aspect  ; }
  void  set_near     (float value)    { m_near     = value   ; }
  void  set_far      (float value)    { m_far      = value   ; }

  Eigen::Vector3f get_coi() const
  {
    return m_center_of_interest;
  }
  void            set_coi(const Eigen::Vector3f coi)
  {
    m_center_of_interest = coi;
  }

  void print_info() const;
  
private:
  float m_distance;
  float m_lambda;
  float m_phi;
  float m_focal_length;
  float m_aspect;
  float m_near;
  float m_far;
  
  Eigen::Vector3f m_center_of_interest;
};

} // namespace pvt

#endif // #ifndef PVT_CAMERA_HPP_
