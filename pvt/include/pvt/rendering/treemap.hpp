//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_TREEMAP_HPP_
#define PVT_TREEMAP_HPP_

#include <vector>

#include <Eigen/Dense>

#include <pvt/data_array.hpp>

namespace pvt {

//------------------------------------------------------------------------------
template<
  typename type, 
  typename tessellation_policy, 
  typename shading_policy, 
  typename interaction_policy>
class treemap 
  : public tessellation_policy, 
    public shading_policy, 
    public interaction_policy
{
public:
  treemap(
    const Eigen::Vector2i&    size,
    const std::vector<type>&  nodes,
    const data_array<double>* p_weights,
    type*                     p_root_node,
    float                     border_width = 0.0F,
    bool                      leaves_only  = true);

  void build ();
  void render();

  type* get_node (const Eigen::Vector2i& position);
  void  mark_node(type* p_node);

  type* get_root_node()
  {
    return mp_root_node;
  }

  void set_size        (const Eigen::Vector2i&    size)
  {
    m_size = size;
  }
  void set_root_node   (type*                     p_root_node)
  {
    mp_root_node = p_root_node;
  }
  void set_weights     (const data_array<double>* p_weights)
  {
    mp_weights = p_weights;
  }
  void set_border_width(const float&              border_width)
  {
    m_border_width = border_width;
  }
  void set_leaves_only (bool                      leaves_only)
  {
    m_leaves_only = leaves_only;
  }
  
private:
  void traverse(
    type*                      p_node,
    const Eigen::AlignedBox2f& rectangle,
    Eigen::AlignedBox2f        surface,
    int                        recursion_depth = 0);

  Eigen::Vector2i           m_size;
  const std::vector<type>&  m_nodes;
  const data_array<double>* mp_weights;
  type*                     mp_root_node;
  float                     m_border_width;
  bool                      m_leaves_only; 

};


//------------------------------------------------------------------------------
template <
  typename type, 
  typename tessellation_policy, 
  typename shading_policy, 
  typename interaction_policy>
treemap<type, tessellation_policy, shading_policy, interaction_policy>::treemap
(const Eigen::Vector2i&    size, 
 const std::vector<type>&  nodes, 
 const data_array<double>* p_weights, 
 type*                     p_root_node, 
 float                     border_width, 
 bool                      leaves_only)
: m_size        (size)
, m_nodes       (nodes)
, mp_weights    (p_weights)
, mp_root_node  (p_root_node)
, m_border_width(border_width)
, m_leaves_only (leaves_only)
{
    
}


//------------------------------------------------------------------------------
template <
  typename type, 
  typename tessellation_policy, 
  typename shading_policy, 
  typename interaction_policy>
void 
treemap<type, tessellation_policy, shading_policy, interaction_policy>::build
()
{
  traverse(mp_root_node,
           Eigen::AlignedBox2f(Eigen::Vector2f(0.0F, 0.0F),
                               Eigen::Vector2f(m_size[0], m_size[1])),
           Eigen::AlignedBox2f(Eigen::Vector2f(0.0F, 0.0F),
                               Eigen::Vector2f(0.0F, 0.0F)));
  shading_policy    ::build(m_size);
  interaction_policy::build(m_size);
}


//------------------------------------------------------------------------------
template <
  typename type, 
  typename tessellation_policy, 
  typename shading_policy, 
  typename interaction_policy>
void 
treemap<type, tessellation_policy, shading_policy, interaction_policy>::render
()
{
  shading_policy    ::render();
  interaction_policy::render();
}


//------------------------------------------------------------------------------
template <
  typename type,
  typename tessellation_policy,
  typename shading_policy,
  typename interaction_policy>
type* 
treemap<type, tessellation_policy, shading_policy, interaction_policy>::get_node
(const Eigen::Vector2i& position)
{
  auto id    = interaction_policy::get_node(position);
  const auto& node = std::find_if(m_nodes.begin(), m_nodes.end(),
    [id](const type& candidate) -> bool {return candidate.get_id() == id;});
  return node != m_nodes.end() ? const_cast<type*>(&*node) : nullptr;
}


//------------------------------------------------------------------------------
template <
  typename type,
  typename tessellation_policy,
  typename shading_policy,
  typename interaction_policy>
void 
treemap<type, tessellation_policy, shading_policy, interaction_policy>::mark_node
(type* pNode)
{
  // TODO!
}


//------------------------------------------------------------------------------
// Note: There is no graceful way to avoid the surface parameter
// within the traversal recursion in this base class. Do not use 
// it in the shading policy classes if not necessary.
//------------------------------------------------------------------------------
template <
  typename type,
  typename tessellation_policy,
  typename shading_policy,
  typename interaction_policy>
void 
treemap<type, tessellation_policy, shading_policy, interaction_policy>::traverse
(type*                      p_node, 
 const Eigen::AlignedBox2f& rectangle, 
 Eigen::AlignedBox2f        surface, 
 int                        recursion_depth)
{
  shading_policy::update_parameters(rectangle, surface, recursion_depth);

  // Record the rectangle for rendering.
  if (!m_leaves_only || (m_leaves_only && !p_node->has_children()))
  {
    shading_policy    ::append_rectangle(rectangle, surface);
    interaction_policy::append_rectangle(rectangle, 
      (unsigned int) p_node->get_id());
  }

  // If there are no children, end recursion.
  if (!p_node->has_children())
    return;

  // Sort children in descending order by weight.
  auto children = p_node->get_children();
  std::sort(children.begin(), children.end(),
    [&](const type* lhs, const type* rhs) -> bool {
      return mp_weights->at(lhs->get_id()) > mp_weights->at(rhs->get_id());});

  // Create a vector of normalized (scaled by sum) child weights.
  std::vector<float> normalized_child_weights;
  float child_weights_sum = 0.0;
  for (auto i = 0; i < children.size(); i++)
    child_weights_sum += mp_weights->at(children[i]->get_id());
  for (auto i = 0; i < children.size(); i++)
    normalized_child_weights.push_back(
      mp_weights->at(children[i]->get_id()) / child_weights_sum);

  // Tessellate the rectangle via normalized child weights.
  std::vector<Eigen::AlignedBox2f> subrectangles;
  tessellation_policy::tessellate(rectangle, 
                                  normalized_child_weights,
                                  recursion_depth,
                                  subrectangles);
    
  // Repeat for each child with its corresponding subrectangle.
  for (auto i = 0; i < children.size(); i++)
  {
    auto subrectangle = subrectangles[i];
    subrectangle.min() += Eigen::Vector2f(m_border_width, m_border_width);
    subrectangle.max() -= Eigen::Vector2f(m_border_width, m_border_width);
    traverse(children[i],
             subrectangle,
             surface,
             recursion_depth + 1);
  }
}

} // namespace pvt

#endif // #ifndef PVT_TREEMAP_HPP_
