//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_RENDERABLE_LINE_PLOT_AXES_HPP_
#define PVT_RENDERABLE_LINE_PLOT_AXES_HPP_

#include <pvt/api.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/rendering/renderable.hpp>
#include <pvt/rendering/vertex_array_shader_pair.hpp>

namespace pvt {

class camera;

//------------------------------------------------------------------------------
class PVT_API renderable_line_plot_axes 
: public renderable, vertex_array_shader_pair
{
public:
  explicit renderable_line_plot_axes();
  
  void on_setup()                        override;
  void on_render(const camera* p_camera) override;

  void set_num_axes(int num_axes) { m_num_axes = num_axes; }
  
private:
  optional<vertex_buffer> m_vertexBuffer;
  int                     m_num_axes;
};

} // namespace pvt

#endif // #ifndef PVT_RENDERABLE_LINE_PLOT_AXES_HPP_
