//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_RENDERABLE_CARTESIAN_GRID_HPP_
#define PVT_RENDERABLE_CARTESIAN_GRID_HPP_

#include <Eigen/Core>

#include <pvt/api.hpp>
#include <pvt/optional.hpp>
#include <pvt/gl/buffer.hpp>
#include <pvt/rendering/renderable.hpp>
#include <pvt/rendering/vertex_array_shader_pair.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API renderable_cartesian_grid 
: public renderable, vertex_array_shader_pair
{
public:
  renderable_cartesian_grid(const Eigen::Vector3f& size,
                            const Eigen::Vector3f& block_size);
  
  const Eigen::Vector3f& get_translation() const
  {
    return m_translation;
  }
  const Eigen::Vector3f& get_size       () const
  {
    return m_size;
  }
  const Eigen::Vector3f& get_block_size () const
  {
    return m_block_size;
  }
  const Eigen::Vector4f& get_color      () const
  {
    return m_color;
  }

  void set_size       (const Eigen::Vector3f& size       )
  {
    m_size         = size;
    m_needs_upload = true;
  }
  void set_block_size (const Eigen::Vector3f& block_size )
  {
    m_block_size   = block_size;
    m_needs_upload = true;
  }
  void set_translation(const Eigen::Vector3f& translation)
  {
    m_translation  = translation;
    m_needs_upload = true;
  }
  void set_color      (const Eigen::Vector4f& color      )
  {
    m_color        = color;
    m_needs_upload = true;
  }

  void on_setup () override;
  void on_render(const camera* p_camera) override;

private:
  void upload();

  Eigen::Vector3f         m_size         ;
  Eigen::Vector3f         m_block_size   ;
  Eigen::Vector3f         m_translation  ;
  Eigen::Vector4f         m_color        ;
  optional<vertex_buffer> m_vertex_buffer;
  optional<vertex_buffer> m_normal_buffer;
  bool                    m_needs_upload = false;
  std::size_t             m_draw_count   = 0    ;
};

} // namespace pvt

#endif // #ifndef PVT_RENDERABLE_CARTESIAN_GRID_HPP_
