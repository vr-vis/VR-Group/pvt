//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_RENDERABLE_HPP_
#define PVT_RENDERABLE_HPP_

#include <pvt/api.hpp>

namespace pvt {

class camera;

//------------------------------------------------------------------------------
class PVT_API renderable
{
public:
  renderable() {}
  virtual ~renderable() {}
  
  virtual void on_setup () = 0;
  virtual void on_render(const camera* p_camera) = 0;
  
};

} // namespace pvt

#endif // #ifndef PVT_RENDERABLE_HPP_
