//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_SHADER_PROGRAM_HPP_
#define PVT_SHADER_PROGRAM_HPP_

#include <string>
#include <vector>

#include <Eigen/Dense>

#include <pvt/api.hpp>
#include <pvt/gl/opengl.hpp>

namespace pvt {

template<GLenum Type>
class shader;

//------------------------------------------------------------------------------
class PVT_API shader_program
{
public:
   shader_program();
  ~shader_program();

  template<GLenum Type>
  void attach_shader     (const shader<Type>& shader);
  void detach_all_shaders();

  bool link();

  void bind  ();
  void unbind();

  void set_attribute_buffer        (const std::string& name,
                                    GLuint             size,
                                    GLuint             type,
                                    bool               normalize = true,
                                    GLuint             stride    = 0,
                                    GLuint             offset    = 0);
  void set_attribute_buffer_integer(const std::string& name,
                                    GLuint             size,
                                    GLuint             type,
                                    GLuint             stride    = 0,
                                    GLuint             offset    = 0);
  void set_attribute_buffer_double (const std::string& name,      
                                    GLuint             size,     
                                    GLuint             type,     
                                    GLuint             stride    = 0,
                                    GLuint             offset    = 0);

  void enable_attribute_array (const std::string& name);
  void disable_attribute_array(const std::string& name);

  GLint get_uniform_location  (const std::string& name);
  GLint get_attribute_location(const std::string& name);

  template<typename Type>
  void set_uniform      (const std::string&       name, 
                         const Type&              value ) {}
  template<typename Type>
  void set_uniform_array(const std::string&       name, 
                         const std::vector<Type>& values) {}

  bool   is_valid         () const;
  GLuint get_native_handle() const;

private:
  GLuint m_id;
};

template <GLenum Type>
void shader_program::attach_shader(const shader<Type>& shader)
{
  glAttachShader(m_id, shader.get_native_handle());
}

#define SPECIALIZE_SET_UNIFORM_BASIC_TYPES(TYPE, GL_POSTFIX) \
template <> \
inline void shader_program::set_uniform \
(const std::string& name, const TYPE& value) \
{ \
  auto location = get_uniform_location(name); \
  if (location < 0) \
    return; \
  glUniform##GL_POSTFIX(location, value); \
} \
template <> \
inline void shader_program::set_uniform_array \
(const std::string& name, const std::vector<TYPE>& values) \
{ \
  auto location = get_uniform_location(name); \
  if (location < 0) \
    return; \
  glUniform##GL_POSTFIX##v(location, (GLsizei) values.size(), values.data()); \
} \

#define SPECIALIZE_SET_UNIFORM_VECTORS(TYPE, GL_POSTFIX) \
template <> \
inline void shader_program::set_uniform \
(const std::string& name, const TYPE& value) \
{ \
  auto location = get_uniform_location(name); \
  if (location < 0) \
    return; \
  glUniform##GL_POSTFIX##v(location, 1, value.data()); \
} \
template <> \
inline void shader_program::set_uniform_array \
(const std::string& name, const std::vector<TYPE>& values) \
{ \
  auto location = get_uniform_location(name); \
  if (location < 0) \
    return; \
  glUniform##GL_POSTFIX##v(location, (GLsizei) values.size(), values[0].data()); \
} \

#define SPECIALIZE_SET_UNIFORM_MATRICES(TYPE, GL_POSTFIX) \
template <> \
inline void shader_program::set_uniform \
(const std::string& name, const TYPE& value) \
{ \
  auto location = get_uniform_location(name); \
  if (location < 0) \
    return; \
  glUniformMatrix##GL_POSTFIX##v(location, 1, GL_FALSE, value.data()); \
} \
template <> \
inline void shader_program::set_uniform_array \
(const std::string& name, const std::vector<TYPE>& values) \
{ \
  auto location = get_uniform_location(name); \
  if (location < 0) \
    return; \
  glUniformMatrix##GL_POSTFIX##v(location, (GLsizei) values.size(), GL_FALSE, values[0].data()); \
} \

// Boolean is a special case (std::vector<bool> does not have a .data() function.
template <>
inline void shader_program::set_uniform(const std::string& name, const bool& value)
{
  auto location = get_uniform_location(name);
  if (location < 0)
    return;
  glUniform1i(location, value);
}

using Vector2ui = Eigen::Matrix<unsigned int, 2, 1>;
using Vector3ui = Eigen::Matrix<unsigned int, 3, 1>;
using Vector4ui = Eigen::Matrix<unsigned int, 4, 1>;

SPECIALIZE_SET_UNIFORM_BASIC_TYPES(float       , 1f)
SPECIALIZE_SET_UNIFORM_BASIC_TYPES(int         , 1i)
SPECIALIZE_SET_UNIFORM_BASIC_TYPES(unsigned int, 1ui)

SPECIALIZE_SET_UNIFORM_VECTORS(Eigen::Vector2f, 2f)
SPECIALIZE_SET_UNIFORM_VECTORS(Eigen::Vector2i, 2i)
SPECIALIZE_SET_UNIFORM_VECTORS(Vector2ui      , 2ui)
SPECIALIZE_SET_UNIFORM_VECTORS(Eigen::Vector3f, 3f)
SPECIALIZE_SET_UNIFORM_VECTORS(Eigen::Vector3i, 3i)
SPECIALIZE_SET_UNIFORM_VECTORS(Vector3ui      , 3ui)
SPECIALIZE_SET_UNIFORM_VECTORS(Eigen::Vector4f, 4f)
SPECIALIZE_SET_UNIFORM_VECTORS(Eigen::Vector4i, 4i)
SPECIALIZE_SET_UNIFORM_VECTORS(Vector4ui      , 4ui)

SPECIALIZE_SET_UNIFORM_MATRICES(Eigen::Matrix2f, 2f)
SPECIALIZE_SET_UNIFORM_MATRICES(Eigen::Matrix3f, 3f)
SPECIALIZE_SET_UNIFORM_MATRICES(Eigen::Matrix4f, 4f)

#undef SPECIALIZE_SET_UNIFORM_BASIC_TYPES
#undef SPECIALIZE_SET_UNIFORM_VECTORS
#undef SPECIALIZE_SET_UNIFORM_MATRICES

} // namespace pvt

#endif // #ifndef PVT_SHADER_PROGRAM_HPP_
