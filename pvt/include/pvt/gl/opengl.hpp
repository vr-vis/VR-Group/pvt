//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_OPENGL_HPP_
#define PVT_OPENGL_HPP_

#include <iostream>

#ifdef _WIN32
#include <gl/glew.h>
#elif __APPLE__
#include <OpenGL/gl3.h>
#else
#include <gl/gl.h>
#endif

namespace pvt {

//------------------------------------------------------------------------------
namespace opengl
{
  inline void init()
  {
    #ifdef _WIN32
    glewExperimental = true;
    glewInit();
    #elif __APPLE__

    #else

    #endif
  }

  inline void print_error(const char* p_prefix)
  {
    GLenum error = GL_NO_ERROR;
    do 
    {
      error = glGetError();
      if (error != GL_NO_ERROR)
        std::cout << p_prefix << ": " << error << std::endl;
    } 
    while (error != GL_NO_ERROR);
  }
} // namespace opengl

} // namespace pvt

#endif // #ifndef PVT_OPENGL_HPP_