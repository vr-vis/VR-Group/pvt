//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_BUFFER_HPP_
#define PVT_BUFFER_HPP_

#include <pvt/gl/opengl.hpp>

namespace pvt {

//------------------------------------------------------------------------------
template<GLenum target, GLenum usage = GL_STATIC_DRAW>
class buffer
{
public:
   buffer()
  {
    glGenBuffers(1, &m_id);
  }
   buffer(GLuint native_handle) : m_id(native_handle), m_managed(false)
  {
    
  }
  ~buffer()
  {
    if (m_managed)
      glDeleteBuffers(1, &m_id);
  }

  void bind  ()
  {
    glBindBuffer(target, m_id);
  }
  void unbind()
  {
    glBindBuffer(target, 0   );
  }

  void bind_base (GLuint index)
  {
    glBindBufferBase(target, index, m_id);
  };
  void bind_range(GLuint index, GLintptr offset, GLsizei length)
  {
    glBindBufferRange(target, index, m_id, offset, length);
  }
  
  void allocate(GLsizeiptr size)
  {
    set_data(size, nullptr);
  }
  void set_data(GLsizeiptr size, const void* data)
  {
    glBufferData(target, size, data, usage);
  }

  void  set_sub_data(GLintptr offset, GLsizeiptr size, const void* data)
  {
    glBufferSubData(target, offset, size, data);
  }
  void* get_sub_data(GLintptr offset, GLsizeiptr size) const
  {
    void* subdata = nullptr;
    glGetBufferSubData(target, offset, size, subdata);
    return subdata;
  }

  void* map  (GLenum access = GL_READ_WRITE)
  {
    return glMapBuffer(target, access);
  }
  void  unmap()
  {
    glUnmapBuffer(target);
  }

  bool   is_valid         () const
  {
    return glIsBuffer(m_id);
  }
  GLuint get_native_handle() const
  {
    return m_id;
  }

private:
  GLuint m_id      = 0;
  bool   m_managed = true;
};

typedef buffer<GL_ARRAY_BUFFER>         array_buffer;
typedef buffer<GL_ELEMENT_ARRAY_BUFFER> element_array_buffer;
typedef buffer<GL_PIXEL_PACK_BUFFER>    pixel_pack_buffer;
typedef buffer<GL_PIXEL_UNPACK_BUFFER>  pixel_unpack_buffer;
typedef array_buffer                    vertex_buffer;
typedef element_array_buffer            index_buffer;

} // namespace pvt

#endif // #ifndef PVT_BUFFER_HPP_