//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_TEXTURE_HPP_
#define PVT_TEXTURE_HPP_

#include <pvt/gl/buffer.hpp>
#include <pvt/gl/opengl.hpp>

namespace pvt {

//------------------------------------------------------------------------------
template<GLenum target>
class texture
{
public:
   texture()
  {
    glGenTextures   (1, &m_id);
  }
  ~texture()
  {
    glDeleteTextures(1, &m_id);
  }
  
  static void set_active(GLenum texture_unit)
  {
    glActiveTexture(texture_unit);
  }
  static void enable    ()
  {
    glEnable(target);
  }
  static void disable   ()
  {
    glDisable(target);
  }
  
  void bind  ()
  {
    glBindTexture(target, m_id);
  }
  void unbind()
  {
    glBindTexture(target, 0);
  }

  static void set_min_filter(GLenum mode)
  {
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, mode);
  }
  static void set_mag_filter(GLenum mode)
  {
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, mode);
  }

  static void set_wrap_s(GLenum mode)
  {
    glTexParameteri(target, GL_TEXTURE_WRAP_S, mode);
  }
  static void set_wrap_t(GLenum mode)
  {
    glTexParameteri(target, GL_TEXTURE_WRAP_T, mode);
  }
  static void set_wrap_r(GLenum mode)
  {
    glTexParameteri(target, GL_TEXTURE_WRAP_R, mode);
  }

  static void generate_mipmaps()
  {
    glGenerateMipmap(target);
  }

  bool   is_valid         () const
  {
    return glIsTexture(m_id);
  }
  GLuint get_native_handle() const
  {
    return m_id;
  }

private:
  GLuint m_id;
};


//------------------------------------------------------------------------------
class texture_1d : public texture<GL_TEXTURE_1D>
{
public:
   texture_1d() = default;
  ~texture_1d() = default;

  static void image_1d(
    GLenum       internal_format,
    GLsizei      width, 
    GLenum       format, 
    GLenum       type, 
    const void*  data = nullptr)
  {
    glTexImage1D(GL_TEXTURE_1D, 
                 0, 
                 internal_format, 
                 width, 
                 0, 
                 format, 
                 type, 
                 data);
  }
};


//------------------------------------------------------------------------------
class texture_2d : public texture<GL_TEXTURE_2D>
{
public:
  static void image_2d(
    GLenum       internal_format,
    GLsizei      width,
    GLsizei      height,
    GLenum       format,
    GLenum       type,
    const void*  data = nullptr)
  {
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 internal_format,
                 width,
                 height,
                 0,
                 format,
                 type,
                 data);
  }
};


//------------------------------------------------------------------------------
class texture_3d : public texture<GL_TEXTURE_3D>
{
public:
  static void image_3d(
    GLenum       internal_format,
    GLsizei      width,
    GLsizei      height,
    GLsizei      depth,
    GLenum       format,
    GLenum       type,
    const void*  data = nullptr)
  {
    glTexImage3D(GL_TEXTURE_3D,
                 0,
                 internal_format,
                 width,
                 height,
                 depth,
                 0,
                 format,
                 type,
                 data);
  }
};


//------------------------------------------------------------------------------
class buffer_texture : public texture<GL_TEXTURE_BUFFER>
{
public:
  template<GLenum buffer_type>
  void set_buffer(GLenum internal_format, const buffer<buffer_type>& buffer)
  {
     glTexBuffer(GL_TEXTURE_BUFFER, internal_format, buffer.get_native_handle());
  }
};

} // namespace pvt

#endif // #ifndef PVT_TEXTURE_HPP_
