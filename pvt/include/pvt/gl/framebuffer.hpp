﻿//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_FRAMEBUFFER_HPP_
#define PVT_FRAMEBUFFER_HPP_

#include <vector>

#include <pvt/gl/opengl.hpp>
#include <pvt/gl/texture.hpp>

namespace pvt {

//------------------------------------------------------------------------------
template<GLenum target>
class framebuffer_base
{
public:
   framebuffer_base()
  {
    glGenFramebuffers(1, &m_id);
  }
   framebuffer_base(GLuint native_handle) : m_id(native_handle), m_managed(false)
  {

  }
  ~framebuffer_base()
  {
    if (m_managed)
      glDeleteFramebuffers(1, &m_id);
  }

  void bind  ()
  {
    glBindFramebuffer(target, m_id);
  }
  void unbind()
  {
    glBindFramebuffer(target, 0);
  }

  void texture_1d(GLenum attachment, const texture_1d& texture, GLuint level = 0)
  {
    glFramebufferTexture1D(target, attachment, GL_TEXTURE_1D, texture.get_native_handle(), level);
  }
  void texture_2d(GLenum attachment, const texture_2d& texture, GLuint level = 0)
  {
    glFramebufferTexture2D(target, attachment, GL_TEXTURE_2D, texture.get_native_handle(), level);
  }
  void texture_3d(GLenum attachment, const texture_3d& texture, GLuint level = 0, GLuint layer = 0)
  {
    glFramebufferTexture3D(target, attachment, GL_TEXTURE_3D, texture.get_native_handle(), level, layer);
  }

  static void set_read_buffer (GLenum attachment)
  {
    glReadBuffer(attachment);
  }
  static void set_draw_buffer (GLenum attachment)
  {
    glDrawBuffers(1, &attachment);
  }
  static void set_draw_buffers(const std::vector<GLenum> attachments)
  {
    glDrawBuffers(attachments.size(), attachments.data());
  }
  
  static void clear_color_buffer  (GLint draw_buffer_index, GLint   value)
  {
    glClearBufferiv (GL_COLOR, draw_buffer_index, &value);
  }
  static void clear_color_buffer  (GLint draw_buffer_index, GLuint  value)
  {
    glClearBufferuiv(GL_COLOR, draw_buffer_index, &value);
  }
  static void clear_color_buffer  (GLint draw_buffer_index, GLfloat value)
  {
    glClearBufferfv (GL_COLOR, draw_buffer_index, &value);
  }
  static void clear_color_buffer  (GLint draw_buffer_index, std::vector<GLint>   value)
  {
    glClearBufferiv (GL_COLOR, draw_buffer_index, value.data());
  }
  static void clear_color_buffer  (GLint draw_buffer_index, std::vector<GLuint>  value)
  {
    glClearBufferuiv(GL_COLOR, draw_buffer_index, value.data());
  }
  static void clear_color_buffer  (GLint draw_buffer_index, std::vector<GLfloat> value)
  {
    glClearBufferfv (GL_COLOR, draw_buffer_index, value.data());
  }
  static void clear_depth_buffer  (GLfloat value = 0)
  {
    glClearBufferfv(GL_DEPTH, 0, &value);
  }
  static void clear_stencil_buffer(GLint   value = 0)
  {
    glClearBufferiv(GL_STENCIL, 0, &value);
  }

  bool is_complete()
  {
    return glCheckFramebufferStatus(target) == GL_FRAMEBUFFER_COMPLETE;
  }

  bool   is_valid         () const
  {
    return glIsFramebuffer(m_id);
  }
  GLuint get_native_handle() const
  {
    return m_id;
  }

private:
  GLuint m_id      = 0;
  bool   m_managed = true;
};

typedef framebuffer_base<GL_FRAMEBUFFER>      framebuffer;
typedef framebuffer_base<GL_READ_FRAMEBUFFER> read_framebuffer;
typedef framebuffer_base<GL_DRAW_FRAMEBUFFER> draw_framebuffer;

} // namespace pvt

#endif // #ifndef PVT_FRAMEBUFFER_HPP_