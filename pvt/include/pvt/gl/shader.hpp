//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_SHADER_HPP_
#define PVT_SHADER_HPP_

#include <fstream>

#include <pvt/gl/opengl.hpp>

namespace pvt {

//------------------------------------------------------------------------------
template<GLenum type>
class shader
{
public:
   shader()
  {
    m_id = glCreateShader(type);
  }
   shader(const std::string& shader_string) : shader()
  {
    from_string(shader_string);
    compile    ();
  }
  ~shader()
  {
    glDeleteShader(m_id);
  }

  void from_file  (const std::string& filename)
  {
    std::ifstream filestream(filename);
    from_string(std::string(
      std::istreambuf_iterator<char>(filestream),
      std::istreambuf_iterator<char>()));
  }
  void from_string(const std::string& shader_string)
  {
    auto shader_cstring = shader_string.c_str();
    glShaderSource(m_id, 1, &shader_cstring, nullptr);
  }

  bool compile()
  {
    glCompileShader(m_id);

    auto compile_status = 0;
    glGetShaderiv(m_id, GL_COMPILE_STATUS, &compile_status);
    return compile_status;
  }

  GLuint get_native_handle() const
  {
    return m_id;
  }

private:
  GLuint m_id;
};

typedef shader<GL_VERTEX_SHADER>          vertex_shader;
typedef shader<GL_FRAGMENT_SHADER>        fragment_shader;
typedef shader<GL_GEOMETRY_SHADER>        geometry_shader;
typedef shader<GL_TESS_CONTROL_SHADER>    tess_control_shader;
typedef shader<GL_TESS_EVALUATION_SHADER> tess_evaluation_shader;
#ifndef __APPLE__
typedef shader<GL_COMPUTE_SHADER>         compute_shader;
#endif

} // namespace pvt

#endif // #ifndef PVT_SHADER_HPP_
