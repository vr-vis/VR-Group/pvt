//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_METRIC_HPP_
#define PVT_METRIC_HPP_

#include <pvt/api.hpp>
#include <pvt/hierarchy.hpp>
#include <pvt/name_id_pair.hpp>

namespace pvt {

class perf_data;

//------------------------------------------------------------------------------
class PVT_API metric : public name_id_pair<'M'>, public hierarchy<metric>
{
public:
  metric(std::size_t        id,
         const std::string& display_name,
         const std::string& unit_of_measurement)
  : name_id_pair         (display_name, id)
  , m_unit_of_measurement(unit_of_measurement)
  {
    
  }
  
  const std::string& get_unit_of_measurement() const
  {
    return m_unit_of_measurement;
  }
  
private:
  std::string m_unit_of_measurement;
};

} // namespace pvt

#endif // #ifndef PVT_METRIC_HPP_
