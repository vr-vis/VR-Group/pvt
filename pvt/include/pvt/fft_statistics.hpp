//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_FFT_STATISTICS_HPP_
#define PVT_FFT_STATISTICS_HPP_

#include <cassert>

#include <fftw3.h>

#include <pvt/cartesian_topology_index.hpp>
#include <pvt/data_array.hpp>
#include <pvt/severity_view.hpp>

namespace pvt {

class fft;

//------------------------------------------------------------------------------
class fft_statistics
{
public:
  enum fft_statistics_attribute
  {
    autocorrelation = 0,
    mean,
    variance,
    standard_deviation,
    num_attributes
  };
  
  fft_statistics(std::size_t num_dimensions)
  : m_data(num_dimensions)
  {
    
  }
  
  /// \todo Better take cartesian_topology as parameter in all related c'tors?
  fft_statistics()
  : fft_statistics(num_attributes)
  {
    
  }
  
  double get(fft_statistics_attribute attribute) const
  {
    assert(attribute < m_data.size());
    return m_data.at(attribute);
  }
  void   set(std::size_t attribute, double value)
  {
    assert(attribute < m_data.size());
    m_data.at(attribute) = value;
  }
  
  void read(const std::string& filename)
  {
    m_data.read(filename);
  }  
  void dump(const std::string& filename) const
  {
    m_data.dump(filename);
  }
  
  void compute(const severity_view& severity_view)
  {
    severity_view.get_performance_data()->
      get_fft_statistics(severity_view, *this);
  } 
  /// \todo Implement in order to allow for getting fft_statistics via perf_data_file interface, similar to directed_variance.
  void compute(const fft& fft); 
  
private:
  data_array<double> m_data;
};

} // namespace pvt

#endif // #ifndef PVT_FFT_STATISTICS_HPP_
