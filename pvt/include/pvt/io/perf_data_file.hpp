//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_FILE_HPP_
#define PVT_PERF_DATA_FILE_HPP_

#include <memory>
#include <string>
#include <vector>

#include <fftw3.h>

#include <pvt/export_flags.hpp>

namespace pvt {

template <typename T> class data_array;
class cartesian_topology;
class cnode;
class directed_variance;
class fft;
class fft_statistics;
class metric;
class perf_data;
class severity_view;
class sysnode;
class system_geometry;

//------------------------------------------------------------------------------
class perf_data_file
{
public:
  perf_data_file()
  {
    
  }
  perf_data_file(const std::string& filename) : m_filename(filename)
  {
    
  }
  virtual ~perf_data_file()
  {
    
  }

  static std::unique_ptr<perf_data_file> construct(const std::string& filename);
  
  const std::string& get_filename () const
  {
    return m_filename;
  }
  std::size_t        get_data_size() const
  {
    return m_data_size;
  }
  
  virtual bool setup() = 0;
  virtual bool load_cnodes (
    std::vector<cnode>&              cnodes,
    std::vector<cnode*>&             root_cnodes         ) = 0;
  virtual bool load_metrics(
    std::vector<metric>&             metrics,
    std::vector<metric*>&            root_metrics        ) = 0;
  virtual bool load_sysnodes(
    std::vector<sysnode>&            sysnodes,
    std::vector<sysnode*>&           root_sysnodes       ) = 0;
  virtual bool load_cartesian_topologies(
    std::vector<cartesian_topology>& cartesian_topologies) = 0;
  virtual bool load_system_geometries(
    const std::vector<sysnode*>&     root_sysnodes,
    std::vector<system_geometry>&    system_geometries   ) = 0;
  
  virtual bool write(const perf_data&                  perf_data,
                     export_flags                      export_flags) const = 0;
  virtual bool write(const perf_data&                  perf_data,
                     export_flags                      export_flags,
                     const std::vector<const metric*>& metric_ids,
                     const std::vector<const cnode *>& cnode_ids   ) const = 0;
  
  virtual double get_severity              (const severity_view& severity_view    ) const = 0;
  virtual void   get_severities            (const severity_view& severity_view,
                                            data_array<double>&  severities       ) const = 0;
  virtual void   get_fft                   (const severity_view& severity_view, 
                                            fft&                 fft              ) const = 0;
  virtual void   get_fft_statistics        (const severity_view& severity_view, 
                                            fft_statistics&      fft_statistics   ) const = 0;
  virtual void   get_directed_variance     (const severity_view& severity_view,
                                            directed_variance&   directed_variance) const = 0;
  virtual double get_autocorrelation_off_dc(const severity_view& severity_view    ) const = 0;
  
protected:
  std::string m_filename;
  std::size_t m_data_size = 0;
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_FILE_HPP_
