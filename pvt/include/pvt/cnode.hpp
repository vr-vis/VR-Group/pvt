//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_CNODE_HPP_
#define PVT_CNODE_HPP_

#include <pvt/api.hpp>
#include <pvt/hierarchy.hpp>
#include <pvt/name_id_pair.hpp>

namespace pvt {

class perf_data;

//------------------------------------------------------------------------------
class PVT_API cnode : public name_id_pair<'C'>, public hierarchy<cnode>
{
public:
  cnode(std::size_t id, const std::string& display_name)
  : name_id_pair(display_name, id)
  {
    
  }
};

} // namespace pvt

#endif // #ifndef PVT_CNODE_HPP_
