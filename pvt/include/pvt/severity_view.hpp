//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_SEVERITY_VIEW_HPP_
#define PVT_SEVERITY_VIEW_HPP_

#include <vector>

#include <pvt/api.hpp>
#include <pvt/cartesian_topology.hpp>
#include <pvt/perf_data.hpp>

namespace pvt {

class cnode;
class metric;

//------------------------------------------------------------------------------
class PVT_API severity_view
{
public:
  static const bool total = true;
  static const bool self  = false;
  
  /// \todo Try to remove PerfData*
  severity_view(const perf_data* p_performance_data,
                const metric*    p_metric        = nullptr,
                bool             is_metric_total = true,
                const cnode*     p_cnode         = nullptr,
                bool             is_cnode_total  = true)
  : mp_perf_data      (p_performance_data)
  , mp_metric         (p_metric          )
  , m_is_metric_total (is_metric_total   )
  , mp_cnode          (p_cnode           )
  , m_is_cnode_total  (is_cnode_total    )
  {
    
  }
  
  const perf_data* get_performance_data() const
  {
    return mp_perf_data;
  }
  
  const metric* get_metric      () const
  {
    return mp_metric;
  }
  void          set_metric      (const metric* p_metric)
  {
    mp_metric = p_metric;
  }
  bool          is_metric_total () const
  {
    return m_is_metric_total;
  }
  void          set_metric_total(bool is_total)
  {
    m_is_metric_total = is_total;
  }

  const cnode*  get_cnode       () const
  {
    return mp_cnode;
  }
  void          set_cnode       (const cnode* p_cnode)
  {
    mp_cnode = p_cnode;
  }
  bool          is_cnode_total  () const
  {
    return m_is_cnode_total;
  }
  void          set_cnodes_total(bool is_total)
  {
    m_is_cnode_total = is_total;
  }

  bool          is_valid        () const
  {
    return mp_metric != nullptr && mp_cnode != nullptr;
  }

  bool operator==(const severity_view& other) const
  {
    bool result = true;
    
    result &= (mp_perf_data      == other.mp_perf_data     );
    result &= (mp_metric         == other.mp_metric        );
    result &= (mp_cnode          == other.mp_cnode         );
    result &= (m_is_metric_total == other.m_is_metric_total);  
    result &= (m_is_cnode_total  == other.m_is_cnode_total );
    
    return result;
  } 
  bool operator!=(const severity_view& other) const
  {
    return !(*this == other);
  }
  
private:
  const perf_data* mp_perf_data;
  const metric*    mp_metric;
  bool             m_is_metric_total;
  const cnode*     mp_cnode;
  bool             m_is_cnode_total;
};

} // namespace pvt

#endif // #ifndef PVT_SEVERITY_VIEW_HPP_
