//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_HIERARCHY_HPP_
#define PVT_HIERARCHY_HPP_

#include <vector>

namespace pvt {

//------------------------------------------------------------------------------
template <typename type>
class hierarchy
{
public:
  void                      add_child       (type* p_child)
  {
    m_children.push_back(p_child);
    p_child->mp_parent = static_cast<type*>(this);
  }
  const std::vector<type*>& get_children    () const
  {
    return m_children;
  }
  std::size_t               get_num_children() const
  {
    return m_children.size();
  }
  bool                      has_children    () const
  {
    return m_children.size() > 0;
  }
  type*                     parent          () const
  {
    return mp_parent;
  }

protected:
  std::vector<type*> m_children;
  type*              mp_parent = nullptr;
};

} // namespace pvt

#endif // #ifndef PVT_HIERARCHY_HPP_
