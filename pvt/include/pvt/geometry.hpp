//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_GEOMETRY_HPP_
#define PVT_GEOMETRY_HPP_

#include <vector>

#include <Eigen/Dense>

namespace pvt {

//------------------------------------------------------------------------------
template<
  typename vertex_type   = float, 
  typename normal_type   = float, 
  typename texcoord_type = float,
  typename index_type    = unsigned int>
class geometry_t
{
public:
  geometry_t()
  {

  }
  geometry_t(const std::string& name) : m_name(name)
  {
    
  }
  geometry_t(const std::string&                name     ,
             const std::vector<vertex_type  >& vertices , 
             const std::vector<normal_type  >& normals  ,
             const std::vector<texcoord_type>& texcoords,
             const std::vector<index_type   >& indices) :
             m_name     (name     ),
             m_vertices (vertices ),
             m_normals  (normals  ),
             m_texcoords(texcoords),
             m_indices  (indices  )
  {
    
  }

  const std::string&                get_name            () const
  {
    return m_name;
  }
  const std::vector<vertex_type  >& get_vertices        () const
  {
    return m_vertices;
  }
  const std::vector<normal_type  >& get_normals         () const
  {
    return m_normals;
  }
  const std::vector<texcoord_type>& get_texcoords       () const
  {
    return m_texcoords;
  }
  const std::vector<index_type   >& get_indices         () const
  {
    return m_indices;
  }
  const std::vector<std::size_t  >& get_severity_indices() const
  {
    return m_severity_indices;
  }
  const float&                      get_area            () const
  {
    return m_area;
  }
  
  void set_name            (const std::string&                name            )
  {
    m_name      = name;
  }
  void set_vertices        (const std::vector<vertex_type  >& vertices        )
  {
    m_vertices  = vertices;
  }
  void set_normals         (const std::vector<normal_type  >& normals         )
  {
    m_normals   = normals;
  }
  void set_texcoords       (const std::vector<texcoord_type>& texcoords       )
  {
    m_texcoords = texcoords;
  }
  void set_indices         (const std::vector<index_type   >& indices         ) 
  { 
    m_indices   = indices;
  }
  void set_severity_indices(const std::vector<std::size_t  >& severity_indices)
  { 
    m_severity_indices = severity_indices;
  }
  void set_area            (const float&                      area            )
  {
    m_area      = area;
  }

private:
  friend class perf_data_file_native_system_geometry_loader;

  std::string                m_name;
  std::vector<vertex_type>   m_vertices;
  std::vector<normal_type>   m_normals;
  std::vector<texcoord_type> m_texcoords;
  std::vector<index_type>    m_indices;
  std::vector<std::size_t>   m_severity_indices;
  float                      m_area = 0;
};

typedef geometry_t<Eigen::Vector3f, Eigen::Vector3f, Eigen::Vector2f> geometry;

} // namespace pvt

#endif // #ifndef PVT_GEOMETRY_HPP_
