// Copyright 2011 - 2012 Andrzej Krzemienski.
// Copyright 2016 by Ali Demiralp
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// nstd::optional is based on and implements a subset of 
//  std::optional by Fernando Cacciola and Andrzej Krzemienski.

#ifndef PVT_OPTIONAL_HPP_
#define PVT_OPTIONAL_HPP_

#include <cassert>
#include <functional>
#include <initializer_list>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

namespace pvt {

// 20.5.5, In-place construction
struct in_place_t {};

// 20.5.6, Disengaged state indicator
struct nullopt_t
{
  struct    init  {};
  nullopt_t(init) {}
};
const nullopt_t nullopt((nullopt_t::init()));

// 20.5.7, Class bad_optional_access
class bad_optional_access : public std::logic_error
{
public:
  explicit bad_optional_access(const std::string& what_arg) : logic_error { what_arg } {}
  explicit bad_optional_access(const char*        what_arg) : logic_error { what_arg } {}
};

// 20.5.4, Optional for object types
template<class T>
class optional
{
  typedef typename std::aligned_storage<sizeof(T), std::alignment_of<T>::value>::type aligned_storage_t;

public:
  // 20.5.4.1, Constructors
  optional()          {}
  optional(nullopt_t) {}
  optional(const T&  value)
  {
    initialize(value);
  }
  optional(      T&& value)
  {
    initialize(std::move(value));
  }
  template <class... Args>
  optional(in_place_t,                              Args&&... args)
  {
    initialize(std::forward<Args>(args)...);
  }
  template <class U, class... Args>
  optional(in_place_t, std::initializer_list<U> il, Args&&... args)
  {
    initialize(il, std::forward<Args>(args)...);
  }

  optional(const optional&  rhs)
  {
    if (rhs.init_)
      initialize(*rhs);
  }
  optional(      optional&& rhs)       
  {
    if (rhs.init_)
      initialize(std::move(*rhs));
  }

  // 20.5.4.2, Destructor
  ~optional()
  {
    if (init_)
      dataptr()->T::~T();
  }

  // 20.5.4.3, Assignment
  optional& operator=(nullopt_t)
  {
    clear();
    return *this;
  }
  optional& operator=(const optional&  rhs)
  {
    if      (init_ == true  && rhs.init_ == false) clear();
    else if (init_ == false && rhs.init_ == true ) initialize(*rhs);
    else if (init_ == true  && rhs.init_ == true ) contained_val() = *rhs;
    return *this;
  }
  optional& operator=(      optional&& rhs)
  {
    if      (init_ == true  && rhs.init_ == false) clear();
    else if (init_ == false && rhs.init_ == true ) initialize(std::move(*rhs));
    else if (init_ == true  && rhs.init_ == true ) contained_val() = std::move(*rhs);
    return *this;
  }

  template <class... Args>
  void emplace(Args&&... args)
  {
    clear();
    initialize(std::forward<Args>(args)...);
  }
  template <class U, class... Args>
  void emplace(std::initializer_list<U> il, Args&&... args)
  {
    clear();
    initialize(il, std::forward<Args>(args)...);
  }

  // 20.5.4.4, Swap
  void swap(optional& rhs)
  {
    if      (init_ == true  && rhs.init_ == false) { rhs.initialize(std::move(**this));     clear(); }
    else if (init_ == false && rhs.init_ == true ) {     initialize(std::move(*rhs  )); rhs.clear(); }
    else if (init_ == true  && rhs.init_ == true ) { std::swap(**this, *rhs); }
  }

  // 20.5.4.5, Observers
  T const* operator->() const
  {
    assert(init_);
    return dataptr();
  }
  T*       operator->()
  {
    assert(init_);
    return dataptr();
  }
  T const& operator*() const
  {
    assert(init_);
    return (T const&)(storage_);
  }
  T&       operator*()
  {
    assert(init_);
    return contained_val();
  }
  T const& value() const
  {
    if (!init_)
      throw bad_optional_access("accessing value of disengaged optional (const)");
    return contained_val();
  }
  T&       value()
  {
    if (!init_)
      throw bad_optional_access("accessing value of disengaged optional (non-const)");
    return contained_val();
  }

  T value_or(const T& default_value)
  {
    return init_ ? contained_val() : default_value;
  }

private:
  T* dataptr      () { return (T*)(&storage_); }
  T& contained_val() { return (T&)( storage_); }

  template <class... Args>
  void initialize(Args&&... args)
  {
    assert(!init_);
    ::new (static_cast<void*>(dataptr())) T(std::forward<Args>(args)...);
    init_ = true;
  }
  template <class U, class... Args>
  void initialize(std::initializer_list<U> il, Args&&... args)
  {
    assert(!init_);
    ::new (static_cast<void*>(dataptr())) T(il, std::forward<Args>(args)...);
    init_ = true;
  }
  void clear     ()
  {
    if (init_)
      dataptr()->T::~T();
    init_ = false;
  }

  aligned_storage_t storage_;
  bool init_ = false;
};

// 20.5.8, Relational operators
template <class T> bool operator==(optional<T> const & x, optional<T> const & y)
{
  return bool(x) != bool(y) ? false : bool(x) == false ? true : *x == *y;
}
template <class T> bool operator!=(optional<T> const & x, optional<T> const & y)
{
  return !(x == y);
}
template <class T> bool operator< (optional<T> const & x, optional<T> const & y)
{
  return (!y) ? false : (!x) ? true : *x < *y;
}
template <class T> bool operator> (optional<T> const & x, optional<T> const & y)
{
  return (y < x);
}
template <class T> bool operator<=(optional<T> const & x, optional<T> const & y)
{
  return !(y < x);
}
template <class T> bool operator>=(optional<T> const & x, optional<T> const & y)
{
  return !(x < y);
}

// 20.5.9, Comparison with nullopt
template <class T> bool operator==(optional<T> const & x, nullopt_t)
{
  return (!x);
}
template <class T> bool operator==(nullopt_t, optional<T> const & x)
{
  return (!x);
}
template <class T> bool operator!=(optional<T> const & x, nullopt_t)
{
  return bool(x);
}
template <class T> bool operator!=(nullopt_t, optional<T> const & x)
{
  return bool(x);
}
template <class T> bool operator< (optional<T> const &, nullopt_t)
{
  return false;
}
template <class T> bool operator< (nullopt_t, optional<T> const & x)
{
  return bool(x);
}
template <class T> bool operator<=(optional<T> const & x, nullopt_t)
{
  return (!x);
}
template <class T> bool operator<=(nullopt_t, optional<T> const &)
{
  return true;
}
template <class T> bool operator> (optional<T> const & x, nullopt_t)
{
  return bool(x);
}
template <class T> bool operator> (nullopt_t, optional<T> const &)
{
  return false;
}
template <class T> bool operator>=(optional<T> const &, nullopt_t)
{
  return true;
}
template <class T> bool operator>=(nullopt_t, optional<T> const & x)
{
  return (!x);
}

// 20.5.10, Comparison with T
template <class T> bool operator==(const optional<T>& x, const T& v)
{
  return bool(x) ? *x == v : false;
}
template <class T> bool operator==(const T& v, const optional<T>& x)
{
  return bool(x) ? v == *x : false;
}
template <class T> bool operator!=(const optional<T>& x, const T& v)
{
  return bool(x) ? *x != v : true;
}
template <class T> bool operator!=(const T& v, const optional<T>& x)
{
  return bool(x) ? v != *x : true;
}
template <class T> bool operator< (const optional<T>& x, const T& v)
{
  return bool(x) ? *x < v : true;
}
template <class T> bool operator> (const T& v, const optional<T>& x)
{
  return bool(x) ? v > *x : true;
}
template <class T> bool operator> (const optional<T>& x, const T& v)
{
  return bool(x) ? *x > v : false;
}
template <class T> bool operator< (const T& v, const optional<T>& x)
{
  return bool(x) ? v < *x : false;
}
template <class T> bool operator>=(const optional<T>& x, const T& v)
{
  return bool(x) ? *x >= v : false;
}
template <class T> bool operator<=(const T& v, const optional<T>& x)
{
  return bool(x) ? v <= *x : false;
}
template <class T> bool operator<=(const optional<T>& x, const T& v)
{
  return bool(x) ? *x <= v : true;
}
template <class T> bool operator>=(const T& v, const optional<T>& x)
{
  return bool(x) ? v >= *x : true;
}

// 20.5.11, Specialized algorithms
template<class T>
void swap(optional<T>& x, optional<T>& y)
{
  x.swap(y);
}
template <class T>
optional<typename std::decay<T>::type> make_optional(T&& v)
{
  return optional<typename std::decay<T>::type>(std::forward<T>(v));
}
template <class X>
optional<X&> make_optional(std::reference_wrapper<X> v)
{
  return optional<X&>(v.get());
}

} // namespace pvt

#endif // #ifndef PVT_OPTIONAL_HPP_