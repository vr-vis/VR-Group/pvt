//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_CARTESIAN_TOPOLOGY_INDEX_HPP_
#define PVT_CARTESIAN_TOPOLOGY_INDEX_HPP_

#include <algorithm>
#include <numeric>
#include <vector>

#include <pvt/api.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API cartesian_topology_index
{
public:
  template <typename size_type>
  cartesian_topology_index(const std::vector<size_type>& dimension_sizes)
  : m_num_dimensions   (dimension_sizes.size())
  , m_num_positions    (0)
  , m_dimension_sizes  (dimension_sizes.size(), 0)
  , m_dimension_offsets(dimension_sizes.size(), 1)
  {
    std::copy(
      dimension_sizes  .begin(),
      dimension_sizes  .end  (),
      m_dimension_sizes.begin());
    
    const auto product_callable = [ ] (std::size_t product, std::size_t factor)
    {
      return product *= factor;
    };
    
    // compute index offsets per dimension
    std::partial_sum(  
        m_dimension_sizes  .rbegin(),
      --m_dimension_sizes  .rend  (),
      ++m_dimension_offsets.rbegin(),
      product_callable);
    
    // compute the number of indexable positions
    m_num_positions = std::accumulate(
      m_dimension_sizes.begin(),
      m_dimension_sizes.end  (),
      1,
      product_callable);
  }
  
  template <typename T>
  std::size_t              get_index     (const std::vector<T>& coordinate) const
  {
    return std::inner_product(
      m_dimension_offsets.begin(),
      m_dimension_offsets.end  (),
      coordinate         .begin(),
      0);
  }
  template <typename T>
  void                     get_coordinate(std::size_t index, std::vector<T>& coordinate) const
  {
    std::transform(
      m_dimension_sizes  .begin(),
      m_dimension_sizes  .end  (),
      m_dimension_offsets.begin(),
      coordinate         .begin(),
      [index] (std::size_t size, std::size_t offset)
      {
        return (index / offset) % size; 
      });
  }
  std::vector<std::size_t> get_coordinate(std::size_t index) const
  {
    std::vector<std::size_t> coordinate(m_num_dimensions, 0);
    get_coordinate(index, coordinate);
    return coordinate;
  }
 
  std::size_t                     get_num_dimensions   () const
  {
    return m_num_dimensions;
  }
  std::size_t                     get_num_positions    () const
  {
    return m_num_positions ;
  }
  const std::vector<std::size_t>& get_dimension_sizes  () const
  {
    return m_dimension_sizes;
  }
  const std::vector<std::size_t>& get_dimension_offsets() const
  {
    return m_dimension_offsets;
  }

private:
  std::size_t              m_num_dimensions;
  std::size_t              m_num_positions;
  std::vector<std::size_t> m_dimension_sizes;
  std::vector<std::size_t> m_dimension_offsets;
};

} // namespace pvt

#endif // #ifndef PVT_CARTESIAN_TOPOLOGY_INDEX_HPP_
