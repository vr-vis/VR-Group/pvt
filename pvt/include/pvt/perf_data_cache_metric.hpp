//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_CACHE_METRIC_HPP_
#define PVT_PERF_DATA_CACHE_METRIC_HPP_

#include <cmath>

#include <pvt/api.hpp>
#include <pvt/metric.hpp>
#include <pvt/severity_view.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API perf_data_cache_metric
{
public:
  perf_data_cache_metric(const severity_view* p_severity_view);
  
  double get_severity_total         (const metric* p_metric)
  {
    check_and_update_severity(p_metric);
    return m_severities_total[p_metric->get_id()];
  }
  double get_severity_self          (const metric* p_metric)
  {
    check_and_update_severity(p_metric);
    return m_severities_self[p_metric->get_id()];
  }
  double get_severity_total_relative(const metric* p_metric)
  {
    const auto metric_severity   = get_severity_total(p_metric);
    const auto relative_severity = p_metric->parent() != nullptr ?
      metric_severity / get_severity_total(p_metric->parent()) : 1.0;
    return std::isfinite(relative_severity) ? relative_severity : 0.0;
  }
  double get_severity_self_relative (const metric* p_metric)
  {
    const auto metric_severity   = get_severity_self(p_metric);
    const auto relative_severity = p_metric->parent() != nullptr ?
      metric_severity / get_severity_total(p_metric->parent()) : 1.0;
    return std::isfinite(relative_severity) ? relative_severity : 0.0;
  }
  
private:
  void check_and_update_severity(const metric* p_metric)
  {
    if (m_dirty_severities[p_metric->get_id()])
      update_severity(p_metric);
  }
  void update_severity          (const metric* p_metric);
  
  const severity_view* mp_severity_view;
  data_array<double>   m_severities_load_buffer;
  
  std::vector<double>  m_severities_total;
  std::vector<double>  m_severities_self ;
  std::vector<bool>    m_dirty_severities;
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_CACHE_METRIC_HPP_
