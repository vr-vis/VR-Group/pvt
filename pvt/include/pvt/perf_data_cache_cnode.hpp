//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_CACHE_CNODE_HPP_
#define PVT_PERF_DATA_CACHE_CNODE_HPP_

#include <algorithm>
#include <cmath>
#include <vector>

#include <pvt/api.hpp>
#include <pvt/cnode.hpp>
#include <pvt/data_array.hpp>
#include <pvt/directed_variance.hpp>
#include <pvt/severity_view.hpp>
#include <pvt/suitable_3d_view_computer.hpp>

namespace pvt {

class cnode;
class metric;

//------------------------------------------------------------------------------
class PVT_API perf_data_cache_cnode
{
public:
  perf_data_cache_cnode(const severity_view* p_severity_view);
  
  double get_severity_total         (const cnode* p_cnode)
  {
    check_and_update_severity(p_cnode);
    return m_severities_total[p_cnode->get_id()];
  }
  double get_severity_self          (const cnode* p_cnode)
  {
    check_and_update_severity(p_cnode);
    return m_severities_self[p_cnode->get_id()];
  }
  double get_severity_total_relative(const cnode* p_cnode)
  {
    const auto cnode_severity    = get_severity_total(p_cnode);
    const auto relative_severity = p_cnode->parent() != nullptr ?
      cnode_severity / get_severity_total(p_cnode->parent()) : 1.0;
    return std::isfinite(relative_severity) ? relative_severity : 0.0;
  }
  double get_severity_self_relative (const cnode* p_cnode)
  {
    const auto cnode_severity    = get_severity_self(p_cnode);
    const auto relative_severity = p_cnode->parent() != nullptr ?
      cnode_severity / get_severity_total(p_cnode->parent()) : 1.0;
    return std::isfinite(relative_severity) ? relative_severity : 0.0;
  }

  const directed_variance&  get_directed_variance(const cnode* p_cnode)
  {
    check_and_update_directed_variance(p_cnode);
    return m_directed_variances[p_cnode->get_id()];
  }

  bool is_suitable_3d_view            (const cnode* p_cnode)
  {
    check_and_update_suitable_3d_view(p_cnode);
    return m_suitable_3d_view[p_cnode->get_id()];
  }
  bool is_suitable_3d_view_in_children(const cnode* p_cnode)
  {
    check_and_update_suitable_3d_view(p_cnode);
    return m_suitable_3d_view_in_children[p_cnode->get_id()];
  }
  
  suitable_3d_view_computer& get_suitable_3d_view_computer()
  {
    return m_suitable_3d_view_computer;
  }

  void set_dirty()
  {
    for (std::size_t i = 0; i < m_dirty_severities.size(); ++i)
    {
      m_dirty_severities       [i] = true;
      m_dirty_directed_variance[i] = true;
      m_dirty_suitable_3d_views[i] = true;
    }
  }
  
private:
  void check_and_update_severity          (const cnode* p_cnode)
  {
    if (m_dirty_severities[p_cnode->get_id()])
      update_severity(p_cnode);
  }
  void update_severity                    (const cnode* p_cnode);
  
  void check_and_update_directed_variance (const cnode* p_cnode)
  {
    if (m_dirty_directed_variance[p_cnode->get_id()])
      update_directed_variance(p_cnode);
  }
  void update_directed_variance           (const cnode* p_cnode);
  
  void check_and_update_suitable_3d_view  (const cnode* p_cnode)
  {
    if (m_dirty_suitable_3d_views[p_cnode->get_id()])
      update_suitable_3d_view(p_cnode);
  }
  void update_suitable_3d_view            (const cnode* p_cnode);
  
  const severity_view*           mp_severity_view;
  data_array<double>             m_severities_load_buffer;
  
  std::vector<double>            m_severities_total;
  std::vector<double>            m_severities_self;
  std::vector<bool>              m_dirty_severities;
  
  std::vector<directed_variance> m_directed_variances;
  std::vector<bool>              m_dirty_directed_variance;
  
  suitable_3d_view_computer      m_suitable_3d_view_computer;
  std::vector<bool>              m_suitable_3d_view;
  std::vector<bool>              m_suitable_3d_view_in_children;
  std::vector<bool>              m_dirty_suitable_3d_views;
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_CACHE_CNODE_HPP_
