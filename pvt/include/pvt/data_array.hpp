//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_DATA_ARRAY_HPP_
#define PVT_DATA_ARRAY_HPP_

#include <algorithm>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

#include <fftw3.h>

namespace pvt {

//------------------------------------------------------------------------------
template <typename type>
class data_array : public std::vector<type>
{
public:
  data_array()
  : m_is_guaranteed_zero(false)
  {

  }
  data_array(std::size_t count)
  : m_is_guaranteed_zero(false)
  {
    std::vector<type>::resize(count);
  }
  data_array(std::initializer_list<type> initializer_list)
  : std::vector<type>(initializer_list)
  , m_is_guaranteed_zero(false)
  {
    
  }
  
  type&       at(std::size_t index)
  {
    m_is_guaranteed_zero = false;
    return std::vector<type>::at(index);
  }
  const type& at(std::size_t index) const
  {
    return std::vector<type>::at(index);
  }
  
  void dump(const std::string& filename) const
  {
    FILE* p_file = fopen(filename.c_str(), "wb");
    fwrite(std::vector<type>::data(),
           std::vector<type>::size() * sizeof(type),
           1,
           p_file);
    fclose(p_file);
  }
  void read(const std::string& filename)
  {
    FILE* p_file = fopen(filename.c_str(), "rb");
    if (p_file != nullptr)
    {
      fread(std::vector<type>::data(),
            std::vector<type>::size() * sizeof(type),
            1,
            p_file);
      m_is_guaranteed_zero = false;
    fclose(p_file);
    }
    else
    {
      zero();
    }
    
  }
  
  void zero();
  bool is_guaranteed_zero() const { return m_is_guaranteed_zero; }
  
private:
  bool m_is_guaranteed_zero;
};


//------------------------------------------------------------------------------
template<>
inline void
data_array<double>::zero
()
{
  std::for_each(begin(), end(), [](double& v){v = 0.0; });
  m_is_guaranteed_zero = true;
}


//------------------------------------------------------------------------------
template<>
inline void
data_array<std::size_t>::zero
()
{
  std::for_each(begin(), end(), [](std::size_t& v){v = 0; });
  m_is_guaranteed_zero = true;
}


//------------------------------------------------------------------------------
template<>
inline void
data_array<fftw_complex>::zero
()
{
  std::for_each(begin(), end(), [](fftw_complex& v){
    v[0] = 0.0;
    v[1] = 0.0;
  });
  m_is_guaranteed_zero = true;
}


//------------------------------------------------------------------------------
template<>
inline void
data_array<fftwf_complex>::zero
()
{
  std::for_each(begin(), end(), [](fftwf_complex& v){
    v[0] = 0.0;
    v[1] = 0.0;
  });
  m_is_guaranteed_zero = true;
}

} // namespace pvt

#endif // #ifndef PVT_DATA_ARRAY_HPP_
