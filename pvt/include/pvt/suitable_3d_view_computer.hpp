//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_SUITABLE_3D_VIEW_COMPUTER_HPP_
#define PVT_SUITABLE_3D_VIEW_COMPUTER_HPP_

#include <algorithm>
#include <cassert>
#include <vector>

#include <pvt/api.hpp>

namespace pvt {

class directed_variance;
class severity_view;

//------------------------------------------------------------------------------
class PVT_API suitable_3d_view_computer
{
public:
  suitable_3d_view_computer(std::size_t num_dimensions)
  : m_num_dimensions             (num_dimensions)
  , m_threshold                  (0.01)
  , m_severity_threshold         (0.01)
  , m_use_dimension_for_detection(num_dimensions, 1.0)
  {
    
  }
  
  double get_threshold               () const
  {
    return m_threshold;
  }
  void   set_threshold               (double threshold)
  {
    m_threshold = threshold;
  }

  bool   get_filter_dimension_enabled(std::size_t index) const
  {
    return !static_cast<bool>(m_use_dimension_for_detection[index]);
  }
  void   set_filter_dimension_enabled(std::size_t index, bool filtered)
  {
    assert(index <= m_use_dimension_for_detection.size());
    m_use_dimension_for_detection[index] = static_cast<double>(!filtered);
  }
  void   set_filter_dimension_enabled(const std::vector<bool>& other )
  {
    assert(other.size() == m_use_dimension_for_detection.size());
    std::transform(other.begin(),
                   other.end  (),
                   m_use_dimension_for_detection.begin(),
    [](bool filtered)
    {
      return static_cast<double>(!filtered);
    });
  }
         
  bool is_suitable_3d_view(const severity_view&     severity_view    ) const;
  bool is_suitable_3d_view(const directed_variance& directed_variance, 
                           double                   mean             ) const;
  
  bool is_severe_enough   (const severity_view&     severity_view    ) const;

private:
  std::size_t         m_num_dimensions;
  double              m_threshold;
  double              m_severity_threshold;
  std::vector<double> m_use_dimension_for_detection;
};

} // namespace pvt

#endif // #ifndef PVT_SUITABLE_3D_VIEW_COMPUTER_HPP_
