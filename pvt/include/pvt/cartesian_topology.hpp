//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_CARTESIAN_TOPOLOGY_HPP_
#define PVT_CARTESIAN_TOPOLOGY_HPP_

#include <string>
#include <vector>

#include <pvt/api.hpp>
#include <pvt/cartesian_topology_index.hpp>
#include <pvt/data_array.hpp>
#include <pvt/name_id_pair.hpp>

namespace pvt {

//------------------------------------------------------------------------------
class PVT_API cartesian_topology
: public name_id_pair<'T'>
{
public:
   cartesian_topology(std::size_t                     id,
                      const std::string&              display_name,
                      const cartesian_topology_index& index,
                      const std::vector<std::string>& dimension_names,
                      const data_array<std::size_t>&  topology_to_data_map);
  
  ~cartesian_topology();
  
  const cartesian_topology_index& get_index               () const
  {
    return m_index;
  }
  const std::vector<std::string>& get_dimension_names     () const
  {
    return m_dimension_names;
  }
  const data_array<std::size_t>&  get_topology_to_data_map() const
  {
    return m_topology_to_data_map;
  }
  std::size_t                     get_num_dimensions      () const
  {
    return m_index.get_num_dimensions();
  }
  
  template <typename input_type, typename output_type>
  void map_data(const data_array<input_type>&  data,
                      data_array<output_type>& topology_mapping)
  const
  {
    std::transform(m_topology_to_data_map.begin(),
                   m_topology_to_data_map.end  (),
                   topology_mapping      .begin(),
                   [&data](std::size_t index)
                   {
                     return data.at(index);
                   });
  }
  
private:
  cartesian_topology_index m_index;
  std::vector<std::string> m_dimension_names;
  data_array<std::size_t>  m_topology_to_data_map;
};

} // namespace pvt

#endif // #ifndef PVT_CARTESIAN_TOPOLOGY_HPP_
