//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cmath>

#include <pvt/cartesian_topology.hpp>
#include <pvt/cartesian_topology_index.hpp>
#include <pvt/correlation_computer.hpp>
#include <pvt/data_array.hpp>
#include <pvt/perf_data.hpp>

namespace pvt {

//------------------------------------------------------------------------------
correlation_computer::correlation_computer
(const severity_view&      severity_view,
 const cartesian_topology* p_cartesian_topology)
: mp_perf_data                (severity_view.get_performance_data())
, m_source_fft                (p_cartesian_topology->get_index())
, m_other_fft                 (p_cartesian_topology->get_index())
, m_fft_product_buffer        (p_cartesian_topology->get_index())
, m_inverse_fft_product_buffer(p_cartesian_topology->get_index()
                              .get_num_positions())
, m_dft_inverse               (m_fft_product_buffer, 
                               p_cartesian_topology->get_index(),
                               &m_inverse_fft_product_buffer)
{
  /// \todo Maybe remove this check, but be careful with tests
  if (mp_perf_data != nullptr)
  {
    mp_perf_data->get_fft(severity_view, m_source_fft);
    m_auto_correlation_source = 
      mp_perf_data->get_autocorrelation_off_dc(severity_view);
  }
}


//------------------------------------------------------------------------------
double
correlation_computer::compute_correlation_coefficient
(const severity_view& severity_view)
{
  mp_perf_data->get_fft(severity_view, m_other_fft);

  const auto auto_correlation_other =
    mp_perf_data->get_autocorrelation_off_dc(severity_view);
  
  const auto max_cross_correlation = compute_max_cross_correlation();
  
  const auto corr_coefficient = max_cross_correlation / 
    sqrt(m_auto_correlation_source * auto_correlation_other);
  
  return corr_coefficient;
}


//------------------------------------------------------------------------------
double
correlation_computer::compute_max_cross_correlation
()
{
  if (m_other_fft .is_guaranteed_zero() ||
      m_source_fft.is_guaranteed_zero())
    return 0.0;

  m_source_fft        .multiply_by_conjugate(m_other_fft, m_fft_product_buffer);
  m_fft_product_buffer.clear_dc();
  m_dft_inverse       .compute();

  return get_max_element(m_inverse_fft_product_buffer.begin(),
                         m_inverse_fft_product_buffer.end  ());
}

} // namespace pvt
