//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <fstream>

#include <pvt/cartesian_topology.hpp>
#include <pvt/cnode.hpp>
#include <pvt/correlated_severity_view.hpp>
#include <pvt/correlation_computer.hpp>
#include <pvt/fft_statistics.hpp>
#include <pvt/metric.hpp>
#include <pvt/perf_data.hpp>
#include <pvt/severity_view.hpp>

namespace pvt {

//------------------------------------------------------------------------------
bool
perf_data::load
(const std::string& filename)
{
  mp_perf_data_file = perf_data_file::construct(filename);
  if (mp_perf_data_file != nullptr)
  {
    mp_perf_data_file->setup();
    return initialize();
  }
  return false;
}


//------------------------------------------------------------------------------
bool
perf_data::write
(const std::string&               filename,
export_flags                      export_flags,
const std::vector<const metric*>& metric_ids,
const std::vector<const cnode *>& cnode_ids)
const
{
  auto   perf_data_file = perf_data_file::construct(filename);
  return perf_data_file != nullptr 
    ? perf_data_file->write(*this, export_flags, metric_ids, cnode_ids) 
    : false;
}


//------------------------------------------------------------------------------
const std::string&
perf_data::get_filename
()
const
{
  return mp_perf_data_file->get_filename();
}


//------------------------------------------------------------------------------
std::vector<severity_view> 
perf_data::get_all_severity_views
()
{
  std::vector<severity_view> severity_views;
  for (const metric& metric : m_metrics)
  {
    const bool curr_metric_total_severity = metric.get_children().size() > 0;
    for (const cnode& cnode : m_cnodes)
    {
      const bool curr_cnode_total_severity = cnode.get_children().size() > 0;
      
      severity_views.push_back(severity_view(this,
                                             &metric,
                                             severity_view::self,
                                             &cnode,
                                             severity_view::self));
      if (curr_metric_total_severity)
      {
        severity_views.push_back(severity_view(this,
                                               &metric,
                                               severity_view::total,
                                               &cnode,
                                               severity_view::self));
      }
      if (curr_cnode_total_severity)
      {
        severity_views.push_back(severity_view(this,
                                               &metric,
                                               pvt::severity_view::self,
                                               &cnode,
                                               pvt::severity_view::total));
      }
      if (curr_metric_total_severity && curr_cnode_total_severity)
      {
        severity_views.push_back(severity_view(this,
                                               &metric,
                                               severity_view::total,
                                               &cnode,
                                               severity_view::total));
      }
    }
  }
  return severity_views;
}


//------------------------------------------------------------------------------
std::vector<correlated_severity_view> 
perf_data::get_correlated_views
(const severity_view&        severity_view, 
 std::function<void(double)> progress_callback,
 double                      correlation_threshold)
{
  std::vector<correlated_severity_view> correlated_views;

  correlation_computer correlation_computer(severity_view,
                                            &get_cartesian_topologies()[0]);

  auto severity_views = get_all_severity_views();
  for (auto i = 0; i < severity_views.size(); i++)
  {
    const pvt::severity_view& curr_severity_view = severity_views[i];

    const double correlation_coefficient =
      correlation_computer.compute_correlation_coefficient(curr_severity_view);

    if (correlation_coefficient >  correlation_threshold || 
        correlation_coefficient < -correlation_threshold)
    {
      correlated_views.push_back(correlated_severity_view(
        curr_severity_view,
        correlation_coefficient));
    }

    progress_callback(static_cast<double>(i) / severity_views.size());
  }

  return correlated_views;
}


//------------------------------------------------------------------------------
double
perf_data::get_severity
(const severity_view& severity_view)
const
{
  return mp_perf_data_file->get_severity(severity_view);
}


//------------------------------------------------------------------------------
void
perf_data::get_severities
(const severity_view& severity_view,
 data_array<double>&  severities)
const
{
  mp_perf_data_file->get_severities(severity_view, severities);
}


//------------------------------------------------------------------------------
void
perf_data::get_fft
(const severity_view& severity_view,
 fft&                 fft)
const
{
  mp_perf_data_file->get_fft(severity_view, fft);
}


//------------------------------------------------------------------------------
void
perf_data::get_fft_statistics
(const severity_view& severity_view,
 fft_statistics&      fft_statistics)
const
{
  mp_perf_data_file->get_fft_statistics(severity_view, fft_statistics);
}


//------------------------------------------------------------------------------
void
perf_data::get_directed_variance
(const severity_view& severity_view,
 directed_variance&   directed_variance)
const
{
  mp_perf_data_file->get_directed_variance(severity_view, directed_variance);
}


//------------------------------------------------------------------------------
double
perf_data::get_mean
(const severity_view& severity_view)
const
{
  fft_statistics fft_stat;
  get_fft_statistics(severity_view, fft_stat);
  return fft_stat.get(fft_statistics::mean);
}


//------------------------------------------------------------------------------
double
perf_data::get_variance
(const severity_view& severity_view)
const
{
  fft_statistics fft_stat;
  get_fft_statistics(severity_view, fft_stat);
  return fft_stat.get(fft_statistics::variance);
}


//------------------------------------------------------------------------------
double
perf_data::get_standard_deviation
(const severity_view& severity_view)
const
{
  fft_statistics fft_stat;
  get_fft_statistics(severity_view, fft_stat);
  return fft_stat.get(fft_statistics::standard_deviation);
}


//------------------------------------------------------------------------------
double
perf_data::get_autocorrelation_off_dc
(const severity_view& severity_view)
const
{
  return mp_perf_data_file->get_autocorrelation_off_dc(severity_view);
}

//------------------------------------------------------------------------------
bool perf_data::initialize()
{
  if (mp_perf_data_file != nullptr)
  {
    m_data_size = mp_perf_data_file->get_data_size();

    mp_perf_data_file->load_cnodes              (m_cnodes  , m_root_cnodes  );
    mp_perf_data_file->load_metrics             (m_metrics , m_root_metrics );
    mp_perf_data_file->load_sysnodes            (m_sysnodes, m_root_sysnodes);
    mp_perf_data_file->load_cartesian_topologies(m_cartesian_topologies);
    mp_perf_data_file->load_system_geometries   (m_root_sysnodes, 
                                                 m_system_geometries);

    return true;
  }
  return false;
}
} // namespace pvt
