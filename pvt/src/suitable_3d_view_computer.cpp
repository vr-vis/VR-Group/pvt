//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cmath>

#include <iostream>

#include <pvt/directed_variance.hpp>
#include <pvt/perf_data.hpp>
#include <pvt/severity_view.hpp>
#include <pvt/suitable_3d_view_computer.hpp>

namespace pvt {

//------------------------------------------------------------------------------
bool
suitable_3d_view_computer::is_suitable_3d_view
(const severity_view& severity_view)
const
{
  const perf_data* p_perf_data = severity_view.get_performance_data();
  const double     mean        = p_perf_data->get_mean(severity_view);

  if (mean == 0.0)
    return false;
  
  directed_variance directed_variance(m_num_dimensions);
  p_perf_data->get_directed_variance(severity_view, directed_variance);
  return is_suitable_3d_view(directed_variance, mean);
}


//------------------------------------------------------------------------------
bool
suitable_3d_view_computer::is_suitable_3d_view
(const directed_variance& directed_variance,
 double                   mean)
const
{
  if (mean == 0.0)
    return false;
  
  const double filtered_variance = std::inner_product(
    directed_variance.begin(),
    directed_variance.end  (),
    m_use_dimension_for_detection.begin(),
    0.0);
  
  return std::max(0.0, sqrt(filtered_variance) / mean) >= m_threshold;
}

//------------------------------------------------------------------------------
bool
suitable_3d_view_computer::is_severe_enough
(const severity_view& severity_view)
const
{
  const perf_data* p_perf_data = severity_view.get_performance_data();
  const double     severity    = p_perf_data->get_severity(severity_view);
  
  pvt::severity_view root_severity_view(p_perf_data,
                                        severity_view.get_metric     (),
                                        severity_view.is_metric_total(),
                                        p_perf_data->get_root_cnodes ()[0],
                                        severity_view::total);

  const double root_severity = p_perf_data->get_severity(root_severity_view);
  return (severity / root_severity >= m_severity_threshold);
}

} // namespace pvt
