//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/cartesian_topology_index.hpp>
#include <pvt/dft_inverse.hpp>
#include <pvt/fft.hpp>

namespace pvt {

//------------------------------------------------------------------------------
dft_inverse::dft_inverse
(fft&                            input_fft,
 const cartesian_topology_index& topology_index,
 data_array<double>*             p_output_data)
: m_fftw_dim_sizes  (topology_index)
, m_fft_inverse_plan(nullptr)
{
  m_fft_inverse_plan = fftw_plan_dft_c2r(
    topology_index.get_num_dimensions(),
    m_fftw_dim_sizes.data(),
    input_fft       .data(),
    p_output_data  ->data(),
    FFTW_ESTIMATE);
}


//------------------------------------------------------------------------------
dft_inverse::~dft_inverse
()
{
  fftw_destroy_plan(m_fft_inverse_plan);
}

} // namespace pvt
