#include <pvt/gl/shader_program.hpp>

namespace pvt {

//------------------------------------------------------------------------------
shader_program::shader_program ()
{
  m_id = glCreateProgram();
}


//------------------------------------------------------------------------------
shader_program::~shader_program()
{
  glDeleteProgram(m_id);
}


//------------------------------------------------------------------------------
void 
shader_program::detach_all_shaders
()
{
  GLsizei count;
  std::vector<GLuint> shaders(32);
  glGetAttachedShaders(m_id, (GLsizei) shaders.size(), &count, shaders.data());
  for (auto i = 0; i < count; i++)
    glDetachShader(m_id, shaders[i]);
}


//------------------------------------------------------------------------------
bool 
shader_program::link
()
{
  glLinkProgram(m_id);

  auto link_status = 0;
  glGetProgramiv(m_id, GL_LINK_STATUS, &link_status);
  return link_status;
}


//------------------------------------------------------------------------------
void 
shader_program::bind
()
{
  glUseProgram(m_id);
}


//------------------------------------------------------------------------------
void 
shader_program::unbind
()
{
  glUseProgram(0);
}


//------------------------------------------------------------------------------
void 
shader_program::set_attribute_buffer
(const std::string& name,
 GLuint             size,
 GLuint             type, 
 bool               normalize,
 GLuint             stride,
 GLuint             offset)
{
  auto location = get_attribute_location(name);
  if (location < 0)
    return;
  glVertexAttribPointer(location, 
                        size, 
                        type, 
                        normalize, 
                        stride, 
                        reinterpret_cast<GLvoid*>(offset));
}


//------------------------------------------------------------------------------
void 
shader_program::set_attribute_buffer_integer
(const std::string& name, 
 GLuint             size, 
 GLuint             type, 
 GLuint             stride,
 GLuint             offset)
{
  auto location = get_attribute_location(name);
  if (location < 0)
    return;
  glVertexAttribIPointer(location,
    size,
    type,
    stride,
    reinterpret_cast<GLvoid*>(offset));
}


//------------------------------------------------------------------------------
void 
shader_program::set_attribute_buffer_double
(const std::string& name, 
 GLuint             size, 
 GLuint             type, 
 GLuint             stride, 
 GLuint             offset)
{
  auto location = get_attribute_location(name);
  if (location < 0)
    return;
  glVertexAttribLPointer(location,
    size,
    type,
    stride,
    reinterpret_cast<GLvoid*>(offset));
}


//------------------------------------------------------------------------------
void 
shader_program::enable_attribute_array
(const std::string& name)
{
  auto location = get_attribute_location(name);
  if (location < 0)
    return;
  glEnableVertexAttribArray(location);
}


//------------------------------------------------------------------------------
void 
shader_program::disable_attribute_array
(const std::string& name)
{
  auto location = get_attribute_location(name);
  if (location < 0)
    return;
  glDisableVertexAttribArray(location);
}


//------------------------------------------------------------------------------
GLint 
shader_program::get_uniform_location
(const std::string& name)
{
  return glGetUniformLocation(m_id, name.c_str());
}


//------------------------------------------------------------------------------
GLint 
shader_program::get_attribute_location
(const std::string& name)
{
  return glGetAttribLocation(m_id, name.c_str());
}


//------------------------------------------------------------------------------
bool 
shader_program::is_valid
() const
{
  return glIsProgram(m_id);
}


//------------------------------------------------------------------------------
GLuint 
shader_program::get_native_handle
() const
{
  return m_id;
}


} // namespace pvt
