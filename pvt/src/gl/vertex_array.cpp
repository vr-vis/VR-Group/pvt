#include <pvt/gl/vertex_array.hpp>

namespace pvt {

//------------------------------------------------------------------------------
vertex_array::vertex_array ()
{
  glGenVertexArrays   (1, &m_id);
}


//------------------------------------------------------------------------------
vertex_array::~vertex_array()
{
  glDeleteVertexArrays(1, &m_id);
}


//------------------------------------------------------------------------------
void 
vertex_array::bind
()
{
  glBindVertexArray(m_id);
}


//------------------------------------------------------------------------------
void 
vertex_array::unbind
()
{
  glBindVertexArray(0);
}


//------------------------------------------------------------------------------
bool 
vertex_array::is_valid
() const
{
  return glIsVertexArray(m_id);
}


//------------------------------------------------------------------------------
GLuint 
vertex_array::get_native_handle
() const
{
  return m_id;
}


} // namespace pvt
