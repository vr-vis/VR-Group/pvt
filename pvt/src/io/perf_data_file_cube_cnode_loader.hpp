//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_FILE_CUBE_CNODE_LOADER_HPP_
#define PVT_PERF_DATA_FILE_CUBE_CNODE_LOADER_HPP_

#include <map>
#include <vector>

namespace cube {
  class Cube;
  class Cnode;
}

namespace pvt {

class cnode;

//------------------------------------------------------------------------------
class perf_data_file_cube_cnode_loader
{
public:
  perf_data_file_cube_cnode_loader(
    const cube::Cube*                     p_cube,
    std::vector<cnode>&                   cnodes,
    std::vector<cnode*>&                  root_cnodes,
    std::map<const cnode*, cube::Cnode*>& cnode_to_cube_cnode_map);

  bool load();
  
private:
  void load_cnodes        ();
  void load_root_cnodes   ();
  void process_child_nodes();

  const cube::Cube*                     mp_cube;
  std::vector<cnode>&                   m_cnodes;
  std::vector<cnode*>&                  m_root_cnodes;
  std::map<const cnode*, cube::Cnode*>& m_cnode_to_cube_cnode_map;
  std::map<cube::Cnode*, cnode*>        m_cube_cnode_to_cnode_map;
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_FILE_CUBE_CNODE_LOADER_HPP_
