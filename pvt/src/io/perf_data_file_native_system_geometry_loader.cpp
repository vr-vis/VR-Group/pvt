//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <array>
#include <sstream>
#include <numeric>

#include <pvt/perf_data.hpp>
#include <pvt/sysnode.hpp>
#include <pvt/system_geometry.hpp>

#include "perf_data_file_native_system_geometry_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_native_system_geometry_loader::
perf_data_file_native_system_geometry_loader
(const std::string&            filename, 
 const std::vector<sysnode*>&  root_sysnodes, 
 std::vector<system_geometry>& system_geometries)
: m_geometry_dir_name(filename            + "/geometry/")
, m_geometry_map_file(m_geometry_dir_name + "geometry.map")
, m_root_sysnodes    (root_sysnodes)
, m_system_geometries(system_geometries)
{

}


//------------------------------------------------------------------------------
bool
perf_data_file_native_system_geometry_loader::load
()
{
  std::vector<geometry> geometries;

  load_geometries(geometries);

  if (geometries.size() == 0)
    return false;

  load_geometry_to_data_map(geometries);
  calculate_areas          (geometries);

  m_system_geometries.emplace_back(geometries);

  return true;
}


//------------------------------------------------------------------------------
void
perf_data_file_native_system_geometry_loader::load_geometries
(std::vector<geometry>& geometries)
{
  std::map<std::string, std::string> proc_to_geom_map;
  std::string process_name, mesh_filename;
  while (std::getline(m_geometry_map_file, process_name))
  {
    std::getline(m_geometry_map_file, mesh_filename);
    proc_to_geom_map[process_name] = m_geometry_dir_name + mesh_filename;
  }

  for (const auto& entry : proc_to_geom_map)
  {
    geometries.emplace_back(entry.first);
    load_geometry          (entry.second, geometries.back());
  }
}


//------------------------------------------------------------------------------
void 
perf_data_file_native_system_geometry_loader::load_geometry
(const std::string& filename, geometry& geometry)
{
  std::ifstream file(filename);

  std::vector<Eigen::Vector3f> vertices;
  std::vector<Eigen::Vector3f> normals;
  std::vector<unsigned int>    indices;

  std::string delimiter;

  // Read number of vertices.
  std::size_t num_vertices = 0;
  file >> num_vertices;

  // Read vertices.
  std::size_t vertex_index = 0u;
  vertices.resize(num_vertices, Eigen::Vector3f::Zero());
  normals .resize(num_vertices, Eigen::Vector3f(0.0f, 1.0f, 0.0f));
  for (auto i = 0; i < num_vertices && file.good(); ++i)
  {
    file >> vertex_index;
    file >> delimiter;
    file >> vertices[i].x();
    file >> delimiter;
    file >> vertices[i].z();
    file >> delimiter;
    file >> vertices[i].y();
    file >> delimiter;
  }

  // Read number of indices.
  std::size_t num_indices = 0;
  file >> num_indices;

  // Read indices.
  std::size_t face_index   = 0u;
  std::size_t element_type = 0u;
  std::array<unsigned int, 6> index_buffer = {{ 0u, 0u, 0u, 0u, 0u, 0u }};
  indices.resize(5 * 3 * num_indices, 0);
  for (auto i = 0; i < num_indices && file.good(); ++i)
  {
    // Parse the pentaeder format.
    file >> face_index;
    file >> delimiter;
    file >> element_type;
    file >> delimiter;
    for (auto j = 0; j < 6; j++)
    {
      file >> index_buffer[j];
      file >> delimiter;
    }

    // Add to indices.
    auto index_offset = 5 * 3 * i;
    indices[index_offset +  0] = index_buffer[0] - 1;
    indices[index_offset +  1] = index_buffer[1] - 1;
    indices[index_offset +  2] = index_buffer[2] - 1;
    indices[index_offset +  3] = index_buffer[3] - 1;
    indices[index_offset +  4] = index_buffer[4] - 1;
    indices[index_offset +  5] = index_buffer[5] - 1;
    indices[index_offset +  6] = index_buffer[0] - 1;
    indices[index_offset +  7] = index_buffer[3] - 1;
    indices[index_offset +  8] = index_buffer[1] - 1;
    indices[index_offset +  9] = index_buffer[1] - 1;
    indices[index_offset + 10] = index_buffer[4] - 1;
    indices[index_offset + 11] = index_buffer[2] - 1;
    indices[index_offset + 12] = index_buffer[2] - 1;
    indices[index_offset + 13] = index_buffer[5] - 1;
    indices[index_offset + 14] = index_buffer[1] - 1;

    // Modify vertex normals.
    const Eigen::Vector3f top_a(vertices[indices[index_offset + 1]] -
                                vertices[indices[index_offset + 0]]);
    const Eigen::Vector3f top_b(vertices[indices[index_offset + 2]] -
                                vertices[indices[index_offset + 0]]);
    const Eigen::Vector3f normal_top(top_a.cross(top_b));
    normals[indices[index_offset + 0]] += normal_top;
    normals[indices[index_offset + 1]] += normal_top;
    normals[indices[index_offset + 2]] += normal_top;
  
    const Eigen::Vector3f bot_a(vertices[indices[index_offset + 4]] -
                                vertices[indices[index_offset + 3]]);
    const Eigen::Vector3f bot_b(vertices[indices[index_offset + 5]] -
                                vertices[indices[index_offset + 3]]);
    const Eigen::Vector3f normalBot(bot_a.cross(bot_b));
    normals[indices[index_offset + 3]] += normalBot;
    normals[indices[index_offset + 4]] += normalBot;
    normals[indices[index_offset + 5]] += normalBot;
  }

  // Normalize vertex normals.
  for (auto& normal : normals)
    normal.normalize();

  geometry.set_vertices(vertices);
  geometry.set_normals (normals );
  geometry.set_indices (indices );
}


//------------------------------------------------------------------------------
void
perf_data_file_native_system_geometry_loader::load_geometry_to_data_map
(std::vector<geometry>& geometries)
{
  std::size_t thread_offset = 0;
  
  sysnode* root_sysnode = m_root_sysnodes[0];
  for (std::size_t i = 0; i < root_sysnode->get_num_children(); ++i)
  {
    sysnode* node = root_sysnode->get_children()[i];
    for (std::size_t j = 0; j < node->get_num_children(); ++j)
    {
      sysnode* process_node = node->get_children()[j];
      
      // Find the mesh matching the process node.
      auto name    = process_node->get_name();
      auto geom_it = std::find_if(geometries.begin(), geometries.end(), 
        [name] (const geometry& geom) { return name == geom.get_name(); });

      if (geom_it != geometries.end())
      {
        geometry& geom = *geom_it;
        
        std::vector<int> thread_range(process_node->get_num_children());
        std::iota(thread_range.begin(), thread_range.end(), thread_offset);

        // Copy prevention via friend class.
        geom.m_severity_indices.insert(geom.m_severity_indices.end  (),
                                       thread_range           .begin(),
                                       thread_range           .end  ());
      }
      thread_offset += process_node->get_num_children();
    }
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_native_system_geometry_loader::calculate_areas
(std::vector<geometry>& geometries)
{
  std::size_t outer_triangle_stride = 5;

  for (auto& geometry : geometries)
  {
    const auto& indices  = geometry.get_indices ();
    const auto& vertices = geometry.get_vertices();
    auto        area     = 0.0f;

    for (auto i = 0; i < indices.size(); i += 3)
    {
      if (i / 3 % outer_triangle_stride < 2)
      {
        auto vertex_1 = vertices[indices[i    ]];
        auto vertex_2 = vertices[indices[i + 1]];
        auto vertex_3 = vertices[indices[i + 2]];
        auto cross    = (vertex_2 - vertex_1).cross(vertex_3 - vertex_1);
        area += cross.norm();
      }
    }

    // Divide by 2 twice to calculate area and take area of only one side.
    area /= 4.0f;

    geometry.set_area(area);
  }
}

} // namespace pvt
