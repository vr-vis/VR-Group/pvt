//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <sstream>

#include <pvt/cartesian_topology.hpp>
#include <pvt/perf_data.hpp>

#include "perf_data_file_native_cartesian_topology_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_native_cartesian_topology_loader::
  perf_data_file_native_cartesian_topology_loader
(const std::string&               filename,
 const std::size_t&               data_size,
 std::vector<cartesian_topology>& cartesian_topologies)
: m_topologies_file          (filename + "/cartTopo.list")
, m_topologies_file_base_name(filename + "/cartTopo")
, m_data_size                (data_size)
, m_cartesian_topologies     (cartesian_topologies)
{

}


//------------------------------------------------------------------------------
bool
perf_data_file_native_cartesian_topology_loader::load
()
{
  load_num_topologies();
  load_topologies    ();

  return m_topologies_file.is_open();
}


//------------------------------------------------------------------------------
void
perf_data_file_native_cartesian_topology_loader::load_num_topologies
()
{
  m_topologies_file >> m_num_topologies;
  m_topologies_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_cartesian_topology_loader::load_topologies
()
{
  for (std::size_t id = 0; id < m_num_topologies; ++id)
  {
    const auto display_name    = load_display_name            ();
    const auto topology_index  = load_cartesian_topology_index();
    const auto dimension_names = 
      load_dimension_names     (topology_index.get_num_dimensions());
    const auto topology_to_data_map = 
      load_topology_to_data_map(id, topology_index);
    
    m_cartesian_topologies.emplace_back(id, 
                                        display_name, 
                                        topology_index,
                                        dimension_names,
                                        topology_to_data_map);
  }

  // If none read, construct a default cartesian topology.
  if (m_cartesian_topologies.size() == 0)
    construct_default_cartesian_topology();
}


//------------------------------------------------------------------------------
void
perf_data_file_native_cartesian_topology_loader::construct_default_cartesian_topology
()
{
  cartesian_topology_index topology_index(std::vector<std::size_t>{ m_data_size, 1, 1 });
  std::vector<std::string> dimension_names = { "x", "y", "z" };
  data_array<std::size_t>  topology_to_data_map =
    construct_default_topology_to_data_map(topology_index);

  m_cartesian_topologies.emplace_back(0,
                                      "default",
                                      topology_index,
                                      dimension_names,
                                      topology_to_data_map);
}


//------------------------------------------------------------------------------
std::string
perf_data_file_native_cartesian_topology_loader::load_display_name
()
{
  std::string display_name;
  std::getline(m_topologies_file, display_name);
  return display_name;
}


//------------------------------------------------------------------------------
cartesian_topology_index
perf_data_file_native_cartesian_topology_loader::load_cartesian_topology_index
()
{
  std::size_t num_dimensions = 0;
  m_topologies_file >> num_dimensions;
  
  std::vector<std::size_t> dimension_sizes(num_dimensions, 0);
  for (std::size_t i = 0; i < num_dimensions; ++i)
    m_topologies_file >> dimension_sizes[i];
  m_topologies_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  
  return cartesian_topology_index(dimension_sizes);
}


//------------------------------------------------------------------------------
std::vector<std::string>
perf_data_file_native_cartesian_topology_loader::load_dimension_names
(std::size_t num_dimensions)
{
  std::vector<std::string> dimension_names(num_dimensions);
  for (std::size_t i = 0; i < num_dimensions; ++i)
    std::getline(m_topologies_file, dimension_names.at(i));
  return dimension_names;
}


//------------------------------------------------------------------------------
data_array<std::size_t>
perf_data_file_native_cartesian_topology_loader::load_topology_to_data_map
(std::size_t                     id,
 const cartesian_topology_index& cart_topo_idx)
{
  std::stringstream map_filename;
  map_filename << m_topologies_file_base_name << id << "ToData.map";
  
  data_array<std::size_t> topology_to_data_map(cart_topo_idx.get_num_positions());
  topology_to_data_map.read(map_filename.str());
  
  if (topology_to_data_map.is_guaranteed_zero())
    topology_to_data_map = construct_default_topology_to_data_map(cart_topo_idx);
  
  return topology_to_data_map;
}


//------------------------------------------------------------------------------
data_array<std::size_t>
perf_data_file_native_cartesian_topology_loader::construct_default_topology_to_data_map
(const cartesian_topology_index& cart_topo_idx)
{
  data_array<std::size_t> topology_to_data_map(cart_topo_idx.get_num_positions());

  const auto& dimension_sizes = cart_topo_idx.get_dimension_sizes();
  
  /// \todo Find a non-hardcoded way.
  if (dimension_sizes.size() != 7)
  {
    for (std::size_t i = 0; i < topology_to_data_map.size(); ++i)
    {
      topology_to_data_map.at(i) = i;
    }
  }
  else
  {
    std::vector<std::size_t> coordinate(7, 0);
    for (std::size_t i = 0; i < topology_to_data_map.size(); ++i)
    {
      topology_to_data_map.at(i) = cart_topo_idx.get_index(coordinate);
      
      ++coordinate[5];
      if (coordinate[5] >= dimension_sizes[5])
      {
        coordinate[5] = 0;
        coordinate[6] += 2;
      }
      
      if (coordinate[6] == 4)
      {
        coordinate[6] = 1;
      }
      else if (coordinate[6] == 5)
      {
        coordinate[6] = 0;
        ++coordinate[4];
      }
      
      if (coordinate[4] >= dimension_sizes[4])
      {
        coordinate[4] = 0;
        ++coordinate[3];
      }
      
      if (coordinate[3] >= dimension_sizes[3])
      {
        coordinate[3] = 0;
        ++coordinate[2];
      }
      
      if (coordinate[2] >= dimension_sizes[2])
      {
        coordinate[2] = 0;
        ++coordinate[1];
      }
      
      if (coordinate[1] >= dimension_sizes[1])
      {
        coordinate[1] = 0;
        ++coordinate[0];
      }
      
      if (coordinate[0] >= dimension_sizes[0])
      {
        coordinate[0] = 0;
        
        static int err_count = 0;
        ++err_count;
        if (err_count > 1)
        {
          std::cerr << "ERROR in PerfDataFileNativeCartesianTopologyLoader::ConstructDefaultTopologyToDataMap" << std::endl;
          std::cerr << "ERROR: Overflow shoud not happen here." << std::endl;
        }
      }
    }
  }

  return topology_to_data_map;
}

} // namespace pvt
