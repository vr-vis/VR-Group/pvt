//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef WITHOUT_CUBE

#include <cassert>

#include <map>
#include <sstream>

#include <cube/Cube.h>
#include <cube/CubeCartesian.h>

#include <pvt/cartesian_topology.hpp>
#include <pvt/cartesian_topology_index.hpp>
#include <pvt/cnode.hpp>
#include <pvt/dft.hpp>
#include <pvt/dft_inverse.hpp>
#include <pvt/directed_variance.hpp>
#include <pvt/fft.hpp>
#include <pvt/metric.hpp>
#include <pvt/severity_view.hpp>
#include <pvt/sysnode.hpp>

#include "perf_data_file_cube.hpp"
#include "perf_data_file_cube_cartesian_topology_loader.hpp"
#include "perf_data_file_cube_cnode_loader.hpp"
#include "perf_data_file_cube_metric_loader.hpp"
#include "perf_data_file_cube_sysnode_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_cube::perf_data_file_cube
(const std::string& filename)
: perf_data_file(filename)
, mp_cube       (new cube::Cube)
, m_managed(true)
{
  mp_cube->openCubeReport(m_filename);
}


//------------------------------------------------------------------------------
perf_data_file_cube::perf_data_file_cube
(cube::Cube* p_cube)
: perf_data_file()
, mp_cube       (p_cube)
, m_managed(false)
{

}


//------------------------------------------------------------------------------
perf_data_file_cube::~perf_data_file_cube
()
{
  if (m_managed && mp_cube != nullptr)
  {
    delete mp_cube;
  }
}


//------------------------------------------------------------------------------
bool
perf_data_file_cube::setup
()
{
  m_data_size = mp_cube->get_locationv().size();

  m_severities_buffer            .resize(m_data_size);
  m_severities_in_topology_buffer.resize(m_data_size);
  
  return
    m_data_size > 0 &&
    mp_cube->get_metv  ().size() != 0 &&
    mp_cube->get_cnodev().size() != 0 &&
    mp_cube->get_sysv  ().size() != 0;
}


//------------------------------------------------------------------------------
bool
perf_data_file_cube::load_cnodes
(std::vector<cnode>&  cnodes,
 std::vector<cnode*>& root_cnodes)
{
  perf_data_file_cube_cnode_loader loader(mp_cube,
                                          cnodes,
                                          root_cnodes,
                                          m_cnode_to_cube_cnode_map);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_cube::load_metrics
(std::vector<metric>&  metrics,
 std::vector<metric*>& root_metrics)
{
  perf_data_file_cube_metric_loader loader(mp_cube,
                                           metrics,
                                           root_metrics,
                                           m_metric_to_cube_metric_map);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_cube::load_sysnodes
(std::vector<sysnode>&  sysnodes,
 std::vector<sysnode*>& root_sysnodes)
{
  perf_data_file_cube_sysnode_loader loader(mp_cube,
                                            sysnodes,
                                            root_sysnodes,
                                            m_sysnode_to_cube_sysnode_map);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_cube::load_cartesian_topologies
(std::vector<cartesian_topology>& cartesian_topologies)
{
  perf_data_file_cube_cartesian_topology_loader loader(mp_cube,
                                                       cartesian_topologies);
  const bool success = loader.load();
  if (cartesian_topologies.size() > 0)
    mp_first_cartesian_topology = &cartesian_topologies[0];
  return success;
}


//------------------------------------------------------------------------------
bool
perf_data_file_cube::load_system_geometries
(const std::vector<sysnode*>&  root_sysnodes,
 std::vector<system_geometry>& system_geometries)
{
  std::cerr << "Geometry loading is not yet supported for Cube files.";
  std::cerr << std::endl;
  return false;
}


//------------------------------------------------------------------------------
double
perf_data_file_cube::get_severity
(const severity_view& severity_view)
const
{
  if (severity_view.get_metric() == nullptr ||
      severity_view.get_cnode() == nullptr)
  {
    return 0.0;
  }
  
  cube::list_of_metrics cube_metrics;

  cube::CalculationFlavour metric_calculation_flavour =
    severity_view.is_metric_total() ?
      cube::CUBE_CALCULATE_INCLUSIVE :
      cube::CUBE_CALCULATE_EXCLUSIVE;

  cube_metrics.push_back(
    cube::metric_pair(m_metric_to_cube_metric_map.at(severity_view.get_metric()),
    metric_calculation_flavour));
  
  cube::list_of_cnodes cube_cnodes;

  cube::CalculationFlavour cnode_calculation_flavour =
  severity_view.is_cnode_total() ?
    cube::CUBE_CALCULATE_INCLUSIVE :
    cube::CUBE_CALCULATE_EXCLUSIVE;

  cube_cnodes.push_back(
    cube::cnode_pair(m_cnode_to_cube_cnode_map.at(severity_view.get_cnode()),
    cnode_calculation_flavour));

  return mp_cube->get_sev(cube_metrics, cube_cnodes);
}


//------------------------------------------------------------------------------
void
perf_data_file_cube::get_severities
(const severity_view& severity_view,
 data_array<double>&  severities)
const
{
  // set output to zero
  severities.zero();
  
  if (severity_view.get_metric() == nullptr ||
      severity_view.get_cnode() == nullptr)
  {
    return;
  }
  
  cube::list_of_metrics cube_metrics;

  cube::CalculationFlavour metric_calculation_flavour =
    severity_view.is_metric_total() ?
      cube::CUBE_CALCULATE_INCLUSIVE :
      cube::CUBE_CALCULATE_EXCLUSIVE;
  
  cube_metrics.push_back(
    cube::metric_pair(m_metric_to_cube_metric_map.at(severity_view.get_metric()),
    metric_calculation_flavour));

  cube::list_of_cnodes cube_cnodes;

  cube::CalculationFlavour cnode_calculation_flavour =
    severity_view.is_cnode_total() ?
      cube::CUBE_CALCULATE_INCLUSIVE :
      cube::CUBE_CALCULATE_EXCLUSIVE;
  
  cube_cnodes.push_back(
    cube::cnode_pair(m_cnode_to_cube_cnode_map.at(severity_view.get_cnode()),
    cnode_calculation_flavour));
  
  double* p_severity_buffer = mp_cube->get_sevs(cube_metrics, cube_cnodes);
  std::copy_n(p_severity_buffer, severities.size(), severities.begin());
}

//------------------------------------------------------------------------------
/// \todo Find a better way
/// \todo Cache latest Fft for future retrieval
//------------------------------------------------------------------------------
void
perf_data_file_cube::get_fft
(const severity_view& severity_view,
 fft& fft)
const
{
  assert(severity_view.is_valid());

  get_severities(severity_view, m_severities_buffer);
  
  mp_first_cartesian_topology->map_data(m_severities_buffer,
                                        m_severities_in_topology_buffer);
  
  dft::compute(m_severities_in_topology_buffer,
               mp_first_cartesian_topology->get_index(),
               &fft);
}


//------------------------------------------------------------------------------
/// \todo Implement
//------------------------------------------------------------------------------
void
perf_data_file_cube::get_fft_statistics
(const severity_view& severity_view,
 fft_statistics&      fft_statistics)
const
{
  std::cerr << "perf_data_file_cube::get_fft_statistics(...) not yet implemented!";
  std::cerr << "Expect undefined behaviour!";
  std::cerr << std::endl;
}


//------------------------------------------------------------------------------
void
perf_data_file_cube::get_directed_variance
(const severity_view& severity_view,
 directed_variance&   directed_variance)
const
{
  fft fft(mp_first_cartesian_topology->get_index());
  get_fft(severity_view, fft);
  directed_variance.compute(fft);
}


//------------------------------------------------------------------------------
double
perf_data_file_cube::get_autocorrelation_off_dc
(const severity_view& severity_view)
const
{
  fft fft_temp       (mp_first_cartesian_topology->get_index());
  fft fft_conj_square(mp_first_cartesian_topology->get_index());

  data_array<double> auto_correlation(
    mp_first_cartesian_topology->get_index().get_num_positions());
  
  get_fft(severity_view, fft_temp);
  fft_temp.square_with_conjugate(fft_conj_square);
  fft_conj_square.clear_dc();

  dft_inverse::compute(fft_conj_square,
                       mp_first_cartesian_topology->get_index(),
                       &auto_correlation);

  return auto_correlation.at(0);
}

} // namespace pvt

#endif // #ifndef WITHOUT_CUBE
