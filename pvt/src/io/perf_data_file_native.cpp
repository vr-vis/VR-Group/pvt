//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cassert>

#include <fstream>
#include <numeric>

#include <pvt/cnode.hpp>
#include <pvt/sysnode.hpp>
#include <pvt/dft.hpp>
#include <pvt/dft_inverse.hpp>
#include <pvt/directed_variance.hpp>
#include <pvt/fft.hpp>
#include <pvt/fft_statistics.hpp>
#include <pvt/metric.hpp>
#include <pvt/severity_view.hpp>

#include "perf_data_file_native.hpp"
#include "perf_data_file_native_cartesian_topology_loader.hpp"
#include "perf_data_file_native_cnode_loader.hpp"
#include "perf_data_file_native_metric_loader.hpp"
#include "perf_data_file_native_sysnode_loader.hpp"
#include "perf_data_file_native_system_geometry_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_native::perf_data_file_native
(const std::string& filename)
: perf_data_file(filename)
{
  
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::setup
()
{
  std::ifstream data_size_file(m_filename + "/data.size");
  data_size_file >> m_data_size;
  data_size_file.close();

  m_severities_load_buffer.resize(m_data_size);

  return m_data_size > 0;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::load_cnodes
(std::vector<cnode>&  cnodes,
 std::vector<cnode*>& root_cnodes)
{
  perf_data_file_native_cnode_loader loader(m_filename, cnodes, root_cnodes);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::load_metrics
(std::vector<metric>&  metrics,
 std::vector<metric*>& root_metrics)
{
  perf_data_file_native_metric_loader loader(m_filename, metrics, root_metrics);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::load_sysnodes
(std::vector<sysnode>&  sysnodes,
 std::vector<sysnode*>& root_sysnodes)
{
  perf_data_file_native_sysnode_loader loader(m_filename, 
                                              sysnodes, 
                                              root_sysnodes);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::load_cartesian_topologies
(std::vector<cartesian_topology>& cartesian_topologies)
{
  perf_data_file_native_cartesian_topology_loader loader(m_filename,
                                                         m_data_size,
                                                         cartesian_topologies);
  return loader.load();
}


//------------------------------------------------------------------------------
bool 
perf_data_file_native::load_system_geometries
(const std::vector<sysnode*>&  root_sysnodes, 
 std::vector<system_geometry>& system_geometries)
{
  perf_data_file_native_system_geometry_loader loader(m_filename, 
                                                      root_sysnodes,
                                                      system_geometries);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write
(const perf_data& perf_data,
 export_flags     export_flags)
 const
{
  bool success = true;
  m_severities_load_buffer.resize(perf_data.get_data_size());

  if (export_flags & export_metadata)
  {
    success &= write_42                 ();
    success &= write_data_size          (perf_data);
    success &= write_call_tree          (perf_data);
    success &= write_cart_topo_list     (perf_data);
    success &= write_metric_tree        (perf_data);
    success &= write_cart_topo_data_maps(perf_data);
  }

  if (export_flags & export_data)
  {
    std::vector<metric const*> metric_refs;
    std::vector<cnode  const*>  cnode_refs;

    for (auto& current_metric : perf_data.get_metrics())
      metric_refs.push_back(&current_metric);
    for (auto& current_cnode  : perf_data.get_cnodes ())
      cnode_refs .push_back(&current_cnode);

    success &= write_data(perf_data,
                          metric_refs,
                          cnode_refs,
                          export_flags);
  }
  return success;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write
(const perf_data&                  perf_data,
 export_flags                      export_flags,
 const std::vector<const metric*>& metric_ids,
 const std::vector<const cnode*>&  cnode_ids)
const
{
  bool success = true;
  m_severities_load_buffer.resize(perf_data.get_data_size());

  if (export_flags & export_metadata)
  {
    success &= write_42                 ();
    success &= write_data_size          (perf_data);
    success &= write_call_tree          (perf_data);
    success &= write_cart_topo_list     (perf_data);
    success &= write_metric_tree        (perf_data);
    success &= write_cart_topo_data_maps(perf_data);
    success &= write_sysnode_tree       (perf_data);
  }

  if (export_flags & export_data)
  {
    success &= write_data(perf_data, metric_ids, cnode_ids, export_flags);
  }
  return success;
}


//------------------------------------------------------------------------------
double
perf_data_file_native::get_severity
(const severity_view& severity_view)
const
{
  double severity = 0.0;
  
  if (!severity_view.is_valid())
    return severity;

  const metric* metric = severity_view.get_metric();
  const cnode*  cnode  = severity_view.get_cnode ();

  const std::string filename(
     m_filename + "/" +
     metric->get_id_string() +
    (metric->has_children () &&
     severity_view.is_metric_total() ? "I" : "E") + "_" +
     cnode->get_id_string() +
    (cnode->has_children () &&
     severity_view.is_cnode_total() ? "I" : "E") + ".fft");

  auto file = fopen(filename.c_str(), "rb");
  if  (file != nullptr)
  {
    fread (&severity, sizeof(double), 1, file);
    fclose(file);
  }
  
  return severity;
}


//------------------------------------------------------------------------------
void
perf_data_file_native::get_severities
(const severity_view& severity_view,
 data_array<double>&  severities)
const
{
  severities.zero();

  if (!severity_view.is_valid())
    return;

  const metric* metric = severity_view.get_metric();
  const cnode*  cnode  = severity_view.get_cnode ();
  
  const std::string filename(
    get_filename() + "/" +
    metric->get_id_string() +
   (metric->has_children () &&
    severity_view.is_metric_total() ? "I" : "E") + "_" +
    cnode->get_id_string() +
   (cnode->has_children () &&
    severity_view.is_cnode_total () ? "I" : "E" ) + ".dat");

  m_severities_load_buffer.read(filename);

  std::transform(m_severities_load_buffer.begin(),
                 m_severities_load_buffer.end  (),
                 severities              .begin(),
                 severities              .begin(),
                 std::plus<double>());
}


//------------------------------------------------------------------------------
void
perf_data_file_native::get_fft
(const severity_view& severity_view,
 fft&                 fft)
const
{
  assert(severity_view.is_valid());
  
  const std::string filename_fft(
    get_filename() + "/" +
     severity_view.get_metric()->get_id_string() +
    (severity_view.get_metric()->has_children () &&
     severity_view.is_metric_total() ? "I" : "E") + "_" +
     severity_view.get_cnode()->get_id_string() +
    (severity_view.get_cnode()->has_children () &&
     severity_view.is_cnode_total() ? "I" : "E") + ".fft");
  
  fft.read(filename_fft);
}


//------------------------------------------------------------------------------
void
perf_data_file_native::get_fft_statistics
(const severity_view& severity_view,
 fft_statistics&      fft_statistics)
const
{
  const std::string filename_stat(
    get_filename() + "/" +
     severity_view.get_metric()->get_id_string() +
    (severity_view.get_metric()->has_children() &&
     severity_view.is_metric_total() ? "I" : "E") + "_" +
     severity_view.get_cnode()->get_id_string() +
    (severity_view.get_cnode()->has_children() &&
     severity_view.is_cnode_total() ? "I" : "E") + ".stat");

  fft_statistics.read(filename_stat);
}


//------------------------------------------------------------------------------
void
perf_data_file_native::get_directed_variance
(const severity_view& severity_view,
 directed_variance&   directed_variance)
const
{
  if (!severity_view.is_valid())
    return;

  const std::string filename_dir_var(
    get_filename() + "/" +
      severity_view.get_metric()->get_id_string() +
    ((severity_view.get_metric()->has_children() &&
      severity_view.is_metric_total()) ? "I" : "E") + "_" +
      severity_view.get_cnode()->get_id_string() +
    ((severity_view.get_cnode()->has_children() &&
      severity_view.is_cnode_total()) ? "I" : "E") + ".dvariance");

  directed_variance.read(filename_dir_var);
}


//------------------------------------------------------------------------------
double
perf_data_file_native::get_autocorrelation_off_dc
(const severity_view& severity_view)
const
{
  fft_statistics fft_stat;
  get_fft_statistics(severity_view, fft_stat);
  return fft_stat.get(fft_statistics::autocorrelation);
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write_42
()
const
{
  const auto filename(m_filename + "/" + "42u");
  auto file = fopen(filename.c_str(), "wb");
  const bool success = file != nullptr;
  unsigned int fortytwo = 42u;
  fwrite(&fortytwo, sizeof(unsigned int), 1, file);
  fclose(file);
  return success;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write_data_size
(const perf_data& perf_data)
const
{
  std::ofstream data_size_file(m_filename + "/data.size");
  const bool success = data_size_file.is_open();
  data_size_file << perf_data.get_data_size();
  data_size_file.close();
  return success;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write_call_tree
(const perf_data& perf_data)
const
{
  std::ofstream cnode_file(m_filename + "/call.tree");
  if (!cnode_file.is_open())
    return false;
  
  cnode_file << perf_data.get_cnodes().size() << '\n';
  
  cnode_file << perf_data.get_root_cnodes().size() << ' ';
  for (const cnode* root_cnode : perf_data.get_root_cnodes())
    cnode_file << root_cnode->get_id() << ' ';
  cnode_file << '\n';
  
  for (const cnode& curr_cnode : perf_data.get_cnodes())
  {
    cnode_file << curr_cnode.get_name() << '\n';
    cnode_file << curr_cnode.get_children().size() << ' ';
    for (const cnode* child : curr_cnode.get_children())
      cnode_file << child->get_id() << ' ';
    cnode_file << '\n';
  }
  
  cnode_file.close();
  
  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write_cart_topo_list
(const perf_data& perf_data)
const
{
  std::ofstream cart_topo_list_file(m_filename + "/cartTopo.list");
  if (!cart_topo_list_file.is_open())
    return false;
  
  cart_topo_list_file << perf_data.get_cartesian_topologies().size() << "\n";
  for (const auto& cart_topo : perf_data.get_cartesian_topologies())
  {
    cart_topo_list_file << cart_topo.get_name          () << "\n";
    cart_topo_list_file << cart_topo.get_num_dimensions() << " ";

    for (std::size_t dim_size : cart_topo.get_index().get_dimension_sizes())
      cart_topo_list_file << dim_size << " ";

    cart_topo_list_file << "\n";
    
    for (const std::string& dim_name : cart_topo.get_dimension_names())
      cart_topo_list_file << dim_name << "\n"; 
  }

  cart_topo_list_file.close();
  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write_metric_tree
(const perf_data& perf_data)
const
{
  std::ofstream metric_file(m_filename + "/metrics.tree");
  if (!metric_file.is_open())
    return false;
  
  metric_file << perf_data.get_metrics().size() << '\n';
                          
  metric_file << perf_data.get_root_metrics().size() << ' ';
  for (const metric* root_metric : perf_data.get_root_metrics())
    metric_file << root_metric->get_id() << ' ';

  metric_file << '\n';
  
  for (const metric& curr_metric : perf_data.get_metrics())
  {
    /// \todo Add unique name to metric for next line?
    metric_file << curr_metric.get_name() << '\n';
    metric_file << curr_metric.get_name() << '\n';
    metric_file << curr_metric.get_unit_of_measurement() << '\n';
    
    metric_file << curr_metric.get_children().size() << ' ';
    for (const metric* child : curr_metric.get_children())
      metric_file << child->get_id() << ' ';

    metric_file << '\n';
  }
  
  metric_file.close();
  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write_sysnode_tree
(const perf_data& perf_data)
const
{
  std::ofstream system_file(m_filename + "/system.tree");
  if (!system_file.is_open())
    return false;
  
  system_file << perf_data.get_sysnodes().size() << '\n';
  
  system_file << perf_data.get_root_sysnodes().size() << ' ';
  for (const sysnode* root_sysnode : perf_data.get_root_sysnodes())
    system_file << root_sysnode->get_id() << ' ';

  system_file << '\n';
  
  for (const sysnode& curr_sysnode : perf_data.get_sysnodes())
  {
    system_file << curr_sysnode.get_name() << '\n';
    system_file << curr_sysnode.get_children().size() << ' ';

    for (const sysnode* child : curr_sysnode.get_children())
      system_file << child->get_id() << ' ';

    system_file << '\n';
  }
  
  system_file.close();
  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write_cart_topo_data_maps
(const perf_data& perf_data)
const
{
  for (const auto& cart_topo : perf_data.get_cartesian_topologies())
  {
    std::stringstream filename;
    filename << m_filename << '/' << "cartTopo";
    filename << cart_topo.get_id();
    filename << "ToData.map";
    cart_topo.get_topology_to_data_map().dump(filename.str());
  }
  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native::write_data
(const perf_data&                  perf_data,
 const std::vector<const metric*>& metric_ids,
 const std::vector<const cnode*>&  cnode_ids,
 export_flags                      export_flags)
const
{
  for (const metric* curr_metric : metric_ids)
  {
    for (const cnode* curr_cnode : cnode_ids)
    {
      write_severities_and_fft(perf_data,
                               { &perf_data,
                                 curr_metric,
                                 severity_view::self,
                                 curr_cnode,
                                 severity_view::self },
                               export_flags);
      if (curr_metric->has_children())
      {
        write_severities_and_fft(perf_data,
                                 { &perf_data,
                                   curr_metric,
                                   severity_view::total,
                                   curr_cnode,
                                   severity_view::self },
                                 export_flags);
      }
      if (curr_cnode->has_children())
      {
        write_severities_and_fft(perf_data,
                                 { &perf_data,
                                   curr_metric,
                                   severity_view::self,
                                   curr_cnode,
                                   severity_view::total },
                                 export_flags);
      }
      if (curr_metric->has_children() && curr_cnode->has_children())
      {
        write_severities_and_fft(perf_data,
                                 { &perf_data,
                                   curr_metric,
                                   severity_view::total,
                                   curr_cnode,
                                   severity_view::total },
                                 export_flags);
      }
    }
  }
  return true;
}


//------------------------------------------------------------------------------
void
perf_data_file_native::write_severities_and_fft
(const perf_data&     perf_data,
 const severity_view& severity_view,
 export_flags         export_flags)
const
{
  perf_data.get_severities(severity_view, m_severities_load_buffer);

  const double sum = std::accumulate(m_severities_load_buffer.begin(),
                                     m_severities_load_buffer.end  (),
                                     0.0);
  if (sum == 0.0)
    return;
  
  // write severities
  const std::string severities_filename_base(
    m_filename + "/" +
    severity_view.get_metric()->get_id_string() +
    (severity_view.is_metric_total() ? "I" : "E") + "_" +
    severity_view.get_cnode ()->get_id_string() +
    (severity_view.is_cnode_total() ? "I" : "E"));

  if (export_flags & export_severities)
    m_severities_load_buffer.dump(severities_filename_base + ".dat");
    
  // fft
  /// \todo Add remaining topologies
  if(perf_data.get_cartesian_topologies().size() != 0)
  {
    const auto& cart_topo = perf_data.get_cartesian_topologies()[0];
    
    fft fft(cart_topo.get_index());
    if (export_flags & recompute_spectra)
      dft::compute(m_severities_load_buffer, cart_topo.get_index(), &fft);
    else
      perf_data.get_fft(severity_view, fft);

    if (export_flags & export_fft)
      fft.dump(severities_filename_base + ".fft");
    
    // Compute autocorrelation.
    const double autocorrelation =
      perf_data.get_autocorrelation_off_dc(severity_view);

    // Additional statistics.
    fft_statistics fft_stat;
    fft_stat.set (fft_statistics::autocorrelation, 
                  autocorrelation);
    fft_stat.set (fft_statistics::mean, 
                  fft.get_mean());
    fft_stat.set (fft_statistics::variance, 
                  fft.get_variance());
    fft_stat.set (fft_statistics::standard_deviation, 
                  fft.get_standard_deviation());

    if (export_flags & export_fft)
      fft_stat.dump(severities_filename_base + ".stat");
    
    if (export_flags & export_directed_variances)
    {
      directed_variance directed_variance(cart_topo.get_num_dimensions());
      if (export_flags & recompute_directed_variances)
        directed_variance.compute(fft);
      else
        perf_data.get_directed_variance(severity_view, directed_variance);
      directed_variance.dump(severities_filename_base + ".dvariances");
    }
  }
}

} // namespace pvt
