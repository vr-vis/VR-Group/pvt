//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef WITHOUT_CUBE

#include <cube/Cube.h>

#include <pvt/metric.hpp>

#include "perf_data_file_cube_metric_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_cube_metric_loader::perf_data_file_cube_metric_loader
(const cube::Cube*                       p_cube,
 std::vector<metric>&                    metrics,
 std::vector<metric*>&                   root_metrics,
 std::map<const metric*, cube::Metric*>& metric_to_cube_metric_map)
 : mp_cube                    (p_cube)
 , m_root_metrics             (root_metrics)
 , m_metrics                  (metrics)
 , m_metric_to_cube_metric_map(metric_to_cube_metric_map)
{

}


//------------------------------------------------------------------------------
bool
perf_data_file_cube_metric_loader::load
()
{
  load_metrics         ();
  load_root_metrics    ();
  process_child_metrics();
  return m_root_metrics.size() != 0;
}


//------------------------------------------------------------------------------
void
perf_data_file_cube_metric_loader::load_metrics
()
{
  const std::size_t num_metrics = mp_cube->get_metv().size();
  m_metrics.reserve(num_metrics);
  
  for (std::size_t id = 0; id < num_metrics; ++id)
  {
    cube::Metric* p_cube_metric = mp_cube->get_metv()[id];
   
    m_metrics.emplace_back(id, 
                           p_cube_metric->get_disp_name(), 
                           p_cube_metric->get_uom());
    metric* p_metric = &m_metrics.back();

    m_cube_metric_to_metric_map[p_cube_metric] = p_metric;
    m_metric_to_cube_metric_map[p_metric     ] = p_cube_metric;
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_cube_metric_loader::load_root_metrics
()
{
  m_root_metrics.reserve(mp_cube->get_root_metv().size());
  for (cube::Metric* p_cube_root_metric : mp_cube->get_root_metv())
    m_root_metrics.push_back(m_cube_metric_to_metric_map[p_cube_root_metric]);
}


//------------------------------------------------------------------------------
void
perf_data_file_cube_metric_loader::process_child_metrics
()
{
  for (auto cube_metric_metric_pair : m_cube_metric_to_metric_map)
  {
    cube::Metric* const p_cube_metric = cube_metric_metric_pair.first;
    metric*       const p_metric      = cube_metric_metric_pair.second;
    
    for (std::size_t i = 0; i < p_cube_metric->get_children().size(); ++i)
    {
      p_metric->add_child(
        m_cube_metric_to_metric_map[p_cube_metric->get_child(i)]);
    }
  }
}

} // namespace pvt

#endif // #ifndef WITHOUT_CUBE
