//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <fstream>
#include <iostream>

#include <pvt/io/perf_data_file.hpp>

#include "perf_data_file_cube.hpp"
#include "perf_data_file_native.hpp"
#include "perf_data_file_native_intermediate.hpp"
#include "perf_data_file_test.hpp"

namespace pvt {

//------------------------------------------------------------------------------
std::unique_ptr<perf_data_file>
perf_data_file::construct
(const std::string& filename)
{
  std::unique_ptr<perf_data_file> perf_data_file;
  if (filename.compare("test.test") == 0)
    perf_data_file.reset(new perf_data_file_test(filename));
  else if (filename.substr(filename.size() - 3 , 3 ).compare(".pv") == 0)
    perf_data_file.reset(new perf_data_file_native(filename));
  else if (filename.substr(filename.size() - 16, 16).compare(".pv-intermediate") == 0)
    perf_data_file.reset(new perf_data_file_native_intermediate(filename));
  else if (filename.substr(filename.size() - 8, 8).compare(".cube.gz") == 0 ||
           filename.substr(filename.size() - 6, 6).compare(".cubex"  ) == 0)
  {
#ifdef WITHOUT_CUBE
    std::cerr << "CUBE is not supported in this build! Please rebuild with CUBE support!";
#else
    perf_data_file.reset(new perf_data_file_cube(filename));
#endif
  }
  else
    std::cerr << "File type is not supported!";
  return std::move(perf_data_file);
}

} // namespace pvt
