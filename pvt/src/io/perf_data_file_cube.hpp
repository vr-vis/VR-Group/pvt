//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_FILE_CUBE_HPP_
#define PVT_PERF_DATA_FILE_CUBE_HPP_

#ifndef WITHOUT_CUBE

#include <map>

#include <pvt/data_array.hpp>
#include <pvt/perf_data.hpp>
#include <pvt/io/perf_data_file.hpp>

namespace cube {
  class Cnode;
  class Cube;
  class Metric;
  class Sysres;
}

namespace pvt {

class directed_variance;
class fft;
class fft_statistics;

//------------------------------------------------------------------------------
class perf_data_file_cube : public perf_data_file
{
public:
  perf_data_file_cube(const std::string& filename);
  perf_data_file_cube(cube::Cube*        p_cube  );

  virtual ~perf_data_file_cube();
  
  bool setup() override;
  bool load_cnodes (
    std::vector<cnode>&           cnodes,
    std::vector<cnode*>&          root_cnodes         ) override;
  bool load_metrics(
    std::vector<metric>&          metrics,
    std::vector<metric*>&         root_metrics        ) override;
  bool load_sysnodes(
    std::vector<sysnode>&         sysnodes,
    std::vector<sysnode*>&        root_sysnodes       ) override;
  bool load_cartesian_topologies(
    std::vector<cartesian_topology>& cartesian_topologies) override;
  bool load_system_geometries(
    const std::vector<sysnode*>&  root_sysnodes,
    std::vector<system_geometry>& system_geometries   ) override;
  
  bool write(const perf_data&                  perf_data,
             export_flags                      export_flags) const override
  {
    return false;
  }
  bool write(const perf_data&                  perf_data,
             export_flags                      export_flags,
             const std::vector<const metric*>& metric_ids,
             const std::vector<const cnode*>&  cnode_ids   ) const override
  {
    return false;
  }
  
  double get_severity              (const severity_view& severity_view     )
                                    const override;
  void   get_severities            (const severity_view& severity_view,
                                    data_array<double>&  severities        ) 
                                    const override;
  void   get_fft                   (const severity_view& severity_view, 
                                    fft&                 fft               ) 
                                    const override;
  void   get_fft_statistics        (const severity_view& severity_view, 
                                    fft_statistics&      fft_statistics    ) 
                                    const override;
  void   get_directed_variance     (const severity_view& severity_view,
                                    directed_variance&   directed_variance ) 
                                    const override;
  double get_autocorrelation_off_dc(const severity_view& severity_view     ) 
                                    const override;

private:
  cube::Cube*                             mp_cube;
  bool                                    m_managed;

  std::map<const cnode*  , cube::Cnode* > m_cnode_to_cube_cnode_map;
  std::map<const metric* , cube::Metric*> m_metric_to_cube_metric_map;
  std::map<const sysnode*, cube::Sysres*> m_sysnode_to_cube_sysnode_map;

  const cartesian_topology*               mp_first_cartesian_topology;  ///< \todo Make topology selectable

  mutable data_array<double>              m_severities_buffer;
  mutable data_array<double>              m_severities_in_topology_buffer;
};

template <>
inline bool perf_data::attach_to_existing(cube::Cube* cube_perf_data_object)
{
  mp_perf_data_file.reset(new perf_data_file_cube(cube_perf_data_object));
  mp_perf_data_file->setup();
  return initialize();
}

} // namespace pvt

#endif // #ifndef WITHOUT_CUBE

#endif // #ifndef PVT_PERF_DATA_FILE_CUBE_HPP_
