//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_FILE_CUBE_METRIC_LOADER_HPP_
#define PVT_PERF_DATA_FILE_CUBE_METRIC_LOADER_HPP_

#include <map>
#include <vector>

namespace cube {
  class Cube;
  class Metric;
}

namespace pvt {

class metric;

//------------------------------------------------------------------------------
class perf_data_file_cube_metric_loader
{
public:
  perf_data_file_cube_metric_loader(
    const cube::Cube*                       p_cube,
    std::vector<metric>&                    metrics,
    std::vector<metric*>&                   root_metrics,
    std::map<const metric*, cube::Metric*>& metric_to_cube_metric_map);

  bool load();
  
private:
  void load_metrics         ();
  void load_root_metrics    ();
  void process_child_metrics();
  
  const cube::Cube*                       mp_cube;
  std::vector<metric*>&                   m_root_metrics;
  std::vector<metric>&                    m_metrics;
  std::map<const metric*, cube::Metric*>& m_metric_to_cube_metric_map;
  std::map<cube::Metric*, metric*>        m_cube_metric_to_metric_map;
  
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_FILE_CUBE_METRIC_LOADER_HPP_
