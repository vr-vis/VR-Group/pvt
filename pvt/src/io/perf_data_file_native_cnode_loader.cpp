//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/cnode.hpp>

#include "perf_data_file_native_cnode_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_native_cnode_loader::perf_data_file_native_cnode_loader
(const std::string&   filename,
 std::vector<cnode>&  cnodes,
 std::vector<cnode*>& root_cnodes)
: m_cnodes_file (filename + "/call.tree")
, m_cnodes      (cnodes)
, m_root_cnodes (root_cnodes)
, m_num_cnodes  (0)
{

}


//------------------------------------------------------------------------------
bool
perf_data_file_native_cnode_loader::load
()
{
  load_num_cnodes                 ();
  load_root_cnode_ids             ();
  load_cnodes_and_child_ids       ();
  process_root_and_cnode_child_ids();

  return m_cnodes_file.is_open();
}


//------------------------------------------------------------------------------
void
perf_data_file_native_cnode_loader::load_num_cnodes
()
{
  m_cnodes_file >> m_num_cnodes;
  m_cnodes_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_cnode_loader::load_root_cnode_ids
()
{
  std::size_t num_root_cnodes = 0;
  m_cnodes_file >> num_root_cnodes;
  m_root_cnode_ids = std::vector<std::size_t>(num_root_cnodes, 0);
  for (std::size_t i = 0; i < num_root_cnodes; ++i)
    m_cnodes_file >> m_root_cnode_ids[i];
  m_cnodes_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_cnode_loader::load_cnodes_and_child_ids
()
{
  for (std::size_t i = 0; i < m_num_cnodes; ++i)
  {
    load_cnode          (i);
    load_cnode_child_ids( );
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_native_cnode_loader::load_cnode
(std::size_t id)
{
  std::string display_name;
  std::getline(m_cnodes_file, display_name);
  m_cnodes.emplace_back(id, display_name);
}


//------------------------------------------------------------------------------
void
perf_data_file_native_cnode_loader::load_cnode_child_ids
()
{
  std::size_t num_children = 0;
  m_cnodes_file >> num_children;
  m_cnode_child_ids.push_back(std::vector<std::size_t>(num_children, 0));
  for (std::size_t i = 0; i < num_children; ++i)
  {
    std::size_t child_cnode_id;
    m_cnodes_file >> child_cnode_id;
    m_cnode_child_ids.back()[i] = child_cnode_id;
  }
  m_cnodes_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_cnode_loader::process_root_and_cnode_child_ids
()
{
  // Process root node ids.
  for (auto root_cnode_id : m_root_cnode_ids)
    m_root_cnodes.push_back(&m_cnodes[root_cnode_id]);
  
  // Process cnode child ids.
  for (std::size_t cnode_id = 0; cnode_id < m_num_cnodes; ++cnode_id)
    for (auto child_id : m_cnode_child_ids[cnode_id])
      m_cnodes[cnode_id].add_child(&m_cnodes[child_id]);
}

} // namespace pvt
