//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cassert>

#include <fstream>
#include <numeric>
#include <random>

#include <pvt/cnode.hpp>
#include <pvt/dft.hpp>
#include <pvt/dft_inverse.hpp>
#include <pvt/directed_variance.hpp>
#include <pvt/fft.hpp>
#include <pvt/fft_statistics.hpp>
#include <pvt/metric.hpp>
#include <pvt/severity_view.hpp>

#include "perf_data_file_test.hpp"
#include "perf_data_file_native_cartesian_topology_loader.hpp"
#include "perf_data_file_native_cnode_loader.hpp"
#include "perf_data_file_native_metric_loader.hpp"
#include "perf_data_file_native_system_geometry_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_test::perf_data_file_test
(const std::string& filename)
: perf_data_file   (filename)
{
  std::cerr << std::string(80, '*') << std::endl;
  std::cerr << "WARNING: Using test data." << std::endl;
  std::cerr << std::string(80, '*') << std::endl;
}


//------------------------------------------------------------------------------
bool
perf_data_file_test::setup
()
{
  m_data_size = 65536;
  m_severities_load_buffer       .resize(m_data_size);
  m_severities_in_topology_buffer.resize(m_data_size);
  return m_data_size > 0;
}


//------------------------------------------------------------------------------
bool
perf_data_file_test::load_cnodes
(std::vector<cnode>&  cnodes,
 std::vector<cnode*>& root_cnodes)
{
  cnodes     .emplace_back(0, "AllZero__HwThread1_One");
  root_cnodes.emplace_back(&cnodes.back());

  cnodes     .emplace_back(1, "AllRandom__HwThread13_One_if_A5_and_B0");
  root_cnodes.emplace_back(&cnodes.back());

  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_test::load_metrics
(std::vector<metric>&  metrics,
 std::vector<metric*>& root_metrics)
{
  metrics     .emplace_back(0, "Test", "nn");
  root_metrics.emplace_back(&metrics.back());

  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_test::load_sysnodes
(std::vector<sysnode>&  sysnodes,
 std::vector<sysnode*>& root_sysnodes)
{
  return false;
}


//------------------------------------------------------------------------------
bool
perf_data_file_test::load_cartesian_topologies
(std::vector<cartesian_topology>& cartesian_topologies)
{
  cartesian_topology_index cart_topo_idx(
    std::vector<std::size_t>{8, 4, 4, 4, 2, 16, 4});
  data_array<std::size_t>  topology_to_data_map(
    cart_topo_idx.get_num_positions());
  for (std::size_t i = 0; i < topology_to_data_map.size(); ++i)
    topology_to_data_map[i] = i;

  cartesian_topologies.emplace_back(
    0,
    "BGQ Hardware Topology",
    cart_topo_idx,
    std::vector<std::string>{ "A", "B", "C", "D", "E", "CoreID", "HWThrd" },
    topology_to_data_map);

  mp_cartesian_topology = &cartesian_topologies.back();

  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_test::load_system_geometries
(const std::vector<sysnode*>& root_sysnodes,
 std::vector<system_geometry>&   system_geometries)
{
  return false;
}


//------------------------------------------------------------------------------
double
perf_data_file_test::get_severity
(const severity_view& severity_view)
const
{
  get_severities(severity_view, m_severities_load_buffer);
  const double sum = std::accumulate(m_severities_load_buffer.begin(),
                                     m_severities_load_buffer.end  (),
                                     0.0);
  return sum;
}


//------------------------------------------------------------------------------
void
perf_data_file_test::get_severities
(const severity_view& severity_view,
 data_array<double>&  severities)
const
{
  // Clear output.
  severities.zero();
  
  if (!severity_view.is_valid())
    return;
  
  static std::default_random_engine         random_engine{};
  static std::uniform_int_distribution<int> uniform_dist {0, 100};
  
  const auto& cart_topo_idx = mp_cartesian_topology->get_index();

  switch (severity_view.get_metric()->get_id())
  {
    case 0:
      switch (severity_view.get_cnode()->get_id())
      {
        case 0:
          for (std::size_t i = 0; i < cart_topo_idx.get_num_positions(); ++i)
          {
            std::vector<std::size_t> coordinate = 
              cart_topo_idx.get_coordinate(i);
            if (coordinate[6] == 1)
              severities[i] = 1.0;
          }
          break;

        case 1:
          for (std::size_t i = 0; i < cart_topo_idx.get_num_positions(); ++i)
          {
            std::vector<std::size_t> coordinate = 
              cart_topo_idx.get_coordinate(i);
            if ((coordinate[6] == 1 || coordinate[6] == 3) &&
                 coordinate[0] == 5 && coordinate[1] == 0)
              severities[i] = 1.0;
            else
              severities[i] = 
                0.01 * 0.25 * static_cast<double>(uniform_dist(random_engine));
          }
          break;
        
        default:
          break;
      }
      break;
      
    default:
      break;
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_test::get_fft
(const severity_view& severity_view,
 fft& p_fft)
const
{
  assert(severity_view.is_valid());
  
  get_severities(severity_view, m_severities_load_buffer);
  
  mp_cartesian_topology->map_data(m_severities_load_buffer,
                                  m_severities_in_topology_buffer);
  
  dft::compute(m_severities_in_topology_buffer,
               mp_cartesian_topology->get_index(),
               &p_fft);
}


//------------------------------------------------------------------------------
void
perf_data_file_test::get_fft_statistics
(const severity_view& severity_view,
 fft_statistics& p_fft_statistics)
const
{
  fft fft(mp_cartesian_topology->get_index());
  get_fft(severity_view, fft);
  
  // Compute autocorrelation.
  const double autocorrelation = get_autocorrelation_off_dc(severity_view);
  
  // Additional statistics.
  p_fft_statistics.set(fft_statistics::autocorrelation, 
                       autocorrelation);
  p_fft_statistics.set(fft_statistics::mean, 
                       fft.get_mean              ());
  p_fft_statistics.set(fft_statistics::variance, 
                       fft.get_variance          ());
  p_fft_statistics.set(fft_statistics::standard_deviation, 
                       fft.get_standard_deviation());
}


//------------------------------------------------------------------------------
void
perf_data_file_test::get_directed_variance
(const severity_view& severity_view,
 directed_variance&   directed_variance)
const
{
  fft fft(mp_cartesian_topology->get_index());
  get_fft(severity_view, fft);
  directed_variance.compute(fft);
}


//------------------------------------------------------------------------------
double
perf_data_file_test::get_autocorrelation_off_dc
(const severity_view& severity_view)
const
{
  const auto& cart_topo_idx = mp_cartesian_topology->get_index();

  fft temp            (cart_topo_idx);
  fft temp_conj_square(cart_topo_idx);
  data_array<double> auto_correlation(cart_topo_idx.get_num_positions());

  get_fft(severity_view, temp);
  temp.square_with_conjugate(temp_conj_square);
  temp_conj_square.clear_dc();

  dft_inverse::compute(temp_conj_square, cart_topo_idx, &auto_correlation);

  return auto_correlation.at(0);
}

} // namespace pvt
