//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef WITHOUT_CUBE

#include <cube/Cube.h>
#include <cube/CubeCartesian.h>

#include <pvt/cartesian_topology.hpp>
#include <pvt/data_array.hpp>

#include "perf_data_file_cube_cartesian_topology_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_cube_cartesian_topology_loader::
  perf_data_file_cube_cartesian_topology_loader
(const cube::Cube*                p_cube,
 std::vector<cartesian_topology>& cartesian_topologies)
: mp_cube               (p_cube)
, m_cartesian_topologies(cartesian_topologies)
{

}


//------------------------------------------------------------------------------
bool
perf_data_file_cube_cartesian_topology_loader::load
()
{
  m_cartesian_topologies.reserve(mp_cube->get_cartv().size());

  for (std::size_t id = 0; id < mp_cube->get_cartv().size(); ++id)
  {
    cube::Cartesian* const curr_cube_cart_topo = mp_cube->get_cartv()[id];
   
    const std::string topology_name(construct_topology_name(id));
   
    const std::vector<std::string> dimension_names(
      construct_dimension_names(curr_cube_cart_topo, id));
    
    cartesian_topology_index cart_topo_idx(
      curr_cube_cart_topo->get_dimv());
    
    data_array<std::size_t> pTopologyToDataMap =
      construct_topology_to_data_map(curr_cube_cart_topo,
                                     cart_topo_idx);
    
    m_cartesian_topologies.emplace_back(id,
                                        topology_name,
                                        cart_topo_idx,
                                        dimension_names,
                                        pTopologyToDataMap);
  }

  return true;
}


//------------------------------------------------------------------------------
std::string
perf_data_file_cube_cartesian_topology_loader::construct_topology_name
(std::size_t id)
{
  std::stringstream topo_id_stream;
  topo_id_stream << "Topology ";
  topo_id_stream << id;
  
  cube::Cartesian* const p_cube_cart_topo = mp_cube->get_cartv()[id];

  return std::string(  p_cube_cart_topo->get_name().size() != 0 
                     ? p_cube_cart_topo->get_name() 
                     : topo_id_stream.str());
}


//------------------------------------------------------------------------------
std::vector<std::string>
perf_data_file_cube_cartesian_topology_loader::construct_dimension_names
(cube::Cartesian* p_cube_cartesian_topology,
 std::size_t      id)
{
  std::vector<std::string> dimension_names(
    p_cube_cartesian_topology->get_ndims(), "");

  char curr_dim_char = 'A';
  for (std::size_t dim_idx = 0; dim_idx < dimension_names.size(); ++dim_idx)
  {
    const std::string dim_name(
      p_cube_cartesian_topology->get_dim_name(dim_idx).size() != 0 ?
      p_cube_cartesian_topology->get_dim_name(dim_idx) :
      std::string(1, curr_dim_char));
    dimension_names[dim_idx] = dim_name;
    ++curr_dim_char;
  }
  return dimension_names;
}


//------------------------------------------------------------------------------
data_array<std::size_t>
perf_data_file_cube_cartesian_topology_loader::construct_topology_to_data_map
(cube::Cartesian*                p_cube_cartesian_topology,
 const cartesian_topology_index& p_cartesian_topology_index)
{
  std::map<const cube::Sysres*, std::size_t> location_id_map =
    construct_location_to_id_map();

  data_array<std::size_t> topology_to_data_map(
    p_cartesian_topology_index.get_num_positions());

  for (auto curr_cart_sys : p_cube_cartesian_topology->get_cart_sys())
  {
    const std::size_t curr_index 
      = p_cartesian_topology_index.get_index(curr_cart_sys.second);
    topology_to_data_map[curr_index] 
      = location_id_map[curr_cart_sys.first];
  }

  return topology_to_data_map;
}


//------------------------------------------------------------------------------
std::map<const cube::Sysres*, std::size_t>
perf_data_file_cube_cartesian_topology_loader::construct_location_to_id_map
()
{
  std::map<const cube::Sysres*, std::size_t> location_id_map;

  std::size_t location_id = 0;
  for (const cube::Location* curr_location : mp_cube->get_locationv())
  {
    location_id_map[curr_location] = location_id;
    ++location_id;
  }

  return location_id_map;
}

} // namespace pvt

#endif // #ifndef WITHOUT_CUBE
