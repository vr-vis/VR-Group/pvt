//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef WITHOUT_CUBE

#include <cube/Cube.h>

#include <pvt/sysnode.hpp>

#include "perf_data_file_cube_sysnode_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_cube_sysnode_loader::perf_data_file_cube_sysnode_loader
(const cube::Cube*                        p_cube,
 std::vector<sysnode>&                    sysnodes,
 std::vector<sysnode*>&                   root_sysnodes,
 std::map<const sysnode*, cube::Sysres*>& sysnode_to_cube_sysnode_map)
: mp_cube                      (p_cube)
, m_sysnodes                   (sysnodes)
, m_root_sysnodes              (root_sysnodes)
, m_sysnode_to_cube_sysnode_map(sysnode_to_cube_sysnode_map)
{

}


//------------------------------------------------------------------------------
bool
perf_data_file_cube_sysnode_loader::load
()
{
  load_sysnodes         ();
  load_root_sysnodes    ();
  process_child_sysnodes();
  
  return m_root_sysnodes.size() != 0;
}


//------------------------------------------------------------------------------
void
perf_data_file_cube_sysnode_loader::load_sysnodes
()
{
  const std::size_t num_sysnodes = mp_cube->get_sysv().size();
  m_sysnodes.reserve(num_sysnodes);
  
  for (std::size_t id = 0; id < num_sysnodes; ++id)
  {
    cube::Sysres* p_cube_sysnode = mp_cube->get_sysv()[id];

    m_sysnodes.emplace_back(id, p_cube_sysnode->get_name());
    sysnode* p_sysnode = &m_sysnodes.back();
    
    m_cube_sysnode_to_sysnode_map[p_cube_sysnode] = p_sysnode;
    m_sysnode_to_cube_sysnode_map[p_sysnode     ] = p_cube_sysnode;
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_cube_sysnode_loader::load_root_sysnodes
()
{
  m_root_sysnodes.reserve(mp_cube->get_root_stnv().size());
  for (cube::SystemTreeNode* p_cube_root_sysnode : mp_cube->get_root_stnv())
    m_root_sysnodes.push_back(m_cube_sysnode_to_sysnode_map[p_cube_root_sysnode]);
}


//------------------------------------------------------------------------------
void
perf_data_file_cube_sysnode_loader::process_child_sysnodes
()
{
  for (auto cube_sysnode_sysnode_pair : m_cube_sysnode_to_sysnode_map)
  {
    cube::Sysres* const p_cube_sysnode = cube_sysnode_sysnode_pair.first ;
    sysnode*      const p_sysnode      = cube_sysnode_sysnode_pair.second;
    
    for (std::size_t i = 0; i < p_cube_sysnode->get_children().size(); ++i)
    {
      p_sysnode->add_child(
        m_cube_sysnode_to_sysnode_map[p_cube_sysnode->get_child(i)]);
    }
  }
  
  // Traverse cube's location groups separately.
  // Apparently, they are no children of the system's nodes,
  // but list the latter as their parents.
  for (cube::LocationGroup* const cube_loc_grp : mp_cube->get_location_groupv())
  {
    sysnode*      const p_sysnode        = m_cube_sysnode_to_sysnode_map[cube_loc_grp];
    cube::Sysres* const p_cube_parent    = cube_loc_grp->get_parent();
    sysnode*      const p_parent_sysnode = m_cube_sysnode_to_sysnode_map[p_cube_parent];
    
    p_parent_sysnode->add_child(p_sysnode);
  }
}

} // namespace pvt

#endif // #ifndef WITHOUT_CUBE
