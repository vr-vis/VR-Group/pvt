//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cassert>

#include <fstream>
#include <numeric>

#include <pvt/cnode.hpp>
#include <pvt/sysnode.hpp>
#include <pvt/dft.hpp>
#include <pvt/dft_inverse.hpp>
#include <pvt/directed_variance.hpp>
#include <pvt/fft.hpp>
#include <pvt/fft_statistics.hpp>
#include <pvt/metric.hpp>
#include <pvt/severity_view.hpp>

#include "perf_data_file_native_intermediate.hpp"
#include "perf_data_file_native_cartesian_topology_loader.hpp"
#include "perf_data_file_native_cnode_loader.hpp"
#include "perf_data_file_native_metric_loader.hpp"
#include "perf_data_file_native_sysnode_loader.hpp"
#include "perf_data_file_native_system_geometry_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_native_intermediate::perf_data_file_native_intermediate
(const std::string& filename)
: perf_data_file(filename)
, m_severities_load_buffer              (0)
, m_severities_load_buffer_severity_view(nullptr)
, m_fft_buffer                          (std::vector<std::size_t>{0})
, m_fft_buffer_severity_view            (nullptr)
, m_fft_square_buffer                   (std::vector<std::size_t>{0})
, m_autocorrelation_buffer              (0)
, m_first_cartesian_topology            (nullptr)
{
  
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::setup
()
{
  std::ifstream dataSizeFile(m_filename + "/data.size");
  dataSizeFile >> m_data_size;
  dataSizeFile.close();
  
  m_severities_load_buffer.resize(m_data_size);
  m_severities_load_buffer_severity_view = severity_view(0);
  
  m_severities_in_topology_buffer.resize(m_data_size);
  m_autocorrelation_buffer.resize(m_data_size);
  
  return m_data_size > 0;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::load_cnodes
(std::vector<cnode>&  cnodes,
 std::vector<cnode*>& root_cnodes)
{
  perf_data_file_native_cnode_loader loader(m_filename, cnodes, root_cnodes);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::load_metrics
(std::vector<metric>&  metrics,
 std::vector<metric*>& root_metrics)
{
  perf_data_file_native_metric_loader loader(m_filename, metrics, root_metrics);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::load_sysnodes
(std::vector<sysnode>&  sysnodes,
 std::vector<sysnode*>& root_sysnodes)
{
  perf_data_file_native_sysnode_loader loader(m_filename, 
                                              sysnodes,
                                              root_sysnodes);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::load_cartesian_topologies
(std::vector<cartesian_topology>& cartesian_topologies)
{
  perf_data_file_native_cartesian_topology_loader loader(m_filename,
                                                         m_data_size,
                                                         cartesian_topologies);
  
  const bool success = loader.load();
  if (cartesian_topologies.size() > 0)
  {
    m_first_cartesian_topology = &cartesian_topologies[0];
    
    m_fft_buffer = fft(m_first_cartesian_topology->get_index());
    m_fft_buffer_severity_view = severity_view(nullptr);
    
    m_fft_square_buffer = fft(m_first_cartesian_topology->get_index());
  }
  return success;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::load_system_geometries
(const std::vector<sysnode*>&  root_sysnodes,
 std::vector<system_geometry>& system_geometries)
{
  perf_data_file_native_system_geometry_loader loader(m_filename,
                                                      root_sysnodes,
                                                      system_geometries);
  return loader.load();
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::write
(const perf_data& perf_data,
 export_flags     export_flags)
const
{
  bool success = true;
  m_severities_load_buffer.resize(perf_data.get_data_size());
  
  if (export_flags & export_metadata)
  {
    success &= write_42                 ();
    success &= write_data_size          (perf_data);
    success &= write_call_tree          (perf_data);
    success &= write_cart_topo_list     (perf_data);
    success &= write_metric_tree        (perf_data);
    success &= write_cart_topo_data_maps(perf_data);
  }
  
  if (export_flags & export_data)
  {
    std::vector<metric const*> metric_refs;
    std::vector<cnode  const*>  cnode_refs;

    for (auto& current_metric : perf_data.get_metrics())
      metric_refs.push_back(&current_metric);
    for (auto& current_cnode : perf_data.get_cnodes())
      cnode_refs.push_back(&current_cnode);

    success &= write_data(perf_data,
                          metric_refs,
                          cnode_refs);
  }
  return success;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::write
(const perf_data&                  perf_data,
 export_flags                      export_flags,
 const std::vector<const metric*>& metric_ids,
 const std::vector<const cnode*>&  cnode_ids)
const
{
  bool success = true;
  m_severities_load_buffer.resize(perf_data.get_data_size());
  
  if (export_flags & export_metadata)
  {
    success &= write_42                 ();
    success &= write_data_size          (perf_data);
    success &= write_call_tree          (perf_data);
    success &= write_cart_topo_list     (perf_data);
    success &= write_metric_tree        (perf_data);
    success &= write_cart_topo_data_maps(perf_data);
    success &= write_sysnode_tree       (perf_data);
  }
  
  if (export_flags & export_data)
  {
    success &= write_data(perf_data, metric_ids, cnode_ids);
  }
  return success;
}


//------------------------------------------------------------------------------
double
perf_data_file_native_intermediate::get_severity
(const severity_view& severity_view)
const
{
  load_severities(severity_view);
  return std::accumulate(m_severities_load_buffer.begin(),
                         m_severities_load_buffer.end  (),
                         0.0);
}


//------------------------------------------------------------------------------
void
perf_data_file_native_intermediate::get_severities
(const severity_view& severity_view,
 data_array<double>&  severities)
const
{
  load_severities(severity_view);
  std::copy(m_severities_load_buffer.begin(),
            m_severities_load_buffer.end  (),
            severities              .begin());
}


//------------------------------------------------------------------------------
void
perf_data_file_native_intermediate::get_fft
(const severity_view& severityView,
 fft&                 fft)
const
{
  compute_fft(severityView);
  for (std::size_t i = 0; i < fft.get_num_data(); ++i)
  {
    fft.real(i) = m_fft_buffer.real(i);
    fft.imag(i) = m_fft_buffer.imag(i);
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_native_intermediate::get_fft_statistics
(const severity_view& severity_view,
fft_statistics&      fft_stat)
const
{
  compute_fft(severity_view);
  
  fft_stat.set(fft_statistics::autocorrelation, 0.0);
  fft_stat.set(fft_statistics::mean,
               m_fft_buffer.get_mean());
  fft_stat.set(fft_statistics::variance,
               m_fft_buffer.get_variance());
  fft_stat.set(fft_statistics::standard_deviation,
               sqrt(fft_stat.get(fft_statistics::variance)));
}


//------------------------------------------------------------------------------
void
perf_data_file_native_intermediate::get_directed_variance
(const severity_view& severity_view,
 directed_variance&   directed_variance)
const
{
  fft fft(m_first_cartesian_topology->get_index());
  get_fft(severity_view, fft);
  directed_variance.compute(fft);
}


//------------------------------------------------------------------------------
double
perf_data_file_native_intermediate::get_autocorrelation_off_dc
(const severity_view& severity_view)
const
{
  compute_fft(severity_view);
  m_fft_buffer.square_with_conjugate(m_fft_square_buffer);
  
  m_fft_square_buffer.clear_dc();
  dft_inverse::compute(m_fft_square_buffer,
                       m_first_cartesian_topology->get_index(),
                       &m_autocorrelation_buffer);
  return m_autocorrelation_buffer[0];
}


//------------------------------------------------------------------------------
void
perf_data_file_native_intermediate::load_severities
(const severity_view& severity_view)
const
{
  if (m_severities_load_buffer_severity_view != severity_view)
  {
    m_severities_load_buffer.zero();

    assert(severity_view.is_valid());

    const metric* const metric = severity_view.get_metric();
    const cnode*  const cnode  = severity_view.get_cnode ();

    const std::string filename(get_filename() + "/" +
       metric->get_id_string() +
      (metric->has_children() &&
       severity_view.is_metric_total() ? "I" : "E") + "_" +
       cnode->get_id_string() +
      (cnode->has_children() &&
       severity_view.is_cnode_total() ? "I" : "E") + ".dat");
    m_severities_load_buffer.read(filename);
    m_severities_load_buffer_severity_view = severity_view;
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_native_intermediate::compute_fft
(const severity_view& severity_view)
const
{
  if (m_fft_buffer_severity_view != severity_view)
  {
    assert(severity_view.is_valid());
    
    load_severities(severity_view);
    m_first_cartesian_topology->map_data(m_severities_load_buffer,
                                         m_severities_in_topology_buffer);
    
    dft::compute(m_severities_in_topology_buffer,
                 m_first_cartesian_topology->get_index(),
                 &m_fft_buffer);
    
    m_fft_buffer_severity_view = severity_view;
  }
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::write_42
()
const
{
  const std::string filename(m_filename + "/" + "42u");
  auto file = fopen(filename.c_str(), "wb");
  const bool success = file != nullptr;
  unsigned int fortytwo = 42u;
  fwrite(&fortytwo, sizeof(unsigned int), 1, file);
  fclose(file);
  return success;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::write_data_size
(const perf_data& perf_data)
const
{
  std::ofstream data_size_file(m_filename + "/data.size");
  const bool success = data_size_file.is_open();
  data_size_file << perf_data.get_data_size();
  data_size_file.close();
  return success;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::write_call_tree
(const perf_data& perf_data)
const
{
  std::ofstream cnode_file(m_filename + "/call.tree");
  if (!cnode_file.is_open())
    return false;
  
  cnode_file << perf_data.get_cnodes().size() << '\n';
  
  cnode_file << perf_data.get_root_cnodes().size() << ' ';
  for (const cnode* root_cnode : perf_data.get_root_cnodes())
    cnode_file << root_cnode->get_id() << ' ';
  cnode_file << '\n';
  
  for (const cnode& curr_cnode : perf_data.get_cnodes())
  {
    cnode_file << curr_cnode.get_name() << '\n';
    cnode_file << curr_cnode.get_children().size() << ' ';
    for (const cnode* child : curr_cnode.get_children())
      cnode_file << child->get_id() << ' ';
    cnode_file << '\n';
  }
  
  cnode_file.close();
  
  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::write_cart_topo_list
(const perf_data& perf_data)
const
{
  std::ofstream cart_topo_list_file(m_filename + "/cartTopo.list");
  if (!cart_topo_list_file.is_open())
    return false;
  
  cart_topo_list_file << perf_data.get_cartesian_topologies().size() << "\n";
  for (const auto& cart_topo : perf_data.get_cartesian_topologies())
  {
    cart_topo_list_file << cart_topo.get_name          () << "\n";
    cart_topo_list_file << cart_topo.get_num_dimensions() << " ";

    for (std::size_t dim_size : cart_topo.get_index().get_dimension_sizes())
      cart_topo_list_file << dim_size << " ";

    cart_topo_list_file << "\n";
    
    for (const std::string& dim_name : cart_topo.get_dimension_names())
      cart_topo_list_file << dim_name << "\n";
  }

  cart_topo_list_file.close();
  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::write_metric_tree
(const perf_data& perf_data)
const
{
  std::ofstream metric_file(m_filename + "/metrics.tree");
  if (!metric_file.is_open())
    return false;
  
  metric_file << perf_data.get_metrics().size() << '\n';
  
  metric_file << perf_data.get_root_metrics().size() << ' ';
  for (const metric* root_metric : perf_data.get_root_metrics())
    metric_file << root_metric->get_id() << ' ';

  metric_file << '\n';
  
  for (const metric& curr_metric : perf_data.get_metrics())
  {
    /// \todo Add unique name to metric for next line?
    metric_file << curr_metric.get_name() << '\n';
    metric_file << curr_metric.get_name() << '\n';
    metric_file << curr_metric.get_unit_of_measurement() << '\n';
    
    metric_file << curr_metric.get_children().size() << ' ';
    for (const metric* child : curr_metric.get_children())
      metric_file << child->get_id() << ' ';

    metric_file << '\n';
  }
  
  metric_file.close();
  return true;
}


//------------------------------------------------------------------------------
bool 
perf_data_file_native_intermediate::write_sysnode_tree
(const perf_data& perf_data) 
const
{
  std::ofstream system_file(m_filename + "/system.tree");
  if (!system_file.is_open())
    return false;

  system_file << perf_data.get_sysnodes().size() << '\n';

  system_file << perf_data.get_root_sysnodes().size() << ' ';
  for (const sysnode* root_sysnode : perf_data.get_root_sysnodes())
    system_file << root_sysnode->get_id() << ' ';

  system_file << '\n';

  for (const sysnode& curr_sysnode : perf_data.get_sysnodes())
  {
    system_file << curr_sysnode.get_name() << '\n';
    system_file << curr_sysnode.get_children().size() << ' ';

    for (const sysnode* child : curr_sysnode.get_children())
      system_file << child->get_id() << ' ';

    system_file << '\n';
  }

  system_file.close();
  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::write_cart_topo_data_maps
(const perf_data& perf_data)
const
{
  for (const auto& cart_topo : perf_data.get_cartesian_topologies())
  {
    std::stringstream filename;
    filename << m_filename << '/' << "cartTopo";
    filename << cart_topo.get_id();
    filename << "ToData.map";
    cart_topo.get_topology_to_data_map().dump(filename.str());
  }
  return true;
}


//------------------------------------------------------------------------------
bool
perf_data_file_native_intermediate::write_data
(const perf_data&                  perf_data,
 const std::vector<const metric*>& metric_ids,
 const std::vector<const cnode* >& cnode_ids )
const
{
  for (const metric* curr_metric : metric_ids)
  {
    for (const cnode* curr_cnode : cnode_ids)
    {
      write_severities(perf_data, { &perf_data,
                                    curr_metric,
                                    severity_view::self,
                                    curr_cnode,
                                    severity_view::self });
      if (curr_metric->has_children())
      {
        write_severities(perf_data, { &perf_data,
                                      curr_metric,
                                      severity_view::total,
                                      curr_cnode,
                                      severity_view::self });
      }
      if (curr_cnode->has_children())
      {
        write_severities(perf_data, { &perf_data,
                                      curr_metric,
                                      severity_view::self,
                                      curr_cnode,
                                      severity_view::total });
      }
      if (curr_metric->has_children() && curr_cnode->has_children())
      {
        write_severities(perf_data, { &perf_data,
                                      curr_metric,
                                      severity_view::total,
                                      curr_cnode,
                                      severity_view::total });
      }
    }
  }
  return true;
}


//------------------------------------------------------------------------------
void
perf_data_file_native_intermediate::write_severities
(const perf_data&     perf_data,
 const severity_view& severity_view)
const
{
  perf_data.get_severities(severity_view, m_severities_load_buffer);
  m_severities_load_buffer_severity_view = severity_view;
  
  const double sum = std::accumulate(m_severities_load_buffer.begin(),
                                     m_severities_load_buffer.end  (),
                                     0.0);
  if (sum == 0.0)
    return;
  
  // Write severities.
  const std::string severities_filename_base(
    m_filename +
    "/" +
     severity_view.get_metric()->get_id_string() +
    (severity_view.is_metric_total() ? "I" : "E") + "_" +
     severity_view.get_cnode ()->get_id_string() +
    (severity_view.is_cnode_total() ? "I" : "E"));
  m_severities_load_buffer.dump(severities_filename_base + ".dat");
}

} // namespace pvt
