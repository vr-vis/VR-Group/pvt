//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_FILE_NATIVE_CNODE_LOADER_HPP_
#define PVT_PERF_DATA_FILE_NATIVE_CNODE_LOADER_HPP_

#include <fstream>
#include <string>
#include <vector>

namespace pvt {

class cnode;
class perf_data;

//------------------------------------------------------------------------------
class perf_data_file_native_cnode_loader
{
public:
   perf_data_file_native_cnode_loader(const std::string&   filename,
                                      std::vector<cnode>&  cnodes,
                                      std::vector<cnode*>& root_cnodes);
  
  bool load();
  
private:
  void load_num_cnodes                 ();
  void load_root_cnode_ids             ();
  void load_cnodes_and_child_ids       ();
  void load_cnode                      (std::size_t id);
  void load_cnode_child_ids            ();
  void process_root_and_cnode_child_ids();

  std::ifstream                         m_cnodes_file;
  std::vector<cnode>&                   m_cnodes;
  std::vector<cnode*>&                  m_root_cnodes;

  std::size_t                           m_num_cnodes;
  std::vector<std::size_t>              m_root_cnode_ids;
  std::vector<std::vector<std::size_t>> m_cnode_child_ids;
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_FILE_NATIVE_CNODE_LOADER_HPP_
