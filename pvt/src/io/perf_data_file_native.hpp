//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_FILE_NATIVE_HPP_
#define PVT_PERF_DATA_FILE_NATIVE_HPP_

#include <pvt/data_array.hpp>
#include <pvt/io/perf_data_file.hpp>

namespace pvt {

class fft;
class fft_statistics;

//------------------------------------------------------------------------------
class perf_data_file_native : public perf_data_file
{
public:
  perf_data_file_native(const std::string& filename);
  
  bool setup() override;
  bool load_cnodes (
    std::vector<cnode>&           cnodes,
    std::vector<cnode*>&          root_cnodes         ) override;
  bool load_metrics(
    std::vector<metric>&          metrics,
    std::vector<metric*>&         root_metrics        ) override;
  bool load_sysnodes(
    std::vector<sysnode>&         sysnodes,
    std::vector<sysnode*>&        root_sysnodes       ) override;
  bool load_cartesian_topologies(
    std::vector<cartesian_topology>& cartesian_topologies) override;
  bool load_system_geometries(
    const std::vector<sysnode*>&  root_sysnodes,
    std::vector<system_geometry>& system_geometries   ) override;
  
  bool write(const perf_data&                  perf_data,
             export_flags                      export_flags) const override;
  bool write(const perf_data&                  perf_data,
             export_flags                      export_flags,
             const std::vector<const metric*>& metric_ids,
             const std::vector<const cnode*>&  cnode_ids   ) const override;
  
  double get_severity              (const severity_view& severity_view    )
                                    const override;
  void   get_severities            (const severity_view& severity_view,
                                    data_array<double>&  severities       ) 
                                    const override;
  void   get_fft                   (const severity_view& severity_view, 
                                    fft&                 fft              ) 
                                    const override;
  void   get_fft_statistics        (const severity_view& severity_view, 
                                    fft_statistics&      fft_statistics   ) 
                                    const override;
  void   get_directed_variance     (const severity_view& severity_view,
                                    directed_variance&   directed_variance) 
                                    const override;
  double get_autocorrelation_off_dc(const severity_view& severity_view    ) 
                                    const override;
  
private:
  bool write_42                 () const;
  bool write_data_size          (const perf_data& perf_data) const;
  bool write_call_tree          (const perf_data& perf_data) const;
  bool write_cart_topo_list     (const perf_data& perf_data) const;
  bool write_metric_tree        (const perf_data& perf_data) const;
  bool write_sysnode_tree       (const perf_data& perf_data) const;
  bool write_cart_topo_data_maps(const perf_data& perf_data) const;
  bool write_data               (const perf_data&                  perf_data,
                                 const std::vector<const metric*>& metric_ids,
                                 const std::vector<const cnode*>&  cnode_ids,
                                 export_flags                      export_flags) const;
  void write_severities_and_fft(const perf_data&                   perf_data,
                                const severity_view&               severity_view,
                                export_flags                       export_flags) const;

  mutable data_array<double> m_severities_load_buffer;
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_FILE_NATIVE_HPP_
