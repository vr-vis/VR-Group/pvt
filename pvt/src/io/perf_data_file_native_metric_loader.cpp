//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/metric.hpp>

#include "perf_data_file_native_metric_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_native_metric_loader::perf_data_file_native_metric_loader
(const std::string&    filename,
 std::vector<metric>&  metrics,
 std::vector<metric*>& root_metrics)
 : m_metrics_file(filename + "/metrics.tree")
 , m_metrics     (metrics)
, m_root_metrics (root_metrics)
, m_num_metrics  (0)
{

}


//------------------------------------------------------------------------------
bool
perf_data_file_native_metric_loader::load
()
{
  load_num_metrics                 ();
  load_root_metric_ids             ();
  load_metrics_and_child_ids       ();
  process_root_and_metric_child_ids();

  return m_metrics_file.is_open();
}


//------------------------------------------------------------------------------
void
perf_data_file_native_metric_loader::load_num_metrics
()
{
  m_metrics_file >> m_num_metrics;
  m_metrics_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_metric_loader::load_root_metric_ids
()
{
  std::size_t num_root_metrics = 0;
  m_metrics_file >> num_root_metrics;
  m_root_metric_ids = std::vector<std::size_t>(num_root_metrics, 0);
  for (std::size_t i = 0; i < num_root_metrics; ++i)
    m_metrics_file >> m_root_metric_ids[i];
  m_metrics_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_metric_loader::load_metrics_and_child_ids
()
{
  for (std::size_t i = 0; i < m_num_metrics; ++i)
  {
    load_metric          (i);
    load_metric_child_ids( );
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_native_metric_loader::load_metric
(std::size_t id)
{
  std::string unique_name;
  std::string display_name;
  std::string unit_of_measurement;
  
  std::getline(m_metrics_file, unique_name);
  std::getline(m_metrics_file, display_name);
  std::getline(m_metrics_file, unit_of_measurement);

  m_metrics.emplace_back(id, display_name, unit_of_measurement);
}


//------------------------------------------------------------------------------
void
perf_data_file_native_metric_loader::load_metric_child_ids
()
{
  std::size_t num_children = 0;
  m_metrics_file >> num_children;
  m_metric_child_ids.push_back(std::vector<std::size_t>(num_children, 0));
  for (std::size_t i = 0; i < num_children; ++i)
  {
    std::size_t child_metric_id;
    m_metrics_file >> child_metric_id;
    m_metric_child_ids.back()[i] = child_metric_id;
  }
  m_metrics_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_metric_loader::process_root_and_metric_child_ids
()
{
  // Process root metric ids.
  for (auto root_metric_id : m_root_metric_ids)
    m_root_metrics.push_back(&m_metrics[root_metric_id]);

  // Process metric child ids.
  for (std::size_t metric_id = 0; metric_id < m_num_metrics; ++metric_id)
    for (auto child_id : m_metric_child_ids[metric_id])
      m_metrics[metric_id].add_child(&m_metrics[child_id]);
}

} // namespace pvt
