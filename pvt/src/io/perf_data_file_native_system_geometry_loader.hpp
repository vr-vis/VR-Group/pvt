//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_FILE_NATIVE_SYSTEM_GEOMETRY_LOADER_HPP_
#define PVT_PERF_DATA_FILE_NATIVE_SYSTEM_GEOMETRY_LOADER_HPP_

#include <fstream>
#include <map>
#include <vector>

#include <pvt/geometry.hpp>

namespace pvt {

template <typename T> 
class data_array;
class sysnode;
class system_geometry;

//------------------------------------------------------------------------------
class perf_data_file_native_system_geometry_loader
{
public:
   perf_data_file_native_system_geometry_loader(
    const std::string&                  filename,
    const std::vector<sysnode*>&        root_sysnodes,
          std::vector<system_geometry>& system_geometries);
 
  bool load();

private:
  void load_geometries          (std::vector<geometry>& geometries);
  void load_geometry            (const std::string&     filename, 
                                 geometry&              geometry);
  void load_geometry_to_data_map(std::vector<geometry>& geometries);

  static void calculate_areas   (std::vector<geometry>& geometries);

  std::string   m_geometry_dir_name;
  std::ifstream m_geometry_map_file;

  const std::vector<sysnode*>&  m_root_sysnodes;
  std::vector<system_geometry>& m_system_geometries;
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_FILE_NATIVE_SYSTEM_GEOMETRY_LOADER_HPP_
