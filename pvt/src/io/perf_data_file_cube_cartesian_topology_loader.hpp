//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_FILE_CUBE_CARTESIAN_TOPOLOGY_LOADER_HPP_
#define PVT_PERF_DATA_FILE_CUBE_CARTESIAN_TOPOLOGY_LOADER_HPP_

#include <map>
#include <vector>

namespace cube {
  class Cartesian;
  class Cube;
  class Sysres;
}

namespace pvt {

class cartesian_topology;

//------------------------------------------------------------------------------
class perf_data_file_cube_cartesian_topology_loader
{
public:
  perf_data_file_cube_cartesian_topology_loader(
    const cube::Cube*                p_cube,
    std::vector<cartesian_topology>& cartesian_topologies);
  
  bool load();
  
private:
  std::string construct_topology_name(std::size_t id);
  
  std::vector<std::string> construct_dimension_names(
    cube::Cartesian* p_cube_cartesian_topology,
    std::size_t      id);
  
  data_array<std::size_t>  construct_topology_to_data_map(
    cube::Cartesian*                p_cube_cartesian_topology,
    const cartesian_topology_index& cart_topo_idx);
  
  std::map<const cube::Sysres*, std::size_t> construct_location_to_id_map();

  const cube::Cube*                mp_cube;
  std::vector<cartesian_topology>& m_cartesian_topologies;
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_FILE_CUBE_CARTESIAN_TOPOLOGY_LOADER_HPP_
