//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef WITHOUT_CUBE

#include <cube/Cube.h>

#include <pvt/cnode.hpp>

#include "perf_data_file_cube_cnode_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_cube_cnode_loader::perf_data_file_cube_cnode_loader
(const cube::Cube*                     p_cube,
 std::vector<cnode>&                   cnodes,
 std::vector<cnode*>&                  root_cnodes,
 std::map<const cnode*, cube::Cnode*>& cnode_to_cube_cnode_map)
: mp_cube                  (p_cube)
, m_cnodes                 (cnodes)
, m_root_cnodes            (root_cnodes)
, m_cnode_to_cube_cnode_map(cnode_to_cube_cnode_map)
{

}


//------------------------------------------------------------------------------
bool
perf_data_file_cube_cnode_loader::load
()
{
  load_cnodes        ();
  load_root_cnodes   ();
  process_child_nodes();
  return m_root_cnodes.size() != 0;
}


//------------------------------------------------------------------------------
void
perf_data_file_cube_cnode_loader::load_cnodes
()
{
  const std::size_t num_cnodes = mp_cube->get_cnodev().size();
  m_cnodes.reserve(num_cnodes);
  
  for (std::size_t id = 0; id < num_cnodes; ++id)
  {
    cube::Cnode* p_cube_cnode = mp_cube->get_cnodev()[id];

    m_cnodes.emplace_back(id, p_cube_cnode->get_callee()->get_name());
    cnode* p_cnode = &m_cnodes.back();

    m_cube_cnode_to_cnode_map[p_cube_cnode] = p_cnode;
    m_cnode_to_cube_cnode_map[p_cnode     ] = p_cube_cnode;
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_cube_cnode_loader::load_root_cnodes
()
{
  m_root_cnodes.reserve(mp_cube->get_root_cnodev().size());
  for (cube::Cnode* p_cube_root_cnode : mp_cube->get_root_cnodev())
    m_root_cnodes.push_back(m_cube_cnode_to_cnode_map[p_cube_root_cnode]);
}


//------------------------------------------------------------------------------
void
perf_data_file_cube_cnode_loader::process_child_nodes
()
{
  for (auto cube_cnode_cnode_pair : m_cube_cnode_to_cnode_map)
  {
    cube::Cnode* const p_cube_cnode = cube_cnode_cnode_pair.first ;
    cnode*       const p_cnode      = cube_cnode_cnode_pair.second;
    
    for (std::size_t i = 0; i < p_cube_cnode->get_children().size(); ++i)
    {
      p_cnode->add_child(
        m_cube_cnode_to_cnode_map[p_cube_cnode->get_child(i)]);
    }
  }
}

} // namespace pvt

#endif // #ifndef WITHOUT_CUBE
