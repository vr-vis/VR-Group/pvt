//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/sysnode.hpp>

#include "perf_data_file_native_sysnode_loader.hpp"

namespace pvt {

//------------------------------------------------------------------------------
perf_data_file_native_sysnode_loader::perf_data_file_native_sysnode_loader
(const std::string&     filename,
 std::vector<sysnode>&  sysnodes,
 std::vector<sysnode*>& root_sysnodes)
: m_sysnodes_file(filename + "/system.tree")
, m_sysnodes     (sysnodes)
, m_root_sysnodes(root_sysnodes)
, m_num_sysnodes (0)
{

}


//------------------------------------------------------------------------------
bool
perf_data_file_native_sysnode_loader::load
()
{
  load_num_sysnodes                 ();
  load_root_sysnode_ids             ();
  load_sysnodes_and_child_ids       ();
  process_root_and_sysnode_child_ids();

  return m_sysnodes_file.is_open();
}


//------------------------------------------------------------------------------
void
perf_data_file_native_sysnode_loader::load_num_sysnodes
()
{
  m_sysnodes_file >> m_num_sysnodes;
  m_sysnodes_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_sysnode_loader::load_root_sysnode_ids
()
{
  std::size_t num_root_sysnodes = 0;
  m_sysnodes_file >> num_root_sysnodes;
  m_root_sysnode_ids = std::vector<std::size_t>(num_root_sysnodes, 0);
  for (std::size_t i = 0; i < num_root_sysnodes; ++i)
    m_sysnodes_file >> m_root_sysnode_ids[i];
  m_sysnodes_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_sysnode_loader::load_sysnodes_and_child_ids
()
{
  for (std::size_t i = 0; i < m_num_sysnodes; ++i)
  {
    load_sysnode          (i);
    load_sysnode_child_ids( );
  }
}


//------------------------------------------------------------------------------
void
perf_data_file_native_sysnode_loader::load_sysnode
(std::size_t id)
{
  std::string display_name;
  std::getline(m_sysnodes_file, display_name);
  m_sysnodes.emplace_back(id, display_name);
}


//------------------------------------------------------------------------------
void
perf_data_file_native_sysnode_loader::load_sysnode_child_ids
()
{
  std::size_t num_children = 0;
  m_sysnodes_file >> num_children;
  m_sysnode_child_ids.push_back(std::vector<std::size_t>(num_children, 0));
  for (std::size_t i = 0; i < num_children; ++i)
  {
    std::size_t child_sysnode_id;
    m_sysnodes_file >> child_sysnode_id;
    m_sysnode_child_ids.back()[i] = child_sysnode_id;
  }
  m_sysnodes_file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//------------------------------------------------------------------------------
void
perf_data_file_native_sysnode_loader::process_root_and_sysnode_child_ids
()
{
  // Process root sysnode ids.
  for (auto root_sysnode_id : m_root_sysnode_ids)
    m_root_sysnodes.push_back(&m_sysnodes[root_sysnode_id]);

  // Process sysnode child ids.
  for (std::size_t sysnode_id = 0; sysnode_id < m_num_sysnodes; ++sysnode_id)
    for (auto child_id : m_sysnode_child_ids[sysnode_id])
      m_sysnodes[sysnode_id].add_child(&m_sysnodes[child_id]);
}

} // namespace pvt
