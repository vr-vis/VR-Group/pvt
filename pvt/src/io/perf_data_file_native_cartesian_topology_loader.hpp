//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#ifndef PVT_PERF_DATA_FILE_NATIVE_CARTESIAN_TOPOLOGY_LOADER_HPP_
#define PVT_PERF_DATA_FILE_NATIVE_CARTESIAN_TOPOLOGY_LOADER_HPP_

#include <fstream>
#include <string>
#include <vector>

namespace pvt {

template <typename T> class data_array;
class cartesian_topology;
class cartesian_topology_index;
class perf_data;

//------------------------------------------------------------------------------
class perf_data_file_native_cartesian_topology_loader
{
public:
  perf_data_file_native_cartesian_topology_loader(
    const std::string&               filename,
    const std::size_t&               data_size,
    std::vector<cartesian_topology>& cartesian_topologies);
  
  bool load();
  
private:
  void load_num_topologies();
  void load_topologies   ();

  void construct_default_cartesian_topology();

  std::string              load_display_name            ();
  cartesian_topology_index load_cartesian_topology_index();
  std::vector<std::string> load_dimension_names     (std::size_t num_dimensions);
  data_array<std::size_t>  load_topology_to_data_map(std::size_t id,
    const cartesian_topology_index& cart_topo_idx);

  static data_array<std::size_t> construct_default_topology_to_data_map(
    const cartesian_topology_index& cart_topo_idx);

  std::ifstream                    m_topologies_file;
  std::string                      m_topologies_file_base_name;
  std::size_t                      m_data_size;
  std::vector<cartesian_topology>& m_cartesian_topologies;

  std::size_t                      m_num_topologies;
};

} // namespace pvt

#endif // #ifndef PVT_PERF_DATA_FILE_NATIVE_CARTESIAN_TOPOLOGY_LOADER_HPP_
