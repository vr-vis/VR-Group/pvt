//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/cartesian_topology.hpp>

namespace pvt {

//------------------------------------------------------------------------------
cartesian_topology::cartesian_topology
(std::size_t                     id,
 const std::string&              display_name,
 const cartesian_topology_index& index,
 const std::vector<std::string>& dimension_names,
 const data_array<std::size_t>&  topology_to_data_map)
: name_id_pair          (display_name, id)
, m_index               (index)
, m_dimension_names     (dimension_names)
, m_topology_to_data_map(topology_to_data_map)
{

}


//------------------------------------------------------------------------------
cartesian_topology::~cartesian_topology
()
{

}

} // namespace pvt
