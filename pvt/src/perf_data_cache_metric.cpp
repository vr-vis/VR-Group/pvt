//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/cnode.hpp>
#include <pvt/perf_data.hpp>
#include <pvt/perf_data_cache_metric.hpp>

namespace pvt {

//------------------------------------------------------------------------------
perf_data_cache_metric::perf_data_cache_metric
(const severity_view* p_severity_view)
: mp_severity_view        (p_severity_view)
, m_severities_load_buffer(p_severity_view->get_performance_data()
                           ->get_data_size())
, m_severities_total      (p_severity_view->get_performance_data()
                           ->get_metrics().size(), 0.0)
, m_severities_self       (p_severity_view->get_performance_data()
                           ->get_metrics().size(), 0.0)
, m_dirty_severities      (p_severity_view->get_performance_data()
                           ->get_metrics().size(), true)
{

}


//------------------------------------------------------------------------------
void
perf_data_cache_metric::update_severity
(const metric* p_metric)
{
  m_severities_total[p_metric->get_id()] = 0.0;
  m_severities_self [p_metric->get_id()] = 0.0;

  auto temp_severity_view(*mp_severity_view);
  temp_severity_view.set_metric(p_metric);

  temp_severity_view.set_cnode(
    mp_severity_view->get_performance_data()->get_root_cnodes()[0]);
  temp_severity_view.set_cnodes_total (true);
  temp_severity_view.set_metric_total (true);
  m_severities_total[p_metric->get_id()] =
   mp_severity_view->get_performance_data()->get_severity(temp_severity_view);
  
  temp_severity_view.set_metric_total(false);
  m_severities_self[p_metric->get_id()] =
   mp_severity_view->get_performance_data()->get_severity(temp_severity_view);
  
  m_dirty_severities[p_metric->get_id()] = false;
}

} // namespace pvt
