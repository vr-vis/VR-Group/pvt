//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/directed_variance.hpp>
#include <pvt/fft.hpp>

namespace pvt {

//------------------------------------------------------------------------------
double
fft::get_variance()
const
{
  auto variance = 0.0;
  const auto& dimension_sizes(m_cart_topo_idx.get_dimension_sizes());
  for (std::size_t i = 1; i < m_cart_topo_idx.get_num_positions  (); ++i)
  {
    const auto curr_coord(m_cart_topo_idx.get_coordinate(i));

    auto boundary = false;
    if ((dimension_sizes.back() % 2 == 1 &&
         dimension_sizes.back() == curr_coord.back() + 1) ||
        curr_coord     .back() == 0)
      boundary = true;

    const auto  power_curr_frequency = (boundary ? 1 : 2) * mod_sq_at(i);
    variance += power_curr_frequency;
  }
  return variance * m_normalizer * m_normalizer;
}


//------------------------------------------------------------------------------
void
fft::zero
()
{
  for (std::size_t i = 0; i < m_num_data; ++i)
  {
    mp_fft_data[i][0] = 0.0f;
    mp_fft_data[i][1] = 0.0f;
  }
  m_is_guaranteed_zero = true;
}

} // namespace pvt
