//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/cartesian_topology.hpp>
#include <pvt/data_array.hpp>
#include <pvt/metric.hpp>
#include <pvt/perf_data.hpp>
#include <pvt/perf_data_cache_cnode.hpp>

namespace pvt {

//------------------------------------------------------------------------------
perf_data_cache_cnode::perf_data_cache_cnode
(const severity_view* p_severity_view)
: mp_severity_view              (p_severity_view)
, m_severities_load_buffer      (p_severity_view->get_performance_data()
                                ->get_data_size())
, m_severities_total            (p_severity_view->get_performance_data()
                                ->get_cnodes().size(), 0.0)
, m_severities_self             (p_severity_view->get_performance_data()
                                ->get_cnodes().size(), 0.0)
, m_dirty_severities            (p_severity_view->get_performance_data()
                                ->get_cnodes().size(), true)
, m_directed_variances          (p_severity_view->get_performance_data()
                                ->get_cnodes().size(), 
                                directed_variance(
                                  p_severity_view
                                  ->get_performance_data()
                                  ->get_cartesian_topologies()[0]
                                  .get_num_dimensions()))
, m_dirty_directed_variance     (p_severity_view->get_performance_data()
                                ->get_cnodes().size(), true)
, m_suitable_3d_view_computer   (p_severity_view->get_performance_data()
                                ->get_cartesian_topologies()[0]
                                .get_num_dimensions())
, m_suitable_3d_view            (p_severity_view->get_performance_data()
                                ->get_cnodes().size(), false)
, m_suitable_3d_view_in_children(p_severity_view->get_performance_data()
                                ->get_cnodes().size(), false)
, m_dirty_suitable_3d_views     (p_severity_view->get_performance_data()
                                ->get_cnodes().size(), true)
{
}


//------------------------------------------------------------------------------
void
perf_data_cache_cnode::update_severity
(const cnode* p_cnode)
{
  m_severities_total[p_cnode->get_id()] = 0.0;
  m_severities_self [p_cnode->get_id()] = 0.0;

  auto temp_severity_view(*mp_severity_view);
  temp_severity_view.set_cnode({p_cnode});
  
  temp_severity_view.set_cnodes_total(true);
  m_severities_total[p_cnode->get_id()] =
    mp_severity_view->get_performance_data()->get_severity(temp_severity_view);
  
  temp_severity_view.set_cnodes_total(false);
  m_severities_self[p_cnode->get_id()] =
    mp_severity_view->get_performance_data()->get_severity(temp_severity_view);
  
  m_dirty_severities[p_cnode->get_id()] = false;
}


//------------------------------------------------------------------------------
void
perf_data_cache_cnode::update_directed_variance
(const cnode* p_cnode)
{
  /// \todo Make this work with total *and* self severity.
  severity_view curr_severity_view(mp_severity_view->get_performance_data());
  curr_severity_view.set_metric   (mp_severity_view->get_metric());
  curr_severity_view.set_cnode    (p_cnode);
  
  mp_severity_view->get_performance_data()->get_directed_variance(
    curr_severity_view, m_directed_variances[p_cnode->get_id()]);
}


//------------------------------------------------------------------------------
void
perf_data_cache_cnode::update_suitable_3d_view
(const cnode* p_cnode)
{
  if (mp_severity_view->get_metric() == nullptr)
    return;

  m_suitable_3d_view            [p_cnode->get_id()] = false;
  m_suitable_3d_view_in_children[p_cnode->get_id()] = false;
  
  // suitable 3D view in p_cnode
  const double mean =
    get_severity_total(p_cnode) /
    static_cast<double>(m_severities_load_buffer.size());
  check_and_update_directed_variance(p_cnode);
  
  const severity_view curr_sev_view(mp_severity_view->get_performance_data(),
                                    mp_severity_view->get_metric          (),
                                    mp_severity_view->is_metric_total     (),
                                    p_cnode,
                                    mp_severity_view->is_cnode_total      ());
  const bool is_large_severity =
    m_suitable_3d_view_computer.is_severe_enough(curr_sev_view);
  
  const bool is_large_variation =
    m_suitable_3d_view_computer.is_suitable_3d_view(
    m_directed_variances[p_cnode->get_id()], mean);

  m_suitable_3d_view[p_cnode->get_id()] = 
    is_large_severity && is_large_variation;

  // suitable 3D view in children
  for (const cnode* p_curr_child_cnode : p_cnode->get_children())
  {
    m_suitable_3d_view_in_children  [p_cnode->get_id()] =
      m_suitable_3d_view_in_children[p_cnode->get_id()] ||
      is_suitable_3d_view             (p_curr_child_cnode) ||
      is_suitable_3d_view_in_children(p_curr_child_cnode);
  }
  m_dirty_suitable_3d_views[p_cnode->get_id()] = false;
}

} // namespace pvt
