//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/cartesian_topology_index.hpp>
#include <pvt/dft.hpp>
#include <pvt/fft.hpp>

namespace pvt {

//------------------------------------------------------------------------------
dft::dft
(data_array<double>&             input_data,
 const cartesian_topology_index& topology_index,
 fft*                            p_output_fft)
: m_fftw_dim_sizes(topology_index)
, m_fft_plan      (nullptr)
{
  m_fft_plan = fftw_plan_dft_r2c(
    topology_index.get_num_dimensions(),
    m_fftw_dim_sizes.data(),
    input_data      .data(),
    p_output_fft   ->data(),
    FFTW_ESTIMATE);
}


//------------------------------------------------------------------------------
dft::~dft
()
{
  fftw_destroy_plan(m_fft_plan);
}

} // namespace pvt
