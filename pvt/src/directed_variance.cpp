//==============================================================================
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//==============================================================================
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//==============================================================================

#include <cmath>

#include <pvt/directed_variance.hpp>
#include <pvt/fft.hpp>

namespace pvt
{

//------------------------------------------------------------------------------
directed_variance::directed_variance
(std::size_t dimension_count) : data_array<double>(dimension_count)
{
  zero();
}


//------------------------------------------------------------------------------
void
directed_variance::compute
(const fft& fft)
{
  zero();
  
  std::vector<double> curr_frequency(size(), 0.0);
  for (std::size_t i = 1; i < fft.get_num_data(); ++i)
  {
    const double curr_spectral_energy =
      fft.get_spectral_energy(i);
    fft.convert_to_dc_centered_frequency(i, curr_frequency);

    const bool   not_on_boundary      = 
      !fft.is_on_boundary(curr_frequency);

    const double inverse_norm_squared =
      1.0 / fft.get_frequency_norm_squared(curr_frequency);
    
    // Add spectral energy to directed variance.
    const auto add_weighted_spectral_energy_callable =
    [curr_spectral_energy,
     inverse_norm_squared,
     not_on_boundary     ]
    (double curr_directed_variance  ,
     double curr_frequency_component)
    {
      return
        curr_directed_variance +
        (1.0 + static_cast<double>(not_on_boundary)) *
        inverse_norm_squared *
        curr_frequency_component * curr_frequency_component *
        curr_spectral_energy;
    };

    std::transform(begin(),
                   end  (),
                   curr_frequency.begin(),
                   begin(),
                   add_weighted_spectral_energy_callable);
  }
} 
} // namespace pvt