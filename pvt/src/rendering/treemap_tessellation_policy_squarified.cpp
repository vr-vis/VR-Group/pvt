//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/rendering/treemap_tessellation_policy_squarified.hpp>

namespace pvt {

//------------------------------------------------------------------------------
void 
treemap_tessellation_policy_squarified::tessellate
(const Eigen::AlignedBox2f&        rectangle, 
 const std::vector<float>&         weights, 
 int                               recursion_depth,
 std::vector<Eigen::AlignedBox2f>& output_subrectangles)
{
  std::vector<Eigen::AlignedBox2f> current_row;
  squarify(rectangle, rectangle, weights, current_row, output_subrectangles);
}


//------------------------------------------------------------------------------
void 
treemap_tessellation_policy_squarified::squarify
(const Eigen::AlignedBox2f&        container_rectangle, 
 const Eigen::AlignedBox2f&        boundary_rectangle, 
 const std::vector<float>&         weights, 
 std::vector<Eigen::AlignedBox2f>& current_row, 
 std::vector<Eigen::AlignedBox2f>& output_subrectangles)
{
  auto edge_lengths         = boundary_rectangle.sizes();
  auto shortest_edge_length = std::min(edge_lengths[0], edge_lengths[1]);
  auto horizontal_division  = edge_lengths[0] >= edge_lengths[1];

  // Calculate the worst aspect ratio without candidate rectangle.
  auto worst = get_worst_aspect_ratio(shortest_edge_length, current_row);
  
  // Clone the next row from the current row.
  auto next_row(current_row);
  // Add the candidate rectangle (as default).
  next_row.push_back(Eigen::AlignedBox2f());
  
  auto sum_row_weights = 0.0;
  for (auto i = 0; i < next_row.size(); i++)
    sum_row_weights += weights[output_subrectangles.size() + i];
  
  auto sum_weights = 0.0;
  for (auto i = 0; i < weights.size(); i++)
    sum_weights += weights[i];

  // Recalculate rectangle boundaries.
  auto offset = 0.0;
  for (auto i = 0; i < next_row.size(); i++)
  {
    auto weight = weights[output_subrectangles.size() + i];

    auto& rectangle_min = next_row[i].min();
    auto& rectangle_max = next_row[i].max();

    double absolute;
    if (horizontal_division)
    {
      absolute = boundary_rectangle.sizes()[1] * weight / sum_row_weights;
      rectangle_min[1] = boundary_rectangle.min()[1] + offset;
      rectangle_max[1] = boundary_rectangle.min()[1] + offset + absolute;
      rectangle_min[0] = boundary_rectangle.min()[0];
      rectangle_max[0] = boundary_rectangle.min()[0] + 
        (container_rectangle.volume() / sum_weights) * 
        weight / (rectangle_max[1] - rectangle_min[1]);
    }
    else
    {
      absolute = boundary_rectangle.sizes()[0] * weight / sum_row_weights;
      rectangle_min[0] = boundary_rectangle.min()[0] + offset;
      rectangle_max[0] = boundary_rectangle.min()[0] + offset + absolute;
      rectangle_min[1] = boundary_rectangle.min()[1];
      rectangle_max[1] = boundary_rectangle.min()[1] + 
        (container_rectangle.volume() / sum_weights) * 
        weight / (rectangle_max[0] - rectangle_min[0]);
    }
    offset += absolute;
  }

  // Calculate the worst aspect ratio with candidate rectangle.
  auto next_worst = get_worst_aspect_ratio(shortest_edge_length, next_row);

  // If candidate rectangle resulted in a better aspect ratio:
  // Note: This inequality is the other way around in paper (paper is wrong).
  if (worst >= next_worst || current_row.size() == 0)
  {
    // If this is the last area we are processing, end.
    if (next_row.size() + output_subrectangles.size() == weights.size())
    {
      // Fix the last row by appending it to the output rectangles.
      output_subrectangles.insert(output_subrectangles.end(),
                                  next_row.begin(),
                                  next_row.end  ());
      return;
    }

    // Keep the candidate rectangle, and proceed with next area.
    squarify(container_rectangle,
             boundary_rectangle,
             weights,
             next_row,
             output_subrectangles);
  }
  // If candidate rectangle resulted in a worse aspect ratio:
  else
  {
    // Fix the current row by appending it to the output rectangles.
    output_subrectangles.insert(output_subrectangles.end(),
                                current_row.begin(), 
                                current_row.end  ());

    // Update container rectangle boundaries.
    Eigen::AlignedBox2f next_boundary_rectangle;
    next_boundary_rectangle =
      Eigen::AlignedBox2f(horizontal_division ? 
        Eigen::Vector2f(boundary_rectangle.min()[0] + current_row[0].sizes()[0],
                        boundary_rectangle.min()[1]) : 
        Eigen::Vector2f(boundary_rectangle.min()[0],
                        boundary_rectangle.min()[1] + current_row[0].sizes()[1]),
        boundary_rectangle.max());

    // Clear current row.
    current_row.clear();

    // Retry candidate rectangle.
    squarify(container_rectangle, 
             next_boundary_rectangle, 
             weights,
             current_row, 
             output_subrectangles);
  }
}


//------------------------------------------------------------------------------
float 
treemap_tessellation_policy_squarified::get_worst_aspect_ratio
(const float&                            shortest_edge_length, 
 const std::vector<Eigen::AlignedBox2f>& row)
{
  if (row.size() == 0)
    return std::numeric_limits<float>::max();

  auto min_area  = std::numeric_limits<float>::max();
  auto max_area  = std::numeric_limits<float>::min();
  auto sum_areas = 0.0;

  for (const auto& rectangle : row)
  {
    auto area  = rectangle.volume();
    min_area   = std::min(min_area, area);
    max_area   = std::max(max_area, area);
    sum_areas += area;
  }

  auto sum_areas_sq            = sum_areas * sum_areas;
  auto shortest_edge_length_sq = shortest_edge_length * shortest_edge_length;

  return std::max(shortest_edge_length_sq * max_area / sum_areas_sq,
                  sum_areas_sq / (shortest_edge_length_sq * min_area));
}

} // namespace pvt
