//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/gl/shader.hpp>
#include <pvt/rendering/renderable_parallel_coordinates_plot.hpp>
#include <pvt/rendering/shader_strings.hpp>

namespace pvt {

//------------------------------------------------------------------------------
void 
renderable_parallel_coordinates_plot::on_setup
()
{
  m_axes_vertex_buffer .emplace();
  m_axes_vertex_array  .emplace();
  m_axes_shader_program.emplace();

  m_axes_shader_program->attach_shader(vertex_shader(
    shader_strings::parallel_coord_axis_vert));
  m_axes_shader_program->attach_shader(geometry_shader(
    shader_strings::parallel_coord_axis_geom));
  m_axes_shader_program->attach_shader(fragment_shader(
    shader_strings::parallel_coord_axis_frag));
  m_axes_shader_program->link();

  m_axes_shader_program->bind  ();
  m_axes_vertex_array  ->bind  ();
  m_axes_vertex_buffer ->bind  ();
  m_axes_shader_program->set_attribute_buffer  ("position", 3, GL_FLOAT);
  m_axes_shader_program->enable_attribute_array("position");
  m_axes_vertex_buffer ->unbind();
  m_axes_vertex_array  ->unbind();
  m_axes_shader_program->unbind();
  
  m_plot_vertex_buffer .emplace();
  m_plot_vertex_array  .emplace();
  m_plot_shader_program.emplace();

  m_plot_shader_program->attach_shader(vertex_shader(
    shader_strings::parallel_coord_vert));
  m_plot_shader_program->attach_shader(geometry_shader(
    shader_strings::parallel_coord_geom));
  m_plot_shader_program->attach_shader(fragment_shader(
    shader_strings::parallel_coord_frag));
  m_plot_shader_program->link();

  m_plot_shader_program->bind  ();
  m_plot_vertex_array  ->bind  ();
  m_plot_vertex_buffer ->bind  ();
  m_plot_shader_program->set_attribute_buffer  ("position", 3, GL_FLOAT);
  m_plot_shader_program->enable_attribute_array("position");
  m_plot_vertex_buffer ->unbind();
  m_plot_vertex_array  ->unbind();
  m_plot_shader_program->unbind();
}


//------------------------------------------------------------------------------
void 
renderable_parallel_coordinates_plot::on_render
(const camera* pCamera)
{
  if (m_needs_upload)
    upload();

  glClearColor(1.0, 1.0, 1.0, 1.0);
  glClear     (GL_COLOR_BUFFER_BIT);

  Eigen::Vector2f inverse_pixel_size(1.0f / m_size[0], 1.0f / m_size[1]);
  
  if (m_draw_axes)
  {
    m_axes_vertex_array  ->bind();
    m_axes_shader_program->bind();
    m_axes_shader_program->set_uniform("inversePixelSize", inverse_pixel_size);
    glDrawArrays(GL_LINES, 0, m_axes_draw_count);
    m_axes_shader_program->unbind();
    m_axes_vertex_array  ->unbind();
  }

  m_plot_vertex_array  ->bind();
  m_plot_shader_program->bind();
  m_plot_shader_program->set_uniform("inversePixelSize", inverse_pixel_size);
  for (auto i = 0; i < m_entries.size(); i++)
  {
    m_plot_shader_program->set_uniform("selected", m_entries[i].m_is_selected);
    glDrawArrays(GL_LINE_STRIP, i * m_axes.size(), m_axes.size());
  }
  m_plot_shader_program->unbind();
  m_plot_vertex_array  ->unbind();
}


//------------------------------------------------------------------------------
void 
renderable_parallel_coordinates_plot::set_entry_selected
(unsigned index, bool selected)
{
  if (m_entries.size() <= index)
    return;
  m_entries[index].m_is_selected = selected;
}


//------------------------------------------------------------------------------
void 
renderable_parallel_coordinates_plot::set_axis_inverted
(unsigned index, bool inverted)
{
  if (m_axes.size() <= index)
    return;
  m_axes[index].m_is_inverted = inverted;
  m_needs_upload = true;
}


//------------------------------------------------------------------------------
void 
renderable_parallel_coordinates_plot::deselect_all_entries
()
{
  for (auto& entry : m_entries)
    entry.m_is_selected = false;
}


//------------------------------------------------------------------------------
void 
renderable_parallel_coordinates_plot::upload
()
{
  // Setup axes vertices.
  std::vector<Eigen::Vector3f> axes_vertices;
  for (auto axis : m_axes)
  {
    axes_vertices.push_back(
      Eigen::Vector3f(-1.0f + 2.0f * axis.m_x_offset, -1.0f, 0.0f));
    axes_vertices.push_back(
      Eigen::Vector3f(-1.0f + 2.0f * axis.m_x_offset,  1.0f, 0.0f));
  }

  m_axes_draw_count = axes_vertices.size();
  m_axes_vertex_array ->bind    ();
  m_axes_vertex_buffer->bind    ();
  m_axes_vertex_buffer->set_data(axes_vertices.size() * sizeof(Eigen::Vector3f),
                                 axes_vertices.data());
  m_axes_vertex_buffer->unbind  ();
  m_axes_vertex_array ->unbind  ();
  
  // Setup plot vertices from the axes.
  const float margin      = 0.01f;
  const float plot_height = 2.0f - 2.0 * margin;
  std::vector<Eigen::Vector3f> plot_vertices;
  for (auto& entry : m_entries)
  {
    std::vector<Eigen::Vector3f> entry_vertices;
    for (auto& axe : m_axes)
    {
      entry_vertices.push_back(
        Eigen::Vector3f(-1.0f + 2.0f * axe.m_x_offset,
                        -1.0f + margin, 
                         0.0f));
    }

    // Adjust height of vertices with percentages.
    auto& percentages = entry.m_percentages;
    assert(percentages.size() == m_axes.size());
    for (auto i = 0; i < percentages.size(); i++)
    {
      auto value = m_axes[i].m_is_inverted 
        ? 1.0f - percentages[i] 
        : percentages[i];
      entry_vertices[i][1] += plot_height * value;
    }

    plot_vertices.insert(plot_vertices .end  (), 
                         entry_vertices.begin(), 
                         entry_vertices.end  ());
  }

  m_plot_vertex_array ->bind   ();
  m_plot_vertex_buffer->bind   ();
  m_plot_vertex_buffer->set_data(plot_vertices.size() * sizeof(Eigen::Vector3f), 
                                 plot_vertices.data());
  m_plot_vertex_buffer->unbind ();
  m_plot_vertex_array ->unbind ();

  m_needs_upload = false;
}

} // namespace pvt
