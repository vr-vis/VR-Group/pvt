//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/rendering/treemap_tessellation_policy_slice_and_dice.hpp>

namespace pvt {

//------------------------------------------------------------------------------
void 
treemap_tessellation_policy_slice_and_dice::tessellate
(const Eigen::AlignedBox2f&        rectangle, 
 const std::vector<float>&         weights, 
 int                               recursion_depth,
 std::vector<Eigen::AlignedBox2f>& output_subrectangles)
{
  auto& rectangle_min = rectangle.min();
  auto& rectangle_max = rectangle.max();

  auto offset = 0.0F;
  for (auto i = 0; i < weights.size(); i++)
  {
    auto absolute = 0.0F;
    if (recursion_depth % 2 == 0)
    {
      absolute = (rectangle_max[1] - rectangle_min[1]) * weights[i];
      output_subrectangles.push_back(
        Eigen::AlignedBox2f(
          Eigen::Vector2f(rectangle_min[0],
                          rectangle_min[1] + offset),
          Eigen::Vector2f(rectangle_max[0],
                          rectangle_min[1] + offset + absolute)));

    }
    else
    {
      absolute = (rectangle_max[0] - rectangle_min[0]) * weights[i];
      output_subrectangles.push_back(
        Eigen::AlignedBox2f(
          Eigen::Vector2f(rectangle_min[0] + offset,
                          rectangle_min[1]),
          Eigen::Vector2f(rectangle_min[0] + offset + absolute,
                          rectangle_max[1])));
    }
    offset += absolute;
  }
}

} // namespace pvt
