//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/rendering/camera.hpp>
#include <pvt/rendering/vertex_array_shader_pair.hpp>

namespace pvt {

//------------------------------------------------------------------------------
void 
vertex_array_shader_pair::setup
()
{
  m_vertex_array  .emplace();
  m_shader_program.emplace();
  
  m_vertex_array->bind();
}


//------------------------------------------------------------------------------
void 
vertex_array_shader_pair::bind
()
{
  m_vertex_array  ->bind();
  m_shader_program->bind();
}


//------------------------------------------------------------------------------
void 
vertex_array_shader_pair::unbind
()
{
  m_shader_program->unbind();
  m_vertex_array  ->unbind();
}


//------------------------------------------------------------------------------
void
vertex_array_shader_pair::pre_render
(const camera* p_camera)
{
  bind();

  if (p_camera != nullptr)
  {
    m_shader_program->set_uniform("view", 
      p_camera->get_view_matrix());
    m_shader_program->set_uniform("projection", 
      p_camera->get_projection_matrix());
  }
}


//------------------------------------------------------------------------------
void
vertex_array_shader_pair::post_render
()
{
  unbind();
}

} // namespace pvt
