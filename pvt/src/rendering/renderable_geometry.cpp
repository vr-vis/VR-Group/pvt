//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/gl/shader.hpp>
#include <pvt/rendering/camera.hpp>
#include <pvt/rendering/renderable_geometry.hpp>
#include <pvt/rendering/shader_strings.hpp>

namespace pvt {

//------------------------------------------------------------------------------
renderable_geometry::renderable_geometry
(const geometry* p_geometry, float severity, bool selected)
: mp_geometry(p_geometry)
, m_severity (severity)
, m_selected (selected)
{

}


//------------------------------------------------------------------------------
void 
renderable_geometry::on_setup
()
{
  m_shader_program      .emplace();
  m_vertex_array        .emplace();
  m_click_vertex_array  .emplace();
  m_click_shader_program.emplace();

  m_vertex_buffer       .emplace();
  m_normal_buffer       .emplace();
  m_texcoord_buffer     .emplace();
  m_index_buffer        .emplace();

  upload_buffer_data(mp_geometry->get_vertices (), *m_vertex_buffer  );
  upload_buffer_data(mp_geometry->get_normals  (), *m_normal_buffer  );
  upload_buffer_data(mp_geometry->get_texcoords(), *m_texcoord_buffer);
  upload_buffer_data(mp_geometry->get_indices  (), *m_index_buffer   );
  m_draw_count = mp_geometry->get_indices().size();

  m_shader_program->attach_shader(
    vertex_shader  (shader_strings::mesh_vert));
  m_shader_program->attach_shader(
    fragment_shader(shader_strings::mesh_frag));
  m_shader_program->link();

  m_click_shader_program->attach_shader(
    vertex_shader  (shader_strings::mesh_id_vert));
  m_click_shader_program->attach_shader(
    fragment_shader(shader_strings::mesh_id_frag));
  m_click_shader_program->link();

  m_vertex_array  ->bind  ();
  m_vertex_buffer ->bind  ();
  m_shader_program->set_attribute_buffer  ("vertex", 3, GL_FLOAT);
  m_shader_program->enable_attribute_array("vertex");
  m_vertex_buffer ->unbind();
  m_normal_buffer ->bind  ();
  m_shader_program->set_attribute_buffer  ("normal", 3, GL_FLOAT);
  m_shader_program->enable_attribute_array("normal");
  m_normal_buffer ->unbind();
  m_vertex_array  ->unbind();

  m_click_vertex_array  ->bind  ();
  m_vertex_buffer       ->bind  ();
  m_click_shader_program->set_attribute_buffer  ("vertex", 3, GL_FLOAT);
  m_click_shader_program->enable_attribute_array("vertex");
  m_vertex_buffer       ->unbind();
  m_click_vertex_array  ->unbind();
}


//------------------------------------------------------------------------------
void 
renderable_geometry::on_render
(const camera* p_camera)
{
  m_vertex_array  ->bind();
  m_shader_program->bind();
  
  if (p_camera != nullptr)
  {
    m_shader_program->set_uniform("view",       
      p_camera->get_view_matrix());
    m_shader_program->set_uniform("projection", 
      p_camera->get_projection_matrix());
  }

  m_shader_program->set_uniform("selected", m_selected);
  m_shader_program->set_uniform("severity", m_severity);

  m_index_buffer->bind();
  glDrawElements(GL_TRIANGLES, m_draw_count, GL_UNSIGNED_INT, nullptr);
  m_index_buffer->unbind();
  
  m_shader_program->unbind();
  m_vertex_array  ->unbind();
}


//------------------------------------------------------------------------------
void 
renderable_geometry::on_render_to_click_buffer
(const camera* p_camera)
{
  m_click_vertex_array  ->bind();
  m_click_shader_program->bind();

  if (p_camera != nullptr)
  {
    m_click_shader_program->set_uniform("view", 
      p_camera->get_view_matrix());
    m_click_shader_program->set_uniform("projection", 
      p_camera->get_projection_matrix());
  }

  m_click_shader_program->set_uniform("id", m_id);

  m_index_buffer->bind();
  glDrawElements(GL_TRIANGLES, m_draw_count, GL_UNSIGNED_INT, nullptr);
  m_index_buffer->unbind();

  m_click_shader_program->unbind();
  m_click_vertex_array  ->unbind();
}

} // namespace pvt
