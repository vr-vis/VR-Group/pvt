//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/gl/shader.hpp>
#include <pvt/rendering/renderable_axes.hpp>
#include <pvt/rendering/shader_strings.hpp>

namespace pvt {

//------------------------------------------------------------------------------
renderable_axes::renderable_axes
(const Eigen::Vector3i& lengths)
: m_lengths(lengths)
, m_scale  (1.0f, 1.0f, 1.0f)
{
  
}


//------------------------------------------------------------------------------
void
renderable_axes::on_setup
()
{
  vertex_array_shader_pair::setup();

  const float vertices[] = { 0.0f, 0.0f, 0.0f };

  m_vertex_buffer. emplace ();
  m_vertex_buffer->bind    ();
  m_vertex_buffer->set_data(3 * sizeof(float), vertices);
 
  m_shader_program->attach_shader(vertex_shader  (shader_strings::axes_vert));
  m_shader_program->attach_shader(geometry_shader(shader_strings::axes_geom));
  m_shader_program->attach_shader(fragment_shader(shader_strings::axes_frag));
  m_shader_program->link();
  m_shader_program->bind();
  m_shader_program->set_attribute_buffer  ("pos", 3, GL_FLOAT);
  m_shader_program->enable_attribute_array("pos");
  
  m_vertex_buffer->unbind();

  vertex_array_shader_pair::unbind();
}


//------------------------------------------------------------------------------
void
renderable_axes::on_render
(const camera* p_camera)
{
  vertex_array_shader_pair::pre_render(p_camera);
  
  GLint viewport[4];
  glGetIntegerv(GL_VIEWPORT, viewport);
  
  m_shader_program->set_uniform("lx"            , m_lengths[0]);
  m_shader_program->set_uniform("ly"            , m_lengths[1]);
  m_shader_program->set_uniform("lz"            , m_lengths[2]);
  m_shader_program->set_uniform("axisScale"     , m_scale);
  m_shader_program->set_uniform("lineWidth"     , 10.0f);
  m_shader_program->set_uniform("screenWidthInv", 1.0f / viewport[2]);
  
  glDrawArrays(GL_POINTS, 0, 1);
  
  vertex_array_shader_pair::post_render();
}

} // namespace pvt
