//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/gl/shader.hpp>

#include <pvt/rendering/treemap_interaction_policy_basic.hpp>
#include <pvt/rendering/shader_strings.hpp>

namespace pvt {

//------------------------------------------------------------------------------
treemap_interaction_policy_basic::treemap_interaction_policy_basic()
{
  m_framebuffer.bind();

  m_framebuffer_texture.bind();
  m_framebuffer_texture.set_min_filter(GL_NEAREST);
  m_framebuffer_texture.set_mag_filter(GL_NEAREST);
  m_framebuffer_texture.image_2d(GL_R32UI, 1, 1, GL_RED_INTEGER, GL_UNSIGNED_INT);
  m_framebuffer_texture.unbind();

  m_framebuffer.texture_2d(GL_COLOR_ATTACHMENT0, m_framebuffer_texture);
  m_framebuffer.unbind();
  
  m_shader_program.attach_shader(vertex_shader  (shader_strings::node_id_vert));
  m_shader_program.attach_shader(fragment_shader(shader_strings::node_id_frag));
  m_shader_program.link();
}


//------------------------------------------------------------------------------
void 
treemap_interaction_policy_basic::append_rectangle
(const Eigen::AlignedBox2f& rectangle, unsigned id)
{
  auto rectangle_min = rectangle.min();
  auto rectangle_max = rectangle.max();

  unsigned int index = m_vertices.size() / 2;

  m_vertices.insert(m_vertices.end(), {rectangle_min[0], rectangle_min[1],
                                       rectangle_max[0], rectangle_min[1],
                                       rectangle_min[0], rectangle_max[1],
                                       rectangle_max[0], rectangle_max[1]});

  m_ids.insert(m_ids.end(), {id, id, id, id});

  m_indices.insert(m_indices.end(), 
    {index, index + 1, index + 2, index + 2, index + 1, index + 3});
}


//------------------------------------------------------------------------------
void 
treemap_interaction_policy_basic::build
(const Eigen::Vector2i& size)
{
  m_size = size;

  m_framebuffer_texture.bind();
  m_framebuffer_texture.image_2d(GL_R32UI, 
                               m_size[0], 
                               m_size[1], 
                               GL_RED_INTEGER, 
                               GL_UNSIGNED_INT);
  m_framebuffer_texture.unbind();
    
  m_vertex_array.bind();
  m_shader_program    .bind();

  m_attribute_buffers[0].bind();
  m_attribute_buffers[0].set_data(m_vertices.size() * sizeof(float),
                                  m_vertices.data());
  m_shader_program.set_attribute_buffer  ("vertex", 2, GL_FLOAT);
  m_shader_program.enable_attribute_array("vertex");

  m_attribute_buffers[1].bind();
  m_attribute_buffers[1].set_data(m_ids.size() * sizeof(unsigned int),
                                  m_ids.data());

  m_shader_program.set_attribute_buffer_integer("id", 1, GL_UNSIGNED_INT);
  m_shader_program.enable_attribute_array      ("id");
    
  m_index_buffer.bind();
  m_index_buffer.set_data(m_indices.size() * sizeof(unsigned int), 
                          m_indices.data());

  Eigen::Matrix4f projection_matrix;
  projection_matrix << 2.0f / size[0], 0             ,  0, -1,
                      0              , 2.0f / size[1],  0, -1,
                      0              , 0             , -2, -1,
                      0              , 0             ,  0,  1;
  m_shader_program.set_uniform("projection", projection_matrix);
    
  for (auto i = 0; i < m_attribute_buffers.size(); i++)
    m_attribute_buffers[i].unbind();
  m_shader_program.unbind();
  m_index_buffer  .unbind();
  m_vertex_array  .unbind();

  m_draw_count = m_indices.size();

  m_vertices.clear();
  m_ids     .clear();
  m_indices .clear();
}


//------------------------------------------------------------------------------
void 
treemap_interaction_policy_basic::render
()
{
  glViewport(0, 0, m_size[0], m_size[1]);

  m_framebuffer.bind();
  m_framebuffer.set_draw_buffer(GL_COLOR_ATTACHMENT0);
  m_framebuffer.clear_color_buffer(0, 0u);

  m_vertex_array  .bind();
  m_shader_program.bind();
  m_index_buffer  .bind();

  glDrawElements(GL_TRIANGLES, m_draw_count, GL_UNSIGNED_INT, nullptr);

  m_index_buffer  .unbind();
  m_shader_program.unbind();
  m_vertex_array  .unbind();

  m_framebuffer.unbind();
}


//------------------------------------------------------------------------------
unsigned int
treemap_interaction_policy_basic::get_node
(const Eigen::Vector2i& position)
{
  unsigned int id;

  m_framebuffer.bind();
  m_framebuffer.set_read_buffer(GL_COLOR_ATTACHMENT0);

  glReadPixels(position[0],
               position[1],
               1,
               1,
               GL_RED_INTEGER,
               GL_UNSIGNED_INT,
               &id);
  
  m_framebuffer.set_read_buffer(GL_NONE);
  m_framebuffer.unbind();
  
  return id;
}

} // namespace pvt
