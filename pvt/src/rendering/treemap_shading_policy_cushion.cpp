//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/gl/shader.hpp>

#include <pvt/rendering/shader_strings.hpp>
#include <pvt/rendering/treemap_shading_policy_cushion.hpp>

namespace pvt {

//------------------------------------------------------------------------------
treemap_shading_policy_cushion::treemap_shading_policy_cushion()
{
  m_shader_program.attach_shader(vertex_shader(
    shader_strings::cushion_treemap_vert));
  m_shader_program.attach_shader(fragment_shader(
    shader_strings::cushion_treemap_frag));
  m_shader_program.link();
}


//------------------------------------------------------------------------------
void 
treemap_shading_policy_cushion::update_parameters
(const Eigen::AlignedBox2f& rectangle, 
 Eigen::AlignedBox2f&       surface, 
 int                        recursion_depth)
{
  if (recursion_depth != 0)
  {
    auto height = pow(m_scale_factor, recursion_depth) * m_height;
    add_ridge(rectangle, surface, height, 0);
    add_ridge(rectangle, surface, height, 1);
  }
}


//------------------------------------------------------------------------------
void 
treemap_shading_policy_cushion::append_rectangle
(const Eigen::AlignedBox2f& rectangle, 
 Eigen::AlignedBox2f&       surface)
{
  auto& vertices       = m_attributes[0];
  auto& colors         = m_attributes[1];
  auto& surface_params = m_attributes[2];

  unsigned int index = vertices.size() / 2;

  auto rectangle_min = rectangle.min();
  auto rectangle_max = rectangle.max();
  auto surface_min   = surface  .min();
  auto surface_max   = surface  .max();

  vertices.insert(vertices.end(), {
                  rectangle_min[0], rectangle_min[1],
                  rectangle_max[0], rectangle_min[1],
                  rectangle_min[0], rectangle_max[1],
                  rectangle_max[0], rectangle_max[1] });

  colors.insert(colors.end(), {
    m_base_color[0], m_base_color[1], m_base_color[2], m_base_color[3],
    m_base_color[0], m_base_color[1], m_base_color[2], m_base_color[3],
    m_base_color[0], m_base_color[1], m_base_color[2], m_base_color[3],
    m_base_color[0], m_base_color[1], m_base_color[2], m_base_color[3] });

  surface_params.insert(surface_params.end(), {
    surface_min[0], surface_max[0], surface_min[1], surface_max[1],
    surface_min[0], surface_max[0], surface_min[1], surface_max[1],
    surface_min[0], surface_max[0], surface_min[1], surface_max[1],
    surface_min[0], surface_max[0], surface_min[1], surface_max[1] });

  m_indices.insert(m_indices.end(), {
    index, index + 1, index + 2, index + 2, index + 1, index + 3 });
}


//------------------------------------------------------------------------------
void 
treemap_shading_policy_cushion::build
(const Eigen::Vector2i& size)
{
  glClearColor(0.0F, 0.0F, 0.0F, 1.0F);

  m_size = size;

  m_vertex_array.bind();
  for (auto i = 0; i < m_attribute_buffers.size(); i++)
  {
    m_attribute_buffers[i].bind();
    m_attribute_buffers[i].set_data(m_attributes[i].size() * sizeof(float),
                                    m_attributes[i].data());
  }
  m_index_buffer.bind();
  m_index_buffer.set_data(m_indices.size() * sizeof(unsigned int),
                          m_indices.data());
  m_shader_program.bind();

  m_attribute_buffers[0].bind();
  m_shader_program.set_attribute_buffer  ("vertex", 2, GL_FLOAT);
  m_shader_program.enable_attribute_array("vertex");

  m_attribute_buffers[1].bind();
  m_shader_program.set_attribute_buffer  ("color", 4, GL_FLOAT);
  m_shader_program.enable_attribute_array("color");

  m_attribute_buffers[2].bind();
  m_shader_program.set_attribute_buffer  ("surfaceParams", 4, GL_FLOAT);
  m_shader_program.enable_attribute_array("surfaceParams");

  m_shader_program.set_uniform("ambientCoef"  , m_ambient_coef);
  m_shader_program.set_uniform("diffuseCoef"  , m_diffuse_coef);
  m_shader_program.set_uniform("lightPosition", m_light_position);

  Eigen::Matrix4f projection_matrix;
  projection_matrix << 2.0f / size[0], 0             ,  0, -1,
                      0              , 2.0f / size[1],  0, -1,
                      0              , 0             , -2, -1,
                      0              , 0             ,  0,  1;
  m_shader_program.set_uniform("projection", projection_matrix);

  for (auto i = 0; i < m_attribute_buffers.size(); i++)
    m_attribute_buffers[i].unbind();
  m_shader_program.unbind();
  m_index_buffer  .unbind();
  m_vertex_array  .unbind();

  m_draw_count = m_indices.size();

  for (auto i = 0; i < m_attributes.size(); i++)
    m_attributes[i].clear();
  m_indices.clear();
}


//------------------------------------------------------------------------------
void 
treemap_shading_policy_cushion::render
()
{
  if (m_draw_count == 0)
    return;

  glClear   (GL_COLOR_BUFFER_BIT);
  glViewport(0, 0, m_size[0], m_size[1]);

  m_vertex_array  .bind();
  m_shader_program.bind();
  m_index_buffer  .bind();

  glDrawElements(GL_TRIANGLES, m_draw_count, GL_UNSIGNED_INT, nullptr);

  m_index_buffer  .unbind();
  m_shader_program.unbind();
  m_vertex_array  .unbind();
}


//------------------------------------------------------------------------------
void 
treemap_shading_policy_cushion::add_ridge
(const Eigen::AlignedBox2f& rectangle, 
 Eigen::AlignedBox2f&       surface, 
 const float&               height, 
 int                        direction)
{
  auto rectangle_min = rectangle.min();
  auto rectangle_max = rectangle.max();

  auto& surface_min = surface.min();
  auto& surface_max = surface.max();

  surface_min[direction] += 4 * height *
    (rectangle_max[direction] + rectangle_min[direction]) /
    (rectangle_max[direction] - rectangle_min[direction]);

  surface_max[direction] -= 4 * height / 
    (rectangle_max[direction] - rectangle_min[direction]);
}

} // namespace pvt
