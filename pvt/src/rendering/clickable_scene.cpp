//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/rendering/clickable.hpp>
#include <pvt/rendering/clickable_scene.hpp>
#include <pvt/rendering/renderable_geometry.hpp>

namespace pvt {

//------------------------------------------------------------------------------
void clickable_scene::setup()
{
  scene::setup();

  m_click_framebuffer.emplace();
  m_click_texture    .emplace();

  m_click_framebuffer->bind();
  m_click_texture    ->bind();

  m_click_texture->set_min_filter(GL_NEAREST);
  m_click_texture->set_mag_filter(GL_NEAREST);
  m_click_texture->image_2d(GL_R32I, 1, 1, GL_RED_INTEGER, GL_INT);

  m_click_framebuffer->texture_2d(GL_COLOR_ATTACHMENT0, *m_click_texture);

  m_click_texture    ->unbind();
  m_click_framebuffer->unbind();

  for (auto i = 0; i < m_renderables.size(); i++)
    if (auto clickable_object = dynamic_cast<clickable*>(m_renderables[i].get()))
      clickable_object->set_id(i + 1);
}


//------------------------------------------------------------------------------
void clickable_scene::render(const camera* p_camera)
{
  scene::render(p_camera);

  if (m_needs_upload)
  {
    m_click_texture->bind();
    m_click_texture->image_2d(GL_R32I, 
                              m_size[0], 
                              m_size[1], 
                              GL_RED_INTEGER,
                              GL_INT);
    m_click_texture->unbind();
    m_needs_upload = false;
  }

  m_click_framebuffer->bind();
  m_click_framebuffer->set_draw_buffer(GL_COLOR_ATTACHMENT0);
  m_click_framebuffer->clear_color_buffer(0, 0u);

  for (auto& renderable : m_renderables)
    if (auto clickable_object = dynamic_cast<clickable*>(renderable.get())) 
      clickable_object->on_render_to_click_buffer(p_camera);

  m_click_framebuffer->unbind();
}


//------------------------------------------------------------------------------
void clickable_scene::resize(const Eigen::Vector2i& size)
{
  m_size         = size;
  m_needs_upload = true;
}


//------------------------------------------------------------------------------
int clickable_scene::get_id(const Eigen::Vector2i& position)
{
  auto id = 0;

  m_click_framebuffer->bind();
  m_click_framebuffer->set_read_buffer(GL_COLOR_ATTACHMENT0);

  glReadPixels(position[0],
               position[1],
               1,
               1,
               GL_RED_INTEGER,
               GL_INT,
               &id);

  m_click_framebuffer->set_read_buffer(GL_NONE);
  m_click_framebuffer->unbind();

  return id - 1;
}


//------------------------------------------------------------------------------
renderable* clickable_scene::get_renderable(const Eigen::Vector2i& position)
{
  auto id = get_id(position);

  if (id < 0)
    return nullptr;

  for (auto& renderable : m_renderables)
    if (auto clickable_object = dynamic_cast<clickable*>(renderable.get()))
      if (clickable_object->get_id() == id)
        return renderable.get();

  return nullptr;
}

} // namespace pvt
