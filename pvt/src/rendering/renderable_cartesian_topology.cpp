//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <iostream>

#include <pvt/cartesian_topology.hpp>
#include <pvt/cartesian_topology_index.hpp>
#include <pvt/gl/shader.hpp>
#include <pvt/rendering/cube_vertices.hpp>
#include <pvt/rendering/renderable_cartesian_topology.hpp>
#include <pvt/rendering/shader_strings.hpp>

namespace pvt {

//------------------------------------------------------------------------------
renderable_cartesian_topology::renderable_cartesian_topology
(const cartesian_topology* p_cartesian_topology,
 const data_array<float>*  p_topology_mapping)
: mp_cartesian_topology    (p_cartesian_topology)
, mp_topology_mapping      (p_topology_mapping)
, m_ranges_on_dimensions   (std::vector<std::pair<float, float>>(
                              p_cartesian_topology->get_num_dimensions(),
                              std::pair<float, float>(0.0f, 1.0f)))
, m_projection({{ 0, 1, 2 }})
{

}


//------------------------------------------------------------------------------
void
renderable_cartesian_topology::on_setup
()
{
  vertex_array_shader_pair::setup();

  m_data_texture      .emplace();
  m_data_vertex_buffer.emplace();
  m_vertex_buffer     .emplace();

  init_shader      ();
  init_geometry    ();
  init_data_texture();
  
  vertex_array_shader_pair::unbind();
}


//------------------------------------------------------------------------------
void
renderable_cartesian_topology::on_render
(const camera* p_camera)
{
  if (m_needs_upload)
    upload();

  vertex_array_shader_pair::pre_render(p_camera);

  update_uniforms();
  render_geometry();

  vertex_array_shader_pair::post_render();
}


//------------------------------------------------------------------------------
void
renderable_cartesian_topology::init_shader
()
{
  const auto& cart_topo_idx = mp_cartesian_topology->get_index();
  
  m_shader_program->attach_shader(vertex_shader(
    shader_strings::cartesian_topology_vert));
  m_shader_program->attach_shader(fragment_shader(
    shader_strings::cartesian_topology_frag));
  m_shader_program->link();
  
  m_shader_program->bind();
  m_shader_program->set_uniform("numCellsX",
    static_cast<int>(cart_topo_idx.get_dimension_sizes  ()[m_projection[0]]));
  m_shader_program->set_uniform("numCellsY",
    static_cast<int>(cart_topo_idx.get_dimension_sizes  ()[m_projection[1]]));
  m_shader_program->set_uniform("numCellsZ",
    static_cast<int>(cart_topo_idx.get_dimension_sizes  ()[m_projection[2]]));
  m_shader_program->set_uniform("offsetX",
    static_cast<int>(cart_topo_idx.get_dimension_offsets()[m_projection[0]]));
  m_shader_program->set_uniform("offsetY",
    static_cast<int>(cart_topo_idx.get_dimension_offsets()[m_projection[1]]));
  m_shader_program->set_uniform("offsetZ",
    static_cast<int>(cart_topo_idx.get_dimension_offsets()[m_projection[2]]));
  m_shader_program->set_uniform("dataTextureBuffer", 0);
}


//------------------------------------------------------------------------------
void
renderable_cartesian_topology::init_geometry
()
{
  m_vertex_buffer->bind();
  m_vertex_buffer->set_data(cube_vertices_size * sizeof(float), cube_vertices);

  m_shader_program->set_attribute_buffer  ("pos", 
                                           3, 
                                           GL_FLOAT, 
                                           GL_TRUE, 
                                           8 * sizeof(float));
  m_shader_program->enable_attribute_array("pos");
  m_shader_program->set_attribute_buffer  ("texCoord", 
                                           2, 
                                           GL_FLOAT, 
                                           GL_TRUE, 
                                           8 * sizeof(float), 
                                           3 * sizeof(float));
  m_shader_program->enable_attribute_array("texCoord");
  m_shader_program->set_attribute_buffer  ("normal", 
                                           3, 
                                           GL_FLOAT, 
                                           GL_TRUE, 
                                           8 * sizeof(float), 
                                           5 * sizeof(float));
  m_shader_program->enable_attribute_array("normal");

  m_vertex_buffer->unbind();
}


//------------------------------------------------------------------------------
void
renderable_cartesian_topology::init_data_texture
()
{
  const auto num_data = mp_topology_mapping->size();
  
  m_data_texture      ->bind();
  m_data_vertex_buffer->bind();

  m_data_vertex_buffer->allocate  (num_data * sizeof(float)); 
  m_data_texture      ->set_buffer(GL_R32F, *m_data_vertex_buffer);

  m_data_vertex_buffer->unbind();
  m_data_texture      ->unbind();
}


//------------------------------------------------------------------------------
void
renderable_cartesian_topology::update_uniforms
()
{
  const auto  min_to_use    = m_minimum * m_map_minimum_to_zero;
  const auto& cart_topo_idx = mp_cartesian_topology->get_index();
  const auto  num_cells_x   = cart_topo_idx.get_dimension_sizes()[m_projection[0]];
  const auto  num_cells_y   = cart_topo_idx.get_dimension_sizes()[m_projection[1]];
  const auto  num_cells_z   = cart_topo_idx.get_dimension_sizes()[m_projection[2]];

  m_shader_program->set_uniform("axisScale"  , m_scale      );
  m_shader_program->set_uniform("minimum"    , min_to_use   );
  m_shader_program->set_uniform("maximum"    , m_maximum    );
  m_shader_program->set_uniform("baseOffset" , m_base_offset);
  m_shader_program->set_uniform("filteredMinimum",
    m_filtered_minimum);
  m_shader_program->set_uniform("filteredMaximum",
    m_filtered_maximum);
  m_shader_program->set_uniform("numCellsX",
    static_cast<int>(cart_topo_idx.get_dimension_sizes  ()[m_projection[0]]));
  m_shader_program->set_uniform("numCellsY",                                        
    static_cast<int>(cart_topo_idx.get_dimension_sizes  ()[m_projection[1]]));
  m_shader_program->set_uniform("numCellsZ",                                        
    static_cast<int>(cart_topo_idx.get_dimension_sizes  ()[m_projection[2]]));
  m_shader_program->set_uniform("offsetX",
    static_cast<int>(cart_topo_idx.get_dimension_offsets()[m_projection[0]]));
  m_shader_program->set_uniform("offsetY",
    static_cast<int>(cart_topo_idx.get_dimension_offsets()[m_projection[1]]));
  m_shader_program->set_uniform("offsetZ",
    static_cast<int>(cart_topo_idx.get_dimension_offsets()[m_projection[2]]));
  m_shader_program->set_uniform("rangeMinX",
    m_ranges_on_dimensions[0].first  * num_cells_x);
  m_shader_program->set_uniform("rangeMaxX",
    m_ranges_on_dimensions[0].second * num_cells_x);
  m_shader_program->set_uniform("rangeMinY",
    m_ranges_on_dimensions[1].first  * num_cells_y);
  m_shader_program->set_uniform("rangeMaxY",
    m_ranges_on_dimensions[1].second * num_cells_y);
  m_shader_program->set_uniform("rangeMinZ",
    m_ranges_on_dimensions[2].first  * num_cells_z);
  m_shader_program->set_uniform("rangeMaxZ",
    m_ranges_on_dimensions[2].second * num_cells_z);
}


//------------------------------------------------------------------------------
void
renderable_cartesian_topology::render_geometry
()
{
  const auto& cart_topo_idx = mp_cartesian_topology->get_index();
  const auto  num_visible_cuboids =
    cart_topo_idx.get_dimension_sizes()[m_projection[0]] *
    cart_topo_idx.get_dimension_sizes()[m_projection[1]] *
    cart_topo_idx.get_dimension_sizes()[m_projection[2]];
  
  m_data_texture->bind();
  glDrawArraysInstanced(GL_TRIANGLES, 
                        0, 
                        cube_num_vertices, 
                        num_visible_cuboids);
  m_data_texture->unbind();
}


//------------------------------------------------------------------------------
void 
renderable_cartesian_topology::upload
()
{
  m_data_texture      ->bind();
  m_data_vertex_buffer->bind();

  m_vertex_buffer->set_data(
    mp_topology_mapping->size() * sizeof(float),
    mp_topology_mapping->data());

  m_data_vertex_buffer->unbind();
  m_data_texture      ->unbind();

  m_needs_upload = false;
}

} // namespace pvt
