//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/gl/shader.hpp>
#include <pvt/rendering/renderable_cartesian_grid.hpp>
#include <pvt/rendering/shader_strings.hpp>

namespace pvt {

//------------------------------------------------------------------------------
renderable_cartesian_grid::renderable_cartesian_grid(
  const Eigen::Vector3f& size,
  const Eigen::Vector3f& block_size)
  : m_size(size)
  , m_block_size(block_size)
  , m_translation({ 0.0, 0.0, 0.0 })
  , m_color({ 0.5, 0.5, 0.5, 1.0 })
{

}

void renderable_cartesian_grid::on_setup()
{
  vertex_array_shader_pair::setup();

  m_vertex_buffer.emplace();
  m_normal_buffer.emplace();

  upload();

  m_shader_program->attach_shader(vertex_shader  (shader_strings::cartesian_grid_vert));
  m_shader_program->attach_shader(fragment_shader(shader_strings::cartesian_grid_frag));
  m_shader_program->link();

  m_shader_program->bind  ();
  m_vertex_buffer ->bind  ();
  m_shader_program->set_attribute_buffer  ("vertex", 3, GL_FLOAT);
  m_shader_program->enable_attribute_array("vertex");
  m_vertex_buffer ->unbind();
  m_normal_buffer ->bind  ();
  m_shader_program->set_attribute_buffer  ("normal", 3, GL_FLOAT);
  m_shader_program->enable_attribute_array("normal");
  m_normal_buffer ->unbind();

  vertex_array_shader_pair::unbind();
}

void renderable_cartesian_grid::on_render(const camera* p_camera)
{
  vertex_array_shader_pair::pre_render(p_camera);

  if (m_needs_upload)
  {
    upload();
    m_needs_upload = false;
  }

  m_shader_program->set_uniform("translation", m_translation);
  m_shader_program->set_uniform("color"      , m_color      );

  glDrawArrays(GL_LINES, 0, m_draw_count);

  vertex_array_shader_pair::post_render();
}

void renderable_cartesian_grid::upload()
{
  // Fill via m_size, m_block_size.
  std::vector<float> vertices;
  std::vector<float> normals ;

  // Front and back face.
  for (auto z = 0; z <= m_size.z(); z += m_size.z())
  {
    for (auto x = 0; x <= m_size.x(); x += m_block_size.x())
    {
      vertices.insert(vertices.end(), { (float)x, 0       , (float)z, (float)x  , m_size.y(), (float)z});
      normals .insert(normals .end(), { 0, 0, z == 0 ? 1.0f : -1.0f, 0, 0, z == 0 ? 1.0f : -1.0f });
    }
    for (auto y = 0; y <= m_size.y(); y += m_block_size.y())
    {
      vertices.insert(vertices.end(), { 0       , (float)y, (float)z, m_size.x(), (float)y  , (float)z});
      normals .insert(normals .end(), { 0, 0, z == 0 ? 1.0f : -1.0f, 0, 0, z == 0 ? 1.0f : -1.0f });
    }
  }
  // Left and right.
  for (auto x = 0; x <= m_size.x(); x += m_size.x())
  {
    for (auto y = 0; y <= m_size.y(); y += m_block_size.y())
    {
      vertices.insert(vertices.end(), { (float)x, (float)y, 0       , (float)x, (float)y  , m_size.z()});
      normals .insert(normals .end(), { x == 0 ? 1.0f : -1.0f, 0, 0, x == 0 ? 1.0f : -1.0f, 0, 0});
    }
    for (auto z = 0; z <= m_size.z(); z += m_block_size.z())
    {
      vertices.insert(vertices.end(), { (float)x, 0       , (float)z, (float)x, m_size.y(), (float)z  });
      normals .insert(normals .end(), { x == 0 ? 1.0f : -1.0f, 0, 0, x == 0 ? 1.0f : -1.0f, 0, 0});
    }
  }
  // Top and bottom.
  for (auto y = 0; y <= m_size.y(); y += m_size.y())
  {
    for (auto x = 0; x <= m_size.x(); x += m_block_size.x())
    {
      vertices.insert(vertices.end(), { (float)x, (float)y, 0       , (float)x  , (float)y, m_size.z()});
      normals .insert(normals .end(), { 0, y == 0 ? 1.0f : -1.0f, 0, 0, y == 0 ? 1.0f : -1.0f, 0});
    }
    for (auto z = 0; z <= m_size.z(); z += m_block_size.z())
    {
      vertices.insert(vertices.end(), { 0       , (float)y, (float)z, m_size.x(), (float)y, (float)z  });
      normals .insert(normals .end(), { 0, y == 0 ? 1.0f : -1.0f, 0, 0, y == 0 ? 1.0f : -1.0f, 0});
    }
  }
    
  m_vertex_buffer->bind    ();
  m_vertex_buffer->set_data(vertices.size() * sizeof(float), vertices.data());
  m_vertex_buffer->unbind  ();
  m_normal_buffer->bind    ();
  m_normal_buffer->set_data(normals .size() * sizeof(float), normals .data());
  m_normal_buffer->unbind  ();
  m_draw_count = vertices.size() / 2;
}
} // namespace pvt

