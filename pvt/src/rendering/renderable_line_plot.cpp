//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <iostream>

#include <pvt/cartesian_topology.hpp>
#include <pvt/gl/shader.hpp>
#include <pvt/rendering/renderable_line_plot.hpp>
#include <pvt/rendering/shader_strings.hpp>

namespace pvt {

class cartesian_topology_index;

//------------------------------------------------------------------------------
renderable_line_plot::renderable_line_plot
(const cartesian_topology* p_cartesian_topology,
 const data_array<float>*  p_topology_mapping)
: mp_cartesian_topology             (p_cartesian_topology)
, mp_topology_mapping               (p_topology_mapping)
, m_minimum                         (0.0f)
, m_maximum                         (1.0f)
, m_num_axes                        (0)
, m_data_index_factor_for_axis      (1)
, m_data_index_factor_for_next_axis (0)
, m_needs_upload                    (true)
{

}


//------------------------------------------------------------------------------
void
renderable_line_plot::on_setup
()
{
  vertex_array_shader_pair::setup();

  m_vertex_buffer   .emplace();
  m_vertex_id_buffer.emplace();
  m_data_buffer     .emplace();
  m_data_texture    .emplace();

  init_shader      ();
  init_geometry    ();
  init_data_texture();

  vertex_array_shader_pair::unbind();
}


//------------------------------------------------------------------------------
void
renderable_line_plot::on_render
(const camera* p_camera)
{
  if (m_needs_upload)
    upload();

  vertex_array_shader_pair::pre_render(p_camera);

  update_uniforms();
  render_geometry();

  vertex_array_shader_pair::post_render();
}


//------------------------------------------------------------------------------
void
renderable_line_plot::set_dimension_id
(std::size_t dimension_id)
{
  m_axis_positions.clear();
  m_axis_ids      .clear();

  const std::vector<std::size_t>& dimension_sizes =
    mp_cartesian_topology->get_index().get_dimension_sizes();

  m_num_axes = dimension_sizes[dimension_id];

  for (std::size_t i = 0; i < dimension_sizes[dimension_id]; ++i)
  {
    m_axis_positions.push_back(
      2.0f * static_cast<float>(i) / (dimension_sizes[dimension_id] - 1) - 1.0f);
    m_axis_ids.push_back(i);
  }

  m_data_index_factor_for_axis = std::accumulate(
    dimension_sizes.begin() + dimension_id + 1,
    dimension_sizes.end  (),
    1,
    std::multiplies<std::size_t>());

  m_data_index_factor_for_next_axis =
    m_data_index_factor_for_axis * dimension_sizes[dimension_id];

  set_dirty();
}


//------------------------------------------------------------------------------
void
renderable_line_plot::init_shader
()
{
  m_shader_program->attach_shader(vertex_shader(
    shader_strings::line_plot_vert));
  m_shader_program->attach_shader(fragment_shader(
    shader_strings::line_plot_frag));
  m_shader_program->link();
  m_shader_program->bind();
  m_shader_program->set_uniform("dataTextureBuffer", 0);
  m_shader_program->set_uniform("minimum"          , m_minimum);
  m_shader_program->set_uniform("maximum"          , m_maximum);
  m_shader_program->set_uniform("dataIndexFactorForAxis",
                                m_data_index_factor_for_axis);
  m_shader_program->set_uniform("dataIndexFactorForNextAxis",
                                m_data_index_factor_for_next_axis);
}


//------------------------------------------------------------------------------
void
renderable_line_plot::init_geometry
()
{
  vertex_array_shader_pair::bind();

  m_vertex_buffer   ->bind                        ();
  m_shader_program  ->set_attribute_buffer        ("pos", 1, GL_FLOAT);
  m_shader_program  ->enable_attribute_array      ("pos");
  m_vertex_buffer   ->set_data                    (m_num_axes * sizeof(float), 
                                                   m_axis_positions.data());
  m_vertex_buffer   ->unbind                      ();
  
  m_vertex_id_buffer->bind                        ();
  m_shader_program  ->set_attribute_buffer_integer("id", 1, GL_INT, sizeof(int));
  m_shader_program  ->enable_attribute_array      ("id");
  m_vertex_id_buffer->set_data                    (m_num_axes * sizeof(int),
                                                   m_axis_ids.data());
  m_vertex_id_buffer->unbind                      ();

  vertex_array_shader_pair::unbind();
}



//------------------------------------------------------------------------------
void
renderable_line_plot::init_data_texture
()
{
  m_data_texture->bind();
  m_data_buffer ->bind();

  m_data_buffer ->allocate(mp_topology_mapping->size() * sizeof(float));
  m_data_texture->set_buffer(GL_R32F, *m_data_buffer);

  m_data_buffer ->unbind();
  m_data_texture->unbind();
}


//------------------------------------------------------------------------------
void
renderable_line_plot::update_uniforms
()
{
  m_shader_program->set_uniform("minimum", m_minimum);
  m_shader_program->set_uniform("maximum", m_maximum);
  m_shader_program->set_uniform("numAxes"    , m_num_axes);
  m_shader_program->set_uniform("dataIndexFactorForAxis",
                                m_data_index_factor_for_axis);
  m_shader_program->set_uniform("dataIndexFactorForNextAxis",
                                m_data_index_factor_for_next_axis);
}


//------------------------------------------------------------------------------
void
renderable_line_plot::render_geometry
()
{
  glEnable   (GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  m_data_texture->bind();

  glDrawArraysInstanced(GL_LINE_STRIP,
                        0,
                        m_num_axes,
                        mp_topology_mapping->size() / m_num_axes);
  
  m_data_texture->unbind();

  glDisable(GL_BLEND);
}


//------------------------------------------------------------------------------
void renderable_line_plot::upload()
{
  m_data_texture->bind();
  m_data_buffer ->bind();

  m_vertex_buffer->set_data(mp_topology_mapping->size() * sizeof(float),
                            mp_topology_mapping->data());

  m_data_buffer ->unbind();
  m_data_texture->unbind();

  init_geometry();

  m_needs_upload = false;
}

} // namespace pvt
