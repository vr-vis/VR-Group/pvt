//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <iostream>

#include <pvt/rendering/camera.hpp>

namespace pvt {

//#define PV_CAMERA_DEFAULTS_PSOPEN
#define PV_CAMERA_DEFAULTS_SWEEP
//#define PV_CAMERA_DEFAULTS_ZEUS

//------------------------------------------------------------------------------
camera::camera
()
#if defined PV_CAMERA_DEFAULTS_PSOPEN
: m_distance          (71)
, m_lambda            (-0.46)
, m_phi               (0.37)
, m_focal_length      (8)
, m_aspect            (1.2208)
, m_near              (8)
, m_far               (4096)
, m_center_of_interest(-8.05604, -1.79513, -2.81779)
#elif defined PV_CAMERA_DEFAULTS_SWEEP
: m_distance          (391)
, m_lambda            (-0.55)
, m_phi               (0.22)
, m_focal_length      (8)
, m_aspect            (1.22327)
, m_near              (8)
, m_far               (4096)
, m_center_of_interest(-49.9443, -15.4704, -22.9402)
#elif defined PV_CAMERA_DEFAULTS_ZEUS
: m_distance          (77)
, m_lambda            (-0.36)
, m_phi               (0.4)
, m_focal_length      (8)
, m_aspect            (1.22327)
, m_near              (8)
, m_far               (4096)
, m_center_of_interest(-8.38181, -0.723349, -1.8122)
#else
: m_distance          (450.0f)
, m_lambda            (0.0f) //45.0f / 180.0f * 3.1415926f)
, m_phi               (0.0f) //30.f  / 180.0f * 3.1415926f)
, m_focal_length      (8.0f)
, m_aspect            (4.0f / 3.0f)
, m_near              (8.0f)
, m_far               (4096.0f)
, m_center_of_interest(0.0f, 0.0f, 0.0f)
#endif
{

}


//------------------------------------------------------------------------------
Eigen::Matrix4f
camera::get_rotation_matrix
()
const
{
  Eigen::Matrix4f phi;
  Eigen::Matrix4f lambda;
  
  phi <<
  1.0f, 0.0f,        0.0f,       0.0f,
  0.0f, cos(m_phi), -sin(m_phi), 0.0f,
  0.0f, sin(m_phi),  cos(m_phi), 0.0f,
  0.0f, 0.0f,        0.0f,       1.0f;
  
  lambda <<
  cos(m_lambda), 0.0f, -sin(m_lambda), 0.0f,
  0.0f,          1.0f,  0.0f,          0.0f,
  sin(m_lambda), 0.0f,  cos(m_lambda), 0.0f,
  0.0f,          0.0f,  0.0f,          1.0f;
  
  return phi * lambda;
}


//------------------------------------------------------------------------------
Eigen::Matrix4f
camera::get_view_matrix
()
const
{
  Eigen::Matrix4f pos;
  Eigen::Matrix4f dist;
  
  pos <<
    1.0f, 0.0f, 0.0f, m_center_of_interest.x(),
    0.0f, 1.0f, 0.0f, m_center_of_interest.y(),
    0.0f, 0.0f, 1.0f, m_center_of_interest.z(),
    0.0f, 0.0f, 0.0f, 1.0f;
  
  dist <<
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, -m_distance,
    0.0f, 0.0f, 0.0f, 1.0f;
  
  return dist * get_rotation_matrix() * pos;
}


//------------------------------------------------------------------------------
Eigen::Matrix4f
camera::get_projection_matrix
()
const
{
  Eigen::Matrix4f proj;
  proj <<
    m_focal_length / m_aspect, 0.0f, 0.0f, 0.0f,
    0.0f, m_focal_length, 0.0f, 0.0f,
    0.0f, 0.0f, (m_near + m_far) / (m_near - m_far), 
                2.0f * m_near * m_far / (m_near - m_far),
    0.0f, 0.0f, -1.0f, 0.0f; 
  return proj;
}


//------------------------------------------------------------------------------
void
camera::print_info
()
const
{
  std::cout << ": m_distance("     << m_distance     << ")" << std::endl;
  std::cout << ", m_lambda("       << m_lambda       << ")" << std::endl;
  std::cout << ", m_phi("          << m_phi          << ")" << std::endl;
  std::cout << ", m_focal_length(" << m_focal_length << ")" << std::endl;
  std::cout << ", m_aspect("       << m_aspect       << ")" << std::endl;
  std::cout << ", m_near("         << m_near         << ")" << std::endl;
  std::cout << ", m_far("          << m_far          << ")" << std::endl;
  std::cout << ", m_coi("          << m_center_of_interest.x() << ", " << 
                                      m_center_of_interest.y() << ", " << 
                                      m_center_of_interest.z() << ")"  << std::endl;
}

} // namespace pvt
