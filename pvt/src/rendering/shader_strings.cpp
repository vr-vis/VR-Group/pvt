//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/rendering/shader_strings.hpp>


//------------------------------------------------------------------------------
namespace pvt {

namespace shader_strings {

std::string axes_vert = R"(\
#version 330 core

in vec4 vertex;

void main()
{
  gl_Position = vertex;
}
)";
std::string axes_geom = R"(\
#version 330 core

layout(points) in;
layout(triangle_strip, max_vertices = 12) out;

uniform vec3 axisScale = vec3(1.0, 1.0, 1.0);

uniform mat4 view;
uniform mat4 projection;

uniform int lx;
uniform int ly;
uniform int lz;

uniform float lineWidth;
uniform float screenWidthInv;

out vec4 color;

//------------------------------------------------------------------------------
void drawLine
(vec4 origin,
 vec3 axis,
 mat4 scale,
 float length,
 float addLength)
{
  vec4 end = projection * view * (scale * vec4(length * axis, 1.0) + vec4(addLength * axis, 0.0));
  vec2 direction = end.xy / end.w - origin.xy / origin.w;
  vec2 normal = normalize(vec2(-direction.y, direction.x));
  vec4 side = lineWidth * screenWidthInv * vec4(normal, 0.0, 0.0);

  gl_Position = origin - origin.w * side;
  EmitVertex();
  gl_Position = origin + origin.w * side;
  EmitVertex();

  gl_Position = end - end.w * side;
  EmitVertex();
  gl_Position = end + end.w * side;
  EmitVertex();
  EndPrimitive();
}

//------------------------------------------------------------------------------
void main()
{
  mat4 projView = projection * view;

  mat4 scale = mat4(axisScale.x, 0.0, 0.0, 0.0,
                    0.0, axisScale.y, 0.0, 0.0,
                    0.0, 0.0, axisScale.z, 0.0,
                    0.0, 0.0, 0.0, 1.0);

  vec4 origin = projView * vec4(0.0, 0.0, 0.0, 1.0);

  color = vec4(1.0, 0.0, 0.0, 1.0);
  drawLine(origin, vec3(1.0, 0.0, 0.0), scale, lx, 2.0);

  color = vec4(0.0, 1.0, 0.0, 1.0);
  drawLine(origin, vec3(0.0, 1.0, 0.0), scale, ly, 2.0);

  color = vec4(0.0, 0.0, 1.0, 1.0);
  drawLine(origin, vec3(0.0, 0.0, 1.0), scale, lz, 2.0);
}
)";
std::string axes_frag = R"(\
#version 330 core

in vec4 color;

out vec4 fragColor;

void main()
{
  fragColor = color;
}
)";
std::string cartesian_grid_vert = R"(\
#version 330 core
uniform mat4  projection ;
uniform mat4  view       ;
uniform vec3  translation;
in      vec3  vertex     ;
in      vec3  normal     ;
out     float visible    ;
void main()
{
  gl_Position = projection * view * vec4(vertex + translation, 1.0);

  vec3 vertex_normal  = (view * vec4(normal, 0.0)).xyz;
  if(vertex_normal.z >= 0.0)
    visible = 1.0;
  else
    visible = 0.0;
}
)";
std::string cartesian_grid_frag = R"(\
#version 330 core
uniform vec4  color     ;
in      float visible   ;
out     vec4  frag_color;
void main()
{
  if(visible == 0.0)
    discard;
  else
    frag_color = color;
}
)";
std::string color_treemap_vert = R"(\
#version 400

in vec2 vertex;
in vec3 color;

out vec3 vertColor;

void main()
{
  vertColor   = color;
  gl_Position = vec4((vertex - 0.5) * 2.0, 0.0, 1.0);
}
)";
std::string color_treemap_frag = R"(\
#version 400

in vec3 vertColor;

out vec4 fragColor;

void main()
{
  fragColor = vec4(vertColor, 1.0);
}
)";                                        
std::string cover_vert = R"(\
#version 330 core
in vec3 pos;
uniform float value = 1.0;
uniform bool isMax = false;
uniform bool isVertical = false;

void main()
{
  if (isMax)
  {
    gl_Position = vec4(isVertical ? (2.0 * pos.x - 1.0) : (2.0 * ((1.0 - value) * pos.x + value) - 1.0),
                       isVertical ? (2.0 * ((1.0 - value) * pos.y + value) - 1.0) : (2.0 * pos.y - 1.0),
                       pos.z,
                       1.0);
  }
  else
  {
    gl_Position = vec4(isVertical ? (2.0 * pos.x - 1.0) : (2.0 * value * pos.x - 1.0),
                       isVertical ? (2.0 * value * pos.y - 1.0) : 2.0 * pos.y - 1.0,
                       pos.z,
                       1.0);
  }
}
)";
std::string cover_frag = R"(\
#version 330 core
out vec4 fragColor;

void main()
{
  fragColor = 1.0 / 255.0 * vec4(128.0, 128.0, 128.0, 127.0);
}
)";                                       
std::string cushion_treemap_vert = R"(\
#version 400

uniform mat4 projection;

in vec2 vertex;
in vec4 color;
in vec4 surfaceParams;

out vec2 vertPosition;
out vec4 vertColor;
out vec4 vertSurfaceParams;

void main()
{
  gl_Position  		= projection * vec4(vertex, 0.0, 1.0);
  vertPosition 		= vertex;
  vertColor 		= color;
  vertSurfaceParams = surfaceParams;
}
)";
std::string cushion_treemap_frag = R"(\
#version 400

uniform float ambientCoef;
uniform float diffuseCoef;
uniform vec3 lightPosition;

in vec2 vertPosition;
in vec4 vertColor;
in vec4 vertSurfaceParams;

out vec4 fragColor;

void main()
{
  float nx 	 = - (2 * vertSurfaceParams.y * vertPosition.x + vertSurfaceParams.x);
  float ny 	 = - (2 * vertSurfaceParams.w * vertPosition.y + vertSurfaceParams.z);
  float cosa = (nx * lightPosition.x + 
          ny * lightPosition.y +
               lightPosition.z) /
          sqrt(nx * nx + ny * ny + 1.0);
  float shade = ambientCoef + max(0.0, diffuseCoef * cosa);
  fragColor   = vertColor * shade;
}
)";                                       
std::string data_slider_line_vert = R"(\
#version 330 core

in vec4 vertex;

void main()
{
  gl_Position = vertex;
}
)";
std::string data_slider_line_horizontal_geom = R"(\
#version 330 core

layout(points) in;
layout(triangle_strip, max_vertices = 16) out;

uniform float maxIndexInv = 1.0;
uniform float maxValue = 1.0;
uniform float minValue = 0.0;

uniform vec4 minColor  = vec4(0.0 / 255.0, 128.0 / 255.0, 64.0 / 255.0, 1.0);
uniform vec4 maxColor  = vec4(128.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0, 1.0);
uniform vec4 meanColor = vec4(128.0 / 255.0, 128.0 / 255.0, 0.0 / 255.0, 1.0);

uniform vec2 pixelSize = vec2(1.0 / 100.0, 1.0 / 16.0);

out vec4 color;

void main()
{
  float maxValueInv = 1.0 / (maxValue - minValue);

  vec4 vertex = gl_in[0].gl_Position;

  float scaleX = 2.0 * maxIndexInv;
  float scaleY = 2.0 * maxValueInv;


  float x0 = scaleX * (vertex.x + 0.0) - 1.0;
  float x1 = scaleX * (vertex.x + 1.0) - 1.0;

  float minVal  = scaleY * (vertex.y - minValue) - 1.0;
  float maxVal  = scaleY * (vertex.z - minValue) - 1.0;
  float meanVal = scaleY * (vertex.w - minValue) - 1.0;

  color = vec4(0.9, 0.85, 0.85, 1.0);
  gl_Position = vec4(x0, maxVal-pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x1, maxVal-pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x0, -1.0, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x1, -1.0, 0.0, 1.0);
  EmitVertex();
  EndPrimitive();

  color = minColor;
  gl_Position = vec4(x0, minVal-pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x1, minVal-pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x0, minVal+pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x1, minVal+pixelSize.y, 0.0, 1.0);
  EmitVertex();
  EndPrimitive();

  color = meanColor;
  gl_Position = vec4(x0, meanVal-pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x1, meanVal-pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x0, meanVal+pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x1, meanVal+pixelSize.y, 0.0, 1.0);
  EmitVertex();
  EndPrimitive();

  color = maxColor;
  gl_Position = vec4(x0, maxVal-pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x1, maxVal-pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x0, maxVal+pixelSize.y, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x1, maxVal+pixelSize.y, 0.0, 1.0);
  EmitVertex();
  EndPrimitive();
}
)";
std::string data_slider_line_vertical_geom = R"(\
#version 330 core

layout(lines) in;
layout(triangle_strip, max_vertices = 12) out;

uniform float maxIndexInv = 1.0;
uniform float maxValue = 1.0;
uniform float minValue = 1.0;

uniform vec4 minColor  = vec4(0.0 / 255.0, 128.0 / 255.0, 64.0 / 255.0, 1.0);
uniform vec4 maxColor  = vec4(128.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0, 1.0);
uniform vec4 meanColor = vec4(128.0 / 255.0, 128.0 / 255.0, 0.0 / 255.0, 1.0);

uniform vec2 pixelSize = vec2(1.0 / 100.0, 1.0 / 16.0);

out vec4 color;

void main()
{
  float maxValueInv = 1.0 / (maxValue - minValue);
  vec4 vertex0 = gl_in[0].gl_Position;
  vec4 vertex1 = gl_in[1].gl_Position;

  float scaleX = 2.0 * maxIndexInv;
  float scaleY = 2.0 * maxValueInv;

  float x = scaleX * (vertex1.x) - 1.0;

  float minVal0  = scaleY * (vertex0.y - minValue) - 1.0;
  float minVal1  = scaleY * (vertex1.y - minValue) - 1.0;

  float maxVal0  = scaleY * (vertex0.z - minValue) - 1.0;
  float maxVal1  = scaleY * (vertex1.z - minValue) - 1.0;

  float meanVal0 = scaleY * (vertex0.w - minValue) - 1.0;
  float meanVal1 = scaleY * (vertex1.w - minValue) - 1.0;

  color = minColor;
  gl_Position = vec4(x-pixelSize.x, minVal0, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x-pixelSize.x, minVal1, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x+pixelSize.x, minVal0, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x+pixelSize.x, minVal1, 0.0, 1.0);
  EmitVertex();
  EndPrimitive();

  color = meanColor;
  gl_Position = vec4(x-pixelSize.x, meanVal0, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x-pixelSize.x, meanVal1, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x+pixelSize.x, meanVal0, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x+pixelSize.x, meanVal1, 0.0, 1.0);
  EmitVertex();
  EndPrimitive();

  color = maxColor;
  gl_Position = vec4(x-pixelSize.x, maxVal0, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x-pixelSize.x, maxVal1, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x+pixelSize.x, maxVal0, 0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(x+pixelSize.x, maxVal1, 0.0, 1.0);
  EmitVertex();
  EndPrimitive();
}
)";
std::string data_slider_line_frag = R"(\
#version 330 core

in vec4 color;

out vec4 fragColor;

void main()
{
  fragColor = color;
}
)";
std::string data_slider_vert = R"(\
#version 330 core

in vec4 vertex;

void main()
{
  gl_Position = vertex;
}
)";
std::string data_slider_geom = R"(\
#version 330 core

layout(points) in;
layout (triangle_strip, max_vertices=12) out;

uniform float maxIndexInv = 1.0;
uniform float maxValueInv = 1.0;
uniform bool isVertical = false;

void DrawQuad( float x1, float x2, float y1, float y2)
{
  gl_Position = vec4(x1, y1, 0.0, 1.0);
  EmitVertex();

  gl_Position = vec4(x2, y1, 0.0, 1.0);
  EmitVertex();

  gl_Position = vec4(x1, y2, 0.0, 1.0);
  EmitVertex();

  gl_Position = vec4(x2, y2, 0.0, 1.0);
  EmitVertex();

  EndPrimitive();
}

void main()
{
  vec4 vertex = gl_in[0].gl_Position;

  float scaleX = 2.0 * maxIndexInv;
  float scaleY = 2.0 * maxValueInv;


  float x0 = scaleX * (vertex.x + 0.0) - 1.0;
  float x1 = scaleX * (vertex.x + 1.0) - 1.0;

  float minVal  = scaleY * vertex.y - 1.0;
  float maxVal  = scaleY * vertex.z - 1.0;
  float meanVal = scaleY * vertex.w - 1.0;

  if (!isVertical)
  {
    DrawQuad(x0, x1, -1.0, maxVal);
    DrawQuad(x0, x1, -1.0, meanVal);
    DrawQuad(x0, x1, -1.0, minVal);
  }
  else
  {
    DrawQuad(-1.0, minVal,  x0, x1);
  }
}
)";
std::string data_slider_frag = R"(\
#version 330 core
uniform int red = 255;
uniform int green = 255;
uniform int blue = 255;
uniform int alpha = 255;

uniform vec4 color;

out vec4 fragColor;

void main()
{
  fragColor = color;
  //fragColor = vec4(1.0, 0.0, 0.0, 1.0);
}
)";
std::string line_plot_axes_vert = R"(\
#version 410 core

in float pos;

uniform int numAxes;

//------------------------------------------------------------------------------
void main()
{
  gl_Position = vec4(2.0 * (gl_InstanceID / float(numAxes - 1)) - 1.0,
                     pos,
                     0.0,
                     1.0);
}
)";
std::string line_plot_axes_frag = R"(\
#version 410 core

layout(location = 0) out vec4 outputColor;

//------------------------------------------------------------------------------
void main()
{
  outputColor = vec4(vec3(0.25), 1.0);
}
)";
std::string line_plot_vert = R"(\
#version 410 core

in float pos;
in int id;

uniform samplerBuffer dataTextureBuffer;
uniform float minimum = 0.0;
uniform float maximum = 1.0;
uniform int numAxes = 8;
uniform int dataIndexFactorForAxis = 1;
uniform int dataIndexFactorForNextAxis = 0;

out vec4 color;

//------------------------------------------------------------------------------
ivec3 getCategoryIds()
{
  return ivec3(id - 1 + int(id - 1 < 0) * numAxes,
               id,
               id + 1 - int(id + 1 > (numAxes - 1)) * numAxes);
}


//------------------------------------------------------------------------------
ivec3 getDataIndices(ivec3 categoryIds)
{
  int highIndex = gl_InstanceID / dataIndexFactorForAxis;
  int lowIndex = gl_InstanceID % dataIndexFactorForAxis;
  int indexOffset = highIndex * dataIndexFactorForNextAxis + lowIndex;

  return ivec3(categoryIds * dataIndexFactorForAxis + indexOffset);
}


//------------------------------------------------------------------------------
vec3 determineLoads(ivec3 dataIndices)
{
  vec3 rawLoads = vec3(texelFetch(dataTextureBuffer, dataIndices.r).r,
                       texelFetch(dataTextureBuffer, dataIndices.g).r,
                       texelFetch(dataTextureBuffer, dataIndices.b).r);

  return 2.0 * (rawLoads - minimum) / (maximum - minimum) - 1.0;
}





int computeIdByDelta(vec3 loads)
{
  float deltaLeft = loads.g - loads.r;
  float deltaRight = loads.g - loads.b;

  return clamp(
    int((deltaLeft > 0.2 && deltaRight >= -0.2) ||
        (deltaLeft >= -0.2 && deltaRight > 0.2)) * 1 +
    int((deltaLeft < -0.2 && deltaRight <= 0.2) ||
        (deltaLeft <= 0.2 && deltaRight < -0.2)) * 1,
    0,
    1);
}


//------------------------------------------------------------------------------
vec4 computeColor(vec3 loads)
{
  int colorId = computeIdByDelta(loads);

  float grayMap[] = float[](0.5, 0.0);
  float alphaMap[] = float[](0.5, 0.8);
  float variationMap[] = float[](0.5, 0.5);

  float grayVariation = float(gl_InstanceID % 50) / 50.0;

  float grayValue = clamp(grayMap[colorId] +
                          variationMap[colorId] * grayVariation,
                          0, 2);
  float alphaValue = alphaMap[colorId];

  return vec4(vec3(grayValue), alphaValue);
}


//------------------------------------------------------------------------------
void main()
{
  ivec3 categoryIds = getCategoryIds();
  ivec3 dataIndices = getDataIndices(categoryIds);
  vec3 loads = determineLoads(dataIndices);

  color = computeColor(loads);

  float zValueMap[] = float[](0.0, -0.1);
  int zValueId = computeIdByDelta(loads);

  gl_Position = vec4(pos, loads.g, zValueMap[zValueId], 1.0);
}
)";
std::string line_plot_frag = R"(\
#version 410 core

in vec4 color;

layout(location = 0) out vec4 outputColor;

//------------------------------------------------------------------------------
void main()
{
  outputColor = color;
}
)";
std::string line_plot_2_vert = R"(\
#version 410 core

in float pos;
in int id;

uniform samplerBuffer dataTextureBuffer;
uniform float minimum = 0.0;
uniform float maximum = 1.0;
uniform int numAxes = 0;
uniform int dataIndexFactorForAxis = 0;
uniform int dataIndexFactorForNextAxis = 0;

out vec4 color;

//------------------------------------------------------------------------------
int getCategoryId()
{
  return id;
}


//------------------------------------------------------------------------------
int getDataIndex(int categoryId)
{
  int highIndex = gl_InstanceID / dataIndexFactorForAxis;
  int lowIndex = gl_InstanceID % dataIndexFactorForAxis;
  int indexOffset = highIndex * dataIndexFactorForNextAxis + lowIndex;

  return categoryId * dataIndexFactorForAxis + indexOffset;
}


//------------------------------------------------------------------------------
float determineLoad(int dataIndex)
{
  float rawLoad = texelFetch(dataTextureBuffer, dataIndex).r;

  return (rawLoad - minimum) / (maximum - minimum) - 0.5;
}


//------------------------------------------------------------------------------
vec4 computeColor(float load)
{
  float grayVariation = float(gl_InstanceID % 50) / 50.0;
  float grayValue = clamp(0.5 + (0.5 * grayVariation - 0.25), 0.0, 1.0);
  return vec4(vec3(grayValue), 0.5);
}


//------------------------------------------------------------------------------
void main()
{
  int categoryId = getCategoryId();
  int dataIndex = getDataIndex(categoryId);
  float load = determineLoad(dataIndex);

  color = computeColor(load);
  gl_Position = vec4(pos, load, 0, 1.0);
}
)";
std::string line_plot_2_frag = R"(\
#version 410 core

in vec4 color;

layout(location = 0) out vec4 outputColor;

//------------------------------------------------------------------------------
void main()
{
  outputColor = color;
  outputColor = vec4(vec3(0.0), 1.0);
}
)";
std::string mesh_id_vert = R"(\
#version 330 core

uniform mat4 view;
uniform mat4 projection;

layout(location = 0)in vec3 vertex;

void main()
{
  gl_Position = projection * view * vec4(vertex, 1.0);
}
)";
std::string mesh_id_frag = R"(\
#version 330 core

uniform int id;

layout(location = 0)out int fragId;

void main()
{
  fragId = id;
}
)";                                       
std::string mesh_vert = R"(\
#version 330 core

uniform mat4 view;
uniform mat4 projection;

layout(location = 0)in vec3 vertex;
layout(location = 1)in vec3 normal;

out vec3 vertexNormal;
out vec3 vertexLight;
out vec3 vertexReflection;

void main()
{
  gl_Position = projection * view * vec4(vertex, 1.0);
  
  vertexNormal = (view * vec4(normal, 0)).xyz;
  vertexLight  = normalize((view * vec4(0, 1, 0, 0)).xyz);

  float ndotl = dot(vertexNormal, vertexLight);

  vertexReflection = 2.0 * ndotl * vertexNormal - vertexLight;
}
)";
std::string mesh_frag = R"(\
#version 330 core

uniform bool  selected;
uniform float severity;

in vec3 vertexNormal;
in vec3 vertexLight;
in vec3 vertexReflection;

layout(location = 0)out vec4 fragColor;

void main()
{
  float diffuse = dot(vertexNormal, vertexLight);
  float signDiffuse = sign(diffuse);
  diffuse *= signDiffuse;

  float specular = vertexReflection.z;
  specular *= sign(specular);
  specular = pow(clamp(specular, 0, 1), 5);

  vec3 intensity = vec3(0.1 + 0.5 * clamp(severity, 0.0, 1.0) * diffuse + 0.4 * specular);
  vec3 selectedHighlight = vec3(1.0,
                                1.0 - 0.2 * float(selected),
                                1.0 - 0.6 * float(selected));
  fragColor = vec4(selectedHighlight * intensity, 1.0);
}
)";                                       
std::string node_id_vert = R"(\
#version 400

uniform mat4 projection;

in vec2 vertex;
in uint id;

flat out uint vertId;

void main()
{
  gl_Position = projection * vec4(vertex, 0.0, 1.0);
  vertId = id;
}
)";
std::string node_id_frag = R"(\
#version 400

flat in uint vertId;

layout(location = 0)out uint fragId;

void main()
{
  fragId = vertId;
}
)";   
std::string parallel_coord_axis_vert = R"(\
#version 330 core

layout(location = 0)in vec3 position;

void main()
{
  gl_Position = vec4(position, 1.0);
}
)";
std::string parallel_coord_axis_geom = R"(\
#version 330 core

layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;

uniform vec2 inversePixelSize = vec2(1.0, 1.0);

void main()
{
  vec4 v0 = gl_in[0].gl_Position;
  vec4 v1 = gl_in[1].gl_Position;
  vec2 deltaNorm = 3.0 * normalize(v1.xy - v0.xy);

  gl_Position = vec4(v0.x - deltaNorm.y * inversePixelSize.x,
                     v0.y + deltaNorm.x * inversePixelSize.y,
                     v0.z,
                     v0.w);
  EmitVertex();
  gl_Position = vec4(v0.x + deltaNorm.y * inversePixelSize.x,
                     v0.y - deltaNorm.x * inversePixelSize.y,
                     v0.z,
                     v0.w);
  EmitVertex();
  gl_Position = vec4(v1.x - deltaNorm.y * inversePixelSize.x,
                     v1.y + deltaNorm.x * inversePixelSize.y,
                     v1.z,
                     v1.w);
  EmitVertex();
  gl_Position = vec4(v1.x + deltaNorm.y * inversePixelSize.x,
                     v1.y - deltaNorm.x * inversePixelSize.y,
                     v1.z,
                     v1.w);
  EmitVertex();
  EndPrimitive();
}
)";
std::string parallel_coord_axis_frag = R"(\
#version 330 core

out vec4 fragColor;

void main()
{
  fragColor = vec4(0.75, 0.75, 0.75, 1.0);
}
)";
std::string parallel_coord_vert = R"(\
#version 330 core

layout(location = 0)in vec3 position;

void main()
{
  gl_Position = vec4(position, 1.0);
}
)";
std::string parallel_coord_geom = R"(\
#version 330 core

layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;

uniform vec2 inversePixelSize = vec2(1.0, 1.0);

void main()
{
  vec4 v0 = gl_in[0].gl_Position;
  vec4 v1 = gl_in[1].gl_Position;
  vec2 deltaNorm = normalize(v1.xy - v0.xy);

  gl_Position = vec4(v0.x - deltaNorm.y * inversePixelSize.x,
                     v0.y + deltaNorm.x * inversePixelSize.y,
                     v0.z,
                     v0.w);
  EmitVertex();
  gl_Position = vec4(v0.x + deltaNorm.y * inversePixelSize.x,
                     v0.y - deltaNorm.x * inversePixelSize.y,
                     v0.z,
                     v0.w);
  EmitVertex();
  gl_Position = vec4(v1.x - deltaNorm.y * inversePixelSize.x,
                     v1.y + deltaNorm.x * inversePixelSize.y,
                     v1.z,
                     v1.w);
  EmitVertex();
  gl_Position = vec4(v1.x + deltaNorm.y * inversePixelSize.x,
                     v1.y - deltaNorm.x * inversePixelSize.y,
                     v1.z,
                     v1.w);
  EmitVertex();
  EndPrimitive();
}
)";
std::string parallel_coord_frag = R"(\
#version 330 core

uniform bool selected;

out vec4 fragColor;

void main()
{
  fragColor = vec4(1.0 * float(selected),
                   0.5 * float(selected),
                   0.0 * float(selected),
                   1.0);
}
)";                                        
std::string cartesian_topology_vert = R"(\
#version 410 core

in vec3 pos;
in vec3 normal;
in vec2 texCoord;

uniform mat4 view;
uniform mat4 projection;

uniform vec3 axisScale = vec3(1.0, 1.0, 1.0);

uniform float rangeMinX;
uniform float rangeMaxX;
uniform float rangeMinY;
uniform float rangeMaxY;
uniform float rangeMinZ;
uniform float rangeMaxZ;
uniform float filteredMinimum = 0.0;
uniform float filteredMaximum = 1.0;
uniform float minimum = 0.0;
uniform float maximum = 1.0;

uniform int numCellsX;
uniform int numCellsY;
uniform int numCellsZ;

uniform int baseOffset = 0;

uniform int offsetX;
uniform int offsetY;
uniform int offsetZ;

uniform samplerBuffer dataTextureBuffer;

out vec3 normalV;
out float loadV;
flat out int index;

void main()
{
  int x = ( gl_InstanceID/(numCellsZ*numCellsY) )%numCellsX;
  int y = ( gl_InstanceID/(numCellsZ) )%numCellsY;
  int z = ( gl_InstanceID )%numCellsZ;

  vec3 instancePos = vec3(
    x,
    y,
    z
  );

  vec3 localPos = instancePos + 0.5 * (pos + 1.0);

  index = baseOffset + x*offsetX +  y*offsetY +  z*offsetZ;
  float load = texelFetch(dataTextureBuffer, index).r;//index).r;
  float normedLoad = (load - minimum) / (maximum - minimum);

  bool insideX = x >= rangeMinX && x <= rangeMaxX;
  bool insideY = y >= rangeMinY && y <= rangeMaxY;
  bool insideZ = z >= rangeMinZ && z <= rangeMaxZ;
  bool insideV = normedLoad >= filteredMinimum && normedLoad <= filteredMaximum;
  bool inside = insideX && insideY && insideZ && insideV;

  mat4 scale = mat4(axisScale.x, 0.0, 0.0, 0.0,
                    0.0, axisScale.y, 0.0, 0.0,
                    0.0, 0.0, axisScale.z, 0.0,
                    0.0, 0.0, 0.0, 1.0);
  normalV = normalize(view * vec4(normal, 0.0)).xyz;
  gl_Position = projection * view * scale * vec4(float(inside) * localPos, 1.0);
  loadV = normedLoad;
}
)";
std::string cartesian_topology_frag = R"(\
#version 410 core

in vec3 normalV;
in float loadV;
flat in int   index;

uniform bool writeIndex;

uniform int numCellsX;
uniform int numCellsY;
uniform int numCellsZ;

layout(location = 0) out vec4 outputColor;
layout(location = 1) out vec4 colorPicker;

void main()
{
  float loadLower = 2.0 * loadV;
  float loadUpper = 2.0 * (loadV - 0.5);

  vec3 red = vec3(1.0, 0.0, 0.0);
  vec3 yellow = vec3(1.0, 1.0, 0.0);
  vec3 green = vec3(0.0, 1.0, 0.0);

  vec3 load = float(loadV > 0.5) * (loadUpper * red + (1.0 - loadUpper) * yellow) +
      float(loadV <= 0.5) * (loadLower * yellow + (1.0 - loadLower) * green);

  const vec3 light1 = normalize(vec3(0,0, 1));
  const vec3 light2 = normalize(vec3(1,0, 0));
  const vec3 light3 = normalize(vec3(0,1, 0));

  float diff1 = clamp(dot(normalV, light1),0.0, 1.0);
  float diff2 = clamp(dot(normalV, light2),0.0, 1.0);
  float diff3 = clamp(dot(normalV, light3),0.0, 1.0);

  float intensity = clamp(0.667 * diff1 + 0.1667 * (abs(diff2) + abs(diff3)) + 0.1667, 0.0, 1.0);

  outputColor = intensity * vec4(load, 1.0);

  int r = ((index+1) & 0x000000FF) >>  0;
  int g = ((index+1) & 0x0000FF00) >>  8;
  int b = ((index+1) & 0x00FF0000) >> 16;
  colorPicker = vec4( r/255.0, g/255.0, b/255.0,	1.0);
}
)";

}

} // namespace pvt

