//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <algorithm>

#include <pvt/gl/opengl.hpp>

#include <pvt/rendering/scene.hpp>

namespace pvt {

//------------------------------------------------------------------------------
void
scene::setup
()
{
  opengl::init();
  for (auto& renderable : m_renderables)
    renderable->on_setup();
}


//------------------------------------------------------------------------------
void
scene::render
(const camera* p_camera)
{
  for (auto& renderable : m_renderables)
    renderable->on_render(p_camera);
}


//------------------------------------------------------------------------------
void scene::add_renderable(renderable* p_renderable)
{
  m_renderables.emplace_back(std::unique_ptr<renderable>(p_renderable));
}


//------------------------------------------------------------------------------
void
scene::remove_renderable
(renderable* p_renderable)
{
  m_renderables.erase(std::remove_if(m_renderables.begin(), 
                                     m_renderables.end  (),
                                     [&](std::unique_ptr<renderable>& p)
                                     {
                                       return p.get() == p_renderable;
                                     }), 
                      m_renderables.end());
}

} // namespace pvt
