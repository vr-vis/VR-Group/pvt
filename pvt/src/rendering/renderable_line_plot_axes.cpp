//------------------------------------------------------------------------------
// pvt performance visualization toolkit
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <pvt/gl/shader.hpp>
#include <pvt/rendering/renderable_line_plot_axes.hpp>
#include <pvt/rendering/shader_strings.hpp>

namespace pvt {

//------------------------------------------------------------------------------
renderable_line_plot_axes::renderable_line_plot_axes
()
: m_num_axes(0)
{

}


//------------------------------------------------------------------------------
void
renderable_line_plot_axes::on_setup
()
{
  vertex_array_shader_pair::setup();

  const float vertices[] = { -1.0, 1.0 };

  m_vertexBuffer. emplace ();
  m_vertexBuffer->bind    ();
  m_vertexBuffer->set_data(2 * sizeof(float), vertices);
  
  m_shader_program->attach_shader(vertex_shader(
    shader_strings::line_plot_axes_vert));
  m_shader_program->attach_shader(fragment_shader(
    shader_strings::line_plot_axes_frag));
  m_shader_program->link();
  m_shader_program->bind();
  m_shader_program->set_attribute_buffer  ("pos", 1, GL_FLOAT);
  m_shader_program->enable_attribute_array("pos");

  m_vertexBuffer->unbind();

  vertex_array_shader_pair::unbind();
}


//------------------------------------------------------------------------------
void
renderable_line_plot_axes::on_render
(const camera* p_camera)
{
  vertex_array_shader_pair::pre_render(p_camera);

  m_shader_program->set_uniform("numAxes", m_num_axes);

  glDrawArraysInstanced(GL_LINE_STRIP, 0, 2, m_num_axes);

  vertex_array_shader_pair::post_render();
}

} // namespace pvt
