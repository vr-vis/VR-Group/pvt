#-------------------------------------------------------------------------------
# pvt performance visualization toolkit
#
# Copyright (c) 2014-2016 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualisation Group.
#-------------------------------------------------------------------------------
#                                 License
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# In the future, we may decide to add a commercial license
# at our own discretion without further notice.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

set(SourceFiles

  include/pvt/api.hpp
  include/pvt/cartesian_topology.hpp
  include/pvt/cartesian_topology_index.hpp
  include/pvt/cnode.hpp
  include/pvt/correlated_severity_view.hpp
  include/pvt/correlation_computer.hpp
  include/pvt/data_array.hpp
  include/pvt/dft.hpp
  include/pvt/dft_inverse.hpp
  include/pvt/directed_variance.hpp
  include/pvt/export_flags.hpp
  include/pvt/fft.hpp
  include/pvt/fft_helper.hpp
  include/pvt/fft_statistics.hpp
  include/pvt/geometry.hpp
  include/pvt/hierarchy.hpp
  include/pvt/metric.hpp
  include/pvt/name_id_pair.hpp
  include/pvt/optional.hpp
  include/pvt/perf_data.hpp
  include/pvt/perf_data_cache_cnode.hpp
  include/pvt/perf_data_cache_metric.hpp
  include/pvt/severity_view.hpp
  include/pvt/suitable_3d_view_computer.hpp
  include/pvt/sysnode.hpp
  include/pvt/system_geometry.hpp
  
  include/pvt/gl/buffer.hpp
  include/pvt/gl/framebuffer.hpp
  include/pvt/gl/opengl.hpp
  include/pvt/gl/shader.hpp
  include/pvt/gl/shader_program.hpp
  include/pvt/gl/texture.hpp
  include/pvt/gl/vertex_array.hpp
  
  include/pvt/io/perf_data_file.hpp
  
  include/pvt/rendering/camera.hpp
  include/pvt/rendering/clickable.hpp
  include/pvt/rendering/clickable_scene.hpp
  include/pvt/rendering/cube_vertices.hpp
  include/pvt/rendering/renderable.hpp
  include/pvt/rendering/renderable_axes.hpp
  include/pvt/rendering/renderable_cartesian_grid.hpp
  include/pvt/rendering/renderable_cartesian_topology.hpp
  include/pvt/rendering/renderable_geometry.hpp
  include/pvt/rendering/renderable_line_plot.hpp
  include/pvt/rendering/renderable_line_plot_axes.hpp
  include/pvt/rendering/renderable_parallel_coordinates_plot.hpp
  include/pvt/rendering/scene.hpp
  include/pvt/rendering/shader_strings.hpp
  include/pvt/rendering/treemap.hpp
  include/pvt/rendering/treemap_interaction_policy_basic.hpp
  include/pvt/rendering/treemap_shading_policy_cushion.hpp
  include/pvt/rendering/treemap_tessellation_policy_slice_and_dice.hpp
  include/pvt/rendering/treemap_tessellation_policy_squarified.hpp
  include/pvt/rendering/vertex_array_shader_pair.hpp

  src/cartesian_topology.cpp
  src/correlation_computer.cpp
  src/dft.cpp
  src/dft_inverse.cpp
  src/directed_variance.cpp
  src/fft.cpp
  src/fft_statistics.cpp
  src/nop.cpp
  src/perf_data.cpp
  src/perf_data_cache_cnode.cpp
  src/perf_data_cache_metric.cpp
  src/suitable_3d_view_computer.cpp
  
  src/gl/shader_program.cpp
  src/gl/vertex_array.cpp
  
  src/io/perf_data_file.cpp
  src/io/perf_data_file_cube.cpp
  src/io/perf_data_file_cube.hpp
  src/io/perf_data_file_cube_cartesian_topology_loader.cpp
  src/io/perf_data_file_cube_cartesian_topology_loader.hpp
  src/io/perf_data_file_cube_cnode_loader.cpp
  src/io/perf_data_file_cube_cnode_loader.hpp
  src/io/perf_data_file_cube_metric_loader.cpp
  src/io/perf_data_file_cube_metric_loader.hpp
  src/io/perf_data_file_cube_sysnode_loader.cpp
  src/io/perf_data_file_cube_sysnode_loader.hpp
  src/io/perf_data_file_native.cpp
  src/io/perf_data_file_native.hpp
  src/io/perf_data_file_native_cartesian_topology_loader.cpp
  src/io/perf_data_file_native_cartesian_topology_loader.hpp
  src/io/perf_data_file_native_cnode_loader.cpp
  src/io/perf_data_file_native_cnode_loader.hpp
  src/io/perf_data_file_native_intermediate.cpp
  src/io/perf_data_file_native_intermediate.hpp
  src/io/perf_data_file_native_metric_loader.cpp
  src/io/perf_data_file_native_metric_loader.hpp
  src/io/perf_data_file_native_sysnode_loader.cpp
  src/io/perf_data_file_native_sysnode_loader.hpp
  src/io/perf_data_file_native_system_geometry_loader.cpp
  src/io/perf_data_file_native_system_geometry_loader.hpp
  src/io/perf_data_file_test.cpp
  src/io/perf_data_file_test.hpp
  
  src/rendering/camera.cpp
  src/rendering/clickable_scene.cpp
  src/rendering/renderable_axes.cpp
  src/rendering/renderable_cartesian_grid.cpp
  src/rendering/renderable_cartesian_topology.cpp
  src/rendering/renderable_geometry.cpp
  src/rendering/renderable_line_plot.cpp
  src/rendering/renderable_line_plot_axes.cpp
  src/rendering/renderable_parallel_coordinates_plot.cpp
  src/rendering/scene.cpp
  src/rendering/shader_strings.cpp
  src/rendering/treemap_interaction_policy_basic.cpp
  src/rendering/treemap_shading_policy_cushion.cpp
  src/rendering/treemap_tessellation_policy_slice_and_dice.cpp
  src/rendering/treemap_tessellation_policy_squarified.cpp
  src/rendering/vertex_array_shader_pair.cpp

)
