#-------------------------------------------------------------------------------
# pvt_tests performance visualization toolkit tests
#
# Copyright (c) 2014-2016 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualisation Group.
#-------------------------------------------------------------------------------
#                                 License
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# In the future, we may decide to add a commercial license
# at our own discretion without further notice.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

set(SourceFiles
  ./src/cartesian_topology_index_test.cpp
  ./src/cartesian_topology_test.cpp
  ./src/correlation_computer_test.cpp
  ./src/data_array_test.cpp
  ./src/directed_variance_test.cpp
  ./src/fft_test.cpp
  ./src/perf_lib_tests.cpp
  ./src/severity_view_test.cpp
  ./src/suitable_3d_view_computer_test.cpp
)
