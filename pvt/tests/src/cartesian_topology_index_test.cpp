//------------------------------------------------------------------------------
// pvt_tests performance visualization toolkit tests
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <vector>

#include <catch/catch.hpp>

#include <pvt/cartesian_topology_index.hpp>

//------------------------------------------------------------------------------
TEST_CASE("CartesianTopologyIndex1D", "[CartesianTopologyIndex1D]")
{
  pvt::cartesian_topology_index cart_topo_idx(std::vector<size_t>{4});
  REQUIRE(cart_topo_idx.get_num_dimensions() == 1);
  
  const std::vector<std::size_t>& dim_sizes = cart_topo_idx.get_dimension_sizes();
  CHECK(dim_sizes[0] == 4);
  
  REQUIRE(cart_topo_idx.get_num_positions() == 4);
  
  const std::vector<std::size_t>& dim_offsets = 
    cart_topo_idx.get_dimension_offsets();
  CHECK(dim_offsets[0] == 1);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{0}) == 0);
  CHECK(cart_topo_idx.get_coordinate(0)[0] == 0);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{1}) == 1);
  CHECK(cart_topo_idx.get_coordinate(1)[0] == 1);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{2}) == 2);
  CHECK(cart_topo_idx.get_coordinate(2)[0] == 2);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{3}) == 3);
  CHECK(cart_topo_idx.get_coordinate(3)[0] == 3);
}


//------------------------------------------------------------------------------
TEST_CASE("CartesianTopologyIndex2D", "[CartesianTopologyIndex2D]")
{
  pvt::cartesian_topology_index cart_topo_idx(std::vector<size_t>{4, 6});
  REQUIRE(cart_topo_idx.get_num_dimensions() == 2);
  
  const std::vector<std::size_t>& dim_sizes = cart_topo_idx.get_dimension_sizes();
  CHECK(dim_sizes[0] == 4);
  CHECK(dim_sizes[1] == 6);
  
  REQUIRE(cart_topo_idx.get_num_positions() == 4 * 6);

  const std::vector<std::size_t>& dim_offsets = 
    cart_topo_idx.get_dimension_offsets();
  CHECK(dim_offsets[0] == 6 * 1);
  CHECK(dim_offsets[1] ==     1);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{1, 3}) == 9);
  CHECK(cart_topo_idx.get_coordinate(9)[0] == 1);
  CHECK(cart_topo_idx.get_coordinate(9)[1] == 3);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{2, 4}) == 16);
  CHECK(cart_topo_idx.get_coordinate(16)[0] == 2);
  CHECK(cart_topo_idx.get_coordinate(16)[1] == 4);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{3, 5}) == 23);
  CHECK(cart_topo_idx.get_coordinate(23)[0] == 3);
  CHECK(cart_topo_idx.get_coordinate(23)[1] == 5);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{3, 2}) ==  20);
  CHECK(cart_topo_idx.get_coordinate(20)[0] == 3);
  CHECK(cart_topo_idx.get_coordinate(20)[1] == 2);
}


//------------------------------------------------------------------------------
TEST_CASE("CartesianTopologyIndex3D", "[CartesianTopologyIndex3D]")
{
  pvt::cartesian_topology_index cart_topo_idx(std::vector<size_t>{3, 4, 6});
  REQUIRE(cart_topo_idx.get_num_dimensions() == 3);
  
  const std::vector<std::size_t>& dim_sizes = cart_topo_idx.get_dimension_sizes();
  CHECK(dim_sizes[0] == 3);
  CHECK(dim_sizes[1] == 4);
  CHECK(dim_sizes[2] == 6);
  
  REQUIRE(cart_topo_idx.get_num_positions() == 3 * 4 * 6);
  
  const std::vector<std::size_t>& dim_offsets = 
    cart_topo_idx.get_dimension_offsets();
  CHECK(dim_offsets[0] == 4 * 6 * 1);
  CHECK(dim_offsets[1] ==     6 * 1);
  CHECK(dim_offsets[2] ==         1);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{2, 1, 3}) == 57);
  CHECK(cart_topo_idx.get_coordinate(57)[0] == 2);
  CHECK(cart_topo_idx.get_coordinate(57)[1] == 1);
  CHECK(cart_topo_idx.get_coordinate(57)[2] == 3);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{1, 2, 4}) == 40);
  CHECK(cart_topo_idx.get_coordinate(40)[0] == 1);
  CHECK(cart_topo_idx.get_coordinate(40)[1] == 2);
  CHECK(cart_topo_idx.get_coordinate(40)[2] == 4);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{0, 3, 5}) == 23);
  CHECK(cart_topo_idx.get_coordinate(23)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(23)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(23)[2] == 5);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{0, 3, 2}) ==  20);
  CHECK(cart_topo_idx.get_coordinate(20)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(20)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(20)[2] == 2);
}


//------------------------------------------------------------------------------
TEST_CASE("CartesianTopologyIndex4D", "[CartesianTopologyIndex4D]")
{
  pvt::cartesian_topology_index cart_topo_idx(std::vector<size_t>{3, 4, 5, 6});
  REQUIRE(cart_topo_idx.get_num_dimensions() == 4);
  
  const std::vector<std::size_t>& dim_sizes = cart_topo_idx.get_dimension_sizes();
  CHECK(dim_sizes[0] == 3);
  CHECK(dim_sizes[1] == 4);
  CHECK(dim_sizes[2] == 5);
  CHECK(dim_sizes[3] == 6);
  
  REQUIRE(cart_topo_idx.get_num_positions() == 3 * 4 * 5 * 6);
  
  const std::vector<std::size_t>& dim_offsets = 
    cart_topo_idx.get_dimension_offsets();
  CHECK(dim_offsets[0] == 4 * 5 * 6 * 1);
  CHECK(dim_offsets[1] ==     5 * 6 * 1);
  CHECK(dim_offsets[2] ==         6 * 1);
  CHECK(dim_offsets[3] ==             1);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{2, 1, 4, 3}) == 297);
  CHECK(cart_topo_idx.get_coordinate(297)[0] == 2);
  CHECK(cart_topo_idx.get_coordinate(297)[1] == 1);
  CHECK(cart_topo_idx.get_coordinate(297)[2] == 4);
  CHECK(cart_topo_idx.get_coordinate(297)[3] == 3);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{1, 2, 3, 4}) == 202);
  CHECK(cart_topo_idx.get_coordinate(202)[0] == 1);
  CHECK(cart_topo_idx.get_coordinate(202)[1] == 2);
  CHECK(cart_topo_idx.get_coordinate(202)[2] == 3);
  CHECK(cart_topo_idx.get_coordinate(202)[3] == 4);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{0, 3, 2, 5}) == 107);
  CHECK(cart_topo_idx.get_coordinate(107)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(107)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(107)[2] == 2);
  CHECK(cart_topo_idx.get_coordinate(107)[3] == 5);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{0, 3, 1, 2}) ==  98);
  CHECK(cart_topo_idx.get_coordinate(98)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(98)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(98)[2] == 1);
  CHECK(cart_topo_idx.get_coordinate(98)[3] == 2);
}


//------------------------------------------------------------------------------
TEST_CASE("CartesianTopologyIndex5D", "[CartesianTopologyIndex5D]")
{
  pvt::cartesian_topology_index cart_topo_idx(std::vector<size_t>{3, 4, 5, 7, 6});
  REQUIRE(cart_topo_idx.get_num_dimensions() == 5);
  
  const std::vector<std::size_t>& dim_sizes = cart_topo_idx.get_dimension_sizes();
  CHECK(dim_sizes[0] == 3);
  CHECK(dim_sizes[1] == 4);
  CHECK(dim_sizes[2] == 5);
  CHECK(dim_sizes[3] == 7);
  CHECK(dim_sizes[4] == 6);
  
  REQUIRE(cart_topo_idx.get_num_positions() == 3 * 4 * 5 * 7 * 6);
  
  const std::vector<std::size_t>& dim_offsets = 
    cart_topo_idx.get_dimension_offsets();
  CHECK(dim_offsets[0] == 4 * 5 * 7 * 6 * 1);
  CHECK(dim_offsets[1] ==     5 * 7 * 6 * 1);
  CHECK(dim_offsets[2] ==         7 * 6 * 1);
  CHECK(dim_offsets[3] ==             6 * 1);
  CHECK(dim_offsets[4] ==                 1);

  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{2, 1, 4, 5, 3}) == 2091);
  CHECK(cart_topo_idx.get_coordinate(2091)[0] == 2);
  CHECK(cart_topo_idx.get_coordinate(2091)[1] == 1);
  CHECK(cart_topo_idx.get_coordinate(2091)[2] == 4);
  CHECK(cart_topo_idx.get_coordinate(2091)[3] == 5);
  CHECK(cart_topo_idx.get_coordinate(2091)[4] == 3);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{1, 2, 3, 6, 4}) == 1426);
  CHECK(cart_topo_idx.get_coordinate(1426)[0] == 1);
  CHECK(cart_topo_idx.get_coordinate(1426)[1] == 2);
  CHECK(cart_topo_idx.get_coordinate(1426)[2] == 3);
  CHECK(cart_topo_idx.get_coordinate(1426)[3] == 6);
  CHECK(cart_topo_idx.get_coordinate(1426)[4] == 4);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{0, 3, 2, 1, 5}) == 725);
  CHECK(cart_topo_idx.get_coordinate(725)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(725)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(725)[2] == 2);
  CHECK(cart_topo_idx.get_coordinate(725)[3] == 1);
  CHECK(cart_topo_idx.get_coordinate(725)[4] == 5);
  
  CHECK(cart_topo_idx.get_index(std::vector<std::size_t>{0, 3, 1, 3, 2}) ==  692);
  CHECK(cart_topo_idx.get_coordinate(692)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(692)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(692)[2] == 1);
  CHECK(cart_topo_idx.get_coordinate(692)[3] == 3);
  CHECK(cart_topo_idx.get_coordinate(692)[4] == 2);
}


//------------------------------------------------------------------------------
TEST_CASE("CartesianTopologyIndex6D", "[CartesianTopologyIndex6D]")
{
  pvt::cartesian_topology_index cart_topo_idx(std::vector<size_t>{3, 4, 5, 7, 6, 2});
  REQUIRE(cart_topo_idx.get_num_dimensions() == 6);
  
  const std::vector<std::size_t>& dim_sizes = cart_topo_idx.get_dimension_sizes();
  CHECK(dim_sizes[0] == 3);
  CHECK(dim_sizes[1] == 4);
  CHECK(dim_sizes[2] == 5);
  CHECK(dim_sizes[3] == 7);
  CHECK(dim_sizes[4] == 6);
  CHECK(dim_sizes[5] == 2);
  
  REQUIRE(cart_topo_idx.get_num_positions() == 3 * 4 * 5 * 7 * 6 * 2);
  
  const std::vector<std::size_t>& dim_offsets = 
    cart_topo_idx.get_dimension_offsets();
  CHECK(dim_offsets[0] == 4 * 5 * 7 * 6 * 2 * 1);
  CHECK(dim_offsets[1] ==     5 * 7 * 6 * 2 * 1);
  CHECK(dim_offsets[2] ==         7 * 6 * 2 * 1);
  CHECK(dim_offsets[3] ==             6 * 2 * 1);
  CHECK(dim_offsets[4] ==                 2 * 1);
  CHECK(dim_offsets[5] ==                     1);
  
  CHECK(cart_topo_idx.get_index(
    std::vector<std::size_t>{2, 1, 4, 5, 3, 1}) == 4183);
  CHECK(cart_topo_idx.get_coordinate(4183)[0] == 2);
  CHECK(cart_topo_idx.get_coordinate(4183)[1] == 1);
  CHECK(cart_topo_idx.get_coordinate(4183)[2] == 4);
  CHECK(cart_topo_idx.get_coordinate(4183)[3] == 5);
  CHECK(cart_topo_idx.get_coordinate(4183)[4] == 3);
  CHECK(cart_topo_idx.get_coordinate(4183)[5] == 1);

  CHECK(cart_topo_idx.get_index(
    std::vector<std::size_t>{1, 2, 3, 6, 4, 0}) == 2852);
  CHECK(cart_topo_idx.get_coordinate(2852)[0] == 1);
  CHECK(cart_topo_idx.get_coordinate(2852)[1] == 2);
  CHECK(cart_topo_idx.get_coordinate(2852)[2] == 3);
  CHECK(cart_topo_idx.get_coordinate(2852)[3] == 6);
  CHECK(cart_topo_idx.get_coordinate(2852)[4] == 4);
  CHECK(cart_topo_idx.get_coordinate(2852)[5] == 0);
  
  CHECK(cart_topo_idx.get_index(
    std::vector<std::size_t>{0, 3, 2, 1, 5, 1}) == 1451);
  CHECK(cart_topo_idx.get_coordinate(1451)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(1451)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(1451)[2] == 2);
  CHECK(cart_topo_idx.get_coordinate(1451)[3] == 1);
  CHECK(cart_topo_idx.get_coordinate(1451)[4] == 5);
  CHECK(cart_topo_idx.get_coordinate(1451)[5] == 1);
  
  CHECK(cart_topo_idx.get_index(
    std::vector<std::size_t>{0, 3, 1, 3, 2, 0}) ==  1384);
  CHECK(cart_topo_idx.get_coordinate(1384)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(1384)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(1384)[2] == 1);
  CHECK(cart_topo_idx.get_coordinate(1384)[3] == 3);
  CHECK(cart_topo_idx.get_coordinate(1384)[4] == 2);
  CHECK(cart_topo_idx.get_coordinate(1384)[5] == 0);
}


//------------------------------------------------------------------------------
TEST_CASE("CartesianTopologyIndex7D", "[CartesianTopologyIndex7D]")
{
  pvt::cartesian_topology_index cart_topo_idx(
    std::vector<size_t>{3, 4, 5, 7, 6, 2, 16});
  REQUIRE(cart_topo_idx.get_num_dimensions() == 7);
  
  const std::vector<std::size_t>& dim_sizes = cart_topo_idx.get_dimension_sizes();
  CHECK(dim_sizes[0] == 3);
  CHECK(dim_sizes[1] == 4);
  CHECK(dim_sizes[2] == 5);
  CHECK(dim_sizes[3] == 7);
  CHECK(dim_sizes[4] == 6);
  CHECK(dim_sizes[5] == 2);
  CHECK(dim_sizes[6] == 16);
  
  REQUIRE(cart_topo_idx.get_num_positions() == 3 * 4 * 5 * 7 * 6 * 2 * 16);
  
  const std::vector<std::size_t>& dim_offsets = 
    cart_topo_idx.get_dimension_offsets();
  CHECK(dim_offsets[0] == 4 * 5 * 7 * 6 * 2 * 16 * 1);
  CHECK(dim_offsets[1] ==     5 * 7 * 6 * 2 * 16 * 1);
  CHECK(dim_offsets[2] ==         7 * 6 * 2 * 16 * 1);
  CHECK(dim_offsets[3] ==             6 * 2 * 16 * 1);
  CHECK(dim_offsets[4] ==                 2 * 16 * 1);
  CHECK(dim_offsets[5] ==                     16 * 1);
  CHECK(dim_offsets[6] ==                          1);
  
  CHECK(cart_topo_idx.get_index(
    std::vector<std::size_t>{2, 1, 4, 5, 3, 1, 11}) == 66939);
  CHECK(cart_topo_idx.get_coordinate(66939)[0] == 2);
  CHECK(cart_topo_idx.get_coordinate(66939)[1] == 1);
  CHECK(cart_topo_idx.get_coordinate(66939)[2] == 4);
  CHECK(cart_topo_idx.get_coordinate(66939)[3] == 5);
  CHECK(cart_topo_idx.get_coordinate(66939)[4] == 3);
  CHECK(cart_topo_idx.get_coordinate(66939)[5] == 1);
  CHECK(cart_topo_idx.get_coordinate(66939)[6] == 11);
  
  CHECK(cart_topo_idx.get_index(
    std::vector<std::size_t>{1, 2, 3, 6, 4, 0, 9}) == 45641);
  CHECK(cart_topo_idx.get_coordinate(45641)[0] == 1);
  CHECK(cart_topo_idx.get_coordinate(45641)[1] == 2);
  CHECK(cart_topo_idx.get_coordinate(45641)[2] == 3);
  CHECK(cart_topo_idx.get_coordinate(45641)[3] == 6);
  CHECK(cart_topo_idx.get_coordinate(45641)[4] == 4);
  CHECK(cart_topo_idx.get_coordinate(45641)[5] == 0);
  CHECK(cart_topo_idx.get_coordinate(45641)[6] == 9);
  
  CHECK(cart_topo_idx.get_index(
    std::vector<std::size_t>{0, 3, 2, 1, 5, 1, 14}) == 23230);
  CHECK(cart_topo_idx.get_coordinate(23230)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(23230)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(23230)[2] == 2);
  CHECK(cart_topo_idx.get_coordinate(23230)[3] == 1);
  CHECK(cart_topo_idx.get_coordinate(23230)[4] == 5);
  CHECK(cart_topo_idx.get_coordinate(23230)[5] == 1);
  CHECK(cart_topo_idx.get_coordinate(23230)[6] == 14);
  
  CHECK(cart_topo_idx.get_index(
    std::vector<std::size_t>{0, 3, 1, 3, 2, 0, 3}) ==  22147);
  CHECK(cart_topo_idx.get_coordinate(22147)[0] == 0);
  CHECK(cart_topo_idx.get_coordinate(22147)[1] == 3);
  CHECK(cart_topo_idx.get_coordinate(22147)[2] == 1);
  CHECK(cart_topo_idx.get_coordinate(22147)[3] == 3);
  CHECK(cart_topo_idx.get_coordinate(22147)[4] == 2);
  CHECK(cart_topo_idx.get_coordinate(22147)[5] == 0);
  CHECK(cart_topo_idx.get_coordinate(22147)[6] == 3);
}
