//------------------------------------------------------------------------------
// pvt_tests performance visualization toolkit tests
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <string>
#include <vector>

#include <catch/catch.hpp>

#include <pvt/cartesian_topology.hpp>
#include <pvt/cartesian_topology_index.hpp>

//------------------------------------------------------------------------------
TEST_CASE("CartesianTopologyMapToTopology", "[CartesianTopologyMapToTopology]")
{
  const pvt::data_array<int> raw_data{
    18,19,12,1,6,10,14,20,2,8,0,4,21,13,16,11,7,5,23,9,22,3,15,17
  };
  const pvt::data_array<std::size_t> topology_to_data_map{
      10,3,8,21,11,17,4,16,9,19,5,15,2,13,6,22,14,23,0,1,7,12,20,18
    };
  
  pvt::cartesian_topology_index cart_topo_idx(std::vector<std::size_t>{2, 3, 4});

  pvt::cartesian_topology cart_topo(
    0,
    "dummy",
    cart_topo_idx,
    std::vector<std::string>{"A", "B", "C"},
    topology_to_data_map);
  
  pvt::data_array<int> mapped_data(raw_data.size());
  cart_topo.map_data(raw_data, mapped_data);
  for (int i = 0; i < mapped_data.size(); ++i)
  {
    CHECK(mapped_data[i] == i);
  }
}
