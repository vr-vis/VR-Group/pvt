//------------------------------------------------------------------------------
// pvt_tests performance visualization toolkit tests
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <numeric>

#include <catch/catch.hpp>

#include <pvt/data_array.hpp>

//------------------------------------------------------------------------------
TEST_CASE("Data Array", "[DataArray]")
{
  pvt::data_array<double> data_array;
  REQUIRE(data_array.size() == 0);
  
  SECTION("Resizing sets size")
  {
    SECTION("- Resizing sets size to 9")
    {
      data_array.resize(9);
      CHECK(data_array.size() == 9);
    }
    
    SECTION("- Resizing sets size to 17")
    {
      data_array.resize(17);
      CHECK(data_array.size() == 17);
    }
    
    SECTION("- Resizing sets size to 19071981")
    {
      data_array.resize(19071981);
      CHECK(data_array.size() == 19071981);
    }
  }
  
  SECTION("resizing does not set data to zero")
  {
    data_array.resize(19);
    REQUIRE(data_array.is_guaranteed_zero() == false);
    
    SECTION("... but zero() does")
    {
      data_array.zero();
      CHECK(data_array.is_guaranteed_zero() == true);
      
      SECTION("... and setting any entry invalidates IsZeroByGuarantee()")
      {
        data_array.at(5) = 1.0;
        CHECK_FALSE(data_array.is_guaranteed_zero());
      }
      SECTION("... even for setting any entry to 0.0")
      {
        data_array.at(5) = 0.0;
        CHECK_FALSE(data_array.is_guaranteed_zero());
      }
    }
    
    SECTION("... but zero() does - for the data values")
    {
      data_array.zero();
      for (double datum : data_array)
      {
        CHECK(datum == 0.0);
      }
    }
  }
}


//------------------------------------------------------------------------------
TEST_CASE("DataArray__std::accumulate", "[DataArray__std_accumulate]")
{
  pvt::data_array<double> data_array(8);
  data_array.at(0) = 3.7650338;
  data_array.at(1) = 2.0623098;
  data_array.at(2) = 3.8600956;
  data_array.at(3) = 0.4925934;
  data_array.at(4) = 3.9945867;
  data_array.at(5) = 3.6715258;
  data_array.at(6) = 3.3010846;
  data_array.at(7) = 1.6314950;
  
  REQUIRE(data_array.at(0) == 3.7650338);
  REQUIRE(data_array.at(1) == 2.0623098);
  REQUIRE(data_array.at(2) == 3.8600956);
  REQUIRE(data_array.at(3) == 0.4925934);
  REQUIRE(data_array.at(4) == 3.9945867);
  REQUIRE(data_array.at(5) == 3.6715258);
  REQUIRE(data_array.at(6) == 3.3010846);
  REQUIRE(data_array.at(7) == 1.6314950);
  
  const double sum = std::accumulate(data_array.begin(),
                                     data_array.end  (),
                                     0.0);
  CHECK(sum == Approx(22.77872));
  
  /* R-code producing the numbers for the above test
   d<-c(3.7650338,2.0623098,3.8600956,0.4925934,3.9945867,3.6715258,3.3010846,1.6314950)
   sum(d)
   */
}
