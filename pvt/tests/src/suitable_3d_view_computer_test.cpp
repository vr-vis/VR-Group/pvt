//==============================================================================
// perfLib performance visualisation library
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//==============================================================================
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//==============================================================================

#include <catch/catch.hpp>

#include <pvt/directed_variance.hpp>
#include <pvt/suitable_3d_view_computer.hpp>


//------------------------------------------------------------------------------
TEST_CASE(
  "suitable_3d_view_computer::is_suitable_3d_view(const directed_variance&)",
  "[Suitable3DComputer]")
{
  pvt::suitable_3d_view_computer s3dvc(3);
  
  pvt::directed_variance dir_var(3);
  dir_var[0] = 0.01;
  
  SECTION("Threshold = 0.08"){
    s3dvc.set_threshold(0.08);
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 0.75));
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.25));
  }
  SECTION("Threshold = 0.1"){
    s3dvc.set_threshold(0.1);
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 0.75));
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
    CHECK_FALSE(s3dvc.is_suitable_3d_view(dir_var, 1.25));
  }
  SECTION("Threshold = 0.125"){
    s3dvc.set_threshold(0.125);
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 0.75));
    CHECK_FALSE(s3dvc.is_suitable_3d_view(dir_var, 1.0));
    CHECK_FALSE(s3dvc.is_suitable_3d_view(dir_var, 1.25));
  }
}


//------------------------------------------------------------------------------
TEST_CASE(
  "suitable_3d_view_computer::set_filter_dimension_enabled(std::size_t, bool)",
  "[Suitable3DComputer]")
{
  pvt::suitable_3d_view_computer s3dvc(3);
  
  pvt::directed_variance dir_var(3);
  dir_var[0] = 0.005;
  dir_var[1] = 0.005;
  dir_var[2] = 1.0;
  
  s3dvc.set_threshold(0.1);
  
  SECTION("filter enabled: 0 0 0"){
    s3dvc.set_filter_dimension_enabled(0, false);
    s3dvc.set_filter_dimension_enabled(1, false);
    s3dvc.set_filter_dimension_enabled(2, false);
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 0 0 1"){
    s3dvc.set_filter_dimension_enabled(0, false);
    s3dvc.set_filter_dimension_enabled(1, false);
    s3dvc.set_filter_dimension_enabled(2, true);
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 0 1 0"){
    s3dvc.set_filter_dimension_enabled(0, false);
    s3dvc.set_filter_dimension_enabled(1, true);
    s3dvc.set_filter_dimension_enabled(2, false);
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 0 1 1"){
    s3dvc.set_filter_dimension_enabled(0, false);
    s3dvc.set_filter_dimension_enabled(1, true);
    s3dvc.set_filter_dimension_enabled(2, true);
    CHECK_FALSE(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 1 0 0"){
    s3dvc.set_filter_dimension_enabled(0, true);
    s3dvc.set_filter_dimension_enabled(1, false);
    s3dvc.set_filter_dimension_enabled(2, false);
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 1 0 1"){
    s3dvc.set_filter_dimension_enabled(0, true);
    s3dvc.set_filter_dimension_enabled(1, false);
    s3dvc.set_filter_dimension_enabled(2, true);
    CHECK_FALSE(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 1 1 0"){
    s3dvc.set_filter_dimension_enabled(0, true);
    s3dvc.set_filter_dimension_enabled(1, true);
    s3dvc.set_filter_dimension_enabled(2, false);
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 1 1 1"){
    s3dvc.set_filter_dimension_enabled(0, true);
    s3dvc.set_filter_dimension_enabled(1, true);
    s3dvc.set_filter_dimension_enabled(2, true);
    CHECK_FALSE(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
}


//------------------------------------------------------------------------------
TEST_CASE(
  "suitable_3d_view_computer::set_filter_dimension_enabled(const std::vector<bool>&)",
  "[Suitable3DComputer]")
{
  pvt::suitable_3d_view_computer s3dvc(3);
  
  pvt::directed_variance dir_var(3);
  dir_var[0] = 0.005;
  dir_var[1] = 0.005;
  dir_var[2] = 1.0;
  
  s3dvc.set_threshold(0.1);
  
  SECTION("filter enabled: 0 0 0"){
    s3dvc.set_filter_dimension_enabled(std::vector<bool>{false, false, false});
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 0 0 1"){
    s3dvc.set_filter_dimension_enabled(std::vector<bool>{false, false, true});
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 0 1 0"){
    s3dvc.set_filter_dimension_enabled(std::vector<bool>{false, true, false});
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 0 1 1"){
    s3dvc.set_filter_dimension_enabled(std::vector<bool>{false, true, true});
    CHECK_FALSE(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 1 0 0"){
    s3dvc.set_filter_dimension_enabled(std::vector<bool>{true, false, false});
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 1 0 1"){
    s3dvc.set_filter_dimension_enabled(std::vector<bool>{true, false, true});
    CHECK_FALSE(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 1 1 0"){
    s3dvc.set_filter_dimension_enabled(std::vector<bool>{true, true, false});
    CHECK(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
  SECTION("filter enabled: 1 1 1"){
    s3dvc.set_filter_dimension_enabled(std::vector<bool>{true, true, true});
    CHECK_FALSE(s3dvc.is_suitable_3d_view(dir_var, 1.0));
  }
}