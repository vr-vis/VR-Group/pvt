//------------------------------------------------------------------------------
// pvt_tests performance visualization toolkit tests
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cmath>

#include <catch/catch.hpp>

#include <pvt/severity_view.hpp>

//------------------------------------------------------------------------------
TEST_CASE("SeverityViewIsEqual", "[SeverityViewIsEqual]")
{
  pvt::severity_view sev_view_a(reinterpret_cast<const pvt::perf_data*>(0x17),
                                reinterpret_cast<const pvt::metric*>(0x31415),
                                true,
                                reinterpret_cast<const pvt::cnode*> (0x141),
                                false);
  pvt::severity_view sev_view_b(nullptr);
  
  CHECK(sev_view_a != sev_view_b);
  
  sev_view_b = sev_view_a;
  
  REQUIRE(sev_view_b.get_performance_data() ==
          reinterpret_cast<const pvt::perf_data*>(0x17));
  REQUIRE(sev_view_b.get_metric() ==
          reinterpret_cast<const pvt::metric*>(0x31415));
  REQUIRE(sev_view_b.get_cnode () ==
          reinterpret_cast<const pvt::cnode*> (0x141));

  REQUIRE(sev_view_b.is_metric_total() == true );
  REQUIRE(sev_view_b.is_cnode_total () == false);
  
  CHECK(sev_view_a == sev_view_a);
  CHECK(sev_view_a == sev_view_b);
  
  sev_view_b.set_metric(reinterpret_cast<const pvt::metric*>(0x2718));
  sev_view_b.set_cnode (reinterpret_cast<const pvt::cnode*> (0x707 ));
  
  REQUIRE(sev_view_b.get_metric() ==
          reinterpret_cast<const pvt::metric*>(0x2718));
  REQUIRE(sev_view_b.get_cnode () ==
          reinterpret_cast<const pvt::cnode*> (0x707));
}
