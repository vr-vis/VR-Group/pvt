//------------------------------------------------------------------------------
// pvt_tests performance visualization toolkit tests
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cmath>
#include <iostream>
#include <random>
#include <vector>

#include <catch/catch.hpp>

#include <pvt/cartesian_topology_index.hpp>
#include <pvt/data_array.hpp>
#include <pvt/dft.hpp>
#include <pvt/fft.hpp>

//------------------------------------------------------------------------------
void
random_init_data
(double                   min,
 double                   max,
 pvt::data_array<double>* p_data)
{
  std::random_device                     random_device;
  std::mt19937                           mersenne_twister(random_device());
  std::uniform_real_distribution<double> uniform_real_distribution(min, max);
  
  for (std::size_t i = 0; i < p_data->size(); ++i)
    p_data->at(i) = uniform_real_distribution(mersenne_twister);
}


//------------------------------------------------------------------------------
double
compute_mean
(const pvt::data_array<double>&       data,
 const pvt::cartesian_topology_index& cart_topo_idx)
{
  double mean = 0.0;
  for (std::size_t i = 0; i < cart_topo_idx.get_num_positions(); ++i)
    mean += data.at(i);
  return mean / static_cast<double>(cart_topo_idx.get_num_positions());
}


//------------------------------------------------------------------------------
double
compute_variance
(const pvt::data_array<double>&       data,
 const pvt::cartesian_topology_index& cart_topo_idx,
 double                          mean)
{
  double variance = 0.0;
  for (std::size_t i = 0; i < cart_topo_idx.get_num_positions(); ++i)
  {
    const double delta = mean - data.at(i);
    variance  += delta * delta;
  }
  return variance / static_cast<double>(cart_topo_idx.get_num_positions());
}


//------------------------------------------------------------------------------
void
compute_statistics
(const pvt::data_array<double>&       data,
 const pvt::cartesian_topology_index& cart_topo_idx,
 double*                         p_mean,
 double*                         p_variance,
 double*                         p_standard_deviation)
{                              
  *p_mean               = compute_mean   (data, cart_topo_idx);
  *p_variance           = compute_variance(data, cart_topo_idx, *p_mean);
  *p_standard_deviation = sqrt           (*p_variance);
}


//------------------------------------------------------------------------------
void
do_check
(pvt::data_array<double>&             data,
 const pvt::cartesian_topology_index& cart_topo_idx,
 const std::string&                   dimensions)
{
  double mean               = 0.0;
  double variance           = 0.0;
  double standard_deviation = 0.0;
  compute_statistics(data, 
                    cart_topo_idx, 
                    &mean, 
                    &variance, 
                    &standard_deviation);
  
  pvt::fft fft(cart_topo_idx);
  pvt::dft::compute(data, cart_topo_idx, &fft);

  INFO(dimensions);

  CHECK(fft.get_mean             () == Approx(mean));
  CHECK(fft.get_variance         () == Approx(variance));
  CHECK(fft.get_standard_deviation() == Approx(standard_deviation));
}


//------------------------------------------------------------------------------
TEST_CASE("Fft", "[Fft]")
{
  const pvt::cartesian_topology_index cart_topo_idx(std::vector<std::size_t>{2, 4, 4});
  pvt::fft fft1(cart_topo_idx);
  REQUIRE(fft1.get_num_data() == 24);
  
  fft1.real(0)  = 2.3283047;
  fft1.real(1)  = 0.6224213;
  fft1.real(2)  = 3.8792385;
  fft1.real(3)  = 1.0627531;
  fft1.real(4)  = 2.5206367;
  fft1.real(5)  = 3.7818434;
  fft1.real(6)  = 1.0327380;
  fft1.real(7)  = 2.3867127;
  fft1.real(8)  = 2.7202446;
  fft1.real(9)  = 0.3116695;
  fft1.real(10) = 1.5582814;
  fft1.real(11) = 0.5202683;
  fft1.real(12) = 3.9308342;
  fft1.real(13) = 2.6559791;
  fft1.real(14) = 1.1549986;
  fft1.real(15) = 3.4486611;
  fft1.real(16) = 2.4765859;
  fft1.real(17) = 1.8784461;
  fft1.real(18) = 3.9943018;
  fft1.real(19) = 0.7615286;
  fft1.real(20) = 1.9372657;
  fft1.real(21) = 2.0272956;
  fft1.real(22) = 1.3596697;
  fft1.real(23) = 1.8377995;
  
  fft1.imag(0)  = 0.34366438;
  fft1.imag(1)  = 0.75075560;
  fft1.imag(2)  = 2.82207820;
  fft1.imag(3)  = 3.28473129;
  fft1.imag(4)  = 1.75407060;
  fft1.imag(5)  = 0.53757786;
  fft1.imag(6)  = 0.03867888;
  fft1.imag(7)  = 2.25190682;
  fft1.imag(8)  = 0.27300356;
  fft1.imag(9)  = 2.34251738;
  fft1.imag(10) = 3.02612272;
  fft1.imag(11) = 2.16265078;
  fft1.imag(12) = 2.92721447;
  fft1.imag(13) = 1.41696709;
  fft1.imag(14) = 1.31300708;
  fft1.imag(15) = 2.82157324;
  fft1.imag(16) = 2.89231470;
  fft1.imag(17) = 0.85263736;
  fft1.imag(18) = 1.46803797;
  fft1.imag(19) = 0.92924505;
  fft1.imag(20) = 1.77773160;
  fft1.imag(21) = 0.36831963;
  fft1.imag(22) = 0.48753703;
  fft1.imag(23) = 3.49756724;
  
  REQUIRE(fft1.real(0)  == Approx(2.3283047));
  REQUIRE(fft1.real(1)  == Approx(0.6224213));
  REQUIRE(fft1.real(2)  == Approx(3.8792385));
  REQUIRE(fft1.real(3)  == Approx(1.0627531));
  REQUIRE(fft1.real(4)  == Approx(2.5206367));
  REQUIRE(fft1.real(5)  == Approx(3.7818434));
  REQUIRE(fft1.real(6)  == Approx(1.0327380));
  REQUIRE(fft1.real(7)  == Approx(2.3867127));
  REQUIRE(fft1.real(8)  == Approx(2.7202446));
  REQUIRE(fft1.real(9)  == Approx(0.3116695));
  REQUIRE(fft1.real(10) == Approx(1.5582814));
  REQUIRE(fft1.real(11) == Approx(0.5202683));
  REQUIRE(fft1.real(12) == Approx(3.9308342));
  REQUIRE(fft1.real(13) == Approx(2.6559791));
  REQUIRE(fft1.real(14) == Approx(1.1549986));
  REQUIRE(fft1.real(15) == Approx(3.4486611));
  REQUIRE(fft1.real(16) == Approx(2.4765859));
  REQUIRE(fft1.real(17) == Approx(1.8784461));
  REQUIRE(fft1.real(18) == Approx(3.9943018));
  REQUIRE(fft1.real(19) == Approx(0.7615286));
  REQUIRE(fft1.real(20) == Approx(1.9372657));
  REQUIRE(fft1.real(21) == Approx(2.0272956));
  REQUIRE(fft1.real(22) == Approx(1.3596697));
  
  REQUIRE(fft1.imag(0)  == Approx(0.34366438));
  REQUIRE(fft1.imag(1)  == Approx(0.75075560));
  REQUIRE(fft1.imag(2)  == Approx(2.82207820));
  REQUIRE(fft1.imag(3)  == Approx(3.28473129));
  REQUIRE(fft1.imag(4)  == Approx(1.75407060));
  REQUIRE(fft1.imag(5)  == Approx(0.53757786));
  REQUIRE(fft1.imag(6)  == Approx(0.03867888));
  REQUIRE(fft1.imag(7)  == Approx(2.25190682));
  REQUIRE(fft1.imag(8)  == Approx(0.27300356));
  REQUIRE(fft1.imag(9)  == Approx(2.34251738));
  REQUIRE(fft1.imag(10) == Approx(3.02612272));
  REQUIRE(fft1.imag(11) == Approx(2.16265078));
  REQUIRE(fft1.imag(12) == Approx(2.92721447));
  REQUIRE(fft1.imag(13) == Approx(1.41696709));
  REQUIRE(fft1.imag(14) == Approx(1.31300708));
  REQUIRE(fft1.imag(15) == Approx(2.82157324));
  REQUIRE(fft1.imag(16) == Approx(2.89231470));
  REQUIRE(fft1.imag(17) == Approx(0.85263736));
  REQUIRE(fft1.imag(18) == Approx(1.46803797));
  REQUIRE(fft1.imag(19) == Approx(0.92924505));
  REQUIRE(fft1.imag(20) == Approx(1.77773160));
  REQUIRE(fft1.imag(21) == Approx(0.36831963));
  REQUIRE(fft1.imag(22) == Approx(0.48753703));
  
  CHECK(fft1.get_mean              () == Approx(0.07354784));
  CHECK(fft1.get_variance          () == Approx(0.2776516 ));
  CHECK(fft1.get_standard_deviation() == Approx(0.5269266 ));
  
  /* R-Code producing the numbers for the above test:
   real=c(2.3283047,0.6224213,3.8792385,1.0627531,2.5206367,3.7818434,1.0327380,2.3867127,2.7202446,0.3116695,1.5582814,0.5202683,3.9308342,2.6559791,1.1549986,3.4486611,2.4765859,1.8784461,3.9943018,0.7615286,1.9372657,2.0272956,1.3596697,1.8377995)
   imag = c(0.34366438,0.75075560,2.82207820,3.28473129,1.75407060,0.53757786,0.03867888,2.25190682,0.27300356,2.34251738,3.02612272,2.16265078,2.92721447,1.41696709,1.31300708,2.82157324,2.89231470,0.85263736,1.46803797,0.92924505,1.77773160,0.36831963,0.48753703,3.49756724)
   f<-complex(real=real, imaginary=imag)
   mean = Mod(f[1])
   g <- f
   dim(g)<-c(3,4,2)
   g<-aperm(g)
   g[1]=0.0
   variance = (sum(Mod(g[2:24])*Mod(g[2:24]))+sum(Mod(g[1:2,1:4,2])*Mod(g[1:2,1:4,2])))/(32*32)
   */
  
  pvt::fft fft2(cart_topo_idx);
  fft2.real(0)  = 3.72981106;
  fft2.real(1)  = 3.52498969;
  fft2.real(2)  = 3.56304713;
  fft2.real(3)  = 2.82753163;
  fft2.real(4)  = 0.37631876;
  fft2.real(5)  = 0.08470891;
  fft2.real(6)  = 2.93043673;
  fft2.real(7)  = 2.35150849;
  fft2.real(8)  = 2.95007863;
  fft2.real(9)  = 0.61528196;
  fft2.real(10) = 0.21495015;
  fft2.real(11) = 2.82314403;
  fft2.real(12) = 2.35655725;
  fft2.real(13) = 0.19139296;
  fft2.real(14) = 1.66603140;
  fft2.real(15) = 0.40730335;
  fft2.real(16) = 0.87840785;
  fft2.real(17) = 2.22920256;
  fft2.real(18) = 0.35601346;
  fft2.real(19) = 0.93982470;
  fft2.real(20) = 0.75586910;
  fft2.real(21) = 0.02883176;
  fft2.real(22) = 1.25399747;
  fft2.real(23) = 3.19190760;
  
  fft2.imag(0)  = 1.824448857;
  fft2.imag(1)  = 3.042702205;
  fft2.imag(2)  = 1.198500197;
  fft2.imag(3)  = 2.237584192;
  fft2.imag(4)  = 1.246480022;
  fft2.imag(5)  = 3.953668126;
  fft2.imag(6)  = 0.002700927;
  fft2.imag(7)  = 3.190657068;
  fft2.imag(8)  = 1.919109900;
  fft2.imag(9)  = 1.194979452;
  fft2.imag(10) = 1.760005539;
  fft2.imag(11) = 2.199516289;
  fft2.imag(12) = 2.538499391;
  fft2.imag(13) = 0.087674259;
  fft2.imag(14) = 2.535863978;
  fft2.imag(15) = 0.451757099;
  fft2.imag(16) = 1.198088960;
  fft2.imag(17) = 0.859754622;
  fft2.imag(18) = 0.809649244;
  fft2.imag(19) = 3.918873862;
  fft2.imag(20) = 0.759288884;
  fft2.imag(21) = 1.939099183;
  fft2.imag(22) = 2.681008917;
  fft2.imag(23) = 3.157742844;
  
  pvt::fft fft3(cart_topo_idx);
  fft1.multiply_by_conjugate(fft2, fft3);
  
  CHECK(fft3.real(0)  == Approx(9.3111347));
  CHECK(fft3.real(1)  == Approx(4.4783544));
  CHECK(fft3.real(2)  == Approx(17.2041709));
  CHECK(fft3.real(3)  == Approx(10.3548308));
  CHECK(fft3.real(4)  == Approx(3.1349768));
  CHECK(fft3.real(5)  == Approx(2.4457603));
  CHECK(fft3.real(6)  == Approx(3.0264778));
  CHECK(fft3.real(7)  == Approx(12.7974376));
  CHECK(fft3.real(8)  == Approx(8.5488593));
  CHECK(fft3.real(9)  == Approx(2.9910248));
  CHECK(fft3.real(10) == Approx(5.6609456));
  CHECK(fft3.real(11) == Approx(6.2255780));
  CHECK(fft3.real(12) == Approx(16.6939680));
  CHECK(fft3.real(13) == Approx(0.6325672));
  CHECK(fft3.real(14) == Approx(5.2538713));
  CHECK(fft3.real(15) == Approx(2.6793170));
  CHECK(fft3.real(16) == Approx(5.6407028));
  CHECK(fft3.real(17) == Approx(4.9204958));
  CHECK(fft3.real(18) == Approx(2.6106210));
  CHECK(fft3.real(19) == Approx(4.3572975));
  CHECK(fft3.real(20) == Approx(2.8141311));
  CHECK(fft3.real(21) == Approx(0.7726588));
  CHECK(fft3.real(22) == Approx(3.0121135));
  CHECK(fft3.real(23) == Approx(16.9105041));
  
  CHECK(fft3.imag(0)  == Approx(-2.96606964));
  CHECK(fft3.imag(1)  == Approx(0.75256309));
  CHECK(fft3.imag(2)  == Approx(5.40592953));
  CHECK(fft3.imag(3)  == Approx(6.90968208));
  CHECK(fft3.imag(4)  == Approx(-2.48183362));
  CHECK(fft3.imag(5)  == Approx(-14.90661608));
  CHECK(fft3.imag(6)  == Approx(0.11055666));
  CHECK(fft3.imag(7)  == Approx(-2.31980375));
  CHECK(fft3.imag(8)  == Approx(-4.41506638));
  CHECK(fft3.imag(9)  == Approx(1.06887004));
  CHECK(fft3.imag(10) == Approx(-2.09211837));
  CHECK(fft3.imag(11) == Approx(4.96113604));
  CHECK(fft3.imag(12) == Approx(-3.08027175));
  CHECK(fft3.imag(13) == Approx(0.03833653));
  CHECK(fft3.imag(14) == Approx(-0.74140832));
  CHECK(fft3.imag(15) == Approx(-0.40872090));
  CHECK(fft3.imag(16) == Approx(-0.42653829));
  CHECK(fft3.imag(17) == Approx(0.28569867));
  CHECK(fft3.imag(18) == Approx(-2.71134216));
  CHECK(fft3.imag(19) == Approx(-2.11100707));
  CHECK(fft3.imag(20) == Approx(-0.12721193));
  CHECK(fft3.imag(21) == Approx(-3.92050794));
  CHECK(fft3.imag(22) == Approx(-3.03391639));
  CHECK(fft3.imag(23) == Approx(5.36061323));
  
  /* R-code producing the numbers for the above test:
   real2 <- c(3.72981106,3.52498969,3.56304713,2.82753163,0.37631876,0.08470891,2.93043673,2.35150849,2.95007863,0.61528196,0.21495015,2.82314403,2.35655725,0.19139296,1.66603140,0.40730335,0.87840785,2.22920256,0.35601346,0.93982470,0.75586910,[22] 0.02883176,1.25399747,3.19190760)
   
   imag2 <- c(1.824448857,3.042702205,1.198500197,2.237584192,1.246480022,3.953668126,0.002700927,3.190657068,1.919109900,1.194979452,1.760005539,2.199516289,2.538499391,0.087674259,2.535863978,0.451757099,1.198088960,0.859754622,0.809649244,3.918873862,0.759288884,1.939099183,2.681008917,3.157742844)
   
   f2<-complex(real=real2, imaginary=-imag2)
   
   Re(f*f2)
   Im(f*f2)
   */
  
  pvt::fft fft4(cart_topo_idx);
  fft1.square_with_conjugate(fft4);
  
  CHECK(fft4.real(0)  == Approx(5.5391080));
  CHECK(fft4.real(1)  == Approx(0.9510422));
  CHECK(fft4.real(2)  == Approx(23.0126167));
  CHECK(fft4.real(3)  == Approx(11.9189038));
  CHECK(fft4.real(4)  == Approx(9.4303730));
  CHECK(fft4.real(5)  == Approx(14.5913295));
  CHECK(fft4.real(6)  == Approx(1.0680438));
  CHECK(fft4.real(7)  == Approx(10.7674818));
  CHECK(fft4.real(8)  == Approx(7.4742616));
  CHECK(fft4.real(9)  == Approx(5.5845256));
  CHECK(fft4.real(10) == Approx(11.5856596));
  CHECK(fft4.real(11) == Approx(4.9477375));
  CHECK(fft4.real(12) == Approx(24.0200421));
  CHECK(fft4.real(13) == Approx(9.0620207));
  CHECK(fft4.real(14) == Approx(3.0580094));
  CHECK(fft4.real(15) == Approx(19.8545389));
  CHECK(fft4.real(16) == Approx(14.4989620));
  CHECK(fft4.real(17) == Approx(4.2555502));
  CHECK(fft4.real(18) == Approx(18.1095824));
  CHECK(fft4.real(19) == Approx(1.4434222));
  CHECK(fft4.real(20) == Approx(6.9133280));
  CHECK(fft4.real(21) == Approx(4.2455868));
  CHECK(fft4.real(22) == Approx(2.0863940));
  CHECK(fft4.real(23) == Approx(15.6104836));
  
  CHECK(fft4.imag(0)  == 0.0);
  CHECK(fft4.imag(1)  == 0.0);
  CHECK(fft4.imag(2)  == 0.0);
  CHECK(fft4.imag(3)  == 0.0);
  CHECK(fft4.imag(4)  == 0.0);
  CHECK(fft4.imag(5)  == 0.0);
  CHECK(fft4.imag(6)  == 0.0);
  CHECK(fft4.imag(7)  == 0.0);
  CHECK(fft4.imag(8)  == 0.0);
  CHECK(fft4.imag(9)  == 0.0);
  CHECK(fft4.imag(10) == 0.0);
  CHECK(fft4.imag(11) == 0.0);
  CHECK(fft4.imag(12) == 0.0);
  CHECK(fft4.imag(13) == 0.0);
  CHECK(fft4.imag(14) == 0.0);
  CHECK(fft4.imag(15) == 0.0);
  CHECK(fft4.imag(16) == 0.0);
  CHECK(fft4.imag(17) == 0.0);
  CHECK(fft4.imag(18) == 0.0);
  CHECK(fft4.imag(19) == 0.0);
  CHECK(fft4.imag(20) == 0.0);
  CHECK(fft4.imag(21) == 0.0);
  CHECK(fft4.imag(22) == 0.0);
  CHECK(fft4.imag(23) == 0.0);
  
  /* R-code producing the numbers for the above test:
   f3 <- complex(real=real, imaginary=-imag)
   Re(f*f3)
   Im(f*f3)
   */
}


//------------------------------------------------------------------------------
TEST_CASE("FftZero", "[Fft]")
{
  const pvt::cartesian_topology_index cart_topo_idx(std::vector<std::size_t>{2, 4, 4});
  pvt::fft fft_zero(cart_topo_idx);
  fft_zero.zero();
  
  std::random_device                     random_device;
  std::mt19937                           mersenne_twister  (random_device());
  std::uniform_real_distribution<double> uniform_real_distribution(0.0, 4.0);
  
  pvt::fft fft_random_a(cart_topo_idx);
  pvt::fft fft_random_b(cart_topo_idx);
  for (std::size_t i = 0; i < fft_random_a.get_num_data(); ++i)
  {
    fft_random_a.real(i) = uniform_real_distribution(mersenne_twister);
    fft_random_a.imag(i) = uniform_real_distribution(mersenne_twister);
    fft_random_b.real(i) = uniform_real_distribution(mersenne_twister);
    fft_random_b.imag(i) = uniform_real_distribution(mersenne_twister);
  }
  
  pvt::fft fft_result(cart_topo_idx);
  fft_zero.multiply_by_conjugate(fft_random_a, fft_result);
  CHECK(fft_result.is_guaranteed_zero());
  
  fft_random_a.multiply_by_conjugate(fft_zero, fft_result);
  CHECK(fft_result.is_guaranteed_zero());
  
  fft_zero.square_with_conjugate(fft_result);
  CHECK(fft_result.is_guaranteed_zero());
  
  fft_random_a.multiply_by_conjugate(fft_random_b, fft_result);
  CHECK_FALSE(fft_result.is_guaranteed_zero());
  
  fft_random_a.square_with_conjugate(fft_result);
  CHECK_FALSE(fft_result.is_guaranteed_zero());
}


//------------------------------------------------------------------------------
TEST_CASE("FftClearDC", "[Fft]")
{
  const pvt::cartesian_topology_index cart_topo_idx(std::vector<std::size_t>{2, 4, 4});
  pvt::fft fft(cart_topo_idx);
  fft.real(0) = sqrt(17.0);
  fft.imag(0) = 42.0;
  
  fft.clear_dc();
  
  CHECK(fft.real(0) == 0.0);
  CHECK(fft.imag(0) == 0.0);
}


//------------------------------------------------------------------------------
TEST_CASE("FftStatistics", "[Fft]")
{
  const pvt::cartesian_topology_index total_topo_idx(
    std::vector<size_t>{8, 28, 8, 8, 2, 16, 4});
  pvt::data_array<double> data(total_topo_idx.get_num_positions());
  random_init_data(0.0, 4.0, &data);
  
  typedef pvt::cartesian_topology_index CTI;
  do_check(data, CTI(std::vector<size_t>{4})                    , "1D");
  do_check(data, CTI(std::vector<size_t>{16, 4})                , "2D");
  do_check(data, CTI(std::vector<size_t>{2, 16, 4})             , "3D");
  do_check(data, CTI(std::vector<size_t>{8, 2, 16, 4})          , "4D");
  do_check(data, CTI(std::vector<size_t>{8, 8, 2, 16, 4})       , "5D");
  do_check(data, CTI(std::vector<size_t>{28, 8, 8, 2, 16, 4})   , "6D");
  do_check(data, CTI(std::vector<size_t>{8, 28, 8, 8, 2, 16, 4}), "7D");
}

//------------------------------------------------------------------------------
TEST_CASE("Fft::convert_to_dc_centered_frequency", "[Fft]")
{
  const pvt::cartesian_topology_index topo_idx(
    std::vector<size_t>{8, 28, 8, 8, 2, 16, 4});
  
  pvt::fft spectrum(topo_idx);
  
  std::vector<int> frequency(7, 0);
  
  SECTION("128")
  {
    const std::size_t freq_index = 128;
    spectrum.convert_to_frequency(freq_index, frequency);
    REQUIRE(frequency[0] == 0);
    REQUIRE(frequency[1] == 0);
    REQUIRE(frequency[2] == 0);
    REQUIRE(frequency[3] == 1);
    REQUIRE(frequency[4] == 0);
    REQUIRE(frequency[5] == 10);
    REQUIRE(frequency[6] == 2);
    spectrum.convert_to_dc_centered_frequency(freq_index, frequency);
    CHECK(frequency[0] == 0);
    CHECK(frequency[1] == 0);
    CHECK(frequency[2] == 0);
    CHECK(frequency[3] == 1);
    CHECK(frequency[4] == 0);
    CHECK(frequency[5] == -6);
    CHECK(frequency[6] == 2);
  }
  
  SECTION("218123")
  {
    const std::size_t freq_index = 218123;
    spectrum.convert_to_frequency(218123, frequency);
    REQUIRE(frequency[0] == 1);
    REQUIRE(frequency[1] == 7);
    REQUIRE(frequency[2] == 4);
    REQUIRE(frequency[3] == 0);
    REQUIRE(frequency[4] == 0);
    REQUIRE(frequency[5] == 3);
    REQUIRE(frequency[6] == 2);
    spectrum.convert_to_dc_centered_frequency(freq_index, frequency);
    CHECK(frequency[0] == 1);
    CHECK(frequency[1] == 7);
    CHECK(frequency[2] == 4);
    CHECK(frequency[3] == 0);
    CHECK(frequency[4] == 0);
    CHECK(frequency[5] == 3);
    CHECK(frequency[6] == 2);
  }
}


//------------------------------------------------------------------------------
TEST_CASE("Fft::is_on_boundary", "[Fft]")
{
  SECTION("Even size")
  {
    const pvt::cartesian_topology_index topo_idx(
     std::vector<size_t>{8, 28, 8, 8, 2, 16, 4});
    pvt::fft spectrum(topo_idx);
    
    std::vector<std::size_t> frequency(7, 0);
    for (std::size_t i = 0; i < spectrum.get_num_data(); ++i)
    {
      spectrum.convert_to_frequency(i, frequency);
      if (frequency.back() == 0 || frequency.back() == 2)
      {
        SCOPED_INFO(frequency.back());
        REQUIRE(spectrum.is_on_boundary(frequency));
      }
      else
      {
        SCOPED_INFO(frequency.back());
        REQUIRE_FALSE(spectrum.is_on_boundary(frequency));
      }
      
      spectrum.convert_to_dc_centered_frequency(i, frequency);
      if (frequency.back() == 0 || frequency.back() == 2)
      {
        SCOPED_INFO(frequency.back());
        REQUIRE(spectrum.is_on_boundary(frequency));
      }
      else
      {
        SCOPED_INFO(frequency.back());
        REQUIRE_FALSE(spectrum.is_on_boundary(frequency));
      }
    }
  }
  
  SECTION("Odd Size")
  {
    const pvt::cartesian_topology_index topo_idx(
     std::vector<size_t>{4, 5});
    
    pvt::fft spectrum(topo_idx);
    
    std::vector<std::size_t> frequency(7, 0);
    for (std::size_t i = 0; i < spectrum.get_num_data(); ++i)
    {
      spectrum.convert_to_frequency(i, frequency);
      SCOPED_INFO(frequency.back());
      if (frequency.back() == 0)
      {
        REQUIRE(spectrum.is_on_boundary(frequency));
      }
      else
      {
        REQUIRE_FALSE(spectrum.is_on_boundary(frequency));
      }
      
      spectrum.convert_to_dc_centered_frequency(i, frequency);
      if (frequency.back() == 0)
      {
        SCOPED_INFO(frequency.back());
        REQUIRE(spectrum.is_on_boundary(frequency));
      }
      else
      {
        SCOPED_INFO(frequency.back());
        REQUIRE_FALSE(spectrum.is_on_boundary(frequency));
      }
    }
  }

  SECTION("8*28*8*8*2*16*4")
  {
    const std::vector<size_t> dim_sizes{8, 28, 8, 8, 2, 16, 4};
    const pvt::cartesian_topology_index topo_idx(dim_sizes);
    pvt::fft spectrum(topo_idx);
    for (std::size_t a = 0; a < dim_sizes[0]; ++a)
    {
      for (std::size_t b = 0; b < dim_sizes[1]; ++b)
      {
        for (std::size_t c = 0; c < dim_sizes[2]; ++c)
        {
          for (std::size_t d = 0; d < dim_sizes[3]; ++d)
          {
            for (std::size_t e = 0; e < dim_sizes[4]; ++e)
            {
              for (std::size_t core = 0; core < dim_sizes[5]; ++core)
              {
                CHECK      (spectrum.is_on_boundary(std::vector<std::size_t>{a,b,c,d,e,core,0}));
                CHECK_FALSE(spectrum.is_on_boundary(std::vector<std::size_t>{a,b,c,d,e,core,1}));
                CHECK      (spectrum.is_on_boundary(std::vector<std::size_t>{a,b,c,d,e,core,2}));
              }
            }
          }
        }
      }
    }
  }
}


//------------------------------------------------------------------------------
TEST_CASE("Fft::get_frequency_norm_squared", "[Fft]")
{
  const pvt::cartesian_topology_index topo_idx(std::vector<std::size_t>{2, 4, 4});
  pvt::fft spectrum(topo_idx);
  
  
  std::vector<double> frequency(3, 0);
  for (std::size_t i = 0; i < spectrum.get_num_data(); ++i)
  {
    spectrum.convert_to_dc_centered_frequency(i, frequency);
    const double norm = spectrum.get_frequency_norm_squared(frequency);
    SCOPED_INFO(frequency[0] << " " << frequency[1] << " " << frequency[2]);
    REQUIRE(norm == Approx(frequency[0] * frequency[0] + frequency[1] * frequency[1] + frequency[2] * frequency[2]));
  }
}


//------------------------------------------------------------------------------
TEST_CASE("Fft::get_frequency_norm", "[Fft]")
{
  const pvt::cartesian_topology_index topo_idx(std::vector<std::size_t>{2, 4, 4});
  pvt::fft spectrum(topo_idx);
  
  
  std::vector<double> frequency(3, 0);
  for (std::size_t i = 0; i < spectrum.get_num_data(); ++i)
  {
    spectrum.convert_to_dc_centered_frequency(i, frequency);
    const double norm = spectrum.get_frequency_norm(frequency);
    SCOPED_INFO(frequency[0] << " " << frequency[1] << " " << frequency[2]);
    REQUIRE(norm == Approx(sqrt(frequency[0] * frequency[0] + frequency[1] * frequency[1] + frequency[2] * frequency[2])));
  }
}


//------------------------------------------------------------------------------
TEST_CASE("Fft::get_spectral_energy", "[Fft]")
{
  const pvt::cartesian_topology_index topo_idx(std::vector<std::size_t>{4, 4});
  pvt::fft spectrum(topo_idx);
  
  const double spect_dat[] = {
    18,31,12,27,29,20,11,26,3,23,24,4,16,13,32,15,22,7,25,10,21,1,28,9
  };

  for (std::size_t i = 0; i < spectrum.get_num_data(); ++i)
  {
    spectrum.real(i) = 16.0 * spect_dat[2 * i + 0];
    spectrum.imag(i) = 16.0 * spect_dat[2 * i + 1];
  }
  
  for (std::size_t i = 0; i < spectrum.get_num_data(); ++i)
  {
    CHECK(spectrum.get_spectral_energy(i) == Approx(spect_dat[2 * i + 0] *
                                                    spect_dat[2 * i + 0] +
                                                    spect_dat[2 * i + 1] *
                                                    spect_dat[2 * i + 1]));
  }
}