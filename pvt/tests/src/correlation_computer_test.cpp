//------------------------------------------------------------------------------
// pvt_tests performance visualization toolkit tests
//
// Copyright (c) 2014-2016 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                License
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// In the future, we may decide to add a commercial license
// at our own discretion without further notice.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <string>
#include <vector>

#include <catch/catch.hpp>

#include <pvt/cartesian_topology.hpp>
#include <pvt/correlation_computer.hpp>

//------------------------------------------------------------------------------
TEST_CASE("CorrelationComputerMaxElement", "[CorrelationComputerMaxElement]")
{
  const pvt::data_array<std::size_t> topology_to_data_map{
      10,3,8,21,11,17,4,16,9,19,5,15,2,13,6,22,14,23,0,1,7,12,20,18
    };
 
  pvt::cartesian_topology_index cart_topo_idx(std::vector<std::size_t>{2, 3, 4});

  const pvt::cartesian_topology cart_topo(
    0,
    "dummy",
    cart_topo_idx,
    std::vector<std::string>{"A", "B", "C"},
    topology_to_data_map);
  
  pvt::severity_view sev_view(nullptr);
  
  const pvt::correlation_computer corr_comp(sev_view, &cart_topo);
  
  const pvt::data_array<double> raw_data_a{
    -0.72140726,-0.43618186, 0.54807265, 0.66694602, 0.97099610,-0.72500893,
    -0.36269828, 0.39427053,-0.06781661, 0.51754656,-0.67872783, 0.88858519,
    -0.12192581, 0.65786205, 0.43837897, 0.55329604, 0.19481178,-0.02632383,
    -0.31119346,-0.71044030, 0.62621597,-0.94183804, 0.05634508, 0.85785716
  };
  const pvt::data_array<double> raw_data_b{
     0.54048827, 0.03013358,-0.82297137, 0.64171014,-0.32507124, 0.56998953,
    -0.41648499,-0.16730315,-0.13924763,-0.73964882,-0.80674712,-0.75049723,
    -0.23987249,-0.76638637,-0.51302040,-0.94508949,-0.86431624,-0.98705265,
    -0.60875374, 0.73382757, 0.25088311,-0.41721716, 0.52636579, 0.01023346
  };
  
  CHECK(corr_comp.get_max_element(raw_data_a.begin(),
                                  raw_data_a.end  ()) == Approx( 0.9709961 ));
  CHECK(corr_comp.get_max_element(raw_data_b.begin(),
                                  raw_data_b.end  ()) == Approx(-0.98705265));
}
